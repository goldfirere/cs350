%node = type { i16, %node*, %node* }  ; node for a doubly-linked list

@n1 = global %node { i16 1 , %node* @n3, %node* @n2 }
@n2 = global %node { i16 2 , %node* @n1, %node* @n3 }
@n3 = global %node { i16 3 , %node* @n2, %node* @n1 }
