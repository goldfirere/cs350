% -*- mode: LaTeX -*-

\documentclass[12pt,pdflatex]{article}

\usepackage[dvipsnames]{xcolor}
\usepackage[all]{xy}
\usepackage{relsize}
\usepackage{listings}
\usepackage{changepage}
\usepackage{inconsolata}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage[T1]{fontenc}  % this gets underscores right!
\usepackage{tikz}

\lstset{
  basicstyle=\smaller\upshape\ttfamily,
  keywordstyle=\bfseries,
  framexleftmargin=1ex,
  framexrightmargin=1ex,
  showstringspaces=false,
  commentstyle=\itshape\rmfamily,
  columns=fixed,
  stringstyle=\ttfamily
}


\addtolength{\oddsidemargin}{-1in}
\addtolength{\evensidemargin}{-1in}
\addtolength{\textwidth}{2in}
\addtolength{\topmargin}{-1in}
\addtolength{\textheight}{2in}

%include polycode.fmt
%include rae.fmt

\newcommand{\keyword}[1]{\textcolor{BlueViolet}{\textbf{#1}}}
\newcommand{\id}[1]{\textsf{\textsl{#1}}}
\newcommand{\varid}[1]{\textcolor{Sepia}{\id{#1}}}
\newcommand{\conid}[1]{\textcolor{OliveGreen}{\id{#1}}}
\newcommand{\tick}{\text{\textquoteright}}
\newcommand{\package}[1]{\textsf{#1}}

%if style==newcode
\begin{code}
module Practice where

import CS350.Unique
import Data.Int ( Int16 )
\end{code}
%endif


\begin{document}

\begin{center}
\bf
CMSC 350: Compiler Design \\
Practice Midterm Questions (Supplement)
\end{center}

\begin{enumerate}[start=5]

\item LLVM global data declarations allow the possibility of declaring mutually recursive
global definitions. Here is an example:

\begin{lstlisting}[language=llvm]
%node = type { i16, %node*, %node* }  ; node for a doubly-linked list

@@n1 = global %node { i16 1 , %node* @n3, %node* @n2 }
@@n2 = global %node { i16 2 , %node* @n1, %node* @n3 }
@@n3 = global %node { i16 3 , %node* @n2, %node* @n1 }
\end{lstlisting}

These definitions create a data structure that looks like this in memory:

\tikzset{every picture/.style={line width=0.75pt}} %set default line width to 0.75pt        

\hspace{-1in}
\begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-1,xscale=1]
%uncomment if require: \path (0,183); %set diagram left start at 0, and has height of 183

%Shape: Rectangle [id:dp7090216906318916] 
\draw   (81,46) -- (200.75,46) -- (200.75,130.25) -- (81,130.25) -- cycle ;
%Straight Lines [id:da5306039000708751] 
\draw    (119.5,46) -- (119.5,130.25) ;


%Straight Lines [id:da4828511014361777] 
\draw    (159.5,46) -- (159.5,130.25) ;


%Shape: Rectangle [id:dp8170191049235132] 
\draw   (235,46) -- (354.75,46) -- (354.75,130.25) -- (235,130.25) -- cycle ;
%Straight Lines [id:da6625423790143207] 
\draw    (273.5,46) -- (273.5,130.25) ;


%Straight Lines [id:da8379707035794309] 
\draw    (313.5,46) -- (313.5,130.25) ;


%Shape: Rectangle [id:dp9474465955840586] 
\draw   (381,46) -- (500.75,46) -- (500.75,130.25) -- (381,130.25) -- cycle ;
%Straight Lines [id:da33012859533052863] 
\draw    (419.5,46) -- (419.5,130.25) ;


%Straight Lines [id:da5192803049181894] 
\draw    (459.5,46) -- (459.5,130.25) ;


%Straight Lines [id:da47251090929877215] 
\draw    (175.5,73) -- (232.5,72.03) ;
\draw [shift={(234.5,72)}, rotate = 539.03] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;

%Straight Lines [id:da2533202675289865] 
\draw    (329.5,66) -- (379.5,66) ;
\draw [shift={(381.5,66)}, rotate = 180] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;

%Straight Lines [id:da03291263560741453] 
\draw    (259.5,106) -- (203.5,106) ;
\draw [shift={(201.5,106)}, rotate = 360] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;

%Straight Lines [id:da8382305585498706] 
\draw    (403.5,104) -- (355.5,104) ;
\draw [shift={(353.5,104)}, rotate = 360] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;

%Curve Lines [id:da962890700467085] 
\draw    (99.5,91) .. controls (-133.5,-8) and (653.5,9) .. (501.5,66) ;
\draw [shift={(501.5,66)}, rotate = 339.44] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;

%Curve Lines [id:da10958021002865626] 
\draw    (481.5,86) .. controls (738.5,167) and (-48,171.75) .. (80.5,110) ;
\draw [shift={(80.5,110)}, rotate = 514.3299999999999] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;


% Text Node
\draw (140.88,88.13) node   {$1$};
% Text Node
\draw (294.88,88.13) node   {$2$};
% Text Node
\draw (440.88,88.13) node   {$3$};


\end{tikzpicture}

This is a circular doubly-linked list.

\begin{enumerate}
\item
Write the HERA code that represents this structure, which would be produced by compiling the LLVM code above.
You should write @DLABEL@s for @n1@, @n2@, and @n3@, and @INTEGER@ commands for the data and the pointers.
Recall that you can write label names in @INTEGER@ commands.

\vspace{2in}

\item
The data should take up 9 words in memory. In the memory chart here, write in what should be stored in the
words in memory:

\begin{center}
%\setlength\extrarowheight{10pt}
\begin{tabular}{||c||c||}
\hline
0xc000 & \phantom{some horiz space} \\ \hline
0xc001 & \\ \hline
0xc002 & \\ \hline
0xc003 & \\ \hline
0xc004 & \\ \hline
0xc005 & \\ \hline
0xc006 & \\ \hline
0xc007 & \\ \hline
0xc008 & \\ \hline
\end{tabular}
\end{center}

\end{enumerate}


\end{enumerate}

\end{document}

