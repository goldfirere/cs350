
%expect 0
%name parse
%tokentype { String }
%error     { (error . show) }

%token
  NUMBER   { "0" }
  PLUS     { "+" }
  TIMES    { "*" }
  LPAREN   { "(" }
  RPAREN   { ")" }

%%

sum : sum PLUS prod   { () }
    | prod         { () }

prod : prod TIMES factor { () }
     | factor        { () }

factor : NUMBER      { () }
       | LPAREN sum RPAREN { () }
