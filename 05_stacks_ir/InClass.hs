{-# OPTIONS_GHC -W #-}

module InClass where

import Data.Word

import HERA.Base
import HERA.Assembly

data Exp
  = Var String       -- string representing an object-language variable
  | Const Word16        -- a constant Int value
  | Add Exp Exp      -- sum of two expressions
  | Mult Exp Exp     -- product of two expressions
  | Neg Exp          -- negation of an expression
  deriving (Eq, Show)

type Ctxt = [(String, Word16)]

compile :: Ctxt -> Exp -> AssemblyProgram
compile ctxt exp = data_insns ++ exp_insns ++ [AHalt]
  where
    data_insns = mkDataInsns ctxt
    exp_insns  = mkExpInsns exp

mkDataInsns :: Ctxt -> AssemblyProgram
mkDataInsns [] = []
mkDataInsns ((var_name, value) : rest)
  = ADlabel var_name : AInteger value : mkDataInsns rest

mkExpInsns :: Exp -> AssemblyProgram
mkExpInsns (Var var_name) = [ ASetl R1 var_name
                            , ALoad R1 0 R1 ]
mkExpInsns (Const n) = [ ASet R1 n ]
mkExpInsns (Add e1 e2) = mkExpInsns e1
                      ++ push R1
                      ++ mkExpInsns e2
                      ++ pop R2
                      ++ [AAdd R1 R1 R2]
mkExpInsns _ = error "ran out of time"

push :: Register -> AssemblyProgram
push reg = [ AStore reg 0 sp
           , AInc sp 1 ]

pop :: Register -> AssemblyProgram
pop reg = [ ADec sp 1
          , ALoad reg 0 sp ]

textCtxt :: Ctxt
textCtxt = [("x", 5), ("y", 8)]
