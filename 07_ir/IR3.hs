{- Author: Steve Zdancewic, translated by Richard Eisenberg
   File: IR3.hs

   Intermediate representation for an expression language with commands,
   conditionals, and loops.
-}

{-# OPTIONS_GHC -W #-}

module IR3 where

import Data.Int     ( Int16 )

import CS350.Unique
import CS350.Renderable  ( Renderable(..) )

import HERA.Assembly     ( AssemblyProgram, AInstruction(..) )
import HERA.Base         ( Register(..), Condition(None, Nz), rt )

import Data.List    ( intercalate, nub )
import Data.Maybe   ( isNothing )
import Text.Printf  ( printf )

-- source language ----------------------------------------------------------

-- This variant of the language treats variables as mutable.

type VarName = String

-- Arithmetic expressions
data Exp
  = VarE VarName         -- string representing an object-language variable
  | ConstE Int16         -- a constant Int16 value
  | AddE Exp Exp         -- sum of two expressions
  | MulE Exp Exp         -- product of two expressions
  | NegE Exp             -- negation of an expression
  deriving (Show, Eq)

-- Abstract syntax of commands
data Command
  = Skip                       -- skip
  | Assign VarName Exp         -- X := e
  | Seq Command Command        -- c1 ; c2
  | IfNZ Exp Command Command   -- if (e /= 0) then cmd1 else cmd2
  | WhileNZ Exp Command        -- while (e /= 0) do cmd
  deriving (Show, Eq)


{-
  X2 := X1 + X2;
  IFNZ X2 THEN
    X1 := X1 + 1
  ELSE
    X2 := X1
  X2 := X2 * X1
-}
exampleBranch :: Command
exampleBranch =
  let x1 = "X1"
      x2 = "X2"
      vx1 = VarE x1
      vx2 = VarE x2
  in
  Seq (Assign x1 (AddE vx1 vx2))
      (Seq (IfNZ vx2
                 (Assign x1 (AddE vx1 (ConstE 1)))
                 (Assign x2 vx1))
           (Assign x2 (MulE vx2 vx1)))

{-
   X1 := 6;
   X2 := 1;
   WhileNZ X1 DO
     X2 := X2 * X1;
     X1 := X1 + (-1);
   DONE
-}
factorial :: Command
factorial =
  let x = "X1"
      ans = "X2"
  in
  Seq (Assign x (ConstE 6))
      (Seq (Assign ans (ConstE 1))
           (WhileNZ (VarE x)
                    (Seq (Assign ans (MulE (VarE ans) (VarE x)))
                         (Assign x   (AddE (VarE x) (ConstE (-1)))))))

-------------------------------------
-- Intermediate representation

type Label = Unique

-- operands
data Operand
  = Id Unique
  | Const Int16
  deriving (Show, Eq)

-- binary operations
data BinaryOp
  = Add
  | Mul
  deriving (Show, Eq)

-- comparisons
data CmpOp
  = Equal
  | LessThan
  deriving (Show, Eq)

-- instructions
-- note that there is no nesting of operations!
data Instruction
  = Let Unique BinaryOp Operand Operand
  | Load Unique VarName
  | Store VarName Operand
  | Compare Unique CmpOp Operand Operand
  deriving (Show, Eq)

-- Block terminator
data Terminator
  = Return
  | Branch Label                 -- unconditional branch
  | CondBr Operand Label Label   -- conditional branch

-- Basic blocks
data Block = Bl { instructions :: [Instruction]
                , terminator   :: Terminator
                }

-- Control flow graph: a pair of an entry block and a set of labeled blocks
data Program = Cfg Block [(Label, Block)]

-------------------------------
-- Pretty-printing

instance Renderable Operand where
  render (Id u)    = render u
  render (Const c) = render c

instance Renderable BinaryOp where
  render Add = "add"
  render Mul = "mul"

instance Renderable CmpOp where
  render Equal = "equal"
  render LessThan = "lessthan"

instance Renderable Instruction where
  render (Let u bop op1 op2) =
    printf "let %s = %s %s %s"
      (render u) (render bop) (render op1) (render op2)
  render (Load u x) =
    printf "let %s = load %s"
      (render u) ("var" ++ x)
  render (Store x op) =
    printf "let _ = store %s %s"
      (render op) ("var" ++ x)
  render (Compare u cmpop op1 op2) =
    printf "let %s = icmp %s %s %s"
      (render u) (render cmpop) (render op1) (render op2)

instance Renderable Terminator where
  render Return                = "  ret ()"
  render (Branch lbl)          = printf "  br %s" (render lbl)
  render (CondBr op lbl1 lbl2) = printf "  cbr %s %s %s"
                                        (render op) (render lbl1) (render lbl2)

instance Renderable Block where
  render (Bl { instructions = insns, terminator = term }) =
    intercalate " in\n" (map render insns) ++
    (if length insns > 0 then " in\n" else "") ++
    render term

instance Renderable Program where
  render (Cfg entry blocks) =
    printf "let program () = \n%s\nin entry ()" $
    printf "let rec entry () =\n%s" (render entry) ++ "\n\n" ++
    intercalate "\n\n" (map (\(lbl, block) ->
                               printf "and %s () = \n%s" (render lbl) (render block)) blocks)

------------------------------------------------------------
-- Compilation

data Element
  = L Label
  | I Instruction
  | T Terminator

-- A stream is a sequence of elements *in reverse order*
data Stream = MkStr [Element]

{- During generation, we typically emit code so that it is in
   _reverse_ order when the stream is viewed as a list.  That is,
   instructions closer to the head of the list are to be executed
   later in the program.  That is because cons is more efficient than
   append.


   To help make code generation easier, we define snoc (reverse cons)
   and reverse append, which let us write code sequences in their
   natural order.
-}
-- append in reverse order
(<++>) :: Stream  -- this one logically comes first
       -> Stream  -- this one logically comes second
       -> Stream
MkStr x <++> MkStr y = MkStr (y ++ x)

{-
-- cons in reverse order
(<:>) :: Stream        -- this logically comes first
      -> Instruction   -- this logically comes second
      -> Stream
MkStr x <:> y = MkStr (y : x)
-}

-- Turn a single instruction into a stream
inst :: Instruction -> Stream
inst = MkStr . (:[]) . I

-- Turn a terminator into a stream
term :: Terminator -> Stream
term = MkStr . (:[]) . T

-- Turn a label into a stream
label :: Label -> Stream
label = MkStr . (:[]) . L

-- An empty stream
emptyStream = MkStr []

{- Convert an instruction stream into a control flow graph.
   Assumes that the instructions are in 'reverse' order of execution.
-}
buildCfg :: Stream -> Program
buildCfg (MkStr code) =
  let (insns, m_term, blks) = go [] Nothing [] code in
  case m_term of
    Nothing -> error "buildCfg: entry block has no terminator"
    Just term -> Cfg (Bl { instructions = insns, terminator = term }) blks
  where
    go insns m_term blks [] = (insns, m_term, blks)

    go insns m_term blks (L l : rest)
      | Just term <- m_term
      = go [] Nothing ((l, Bl { instructions = insns, terminator = term }) : blks) rest
      | null insns  -- this happens for a dummy (redundant) label
      = go [] Nothing blks rest
      | otherwise
      = error (printf "buildCfg: block labeled %s has no terminator" (render l))

    go insns m_term blks (T t : rest)
      | null insns && isNothing m_term
      = go [] (Just t) blks rest
      | otherwise
      = error (printf "buildCfg: block is missing label: %s" (unlines (map render insns)))

    go insns m_term blks (I i : rest)
      = go (i : insns) m_term blks rest

compileBop :: BinaryOp -> Exp -> Exp -> UniqueM (Stream, Operand)
compileBop bop e1 e2 = do
  (ins1, ret1) <- compileExp e1
  (ins2, ret2) <- compileExp e2
  ret <- newUnique "tmp"
  pure (ins1 <++> ins2 <++> inst (Let ret bop ret1 ret2), Id ret)

compileExp :: Exp -> UniqueM (Stream, Operand)
compileExp (VarE x) = do ret <- newUnique "tmp"
                         pure (inst (Load ret x), Id ret)
compileExp (ConstE c)   = pure (emptyStream, Const c)
compileExp (AddE e1 e2) = compileBop Add e1 e2
compileExp (MulE e1 e2) = compileBop Mul e1 e2
compileExp (NegE e1)    = compileBop Mul e1 (ConstE (-1))

compileCmd :: Command -> UniqueM Stream
compileCmd Skip = pure emptyStream
compileCmd (Assign v e) = do
  (is, op) <- compileExp e
  pure (is <++> inst (Store v op))
compileCmd (Seq c1 c2) = do
  str1 <- compileCmd c1
  str2 <- compileCmd c2
  pure (str1 <++> str2)
compileCmd (IfNZ e c1 c2) = do
  (is, result) <- compileExp e
  c1_insns <- compileCmd c1
  c2_insns <- compileCmd c2
  guard <- newUnique "guard"
  nz_branch <- newUnique "nz"
  z_branch <- newUnique "z"
  merge <- newUnique "merge"

  pure (is <++>
        -- Compute the guard result
        inst (Compare guard Equal result (Const 0)) <++>
        term (CondBr (Id guard) z_branch nz_branch) <++>

        -- guard is non-zero
        label nz_branch <++>
        c1_insns <++>
        term (Branch merge) <++>

        -- guard is zero
        label z_branch <++>
        c2_insns <++>
        term (Branch merge) <++>

        label merge)

compileCmd (WhileNZ e c) = do
  (is, result) <- compileExp e
  c_insns <- compileCmd c
  guard <- newUnique "guard"
  entry <- newUnique "entry"
  body  <- newUnique "body"
  exit  <- newUnique "exit"

  pure (term (Branch entry) <++>
        label entry <++>
        is <++>
        inst (Compare guard Equal result (Const 0)) <++>
        term (CondBr (Id guard) exit body) <++>
        label body <++>
        c_insns <++>
        term (Branch entry) <++>
        label exit)

compile :: Command -> UniqueM Program
compile cmd = do
  str <- compileCmd cmd
  pure (buildCfg (str <++> term Return))


--------------------------------------------------------
-- Compilation to HERA

{- Strategy:
 - each named variable becomes a named data location in the data segment
 - each temporary corresponds to a memory location, where the temporary number
   is the address of the memory location.
 - No function calls here, so we don't have to worry about interactions with, e.g.,
   the function stack.
-}

compileProgram :: Program -> AssemblyProgram
compileProgram (Cfg entry labeled_blocks)
  = data_insns ++
    compileBlock entry ++
    concatMap compileLabeledBlock labeled_blocks
  where
    all_variables = collectVariables (entry : map snd labeled_blocks)
    data_insns = concatMap mk_data_insns all_variables

    mk_data_insns var_name = [ ADlabel var_name
                             , AInteger 0 ]

compileLabeledBlock :: (Label, Block) -> AssemblyProgram
compileLabeledBlock (lbl, block)
  = [ALabel (uniqueString lbl)] ++
    compileBlock block

compileBlock :: Block -> AssemblyProgram
compileBlock (Bl { instructions = insns, terminator = term })
  = concatMap compileInstruction insns ++
    compileTerminator term

compileInstruction :: Instruction -> AssemblyProgram
compileInstruction (Let u bop op1 op2)
  = getOperand R1 op1 ++
    getOperand R2 op2 ++
    bopInstruction bop R1 R1 R2 ++
    [ ASet R2 (fromIntegral $ uniqueNumber u)
    , AStore R1 0 R2 ]
compileInstruction (Load u var_name)
  = [ ASetl R1 var_name
    , ALoad R1 0 R1
    , ASet R2 (fromIntegral $ uniqueNumber u)
    , AStore R1 0 R2 ]
compileInstruction (Store var_name op)
  = getOperand R1 op ++
    [ ASetl R2 var_name
    , AStore R1 0 R2 ]
compileInstruction (Compare u cmpop op1 op2)
  = getOperand R1 op1 ++
    getOperand R2 op2 ++
    cmpInstruction cmpop R1 R1 R2 ++
    [ ASet R2 (fromIntegral $ uniqueNumber u)
    , AStore R1 0 R2 ]

compileTerminator :: Terminator -> AssemblyProgram
compileTerminator Return = [AHalt]
compileTerminator (Branch lbl) = [ABr None (uniqueString lbl)]
compileTerminator (CondBr op lbl1 lbl2)
  = getOperand R1 op ++
    [ AFlags R1
    , ABr Nz (uniqueString lbl1)
    , ABr None (uniqueString lbl2) ]

getOperand :: Register -> Operand -> AssemblyProgram
getOperand reg (Id u) = [ ASet rt (fromIntegral $ uniqueNumber u)
                        , ALoad reg 0 rt ]
getOperand reg (Const i) = [ ASet reg (fromIntegral i) ]

bopInstruction :: BinaryOp -> Register -> Register -> Register -> AssemblyProgram
bopInstruction Add rd ra rb = [AAdd rd ra rb]
bopInstruction Mul rd ra rb = [AMul rd ra rb]

cmpInstruction :: CmpOp -> Register -> Register -> Register -> AssemblyProgram
cmpInstruction Equal    rd ra rb = [ ACmp ra rb
                                   , ASavef rd
                                   , ALsr rd rd
                                   , ASetlo rt 1
                                   , AAnd rd rt rd ]
cmpInstruction LessThan rd ra rb = [ ACmp ra rb
                                   , ASavef rd
                                   , ASetlo rt 1
                                   , AAnd rd rt rd ]

collectVariables :: [Block] -> [String]
collectVariables blocks = nub $ concatMap collect1 blocks
  where
    collect1 (Bl { instructions = insns }) = concatMap collect_insn insns
    collect_insn (Load _ var_name)  = [var_name]
    collect_insn (Store var_name _) = [var_name]
    collect_insn _                  = []
