{- Author: Richard Eisenberg
   File: UniqueM.hs

   A monad that allows the generation of Uniques, values that
   are guaranteed to be unique.

   CS350 students are not expected to understand the details of the code
   here, but it's not that hard, if you want to learn about monads.
-}

{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module CS350.Unique ( UniqueM ) where

import Control.Monad.State

newtype UniqueM a = UniqueM (State Integer a)
  deriving (Functor, Applicative, Monad, MonadState Integer)

-- under the hood, a Unique is just a positive integer
newtype Unique = MkUnique Integer
  deriving Eq

instance Show Unique where
  show (MkUnique n) = "u" ++ show n

-- Generate a new
newUn
