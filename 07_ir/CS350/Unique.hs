{- Author: Richard Eisenberg
   File: Unique.hs

   A monad that allows the generation of Uniques, values that
   are guaranteed to be unique.

   CS350 students are not expected to understand the details of the code
   here, but it's not that hard, if you want to learn about monads.
-}

{-# LANGUAGE GeneralizedNewtypeDeriving, TypeApplications #-}

module CS350.Unique ( UniqueM, Unique, runUniqueM, newUnique, uniqueString, uniqueNumber ) where

import CS350.Renderable

import Control.Monad.State

import Data.Coerce

newtype UniqueM a = UniqueM (State Integer a)
  deriving (Functor, Applicative, Monad, MonadState Integer)

-- under the hood, a Unique is just a positive integer
-- But we carry a String, too, for easy printing
data Unique = MkUnique String Integer

instance Eq Unique where
  -- only the numbers matter
  MkUnique _ n1 == MkUnique _ n2 = n1 == n2

instance Show Unique where
  show (MkUnique name n) = name ++ show n

instance Renderable Unique where
  render u = show u

-- Generate a new unique value, different from all others
newUnique :: String -> UniqueM Unique
newUnique name = UniqueM $ do u <- get
                              modify (1+)
                              return (MkUnique name u)

-- get a unique string from a Unique
uniqueString :: Unique -> String
uniqueString = show

uniqueNumber :: Unique -> Integer
uniqueNumber (MkUnique _ n) = n

runUniqueM :: UniqueM a -> a
runUniqueM action = evalState @Integer (coerce action) 0
