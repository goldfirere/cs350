---
title: CS 350 Policies
---

<div id="header">

| **CS 350 Course Policies**
| Prof. Richard Eisenberg
| Spring 2019

</div>

\$navbar\$

Prerequisites
=============

All students should have completed the introductory computer science sequence,
including some introductory course (B110, B113, H105, H107, or BIO B115),
some data structures course (B206, H106, or H107), discrete mathematics
(B/H231), and programming languages (B/H245).

Workload
--------

It is my expectation that this course is difficult. The reward is that you will
gain a lot of experience coding and about compilers.
I expect that you will
have to spend 10+ hours working outside of class/lab every week. For a two-week
assignment, this means that the assignment might take 20 hours or more.

Grading
=======

<div class="grading_table">

----------------------------  --------------
Projects                       70%
Midterm                        12%
Final                          18%
<span class="strut" />
Total                          100%
----------------------------  --------------

</div>

Individual assingments/exams will be graded on a percentage basis. At the
end of the semester, these grades will be translated into the 4-point scale
roughly according to the following table (without rounding):

<div class="grading_table">

----------------------------  --------------
**Percentage**                **Grade**
0-50%                         0.0
50-57%                        1.0
57-63%                        1.3
63-69%                        1.7
69-74%                        2.0
74-78%                        2.3
78-82%                        2.7
82-86%                        3.0
86-90%                        3.3
90-94%                        3.7
94-100%                       4.0
----------------------------  --------------

</div>

Note that the correspondence between percentage grade and transcript grade
here is rough; we will discuss updates to this chart throughout the semester.

Assignment grading
------------------

Assignments will be (mostly) autograded upon submission to [Gradescope]. This means that, as
soon as you submit your work, you will get the bulk of your assignment grade.
The rest of the grade will be assigned manually, and will assess the tests
you have written as well as your coding style. There will not be formal style
criteria in this course; instead, you are expected to write code that others
can read and understand.

Code that does not compile will earn no credit. Code that has not been formatted
according to the directions in the assignment (e.g., that uses different function
names or filenames than required) will earn no credit. Case matters.

Exams
-----

This course has one midterm (March 6), and one final exam, to be taken on the last
day of class.

Exams will be open-book and open-note (not open-compiler). Students
using an electronic version of the book or taking electronic notes
may consult those resources: you can do so only with a laptop (no
tablets/phones), with its network connection disabled, and with no use
of the keyboard.

If you miss an exam without emailing me first, you will get a 0 on that
exam. Changes to the exam schedule will be considered only in extreme
circumstances.

Late policy
===========

Assignments will generally be due Wednesday nights at midnight.
You will submit assignments via
[Gradescope], as practiced during the first lab.

Late assignments will lose 20% of their points for every day
late (or portion thereof). Each student gets 5 free *late days* for the semester.
This means that the first five days (or portion thereof) that an assignment
is late will not lead to a penalty. Late days cannot be split; in other words,
the count of the late das available is always an integer.
These late days are intended to account for
unexpected bugs, minor illnesses, planned travel, etc. I will grant further
extensions only in rare circumstances. You do not have to request to use a late day;
they are automatically granted.

Group work policy
=================

Working with a partner
----------------------

The first assignment will be *individual*. You will not be allowed to work with
a partner on that assignment.

Further assignments will be completed *in pairs*. If you wish not to work in a pair,
you must discuss that decision with me first.

When working with a partner:

 * Make sure that both partners' names are on all parts of the assignment.
 * Register as a group within [Gradescope].
 * Submit only one copy of the assignment.

Two partners working together receive one shared grade.

Collaboration policy
--------------------

(In this section, when you are working with a partner, you and your partner are
considered "you". It is expected that partners share code!)

You are encouraged to brainstorm with others for assignments, **but your submission
must be your own**:

<div id="plagiarism">
All the code you submit must be written by you alone.
</div>

This means that, while it's a great idea to discuss general algorithms or
approaches with your classmates, you should make sure that the code you
submit is yours and yours alone. This means that it may be OK to help a
friend debug their code or to look at someone else's approach for inspiration,
but **your own writeup must be made independently,** without the other code
available for consultation.

If your work has been influenced by the work of another student, **you must
cite that student** in your submission. (This citation can take the form
of a comment in that area of your code; there is no prescribed citation
format.) Note that a citation is necessary even if it's just an influence.
These citations have no negative impact on your grade.

Submitting code from any source other than your own brain without citation
is a violation of Bryn Mawr's Honor Code. In particular, **never submit code
you have found online**.

If you have a question, post on [Piazza].

Piazza
======

There is a course question-and-answer forum at
<https://piazza.com/brynmawr/spring2019/cs350/home>. This forum is hosted by an
educational service, Piazza, and it will be the primary means of electronic
communication in this course. It is free to use. (Piazza's business model is about
connecting students with job recruiters, a feature called Piazza Careers.
Recruiters pay for this connection. Your questions and other class participation
remain strictly private. During
sign-up, you may choose whether you wish to participate in Piazza Careers.)
Piazza offers the ability to ask
questions anonymously (either anonymous to everyone or only to fellow students, but not
instructors) and for one student to see others' questions. You may also use the
site to send private messages/questions to the instructors of the course.

If you have a question about the material, the course, extensions, or anything else,
please use Piazza.

Piazza also has the ability to send out announcements. Any coursewide information will
go through that site, which will send everyone an email with the info.

I do tend to respond to questions during business hours only; something posted Friday
evening might not get a response until Monday. However, an advantage to posting on
Piazza is that a fellow student might answer a question before I can.

GitLab
======

The course materials are hosted on [GitLab](https://gitlab.com/goldfirere/cs350).
Feel free to submit corrections/ask clarifications there, via GitLab's [merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html)
or [issues](https://docs.gitlab.com/ee/user/project/issues/) mechanisms.

Accommodations for disability
=============================

Bryn Mawr College is committed to providing equal access to students with a
documented disability. Students needing academic accommodations for a
disability must first register with Access Services. Students can call
610-526-7516 to make an appointment with the Coordinator of Access Services,
Deb Alder, or [email her](mailto:dalder@brynmawr.edu) at `dalder@brynmawr.edu`
to begin this confidential process. Once registered, students should schedule
an appointment with me as early in the semester as possible to share the
verification form and make appropriate arrangements. Please note that
accommodations are not retroactive and require advance notice to implement.
More information can be obtained at the [Access Services
website](http://www.brynmawr.edu/access_services/).

Meetings
========

My office hours for Fall 2019 are Wednesdays 2:40-4:00 and Fridays 1:00-2:00.
This means that, at
these hours, I am guaranteed to be in my office and expecting visitors -- and
I really do want visitors. During class, it's hard to get to know all of you,
and I'd love to know more about what brought you into my class, what else
you're interested in (in computer science and more broadly), and how your
college experience is going generally. Come with a question, come to say hi,
or come to play one of my puzzles. You can even use your curiosity about my
puzzle collection as an excuse to get in the door.

If you have a conflict with my office hours, please email so we can find another
time to meet.

Beyond my office hours, I aim to have an open-door policy. If you see my
office door (Park 204) open, please interrupt me: that's why the door is
open!

For a broader discussion than just homework questions, I'd be happy to join
you for lunch in the dining hall. Just email or ask!

[Piazza]: https://piazza.com/brynmawr/spring2019/cs350/home
[Gradescope]: https://gradescope.com/

Acknowledgments
===============

This course is adapted (with permission) from [Prof. Steve
Zdancewic](http://www.cis.upenn.edu/~stevez/)'s compilers course at the
University of Pennsylvania. The chief difference in this course against
Steve's is that Steve's is in ML, while this course is in Haskell.
