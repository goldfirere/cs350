#include <stdio.h>

struct Point {
  int x;
  int y;
};

struct Rect {
  struct Point ll, lr, ul, ur;
};

int main()
{
  struct Rect square;
  printf("sizeof(int) = %lu\n", sizeof(int));
  printf("%15s: %p\n", "square", &square);
  printf("%15s: %p\n", "square.ll.x", &(square.ll.x));
  printf("%15s: %p\n", "square.ll.y", &(square.ll.y));
  printf("%15s: %p\n", "square.lr.x", &(square.lr.x));
  printf("%15s: %p\n", "square.ul.y", &(square.ul.y));
  
  return 0;
}
