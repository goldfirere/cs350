{- Author: Steve Zdancewic, translated to Haskell by Richard Eisenberg
   File: LL.hs

   A simple LL(1) parser, written without custom tooling.
-}

{-# OPTIONS_GHC -W #-}

module LL where

data Token = NUMBER Int | PLUS | LPAREN | RPAREN | EOF
  deriving (Show, Eq)

data Exp = Lit Int
         | Plus Exp Exp
  deriving (Show, Eq)

-- LL(1) Parser that converts a token stream into an expression.
parse :: [Token] -> Maybe Exp
parse = parseT

-- Helper functions for `parse`. These should really be in a "where"
-- clause of `parse`, but this allows for easier debugging
parseT :: [Token] -> Maybe Exp
parseT all_ts@(NUMBER _:_) = case parseS all_ts of
  Just (exp, [EOF]) -> Just exp
  _                 -> Nothing
parseT all_ts@(LPAREN:_)   = case parseS all_ts of
  Just (exp, [EOF]) -> Just exp
  _                 -> Nothing
parseT _                   = Nothing

parseS :: [Token] -> Maybe (Exp, [Token])
parseS all_ts@(NUMBER _:_) = case parseE all_ts of
  Just (exp, ts) -> parseS' exp ts
  _              -> Nothing
parseS all_ts@(LPAREN:_)   = case parseE all_ts of
  Just (exp, ts) -> parseS' exp ts
  _              -> Nothing
parseS _                   = Nothing

parseS' :: Exp -> [Token] -> Maybe (Exp, [Token])
parseS' exp (PLUS:ts)     = case parseS ts of
  Just (exp', ts') -> Just (Plus exp exp', ts')
  _                -> Nothing
parseS' exp ts@(RPAREN:_) = Just (exp, ts)
parseS' exp ts@(EOF:_)    = Just (exp, ts)
parseS' _   _             = Nothing

parseE :: [Token] -> Maybe (Exp, [Token])
parseE (NUMBER n:ts) = Just (Lit n, ts)
parseE (LPAREN:ts)   = case parseS ts of
  Just (exp, RPAREN:ts') -> Just (exp, ts')
  _                      -> Nothing
parseE _             = Nothing

-- Test input for the source string: (1 + 2 + (3 + 4)) + 5
testInput = [ LPAREN, NUMBER 1, PLUS, NUMBER 2, PLUS
                    , LPAREN, NUMBER 3, PLUS, NUMBER 4, RPAREN, RPAREN, PLUS
            , NUMBER 5, EOF ]

-- helpful for debugging
renderExp :: Exp -> String
renderExp (Lit n)                 = show n
renderExp (Plus e1 e2@(Plus _ _)) = renderExp e1 ++ " + (" ++ renderExp e2 ++ ")"
renderExp (Plus e1 e2)            = renderExp e1 ++ " + " ++ renderExp e2
