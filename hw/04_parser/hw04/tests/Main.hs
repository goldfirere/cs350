{- Author: Richard Eisenberg
   File: Main.hs

   Run tests on HW04
-}

{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -W #-}

module Main where

import Tiger.Lexer  ( lexTiger )
import Tiger.Parser ( parse    )
import Tiger.Syntax ( Exp      )

import Tiger.ParserCorrect    ( parserCorrect )

import Wrap                   ( WrapSettings(..), wrapText )

import System.Directory       ( createDirectoryIfMissing )
import System.Directory.Extra ( listContents )
import System.FilePath        ( (</>), (<.>), takeBaseName, isExtensionOf )

import Data.List              ( partition )

import Control.Exception      ( evaluate, ErrorCall, catch )
import Control.DeepSeq        ( force    )

import Test.Tasty             ( TestTree, localOption, mkTimeout, defaultMain, testGroup )
import Test.Tasty.Golden      ( goldenVsFile )
import Test.Tasty.HUnit       ( testCase, assertFailure )
import Test.Tasty.QuickCheck  ( testProperty, QuickCheckTests(..), QuickCheckReplay(..) )

outputDirName, goldenDirName :: String
outputDirName = "test-output"
goldenDirName = "golden"

failingTigers :: [String]
failingTigers = ["test49"]

main :: IO ()
main = do
  createDirectoryIfMissing False outputDirName
  tigers <- filter ("tig" `isExtensionOf`) <$> listContents "tigers"
  let (bad_tigers, good_tigers) = partition ((`elem` failingTigers) . takeBaseName) tigers
      goldenTests = map (localOption (mkTimeout (2 * 10^6)) . mkGoldenTest) good_tigers
      expectFailure = map (localOption (mkTimeout (2 * 10^6)) . mkExpectFailure) bad_tigers
      allTests    = testGroup "all" $ runCheckParser : goldenTests ++ expectFailure
  defaultMain allTests

runCheckParser :: TestTree
runCheckParser =
  localOption (mkTimeout (120 * 10^6)) $
  localOption (QuickCheckReplay (Just 12345)) $
  localOption (QuickCheckTests 50000) $
  testProperty "checkParser" parserCorrect

mkGoldenTest :: FilePath -> TestTree
mkGoldenTest tiger_path
  = goldenVsFile (takeBaseName tiger_path) golden_path output_path (writeAST tiger_path output_path)
  where
    base = takeBaseName tiger_path

    output_path = outputDirName </> base <.> "ast"
    golden_path = goldenDirName </> base <.> "ast"

writeAST :: FilePath -> FilePath -> IO ()
writeAST tiger_path output_path = do
  ast <- parseFrom tiger_path
  writeFile output_path (wrapText settings 78 $ show ast)
  where
    settings = WrapSettings { preserveIndentation = False
                            , breakLongWords = False }

parseFrom :: FilePath -> IO Exp
parseFrom tiger_path = do
  contents <- readFile tiger_path
  lexed <- evaluate (force (lexTiger contents))
  evaluate (force (parse lexed))

mkExpectFailure :: FilePath -> TestTree
mkExpectFailure tiger_path = testCase (base ++ " (expect parse failure)") $ do
    result <- parseFrom tiger_path
    assertFailure $ "Parse succeeded with " ++ show result
  `catch` \ (_ :: ErrorCall) -> pure ()  -- succeed
  where
    base = takeBaseName tiger_path
