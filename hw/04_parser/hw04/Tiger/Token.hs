{- Author: <Your name here>
   File: Token.hs

   Defines lexical tokens for Tiger
-}

{-# LANGUAGE DeriveGeneric, DeriveAnyClass, EmptyDataDeriving #-}

module Tiger.Token where

import Control.DeepSeq ( NFData )
import GHC.Generics    ( Generic )

data Token
  -- insert token constructors here
  deriving (Show, Generic, NFData)
