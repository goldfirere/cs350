-- -*- mode: haskell -*-

{
{- Author: <Your name here>
   File: Lexer.x

   A lexer file for Tiger.
-}

{-# OPTIONS_GHC -W -Wno-unused-top-binds #-}

module Tiger.Lexer ( lexTiger ) where

import Tiger.Token  ( Token(..) )

import Data.Char    ( isSpace, isDigit, chr )
}

%wrapper "basic"

-- Add definitions for character sets and regular expressions here:

Tiger :-

-- Add lexing rules here:
$white                  ;

{

lexTiger :: String -> [Token]
lexTiger = alexScanTokens

-- Add auxiliary Haskell functions here:

}
