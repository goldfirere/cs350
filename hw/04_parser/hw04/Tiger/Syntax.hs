{- Author: Richard Eisenberg
   File: Syntax.hs

   Defines the AST for the Tiger language. The names in this file
   echo Appel's Appendix A.
-}

{-# LANGUAGE FlexibleInstances, DeriveGeneric, DeriveAnyClass,
             DeriveDataTypeable #-}
{-# OPTIONS_GHC -W #-}

module Tiger.Syntax where

import CS350.Renderable ( Renderable(..) )

import Text.Printf      ( printf )
import Data.Data        ( Data )
import Data.List        ( intercalate )
import Control.DeepSeq  ( NFData )
import GHC.Generics     ( Generic )

-- | Simple representation for Tiger identifiers of all sorts
type TypeId  = String
type FieldId = String
type VarId   = String
type FunId   = String

-------------------------------
-- Appel's A.2
-- Declarations

-- | Top-level declarations
data Dec
  = TypeD TyDec
  | VarD  VarDec
  | FunD  FunDec
  deriving (Show, Eq, Generic, NFData, Data)

-------------------------------
-- Data Types

data TyDec
  = TD TypeId Ty
  deriving (Show, Eq, Generic, NFData, Data)

data Ty
  = Named TypeId
  | Struct [TyField]
  | Array TypeId
  | BuiltIn BuiltInTy
  deriving (Show, Eq, Generic, NFData, Data)

data BuiltInTy
  = TigerInt
  | TigerString
  deriving (Show, Eq, Generic, NFData, Data)

data TyField
  = TF FieldId TypeId    -- the VarId is the field name
  deriving (Show, Eq, Generic, NFData, Data)

-------------------------------
-- Variables

data VarDec
  = PlainVar VarId Exp
  | AnnotVar VarId TypeId Exp
  deriving (Show, Eq, Generic, NFData, Data)

-------------------------------
-- Functions

data FunDec
  = Proc FunId [TyField] Exp
  | Fun FunId [TyField] TypeId Exp
  deriving (Show, Eq, Generic, NFData, Data)

-------------------------------
-- Appel's A.3
-- L-Values

data Lvalue
  = VarLV VarId
  | FieldLV Lvalue FieldId
  | ArrayLV Lvalue Exp
  deriving (Show, Eq, Generic, NFData, Data)

-------------------------------
-- Expressions

data Exp
  = Lvalue Lvalue
  | Nil
  | Seq [Exp]
  | Literal Lit
  | Negate Exp
  | Call FunId [Exp]
  | Binary Exp Op Exp
  | NewRecord TypeId [(FieldId, Exp)]
  | NewArray TypeId Exp Exp
  | Assign Lvalue Exp
  | If Exp Exp (Maybe Exp)
  | While Exp Exp
  | For VarId Exp Exp Exp
  | Break
  | Let [Dec] [Exp]
  deriving (Show, Eq, Generic, NFData, Data)

data Op  -- binary operators
  = Plus
  | Minus
  | Times
  | Divides
  | Equals
  | NotEquals
  | Greater
  | Less
  | GreaterEquals
  | LessEquals
  | LogicalAnd
  | LogicalOr
  deriving (Show, Eq, Generic, NFData, Data)

data Lit   -- literals
  = IntLit Integer
  | StringLit String
  deriving (Show, Eq, Generic, NFData, Data)

-----------------------------------------
-- Pretty-printing

instance Renderable Dec where
  render (TypeD td) = render td ++ "\n"
  render (VarD vd)  = render vd ++ "\n"
  render (FunD fd)  = render fd ++ "\n"

instance Renderable TyDec where
  render (TD id ty) = printf "type %s = %s" id (render ty)

instance Renderable Ty where
  render (Named id) = id
  render (Struct fields) = printf "{%s}" (render fields)
  render (Array id) = "array of " ++ id
  render (BuiltIn bty) = printf "<%s>" (render bty)

instance Renderable BuiltInTy where
  render TigerInt = "int"
  render TigerString = "string"

instance Renderable TyField where
  render (TF f_id t_id) = printf "%s : %s" f_id t_id

instance Renderable [TyField] where
  render fields = (intercalate "," (map render fields))

instance Renderable VarDec where
  render (PlainVar id exp) = printf "var %s = %s" id (render exp)
  render (AnnotVar id ty exp) = printf "var %s : %s = %s" id ty (render exp)

instance Renderable FunDec where
  render (Proc name args exp) = printf "function %s(%s) = %s" name (render args) (render exp)
  render (Fun name args ty exp)
    = printf "function %s(%s) : %s = %s" name (render args) ty (render exp)

instance Renderable Lvalue where
  render (VarLV id) = id
  render (FieldLV lv f_id) = render lv ++ "." ++ f_id
  render (ArrayLV lv exp)  = printf "%s[%s]" (render lv) (render exp)

instance Renderable Exp where
  render (Lvalue lv) = render lv
  render Nil         = "nil"
  render (Seq exps)  = printf "(%s)" (intercalate ";" (map render exps))
  render (Literal l) = render l
  render (Negate e)  = "-" ++ render e
  render (Call f args) = printf "%s(%s)" f (intercalate "," (map render args))
  render (Binary e1 op e2) = printf "%s %s %s" (render e1) (render op) (render e2)
  render (NewRecord ty inits)
    = printf "%s {%s}" ty (intercalate ", " (map (\(field, val) ->
                                                    printf "%s=%s" field (render val)) inits))
  render (NewArray ty sz val) = printf "%s[%s] of %s" ty (render sz) (render val)
  render (Assign lv val) = printf "%s := %s" (render lv) (render val)
  render (If e1 e2 Nothing) = printf "if %s then %s" (render e1) (render e2)
  render (If e1 e2 (Just e3)) = printf "if %s then %s else %s" (render e1) (render e2) (render e3)
  render (While e1 e2) = printf "while %s do %s" (render e1) (render e2)
  render (For id e1 e2 e3) = printf "for %s := %s to %s do %s" id (render e1) (render e2) (render e3)
  render Break = "break"
  render (Let decs exps)
    = printf "let\n %sin\n %s\nend" (concatMap render decs) (intercalate ";" (map render exps))

instance Renderable Op where
  render Plus = "+"
  render Minus = "-"
  render Times = "*"
  render Divides = "/"
  render Equals = "="
  render NotEquals = "<>"
  render Greater = ">"
  render Less = "<"
  render GreaterEquals = ">="
  render LessEquals = "<="
  render LogicalAnd = "&"
  render LogicalOr = "|"

instance Renderable Lit where
  render (IntLit n) = show n
  render (StringLit s) = show s
