{- Author: Richard Eisenberg
   File: ParserCorrect.hs

   Uses QuickCheck to determine whether the parser and lexer is correct.

   Correctness condition: ∀ e ∈ exp,
     parse (lexTiger (render (parse (lexTiger (render e)))))
        ==
     parse (lexTiger (render e))

   We can't just compare to e itself, because e might have, e.g., right-associative
   + constructions that are unparseable.

   Deep Haskell magic is at work here.
-}

{-# LANGUAGE TypeApplications, ScopedTypeVariables, FlexibleContexts, UndecidableInstances,
             MonoLocalBinds, StandaloneDeriving, DeriveGeneric #-}

module Tiger.ParserCorrect where

import Test.QuickCheck
import Test.QuickCheck.Random
import Generic.Random
import GHC.Generics
import Data.Generics

import CS350.Renderable

import Tiger.Syntax
import Tiger.Parser
import Tiger.Lexer

instance Arbitrary Exp where
  arbitrary = genericArbitraryU'

instance Arbitrary Dec where
  arbitrary = genericArbitraryU'

instance Arbitrary TyDec where
  arbitrary = genericArbitraryU'

instance Arbitrary Lit where
  arbitrary = genericArbitraryU'

instance Arbitrary Lvalue where
  arbitrary = genericArbitraryU'

instance Arbitrary Op where
  arbitrary = genericArbitraryU'

instance Arbitrary VarDec where
  arbitrary = genericArbitraryU'

instance Arbitrary FunDec where
  arbitrary = genericArbitraryU'

instance Arbitrary Ty where
  arbitrary = genericArbitraryU'

instance Arbitrary BuiltInTy where
  arbitrary = genericArbitraryU'

instance Arbitrary TyField where
  arbitrary = genericArbitraryU'

roundTrip :: Exp -> Exp
roundTrip = parse . lexTiger . render

parserCorrect :: Exp -> Property
parserCorrect e
  = noEmptyStrings e ==>
    goodPrecedence e ==>
      e1 == e2
  where
    e1 = roundTrip e
    e2 = roundTrip e1

noEmptyStrings :: Exp -> Bool
noEmptyStrings = everything (&&) (mkQ True (\ (s :: String) -> not (null s)))

goodPrecedence :: Data a => a -> Bool
goodPrecedence = everything (&&) (mkQ True ok_prec)
  where
    ok_prec :: Exp -> Bool

    ok_prec (Negate (Binary {})) = False
    ok_prec (Negate (Assign {})) = False

    ok_prec (Binary e1@(Binary _ op1 _) op2 e2@(Binary _ op3 _))
      = op1 `can_be_left_child_of` op2 &&
        op3 `can_be_right_child_of` op2 &&
        ok_prec e1 &&
        ok_prec e2
    ok_prec (Binary e1@(Binary _ l_op _) r_op e2)
      = l_op `can_be_left_child_of` r_op &&
        ok_prec e1 &&
        ok_prec e2
    ok_prec (Binary e1 l_op e2@(Binary _ r_op _))
      = r_op `can_be_right_child_of` l_op &&
        ok_prec e1 &&
        ok_prec e2

    ok_prec (Binary (NewArray {}) _ _) = False
    ok_prec (Binary (Assign {}) _ _)   = False
    ok_prec (Binary (If {}) _ _)       = False
    ok_prec (Binary (While {}) _ _)    = False
    ok_prec (Binary (For {}) _ _)      = False

    ok_prec (Binary _ _ (Assign {}))   = False

    ok_prec other = and $ gmapQ goodPrecedence other

    l_op `can_be_left_child_of` r_op
      | prec_l == prec_r
      = isLeftAssociative l_op && isLeftAssociative r_op
      | otherwise
      = prec_l > prec_r
      where
        prec_l = getPrecedence l_op
        prec_r = getPrecedence r_op

    r_op `can_be_right_child_of` l_op
      | prec_l == prec_r
      = isRightAssociative l_op && isRightAssociative r_op
      | otherwise
      = prec_r > prec_l
      where
        prec_l = getPrecedence l_op
        prec_r = getPrecedence r_op

getPrecedence :: Op -> Int
getPrecedence Plus          = 5
getPrecedence Minus         = 5
getPrecedence Times         = 6
getPrecedence Divides       = 6
getPrecedence Equals        = 3
getPrecedence NotEquals     = 3
getPrecedence Greater       = 3
getPrecedence Less          = 3
getPrecedence GreaterEquals = 3
getPrecedence LessEquals    = 3
getPrecedence LogicalAnd    = 2
getPrecedence LogicalOr     = 1

isLeftAssociative :: Op -> Bool
isLeftAssociative op = case op of
  Plus       -> True
  Minus      -> True
  Times      -> True
  Divides    -> True
  LogicalAnd -> True
  LogicalOr  -> True
  _          -> False

isRightAssociative :: Op -> Bool
isRightAssociative _ = False

-- Prints out results of tests to console. Also returns True for a correct parser
-- and False otherwise.
checkParser :: IO Bool
checkParser = do
  result <- quickCheckWithResult (stdArgs { maxSuccess = 50000
                                          , replay = Just (mkQCGen 12345, 5) }) parserCorrect
  case result of
    Success {} -> pure True
    _          -> pure False
