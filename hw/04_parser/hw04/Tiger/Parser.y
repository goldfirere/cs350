-- -*- mode: haskell -*-

{
{- Author: <Your name here>
   File: Parser.y

   A parser for the Tiger language.
-}

module Tiger.Parser ( parse ) where

import Tiger.Token ( Token(..) )
import Tiger.Syntax

}

%expect 0             -- expect 0 conflicts in this grammar
                      -- comment this line out to allow happy to run
%name parse           -- exported function will be called parse
%tokentype { Token }  -- input is list of tokens
%error     { (error . show) }   -- what to do in event of emergency

-- Declare all terminals with a %token declaration:




%%

-- Expressions. (These come first because an overall Tiger program is an expression.)
exp :     { Nil }   -- delete this line; it's wrong
