{- Author: Richard Eisenberg
   File: Main.hs

   Main program for HW04: read in a Tiger file and print out
   its AST and rendered code.
-}

{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import CS350.Renderable     ( render   )
import Tiger.Lexer          ( lexTiger )
import Tiger.Parser         ( parse    )

import Control.DeepSeq      ( force )
import Control.Exception    ( SomeException, catch, evaluate, displayException )

import System.Environment   ( getArgs, getProgName )
import System.Exit          ( exitSuccess, exitFailure )
import System.FilePath      ( isExtensionOf )

main :: IO ()
main = do
  args <- getArgs

  input_file <- case args of
    [input_file]
      |  "tig" `isExtensionOf` input_file
      -> pure input_file
    _ -> do prog <- getProgName
            putStrLn $ "Usage: " ++ prog ++ " <Tiger-file>"
            putStrLn "The parsed Tiger AST is then printed, along with a rendered version"
            putStrLn "of that AST."
            exitFailure

  input <- readFile input_file

  lexed <- evaluate (force (lexTiger input))
           `catch` (\ (e :: SomeException) ->
                      do putStrLn $ "Exception during lexing: " ++ displayException e
                         exitFailure)

  parsed <- evaluate (force (parse lexed))
            `catch` (\ (e :: SomeException) ->
                       do putStrLn $ "Exception during parsing: " ++ displayException e
                          exitFailure)

  putStrLn "------------ Parsed AST -----------------"
  print parsed

  putStrLn "\n\n\n\n"
  putStrLn "------------ Rendered AST ---------------"
  putStrLn (render parsed)
