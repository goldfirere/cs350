{- Author: Steve Zdancewic, translated to Haskell (and HERA) by Richard Eisenberg
   File: BackEnd.hs

   The backend for our Tiger compiler, compiling LLVM to HERA.
-}

{-# OPTIONS_GHC -W -Wno-unused-imports #-}

module BackEnd ( compileLLVM ) where

import CS350.Panic
import CS350.CompileM   ( CompileM, compileError )
import CS350.Unique     ( newUniqueString )
import CS350.WordN      ( Word5, Word6 )
import CS350.Renderable ( render )

-- Importing these qualified means that you can disambiguate with H. or L.
import HERA.Assembly as H
import HERA.Base     as H
import LLVM.Lite     as L

import Control.Monad        ( zipWithM )
import Control.Monad.Extra  ( concatMapM )

import Data.Bits  ( toIntegralSized )
import Data.Char  ( ord )
import Data.List  ( genericLength, intercalate )
import Data.Word  ( Word16 )

import Data.Map   ( Map )
import qualified Data.Map as M
import Data.Set   ( Set )
import qualified Data.Set as S

-- helpers ------------------------------------------------------------------


-- locals and layout --------------------------------------------------------

{- One key problem in compiling the LLVM IR is how to map its local
   identifiers to HERA abstractions.  For the best performance, one
   would want to use an HERA register for each LLVM %uid.  However,
   since there are an unlimited number of %uids and only 16 registers,
   doing so effectively is quite difficult.  We will see later in the
   course how _register allocation_ algorithms can do a good job at
   this.

   A simpler, but less performant, implementation is to map each %uid
   in the LLVM source to a _stack slot_ (i.e. a region of memory in
   the stack).  Since LLVMlite, unlike real LLVM, permits %uid locals
   to store only 16-bit data, each stack slot is an 16-bit value.

   [ NOTE: For compiling LLVMlite, even i1 data values should be
   represented as a 16-bit word. This greatly simplifies code
   generation. ]

   We call the datastructure that maps each %uid to its stack slot a
   'stack layout'.  A stack layout maps a uid to a HERA operand for
   accessing its contents.  For this compilation strategy, the operand
   is always an offset from the FP (in words) that represents a storage slot in
   the stack.
-}

type Layout = Map L.Local Word16

-- A context contains the global type declarations (needed for getelementptr
-- calculations), a stack layout, and the label to jump to when returning
-- from a function. (See more in compileFunDecl section.)
data Context = Ctxt { tyDecls  :: [L.Named L.Ty]
                    , layout   :: Layout
                    , retLabel :: H.Label
                    }
  deriving (Show, Eq)

-- useful for looking up items in typeDecls
nlookup :: [L.Named a] -> String -> Maybe a
nlookup [] _ = Nothing
nlookup (L.N { L.name = n, L.decl = d } : rest) n'
  | n == n' = Just d
  | otherwise = nlookup rest n'

-- compiling operands  ------------------------------------------------------

{- LLVM IR instructions support several kinds of operands.

   LL local %uids live in stack slots, whereas global ids live at
   global addresses that must be computed from a label.  Constants are
   immediately available, and the operand Null is the 16-bit 0 value.

   One strategy for compiling instruction operands is to use a
   designated register (or registers) for holding the values being
   manipulated by the LLVM IR instruction. You might find it useful to
   implement the following helper function, whose job is to generate
   the HERA instruction that moves an LLVM operand into a designated
   destination (usually a register).
-}
compileOperand :: Context -> H.Register -> L.Operand -> CompileM [AInstruction]
compileOperand = unimplemented "compileOperand"

-- compiling call  ----------------------------------------------------------

{- You will probably find it helpful to implement a helper function that
   generates code for the LLVM IR call instruction.

   The code you generate should follow the HERA calling convention
   as described in the HERA specification, Section 7.5.

   The function to call must be a global id naming a function.
   Anything else is an error.
-}

compileCall :: Context
            -> Operand           -- the function being called
            -> [Operand]         -- arguments
            -> CompileM [AInstruction]
compileCall = unimplemented "compileCall"

-- compiling getelementptr (gep)  -------------------------------------------

{- The getelementptr instruction computes an address by indexing into
   a datastructure, following a path of offsets.  It computes the
   address based on the size of the data, which is dictated by the
   data's type.

   To compile getelmentptr, you must generate HERA code that performs
   the appropriate arithmetic calculations.
-}

{- [sizeTy] maps an LLVMlite type to a size in 16-bit words.
    (needed for getelementptr)

   - the size of a struct is the sum of the sizes of each component
   - the size of an array of t's with n elements is n * the size of t
   - all pointers, i1, and i64 are 1 word
   - the size of a named type is the size of its definition

   - Void, i8, and functions have undefined sizes according to LLVMlite.
     Your function should simply return 0 in those cases
-}
sizeTy :: [Named Ty]   -- known type declarations
       -> Ty
       -> CompileM Word16 -- size, in 16-bit words
sizeTy = unimplemented "sizeTy"

{- Generates code that computes a pointer value.

   1. op must be of pointer type: t*

   2. the value of op is the base address of the calculation

   3. the first index in the path is treated as the index into an array
      of elements of type t located at the base address

   4. subsequent indices are interpreted according to the type t:

     - if t is a struct, the index must be a constant n and it
       picks out the nth element of the struct. [ NOTE: the offset
       within the struct of the nth element is determined by the
       sizes of the types of the previous elements ]

     - if t is an array, the index can be any operand, and its
       value determines the offset within the array.

     - if t is a named type, look up the named type

     - if t is any other type, the path is invalid

   5. if the index is valid, the remainder of the path is computed as
      in (4), but relative to the type f the sub-element picked out
      by the path so far
-}
compileGep :: Context
           -> Ty        -- type of first operand
           -> Operand   -- first operand
           -> [Operand] -- other operands (indices)
           -> CompileM [AInstruction]
compileGep = unimplemented "compileGep"

-- compiling instructions  --------------------------------------------------

{- The result of compiling a single LLVM instruction might be many HERA
   instructions.  We have not determined the structure of this code
   for you. Some of the instructions require only a couple assembly
   instructions, while others require more.  We have suggested that
   you need at least compileOperand, compileCall, and compileGep
   helpers; you may introduce more as you see fit.

   Here are a few notes:

   - Load & Store: these need to dereference the pointers.
       Optional: LLVM forbids dereferencing constants and null. You can too.

   - Alloca: needs to return a pointer into the stack, and update
             the stack pointer

   - Bitcast: does nothing interesting at the assembly level
-}
compileInstruction :: Context -> (Local, L.Instruction) -> CompileM [AInstruction]
compileInstruction = unimplemented "compileInstruction"

-- compiling terminators  ---------------------------------------------------

{- Compiling block terminators is not too difficult:

   - Ret should store the return value in FP + returnOffset (that is, 3),
     and then jump to the retLabel stored in the Context

   - Br should branch

   - Cbr branch should treat its operand as a boolean conditional
-}
compileTerminator :: Context -> Terminator -> CompileM [AInstruction]
compileTerminator = unimplemented "compileTerminator"

-- compiling blocks ---------------------------------------------------------

-- We have left this helper function here for you to complete.
compileBlock :: Context -> Block -> CompileM [AInstruction]
compileBlock = unimplemented "compileBlock"

compileLabeledBlock :: Context -> (L.Label, Block) -> CompileM [AInstruction]
compileLabeledBlock ctxt (label, block) = do
  block' <- compileBlock ctxt block
  pure (ALabel label : block')

-- compileFunDecl ------------------------------------------------------------

{- We suggest that you create a helper function that computes the
   stack layout for a given function declaration.

   - each function argument should be copied into a stack slot
   - in this (inefficient) compilation strategy, each local id
     is also stored as a stack slot.
   - see the discussion about locals

-}
stackLayout :: [Local]   -- function arguments
            -> Cfg       -- function body (from where we can count locals)
            -> CompileM (Word16, Layout)    -- (# of locals in Cfg, function stack layout)
stackLayout = unimplemented "stackLayout"

{- The code for the entry-point of a function must do several things:

   - since our simple compiler maps local %uids to stack slots,
     compiling the control-flow-graph body of a FunDecl requires us to
     compute the layout (see the discussion above of locals and layout)

   - the function code should also comply with the calling
     conventions.

   - the function entry code should allocate the stack storage needed
     to hold all of the local stack slots.

   - returning from a function is a bit involved. Instead of duplicating
     all the code at every place `ret` is used, we make a new block at
     the end of every function that does all the work. So, compiling `ret`
     means to jump to this block. You can usefully create the label for
     this block by using `newUniqueString`.

   - it is hard to know by how much to decrement the stack pointer, because
     alloca might have bumped it up. Instead, just set it to be the FP +
     3 + # of params. This is what the caller is expecting, according to
     the calling convention.
-}
compileFunDecl :: [Named Ty]    -- type declarations
               -> Named FunDecl -- function declaration
               -> CompileM [AInstruction]
compileFunDecl = unimplemented "compileFunDecl"

-- compileGlobalDecl --------------------------------------------------------

{- Compile a global value into a HERA global data declaration and map
   a global uid to its associated HERA label.
-}

compileGlobalInit :: GlobalInit -> [AInstruction]
compileGlobalInit NullGI         = [AInteger 0]
compileGlobalInit (GlobalGI gid) = [AIntegerl gid]
compileGlobalInit (IntGI i)      = [AInteger (fromIntegral i)]

-- NB: Strings in LLVM are 0-terminated; don't use LP_STRING, which
-- is length-prefixed. Encode the string directly.
compileGlobalInit (StringGI s)   = map AInteger $
                                   map (fromIntegral . ord) s ++ [0]
compileGlobalInit (ArrayGI gds)  = concatMap compileGlobalDecl gds
compileGlobalInit (StructGI gds) = concatMap compileGlobalDecl gds

compileGlobalDecl :: GlobalDecl -> [AInstruction]
compileGlobalDecl (GD { globalInit = gi }) = compileGlobalInit gi

compileNamedGlobalDecl :: Named GlobalDecl -> [AInstruction]
compileNamedGlobalDecl (N { name = n
                          , decl = d })
  = ADlabel n : compileGlobalDecl d

-- compileLLVM ---------------------------------------------------------------

compileLLVM :: L.Program -> CompileM H.AssemblyProgram
compileLLVM = unimplemented "compileLLVM"
