{- Author: Steve Zdancewic, translated to Haskell by Richard Eisenberg
   File: Lite.hs

   LLVMlite: A simplified subset of LLVM IR
-}

{-# LANGUAGE FlexibleInstances, DeriveGeneric, DeriveAnyClass #-}
{-# OPTIONS_GHC -W #-}

module LLVM.Lite where

import CS350.Panic      ( panic )
import CS350.Renderable ( Renderable(..), (++.) )

import Data.Int         ( Int16 )
import Data.List        ( intercalate )

import Text.Printf      ( printf )

import GHC.Generics     ( Generic )
import Control.DeepSeq  ( NFData  )

-- Local identifiers
type Local = String

-- Global identifiers
type Global = String

-- Named types
type TypeId = String

-- Labels
type Label = String

-- LLVM types
data Ty
  = Void               -- like Haskell's ()
  | I1                 -- booleans
  | I8                 -- 8-bit signed integers
  | I16                -- 16-bit signed integers
  | Ptr Ty             -- pointers
  | Struct [Ty]        -- structures
  | Array Int Ty       -- arrays of a declared size
  | Fun FunTy          -- functions
  | Named TypeId       -- a named type
  deriving (Show, Eq, Generic, NFData)

data FunTy = FunTy Ty [Ty]   -- functions have a return type and argument types
  deriving (Show, Eq, Generic, NFData)

-- Syntactic Values
data Operand
  = Null
  | Const Int16
  | GlobalId Global
  | LocalId Local
  deriving (Show, Eq, Generic, NFData)

-- Binary i16 Operations
data BinaryOp
  = Add
  | Sub
  | Mul
  | Shl
  | Lshr
  | Ashr
  | And
  | Or
  | Xor
  deriving (Show, Eq, Generic, NFData)

-- Comparison Operators
data Condition
  = Eq
  | Ne
  | Slt
  | Sle
  | Sgt
  | Sge
  deriving (Show, Eq, Generic, NFData)

-- Instructions
data Instruction
  = Binop BinaryOp Ty Operand Operand   -- both operands have type Ty
  | Alloca Ty                           -- returned pointer has type (Ptr Ty)
  | Load Ty Operand                     -- operand has type Ty (Ty is a pointer)
  | Store Ty Operand Operand            -- store first op into second; first has type Ty
  | Icmp Condition Ty Operand Operand   -- both operands have type Ty
  | Call Ty Operand [(Ty, Operand)]     -- Ty is the result type
  | Bitcast Ty Operand Ty               -- first Ty is the "from" type; second is the "to" Ty
  | Gep Ty Operand [Operand]            -- Ty is the type of the first operand
                                        -- it is a pointer
  deriving (Show, Eq, Generic, NFData)

-- Block terminators
data Terminator
  = Ret Ty (Maybe Operand)
  | Br Label
  | Cbr Operand Label Label
  deriving (Show, Eq, Generic, NFData)

-- Basic Blocks
data Block = Bl { instructions :: [(Local, Instruction)]
                , terminator   :: Terminator
                }
  deriving (Show, Eq, Generic, NFData)


-- Control Flow Graphs: entry and labeled blocks
data Cfg = Cfg { entry  :: Block
               , blocks :: [(Label, Block)]
               }
  deriving (Show, Eq, Generic, NFData)

-- Function Declarations
data FunDecl = FD { functionType :: FunTy
                  , functionParams :: [Local]
                  , functionCfg :: Cfg
                  }
  deriving (Show, Eq, Generic, NFData)

data Named a = N { name :: Global
                 , decl :: a }
  deriving (Show, Eq, Generic, NFData)

-- Global Data Initializers
data GlobalInit
  = NullGI
  | GlobalGI Global
  | IntGI Int16
  | StringGI String
  | ArrayGI [GlobalDecl]
  | StructGI [GlobalDecl]
  deriving (Show, Eq, Generic, NFData)

-- Global Declarations
data GlobalDecl = GD { globalType :: Ty
                     , globalInit :: GlobalInit
                     }
  deriving (Show, Eq, Generic, NFData)

-- LLVMlite Programs
data Program = Prog { typeDecls :: [Named Ty]
                    , globalDecls :: [Named GlobalDecl]
                    , functionDecls :: [Named FunDecl]
                    , externalDecls :: [Named FunTy]  -- only allow external functions
                    }
  deriving (Show, Eq, Generic, NFData)

------------------------------------------------------------------
-- Pretty-printing

-- helpful functions
betw :: Renderable a => String -> [a] -> String
betw sep elts = intercalate sep (map render elts)

instance Renderable Ty where
  render Void         = "void"
  render I1           = "i1"
  render I8           = "i8"
  render I16          = "i16"
  render (Ptr ty)     = printf "%s*" (render ty)
  render (Struct ts)  = printf "{ %s }" (betw ", " ts)
  render (Array n t)  = printf "[%s x %s]" (render n) (render t)
  render (Fun fty )   = render fty
  render (Named s)    = printf "%%%s" s

instance Renderable FunTy where
  render (FunTy res args) = printf "%s (%s)" (render res) (betw ", " args)

unPtr :: Ty -> Ty
unPtr (Ptr t) = t
unPtr ty      = error ("Pretty-printer: expected pointer type, but got " ++ render ty)

instance Renderable Operand where
  render Null           = "null"
  render (Const i)      = render i
  render (GlobalId g)   = "@" ++ g
  render (LocalId u)    = "%" ++ u

instance Renderable BinaryOp where
  render bop = case bop of
    Add  -> "add"
    Sub  -> "sub"
    Mul  -> "mul"
    Shl  -> "shl"
    Lshr -> "lshr"
    Ashr -> "ashr"
    And  -> "and"
    Or   -> "or"
    Xor  -> "xor"

instance Renderable Condition where
  render cnd = case cnd of
    Eq  -> "eq"
    Ne  -> "ne"
    Slt -> "slt"
    Sle -> "sle"
    Sgt -> "sgt"
    Sge -> "sge"

instance Renderable Instruction where
  render (Binop b t o1 o2) = printf "%s %s %s, %s"
                                    (render b) (render t) (render o1) (render o2)
  render (Alloca t)        = printf "alloca %s" (render t)
  render (Load t o)        = printf "load %s, %s %s" (render (unPtr t)) (render t) (render o)
  render (Store t os od)   = printf "store %s %s, %s %s"
                               (render t) (render os) (render (Ptr t)) (render od)
  render (Icmp c t o1 o2)  = printf "icmp %s %s %s, %s"
                               (render c) (render t) (render o1) (render o2)
  render (Call t o oa)     = printf "call %s %s(%s)"
                               (render t) (render o) (intercalate ", " (map render_op oa))
    where render_op (ty, operand) = printf "%s %s" (render ty) (render operand)
  render (Bitcast t1 o t2) = printf "bitcast %s %s to %s" (render t1) (render o) (render t2)
  render (Gep t o oi)      = printf "getelementptr %s, %s %s, %s"
                                    (render (unPtr t)) (render t) (render o)
                                    (intercalate ", " (map render_index oi))
    where render_index (Const i) = printf "i32 %s" (render i)
          render_index other     = printf "i16 %s" (render other)

instance Renderable (Local, Instruction) where
  render (_, i@(Store _ _ _))   = render i
  render (_, i@(Call Void _ _)) = render i
  render (u, i)                 = printf "%%%s = %s" u (render i)

instance Renderable Terminator where
  render (Ret _ Nothing)  = "ret void"
  render (Ret t (Just o)) = printf "ret %s %s" (render t) (render o)
  render (Br l)           = printf "br label %%%s" l
  render (Cbr o l m)      = printf "br i1 %s, label %%%s, label %%%s"
                              (render o) l m

instance Renderable Block where
  render (Bl { instructions = insns
             , terminator   = term }) =
    (intercalate "\n" (map (("  " ++) . render) insns) ++. "\n") ++
    "  " ++ render term

instance Renderable Cfg where
  render (Cfg { entry = e
              , blocks = bs }) =
    let render_block (l,b) = l ++ ":\n" ++ render b in
    render e ++ "\n" ++ intercalate "\n" (map render_block bs)

instance Renderable (Named FunDecl) where
  render (N { name = g
            , decl = FD { functionType   = FunTy result args
                        , functionParams = params
                        , functionCfg    = cfg }}) =
    let render_arg (ty,u) = printf "%s %%%s" (render ty) u in
    printf "define %s @%s(%s) {\n%s\n}\n" (render result) g
           (intercalate ", " (map render_arg (zip args params)))
           (render cfg)

instance Renderable GlobalInit where
  render NullGI         = "null"
  render (GlobalGI g)   = printf "@%s" g
  render (IntGI i)      = render i
  render (StringGI s)   = printf "c\"%s\\00\"" s
  render (ArrayGI gis)  = printf "[ %s ]" (betw ", " gis)
  render (StructGI gis) = printf "{ %s }" (betw ", " gis)

instance Renderable GlobalDecl where
  render (GD { globalType = t
             , globalInit = gi })
    = printf "%s %s" (render t) (render gi)

instance Renderable (Named GlobalDecl) where
  render (N { name = g
            , decl = gd }) =
    printf "@%s = global %s" g (render gd)

instance Renderable (Named Ty) where
  render (N { name = n
            , decl = ty })
    = printf "%%%s = type %s" n (render ty)

instance Renderable (Named FunTy) where
  render (N { name = n
            , decl = FunTy ret args })
    = printf "declare %s @%s(%s)" (render ret) n (intercalate "," (map render args))

instance Renderable Program where
  render (Prog { typeDecls     = ts
               , globalDecls   = gs
               , functionDecls = fs
               , externalDecls = xs})
    = concat [ betw "\n" xs ++. "\n\n"
             , betw "\n" ts ++. "\n\n"
             , betw "\n" gs ++. "\n\n"
             , betw "\n" fs ++. "\n\n"
             ]

------------------------------------------------------------
-- Utility definitions

-- Useful to have this during testing
instance Ord Block where
  compare bl1 bl2 = (bl1 { instructions = map del_dummy (instructions bl1) })
                    `compare`
                    (bl2 { instructions = map del_dummy (instructions bl2) })
    where
      del_dummy (u,i) = case i of
        Store _ _ _   -> ("", i)
        Call Void _ _ -> ("", i)
        _             -> (u, i)

-- useful way of building a function declaration
define :: Ty -> Global -> [(Ty, Local)] -> Cfg -> Named FunDecl
define result gid args cfg
  = N { name = gid, decl = FD { functionType   = FunTy result arg_tys
                              , functionParams = params
                              , functionCfg    = cfg }}
  where
    (arg_tys, params) = unzip args

-- Make a CFG by ignoring the first label
makeCfg :: [(Label, Block)] -> Cfg
makeCfg [] = panic "makeCfg: no blocks!"
makeCfg ((_, b1) : lbs) = Cfg { entry  = b1
                              , blocks = lbs }

-- convert unlabeled blocks into labeled ones
mkEntry :: [(Local, Instruction)] -> Terminator -> (Label, Block)
mkEntry insns term = ("", Bl { instructions = insns, terminator = term })

mkLabeledBlock :: Label -> [(Local, Instruction)] -> Terminator -> (Label, Block)
mkLabeledBlock lbl insns term = (lbl, Bl { instructions = insns, terminator = term })

-- convenience functions for building terminators
retVoid :: Terminator
retVoid = Ret Void Nothing

ret :: Ty -> Operand -> Terminator
ret t op = Ret t (Just op)
