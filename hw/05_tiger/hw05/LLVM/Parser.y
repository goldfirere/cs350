-- -*- haskell -*-
-- The line above tells emacs to use Haskell-style syntax highlighting

{
{- Author: Steve Zdancewic, translated to Haskell (happy) by Richard Eisenberg
   File: Parser.y

   An LLVMlite Parser.
-}

module LLVM.Parser ( parse ) where

import Prelude hiding ( EQ )
import LLVM.Lite  as L
import LLVM.Token as T

import Data.Int  ( Int16 )
import Data.List ( intercalate )

import CS350.CompileM   ( CompileM, compileError )
import CS350.Unique     ( newUnique, uniqueString )
import CS350.Renderable ( render )

}

%expect 0                  -- expect 0 conflicts in this grammar
%name parse                -- name the main parser "parse"
%tokentype { Token }       -- use the Token type exported by the lexer
%monad     { CompileM } { (>>=) } { pure }  -- the CompileM allows for better error messages
%error     { parseError }  -- Function to call in case of trouble.

-- declare tokens; this tells happy how to spell the different tokens in the
-- grammar that follows
%token
  STAR     { STAR      }     -- *
  COMMA    { COMMA     }     -- ,
  COLON    { COLON     }     -- :
  EQUALS   { EQUALS    }     -- =
  LPAREN   { LPAREN    }     -- (
  RPAREN   { RPAREN    }     -- )
  LBRACE   { LBRACE    }     -- {
  RBRACE   { RBRACE    }     -- }
  LBRACKET { LBRACKET  }     -- [
  RBRACKET { RBRACKET  }     -- ]
  EOF      { EOF       }

  CROSS    { CROSS     }     -- x
  I1       { T.I1      }     -- i1
  I8       { T.I8      }     -- i8
  I16      { T.I16     }     -- i16
  I32      { I32       }     -- i32
  I64      { I64       }     -- i64
  TO       { TO        }     -- to
  BR       { BR        }     -- br
  EQ       { EQ        }     -- eq
  NE       { NE        }     -- ne
  OR       { OR        }     -- or
  AND      { AND       }     -- and
  ADD      { ADD       }     -- add
  SUB      { SUB       }     -- sub
  MUL      { MUL       }     -- mul
  XOR      { XOR       }     -- xor
  SLT      { SLT       }     -- slt
  SLE      { SLE       }     -- sle
  SGT      { SGT       }     -- sgt
  SGE      { SGE       }     -- sge
  SHL      { SHL       }     -- shl
  RET      { RET       }     -- ret
  TYPE     { TYPE      }     -- type
  NULL     { NULL      }     -- null
  LSHR     { LSHR      }     -- lshr
  ASHR     { ASHR      }     -- ashr
  CALL     { CALL      }     -- call
  ICMP     { ICMP      }     -- icmp
  VOID     { VOID      }     -- void
  LOAD     { LOAD      }     -- load
  STORE    { STORE     }     -- store
  LABEL    { LABEL     }     -- label
  ENTRY    { ENTRY     }     -- entry
  GLOBAL   { GLOBAL    }     -- global
  DEFINE   { DEFINE    }     -- define
  DECLARE  { DECLARE   }     -- declare
  ALLOCA   { ALLOCA    }     -- alloca
  BITCAST  { BITCAST   }     -- bitcast
  GEP      { GEP       }     -- getelementptr

  INT      { INT $$    }     -- int16 values
  LBL      { LBL $$    }     -- labels
  GID      { GID $$    }     -- globals
  UID      { UID $$    }     -- locals
  STRING   { STRING $$ }    -- string literals

-- this ends the definition of tokens, and starts the parsing rules
%%

prog :: { Program }
  : decls EOF
    { $1 }

decls :: { Program }
  : decls_rev
    { Prog { typeDecls = reverse (typeDecls $1)
           , globalDecls = reverse (globalDecls $1)
           , functionDecls = reverse (functionDecls $1)
           , externalDecls = reverse (externalDecls $1)
           }
    }

decls_rev :: { Program }
  : {- empty -}
    { Prog { typeDecls = []
           , globalDecls = []
           , functionDecls = []
           , externalDecls = []
           }
    }

  | decls_rev fdecl
    { $1 { functionDecls = $2 : functionDecls $1 }  }
  | decls_rev gdecl
    { $1 { globalDecls = $2 : globalDecls $1 }  }
  | decls_rev tdecl
    { $1 { typeDecls = $2 : typeDecls $1 } }
  | decls_rev extdecl
    { $1 { externalDecls = $2 : externalDecls $1 } }

extdecl :: { Named FunTy }
  : DECLARE ty GID LPAREN ty_list RPAREN
    { N { name = $3, decl = FunTy $2 $5 } }

fdecl :: { Named FunDecl }
  : DEFINE ty GID LPAREN arg_list RPAREN
      LBRACE entry_block block_list RBRACE
    { N { name = $3
        , decl = FD { functionType = FunTy $2 (map fst $5)
                    , functionParams = map snd $5
                    , functionCfg = Cfg { entry = $8, blocks = $9 } } }
    }

gdecl :: { Named GlobalDecl }
  : GID EQUALS GLOBAL ty ginit
    { N { name = $1
        , decl = GD { globalType = $4
                    , globalInit = $5 } }
    }

tdecl :: { Named Ty }
  : UID EQUALS TYPE ty
    { N { name = $1
        , decl = $4 }
    }

nonptr_ty :: { Ty }
  : VOID { Void }
  | I1 { L.I1 }
  | I8 { L.I8 }
  | I16 { L.I16 }
  | I32 { L.I16 }  -- no higher-bit support here
  | I64 { L.I16 }
  | LBRACE ty_list RBRACE
    { Struct $2 }
  | LBRACKET INT CROSS ty RBRACKET
    { Array (fromIntegral $2) $4 }
  | ty LPAREN ty_list RPAREN
    { Fun (FunTy $1 $3) }
  | UID
    { Named $1 }

ty :: { Ty }
  : ty STAR
    { Ptr $1 }
  | nonptr_ty
    { $1 }

ty_list_rev :: { [Ty] }
  : ty
    { [$1] }
  | ty_list_rev COMMA ty
    { $3:$1 }

ty_list :: { [Ty] }
  : ty_list_rev
    { reverse $1 }

arg :: { (Ty, Local) }
  : ty UID
    { ($1,$2) }

arg_list_rev :: { [(Ty, Local)] }
  : {- empty -}
    { [] }
  | arg
    { [$1] }
  | arg_list_rev COMMA arg
    { $3:$1 }

arg_list :: { [(Ty, Local)] }
  : arg_list_rev
    { reverse $1 }

operand :: { Operand }
  : NULL
    { Null }
  | INT
    { Const $1 }
  | GID
    { GlobalId $1 }
  | UID
    { LocalId $1 }

ty_operand_list_rev :: { [(Ty, Operand)] }
  : {- empty -}
    { [] }
  | ty operand
    { [($1,$2)] }
  | ty_operand_list_rev COMMA ty operand
    { ($3,$4):$1 }

ty_operand_list :: { [(Ty, Operand)] }
  : ty_operand_list_rev
    { reverse $1 }

i_operand_list_rev :: { [Operand] }
  : {- empty -}
    { [] }
  | I16 operand
      { [$2] }
  | I32 operand
    { [$2] }
  | I64 operand
    { [$2] }
  | i_operand_list_rev COMMA I16 operand
    { $4:$1 }
  | i_operand_list_rev COMMA I32 operand
    { $4:$1 }
  | i_operand_list_rev COMMA I64 operand
    { $4:$1 }

i_operand_list :: { [Operand] }
  : i_operand_list_rev
    { reverse $1 }

terminator :: { Terminator }
  : RET ty operand
    { Ret $2 (Just $3) }
  | RET ty
    { Ret $2 Nothing }
  | BR LABEL UID
    { Br $3 }
  | BR I1 operand COMMA LABEL UID COMMA LABEL UID
    { Cbr $3 $6 $9 }

block :: { Block }
  : insn_list terminator
    { Bl { instructions = $1, terminator = $2 } }

block_list_rev :: { [(Label, Block)] }
  : {- empty -}
    { [] }
  | block_list_rev LBL COLON block
    { ($2,$4) : $1 }

block_list :: { [(Label, Block)] }
  : block_list_rev
    { reverse $1 }

entry_block :: { Block }
  : ENTRY COLON block
    { $3 }
  | block
    { $1 }

bop :: { BinaryOp }
  : OR { Or }
  | ADD { Add }
  | SUB { Sub }
  | MUL { Mul }
  | SHL { Shl }
  | XOR { Xor }
  | AND { And }
  | LSHR { Lshr }
  | ASHR { Ashr }

cnd :: { Condition }
  : EQ { Eq }
  | NE { Ne }
  | SLT { Slt }
  | SLE { Sle }
  | SGT { Sgt }
  | SGE { Sge }

insn :: { (Local, Instruction) }
  : UID EQUALS bop ty operand COMMA operand
    { ($1, Binop $3 $4 $5 $7) }
  | UID EQUALS ALLOCA ty
    { ($1, Alloca $4) }
  | UID EQUALS LOAD ty COMMA ty operand
    { ($1, Load $6 $7) }
  | STORE ty operand COMMA ty operand
    {% do { store <- newUnique "store"
          ; pure (uniqueString store, Store $2 $3 $6) } }
  | UID EQUALS ICMP cnd ty operand COMMA operand
    { ($1, Icmp $4 $5 $6 $8) }
  | CALL ty operand LPAREN ty_operand_list RPAREN
    {% do { call <- newUnique "call"
          ; pure (uniqueString call, Call $2 $3 $5) } }
  | UID EQUALS CALL ty operand LPAREN ty_operand_list RPAREN
    { ($1, Call $4 $5 $7) }
  | UID EQUALS BITCAST ty operand TO ty
    { ($1, Bitcast $4 $5 $7) }
  | UID EQUALS GEP ty COMMA ty operand COMMA i_operand_list
    { ($1, Gep $6 $7 $9) }

insn_list :: { [(Local, Instruction)] }
  : {- empty -}
    { [] }
  | insn insn_list
    { $1:$2 }

gdecl_list_rev :: { [GlobalDecl] }
  : {- empty -}
    { [] }
  | ty ginit
    { [GD { globalType = $1
          , globalInit = $2 }]
    }

  | gdecl_list_rev COMMA ty ginit
    { (GD { globalType = $3
          , globalInit = $4 }) : $1 }

gdecl_list :: { [GlobalDecl] }
  : gdecl_list_rev
    { reverse $1 }

ginit :: { GlobalInit }
  : NULL
    { NullGI }
  | GID
    { GlobalGI $1 }
  | INT
    { IntGI $1 }
  | STRING
    { StringGI $1 }
  | LBRACKET gdecl_list RBRACKET
    { ArrayGI $2 }
  | LBRACE gdecl_list RBRACE
    { StructGI $2 }

{

parseError :: [Token] -> CompileM a
parseError toks = compileError ("Parse error at the following tokens:\n" ++
                                "  " ++ intercalate " " (map render (take 5 toks)) ++ " ...")

}
