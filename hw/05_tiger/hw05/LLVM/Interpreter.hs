{- Author: Steve Zdancewic, translated to Haskell by Richard Eisenberg
   File: Interpreter.hs

   An LLVMlite interpreter.

   NB: This was roughly translated. Much of the file still uses OCaml
   idioms, etc., instead of Haskell.
-}

{-# LANGUAGE LambdaCase, FlexibleInstances, TupleSections #-}
{-# OPTIONS_GHC -W #-}

module LLVM.Interpreter where

import LLVM.Lite
import Data.Int ( Int16 )
import Data.Word ( Word16 )
import Data.Bits ( (.&.), (.|.), xor, shiftL, shiftR )
import CS350.CompileM ( CompileM, compileError )
import CS350.Renderable ( Renderable(..) )
import CS350.Unique ( newUnique, Unique )
import Data.List ( intercalate )
import Text.Printf ( printf )
import Control.Monad.IO.Class  ( liftIO )
import Control.Monad ( zipWithM, when )
import Data.Foldable ( foldrM )

type Mid = Unique                  {- memory block id -}
type Fid = Unique                  {- stack frame id -}
type Idx = Int                  {- index -}

{- Memory block identifier -}
data Bid = GlobId Global
         | HeapId Mid
         | StckId Fid
         | NullId
  deriving (Eq, Ord, Show)

{- Pointers are tagged with a description of the block they reference
   offsets are represented as paths into memory values -}
type Ptr = (Ty, Bid, [Idx])

{- "Simple" or stack values  -}
data Sval
  = VUndef
  | VInt Int16
  | VPtr Ptr
  deriving (Show)

{- Memory values -}
data Mtree = MWord Sval
           | MStr String
           | MNode Mval
  deriving (Show)

type Mval = [Mtree]

{- Locals -}
type Locals = Local -> CompileM Sval

{- The memory state -}
data Config = Config
  { globals :: [(Global, Mval)]
  , heap    :: [(Mid, Mval)]
  , stack   :: [(Fid, Mval)]
  }
  deriving (Show)

{- Create memory value for global declaration -}
mval_of_gdecl :: GlobalDecl -> CompileM Mval
mval_of_gdecl gd =
  let mtree_of_gdecl :: GlobalDecl -> CompileM Mtree
      mtree_of_gdecl = \case
        GD (Ptr ty) NullGI       -> pure $ MWord (VPtr (ty, NullId, [0]))
        GD (Ptr ty) (GlobalGI g) -> pure $ MWord (VPtr (ty, GlobId g, [0]))
        GD _ (IntGI i )    -> pure $ MWord (VInt i)
        GD _ (StringGI s)  -> pure $ MStr s
        GD _ (ArrayGI gs)  -> MNode <$> mapM mtree_of_gdecl gs
        GD _ (StructGI gs) -> MNode <$> mapM mtree_of_gdecl gs
        _ -> compileError "mval_of_gdecl: invalid indirection"
  in (:[]) <$> mtree_of_gdecl gd

{- Create fully undefined memory value for a type -}
mval_of_ty :: (TypeId -> Ty) -> Ty -> CompileM Mval
mval_of_ty nt t =
  let mtree_of_ty :: Ty -> CompileM Mtree
      mtree_of_ty = \case
        I1 -> pure $ MWord VUndef
        I8 -> pure $ MWord VUndef
        I16 -> pure $ MWord VUndef
        Ptr _ -> pure $ MWord VUndef
        Array n I8    -> pure $ MStr (replicate n '\x00')
        Array n _     -> pure $ MNode (replicate n (MWord VUndef))
        Struct ts     -> MNode <$> mapM mtree_of_ty ts
        Fun _         -> compileError "mval_of_ty: mval for bad type"
        Void          -> compileError "mval_of_ty: mval for bad type"
        Named id      -> mtree_of_ty (nt id)
  in (:[]) <$> mtree_of_ty t

{- Printing machine states -}
mapcat s f l = intercalate s (map f l)
prefix p f a = p ++ f a

instance Renderable Bid where
  render = \case
    GlobId gid -> "@" ++ gid
    HeapId mid -> "M" ++ (render mid)
    StckId fid -> "S" ++ (render fid)
    NullId -> "null"

instance Renderable Ptr where
  render (t, b, i) =
    printf "%s %s %s" (render t) (render b) (mapcat ", " render i)

instance Renderable Sval where
  render sv = case sv of
    VUndef -> "undef"
    VInt x -> render x
    VPtr p -> render p

instance Renderable Mval where
  render mv =
    "[" ++ (mapcat ", " render mv) ++ "]"

instance Renderable Mtree where
  render = \case
    MWord sv -> render sv
    MStr s -> "\"" ++ s ++ "\""
    MNode m -> render m


{- Arithmetic operations are all signed 16bit 2s complement   -}
interp_bop :: BinaryOp -> Sval -> Sval -> CompileM Sval
interp_bop b v1 v2 = do
  (i, j) <- case (v1, v2) of
        (VInt i, VInt j) -> pure (i, j)
        _ -> compileError "interp_bop"
  let f :: Int16 -> Int16 -> Int16
      f = case b of
           Add -> (+)
           Sub -> (-)
           Mul -> (*)
           And -> (.&.)
           Or -> (.|.)
           Xor -> xor
           Shl -> \ x y -> x `shiftL` fromIntegral y
           Lshr -> \ x y -> fromIntegral ((fromIntegral x :: Word16) `shiftR` fromIntegral y) :: Int16
           Ashr -> \ x y -> x `shiftR` fromIntegral y
  pure (VInt (f i j))

interp_cnd :: Condition -> Sval -> Sval -> CompileM Sval
interp_cnd c v1 v2 = do
  let f :: Ord a => a -> a -> Bool
      f = case c of
           Eq -> (==)
           Ne -> (/=)
           Slt -> (<)
           Sle -> (<=)
           Sgt -> (>)
           Sge -> (>=)
  case (v1, v2, c) of
    (VPtr (_,b1,i1), VPtr (_,b2,i2), Eq) -> pure $ VInt (if f (b1,i1) (b2,i2) then 1 else 0)
    (VPtr (_,b1,i1), VPtr (_,b2,i2), Ne) -> pure $ VInt (if f (b1,i1) (b2,i2) then 1 else 0)
    (VInt i, VInt j, _) -> pure $ VInt (if f i j then 1 else 0)
    _ -> compileError "interp_cnd"

interp_i1 :: Sval -> CompileM Bool
interp_i1 = \case
  VInt 0 -> pure False
  VInt 1 -> pure True
  _ -> compileError "interp_i1"

interp_operand :: (TypeId -> CompileM Ty) -> Locals -> Ty -> Operand -> CompileM Sval
interp_operand nt locs ty o =
  case (ty, o) of
    (I16, Const i)  -> pure $ VInt i
    (I1, Const i)   -> pure $ VInt (fromIntegral i)
    (Ptr ty, Null)  -> pure $ VPtr (ty, NullId, [0])
    (Ptr ty, GlobalId g) -> pure $ VPtr (ty, GlobId g, [0])
    (_, LocalId u)       -> locs u
    (Named id, o)   -> do t <- nt id
                          interp_operand nt locs t o
    _               -> compileError $ "interp_operand: malformed operand " ++
                                      render o ++ ":" ++ render ty


{- Some utility functions -}
update :: Locals -> Local -> Sval -> Locals
update f k v = \ k' -> if k == k' then pure v else f k'

is_prefix :: Eq a => [a] -> [a] -> Bool
is_prefix l m =
  case (l, m) of
    ([], _) -> True
    (_, []) -> False
    (a:l, b:m) -> a == b && is_prefix l m

replace_assoc :: Eq a => [(a,b)] -> a -> b -> [(a,b)]
replace_assoc l a b =
  let loop acc = \case
          [] -> reverse $ (a,b):acc
          (a',_):l' | a == a' -> reverse acc ++ (a,b): l'
          e:l' -> loop (e:acc) l'
  in
  loop [] l

replace_nth :: [a] -> Int -> a -> CompileM [a]
replace_nth l n a =
  let loop acc n  = \case
            [] -> compileError "element not found"
            a':l' -> if n == 0 then pure $ reverse acc ++ (a:l')
                               else loop (a':acc) (n-1) l'
  in
  loop [] n l

nlookup :: String -> [Named a] -> Maybe a
nlookup _ [] = Nothing
nlookup n (N n' x : rest) | n == n'   = Just x
                          | otherwise = nlookup n rest

{- Memory access -}
load_idxs :: Mval -> [Idx] -> CompileM Mtree
load_idxs m idxs =
  case idxs of
    [] -> pure $ MNode m
    i:idxs' ->
     let len = length m in
     if len <= i || i < 0 then compileError "out-of-bounds index dereference" else
     case (idxs', m !! i) of
       ([],  mt)       -> pure mt
       ([0], MStr s)   -> pure $ MStr s  {- [n x i8]* %p and gep [n x i8]* %p, 0, 0 alias -}
       (_,   MWord _)  -> compileError "load_idxs: attempted to index into word"
       (_,   MStr _)   -> compileError "load_idxs: attempted to index into string"
       (_,   MNode m') -> load_idxs m' idxs'

store_idxs :: Mval -> [Idx] -> Mtree -> CompileM Mval
store_idxs m idxs mt =
  case idxs of
    [] -> pure [mt]
    i:idxs' ->
     let len = length m in
     if len <= i || i < 0 then compileError "out-of-bounds index dereference" else
     case (idxs', m !! i) of
       ([], _)        -> replace_nth m i mt
       (_,  MWord _)  -> compileError "store_idxs: attempted to index into word"
       (_,  MStr _)   -> compileError "store_idxs: attempted to index into string"
       (_,  MNode m') -> do mv <- store_idxs m' idxs' mt
                            replace_nth m i (MNode mv)

load_bid :: Config -> Bid -> CompileM Mval
load_bid c bid =
  case bid of
    NullId -> compileError "null pointer dereference"
    HeapId mid ->
     case lookup mid (heap c) of
       Just x -> pure x
       Nothing -> compileError "use after free"
    GlobId gid ->
     case lookup gid (globals c) of
       Just x -> pure x
       Nothing -> compileError "Load: bogus gid"
    StckId fid ->
     case lookup fid (stack c) of
       Just x -> pure x
       Nothing -> compileError "use after free"

load_ptr :: Config -> Ptr -> CompileM Mtree
load_ptr c (_, bid, idxs) = do
  mv <- load_bid c bid
  load_idxs mv idxs

store_ptr :: Config -> Ptr -> Mtree -> CompileM Config
store_ptr c (_, bid, idxs) mt = do
  mval <- load_bid c bid
  case bid of
    NullId -> compileError "null pointer dereference"
    HeapId mid -> do
     mval' <- store_idxs mval idxs mt
     let h = replace_assoc (heap c) mid mval'
     pure $ c { heap = h }
    GlobId gid -> do
     mval' <- store_idxs mval idxs mt
     let g = replace_assoc (globals c) gid mval'
     pure $ c { globals = g }
    StckId fid -> do
     frame' <- store_idxs mval idxs mt
     let s = replace_assoc (stack c) fid frame'
     pure $ c { stack = s }


{- Tag and GEP implementation -}
effective_tag :: (TypeId -> CompileM Ty) -> Ptr -> CompileM Ty
effective_tag nt (tag, _, idxs) =
  let loop tag idxs =
          case (tag, idxs) of
            (t, [])                  -> pure t
            (Struct ts,    i:idxs')  -> if length ts <= i
                                        then compileError "effective_tag: index oob of struct"
                                        else loop (ts !! i) idxs'
            (Array _ t, _:idxs')     -> loop t idxs' {- Don't check if OOB! -}
            (Named id,    _)         -> do t' <- nt id
                                           loop t' idxs
            (_,            _:_)      -> compileError "effective_tag: index into non-aggregate"
  in
  case idxs of
    [] -> compileError "effective_tag: invalid pointer missing first index"
    _:idxs' -> loop tag idxs'

gep_idxs :: [Idx] -> [Idx] -> CompileM [Idx]
gep_idxs p idxs =
  case (p, idxs) of
    ([], _) -> compileError "gep_idxs: invalid indices"
    (_, []) -> compileError "gep_idxs: invalid indices"
    ([i], j:idxs') -> pure $ i+j:idxs'
    (i:p', _) -> (i:) <$> gep_idxs p' idxs

legal_gep :: (TypeId -> CompileM Ty) -> Ty -> Ty -> CompileM Bool
legal_gep nt sty tag =
  let ptrtoi16 :: Ty -> CompileM Ty
      ptrtoi16 = \case
          Ptr _ -> pure $ Ptr I16
          Struct ts -> Struct <$> mapM ptrtoi16 ts
          Array n t -> Array n <$> ptrtoi16 t
          Named id -> do t' <- nt id
                         ptrtoi16 t'
          t -> pure t
  in
  let flatten_ty :: Ty -> [Ty]
      flatten_ty = \case
         Struct ts -> concatMap flatten_ty ts
         t -> [t]
  in do

  needle <- ptrtoi16 sty
  haystack <- ptrtoi16 tag
  pure $ is_prefix (flatten_ty needle) (flatten_ty haystack)

gep_ptr :: (TypeId -> CompileM Ty) -> Ty -> Ptr -> [Idx] -> CompileM Sval
gep_ptr nt ot p idxs' = do
  tag <- effective_tag nt p
  is_legal <- legal_gep nt ot tag
  if not is_legal then pure VUndef else
    case p of
      (_, NullId, _) -> pure VUndef
      (t, bid, idxs) -> do
        idxs2 <- gep_idxs idxs idxs'
        pure $ VPtr (t, bid, idxs2)


{- LLVMlite reference interpreter -}
interp_prog :: Program -> [String] -> CompileM Sval
interp_prog (Prog { typeDecls = tdecls
                  , globalDecls = gdecls
                  , functionDecls = fdecls }) args = do

  gs <- mapM (\ (N g gd) -> (g,) <$> mval_of_gdecl gd) gdecls

  let nt :: TypeId -> CompileM Ty
      nt id = case nlookup id tdecls of
                Nothing -> compileError $ "interp_prog: undefined named type " ++ id
                Just t  -> pure t

  let interp_op = interp_operand nt

  {- Global identifiers that will be interpreted as external functions -}
  let runtime_fns = [ "ll_puts", "ll_strcat", "ll_ltoa" ]


  {- External function implementation -}
  let runtime_call :: Ty -> Global -> [Sval] -> Config -> CompileM (Config, Sval)
      runtime_call t fn args c =
        let load_strptr p = do
              tree <- load_ptr c p
              case tree of
                MStr s -> pure s
                _ -> compileError "runtime_call: non string-ptr arg"
        in
        case (t, fn, args) of
          (Void, "ll_puts", [VPtr p]) -> do
           s <- load_strptr p
           liftIO $ putStrLn s
           pure (c, VUndef)
          (Ptr t, "ll_strcat", [VPtr ps1, VPtr ps2]) -> do
           s1 <- load_strptr ps1
           s2 <- load_strptr ps2
           mid <- newUnique "mid"
           let mv = [MStr (s1 ++ s2)]
           let h = (mid, mv):(heap c)
           pure (c { heap = h }, VPtr (t, HeapId mid, [0]))
          (I16, "ll_ltoa", [VInt i, VPtr _]) -> do
           mid <- newUnique "mid"
           let mv = [MStr (show i)]
           let h = (mid, mv):(heap c)
           pure (c { heap = h }, VPtr (t, HeapId mid, [0]))
          _ -> compileError  $ "runtime_call: invalid call to " ++ fn


  {- Interprety the body of a function -}
  let interp_cfg :: (Block, [(Label, Block)]) -> Locals -> Config -> CompileM (Config, Sval)
      interp_cfg (k, blocks) locs c =
        case (instructions k, terminator k) of

        ((u, Binop b t o1 o2):insns, _) -> do
         v1 <- interp_op locs t o1
         v2 <- interp_op locs t o2
         vr <- interp_bop b v1 v2
         let locs' = update locs u vr
         interp_cfg (k { instructions = insns }, blocks) locs' c

        ((u, Icmp cnd t o1 o2):insns, _) -> do
         v1 <- interp_op locs t o1
         v2 <- interp_op locs t o2
         vr <- interp_cnd cnd v1 v2
         let locs' = update locs u vr
         interp_cfg (k { instructions = insns }, blocks) locs' c

        ((u, Alloca ty):insns, _) ->
         case stack c of
           [] -> compileError "stack empty"
           (fid,frame):stack' ->
            let ptr = VPtr (ty, StckId fid, [length frame]) in
            let s = (fid, frame ++ [MWord VUndef]):stack' in
            let locs' = update locs u ptr in
            interp_cfg (k { instructions = insns }, blocks) locs' (c { stack = s })


        ((u, Load (Ptr t) o):insns, _) -> do
         result <- interp_op locs (Ptr t) o
         mt <- case result of
           VPtr p -> do
              ty <- effective_tag nt p
              if ty /= t
                then compileError "incompatible tag dereference"
                else load_ptr c p
           VUndef -> compileError "undefined pointer dereference"
           VInt _ -> compileError "non-ptr arg for load"

         v <- case mt of
           MWord VUndef -> compileError "uninitialized memory load"
           MWord v -> pure v
           _ -> compileError "load: returned aggregate"

         let locs' = update locs u v
         interp_cfg (k { instructions = insns}, blocks) locs' c

        ((_, Store t os od):insns, _) -> do
         vs <- interp_op locs t os
         vd <- interp_op locs (Ptr t) od
         c' <- case vd of
           VPtr p -> do
             ty <- effective_tag nt p
             if ty /= t
              then compileError "imcompatible tag dereference"
              else store_ptr c p (MWord vs)
           VUndef -> compileError "undefined pointer dereference"
           VInt _ -> compileError "non-vptr arg for load"

         interp_cfg (k { instructions = insns}, blocks) locs c'

        ((u, Call t ofn ato):insns, _) -> do
         let (ats, aos) = unzip ato
         let ft = Ptr (Fun (FunTy t ats))
         result <- interp_op locs ft ofn
         g <- case result of
           VPtr (_, GlobId g, [0]) -> pure g
           _ -> compileError "bad call arg"

         args <- zipWithM (interp_op locs) ats aos
         (c', r) <- interp_call t g args c
         let locs' = update locs u r
         interp_cfg (k {instructions = insns}, blocks) locs' c'

        ((u, Bitcast t1 o _):insns, _) -> do
         v <- interp_op locs t1 o
         let locs' = update locs u v
         interp_cfg (k {instructions = insns}, blocks) locs' c

        ((u, Gep (Ptr t) o os):insns, _) -> do
         let idx_of_sval :: Sval -> CompileM Idx
             idx_of_sval = \case
               VInt i -> pure (fromIntegral i)
               _ -> compileError "idx_of_sval: non-integer value"

         vs <- mapM (interp_op locs I16) os
         idxs' <- mapM idx_of_sval vs
         result <- interp_op locs (Ptr t) o
         v' <- case result of
           VPtr p -> gep_ptr nt t p idxs'
           VUndef -> pure VUndef
           VInt _ -> compileError "non-ptr arg for gep"

         let locs' = update locs u v'
         interp_cfg (k {instructions = insns}, blocks) locs' c

        ([], (Ret _ Nothing)) ->
         pure (c {stack = tail (stack c)}, VUndef)

        ([], (Ret t (Just o))) -> do
          result <- interp_op locs t o
          pure (c {stack = tail (stack c)}, result)

        ([], (Br l)) -> do
         k' <- case lookup l blocks of
                 Just k' -> pure k'
                 Nothing -> compileError "label not found"
         interp_cfg (k', blocks) locs c

        ([], (Cbr o l1 l2)) -> do
         v <- interp_op locs I1 o
         test <- interp_i1 v
         let l' = if test then l1 else l2
         k' <- case lookup l' blocks of
                 Just k' -> pure k'
                 Nothing -> compileError "label not found"
         interp_cfg (k', blocks) locs c

        ((u,i):_, _) -> compileError $ "interp_cfg: invalid instruction \""
                                    ++ render i ++ "\" at %" ++ u


      interp_call :: Ty -> Global -> [Sval] -> Config -> CompileM (Config, Sval)
      interp_call ty fn args c =
        if fn `elem` runtime_fns
        then runtime_call ty fn args c
        else do
        FD { functionParams = f_param
           , functionCfg = Cfg e lbs } <- case nlookup fn fdecls of
                                        Just fd -> pure fd
                                        Nothing -> compileError $
                                                   "interp_call: undefined function " ++ fn

        when (length f_param /= length args) $
          compileError $ "interp_call: wrong no. arguments for " ++ fn
        let init_locs l = compileError $ "interp_call: undefined local " ++ l
        let locs = foldl (\a (b, c) -> update a b c) init_locs (zip f_param args)
        tmp <- newUnique "tmp"
        let s = (tmp, []):(stack c)
        interp_cfg (e,lbs) locs (c { stack = s })

  let mkarg a (p,h) = do
        id <- newUnique "id"
        pure $ (VPtr (I8, HeapId id, [0]):p, (id, [MStr a]):h)

  (ptrs, h0) <- foldrM mkarg ([],[]) ("LLINTERP":args)

  let narg = length args + 1
  let argc = VInt (fromIntegral narg)
  aid <- newUnique "aid"
  let argv = VPtr (Array narg (Ptr I8), HeapId aid, [0, 0])
  let amval = map MWord ptrs
  let h1 = (aid, [MNode amval]):h0

  (_, r) <- interp_call I16 "main" [argc, argv] (Config { globals = gs
                                                        , heap = h1
                                                        , stack=[] })
  pure r
