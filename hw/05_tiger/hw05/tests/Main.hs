{- Author: Richard Eisenberg
   File: Main.hs

   Test runner for HW05
-}

{-# OPTIONS_GHC -W #-}

module Main where

import RunTest    ( TestCase(..), updatePath, runTest )

import Control.Monad     ( forM )
import System.Exit       ( exitSuccess, exitFailure )
import System.FilePath   ( (</>) )

successCases :: [(FilePath, Maybe (String, Int))]
successCases =
  [ ("s1",Just ("",0))
  , ("s2",Just ("",1))
  , ("s3",Just ("",0))
  , ("s4",Just ("",5))
  , ("s5",Just ("",0))
  , ("s6",Just ("",0))
  , ("s7",Just ("",3))
  , ("s8",Just ("",7))
  , ("s9",Just ("",6))
  , ("s10",Just ("",1))
  , ("s11",Just ("",0))
  , ("s12",Just ("",1))
  , ("s13",Just ("",1))
  , ("s14",Just ("",251))
  , ("s15",Just ("",244))
  , ("s16",Just ("",0))
  , ("s17",Just ("",3))
  , ("s18",Just ("",1))
  , ("s19",Just ("",1))
  , ("s20",Just ("",1))
  , ("s21",Just ("",0))
  , ("s22",Just ("",0))
  , ("s23",Just ("hello, world",0))
  , ("s24",Just ("",65))
  , ("s25",Just ("",5))
  , ("s26",Just ("hello",0))
  , ("s27",Just ("",4))
  , ("s28",Just ("2",0))
  , ("s29",Just ("",0))
  , ("s30",Just ("",5))
  , ("s31",Just ("hello",0))
  , ("s32",Just ("5",0))
  , ("s33",Just ("",9))
  , ("s34",Just ("hello10",0))
  , ("s35",Just ("3",0))
  , ("s36",Just ("5",0))
  , ("s37",Just ("hi\n",0))
  , ("s38",Just ("hello\n",0))
  , ("s39",Just ("",6))
  , ("s40",Just ("",6))
  , ("s41",Just ("",5))
  , ("s42",Just ("17",5))
  , ("s43",Just ("",120))
  , ("s44",Just ("910",0))
  , ("s45",Just ("",24))
  , ("s46",Just ("",11))
  , ("s47",Just ("3365",0))
  , ("s48",Just ("",45))
  , ("s49",Just ("",55))
  , ("s50",Just ("10987654321",0))
  , ("s51",Just ("10",0))
  , ("s52",Just ("10",0))
  , ("s53",Just ("4",0))
  , ("s54",Just ("012345678910",0))
  , ("s55",Just ("",15))
  ]

failureCases :: [(FilePath, Maybe (String, Int))]
failureCases =
  [ ("f1",Nothing)
  , ("f2",Nothing)
  , ("f3",Nothing)
  , ("f4",Nothing)
  , ("f5",Nothing)
  , ("f6",Nothing)
  , ("f7",Nothing)
  , ("f8",Nothing)
  , ("f9",Nothing)
  , ("f10",Nothing)
  , ("f11",Nothing)
  , ("f12",Nothing)
  , ("f13",Nothing)
  , ("f14",Nothing)
  , ("f15",Nothing)
  , ("f16",Nothing)
  , ("f17",Nothing)
  , ("f18",Nothing)
  , ("f19",Nothing)
  , ("f20",Nothing)
  , ("f21",Nothing)
  , ("f22",Nothing)
  , ("f23",Nothing)
  , ("f24",Nothing)
  , ("f25",Nothing)
  , ("f26",Nothing)
  , ("f27",Nothing) ]

makeTestCase :: (FilePath, Maybe (String, Int)) -> TestCase
makeTestCase (name, result) = TC { path = name
                                 , testResult = result }

main :: IO ()
main = do
  let successes = map (updatePath ("student_tests" </>) . makeTestCase) successCases
      failures  = map (updatePath ("failing_tests" </>) . makeTestCase) failureCases

  workeds <- forM (successes ++ failures) $ \ tc -> do
    putStrLn ""
    putStrLn $ path tc ++ ":"
    worked <- runTest tc
    if worked
      then putStrLn "PASS"
      else putStrLn "FAIL"
    pure worked

  if and workeds
    then exitSuccess
    else exitFailure
