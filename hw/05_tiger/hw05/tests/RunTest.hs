{- Author: Richard Eisenberg
   File: RunTest.hs

   Testing infrastructure for HW05.
-}

{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -W #-}

module RunTest where

import CS350.CompileM      ( runCompileM )
import CS350.Renderable    ( render )

import Tiger.Lexer         ( lexTiger )
import Tiger.Parser        ( parse )

import Driver              ( ensureStdLibAvailable )
import FrontEnd            ( compileProgram )

import Data.Maybe          ( fromMaybe )
import Control.DeepSeq     ( NFData, force )
import Control.Exception   ( SomeException, evaluate, catch, displayException )
import System.Exit         ( ExitCode(..) )
import System.FilePath     ( (<.>), (</>) )
import System.Process      ( readProcessWithExitCode )
import System.Timeout      ( timeout )

data UserCodeResult a
  = Timeout
  | Exception String
  | Success a

userCode :: NFData a => IO a -> IO (UserCodeResult a)
-- See GHC #16546 for an interesting discussion of how this parses with -XBlockArguments
userCode action = fmap (fromMaybe Timeout) (
                  timeout numMicroseconds (do
                    result <- action
                    forced <- evaluate (force result)
                    pure (Success forced)))
                  `catch` \ (e :: SomeException) ->
                    pure $ Exception $ displayException e
  where
    numMicroseconds = 30 * 10^6

data TestCase
  = TC { path :: FilePath
          -- name of test, without the ".tig"
       , testResult :: Maybe (String, Int)
          -- Just (output, return value) for successful tests;
          -- Nothing for expected-fail tests
       }

-- update the path of a test case
updatePath :: (FilePath -> FilePath) -> TestCase -> TestCase
updatePath f tc = tc { path = f (path tc) }

-- Run an IO action, checking for timeouts and exceptions, reporting any
-- that arise. The String should be the action taken, like "lexing".
-- Written in continuation-passing style to make returning a Bool easier.
-- (This is essentially a mockup of exception handling using CPS.)
reportUserCode :: NFData a => String -> IO a -> (a -> IO Bool) -> IO Bool
reportUserCode desc action k = do
  result <- userCode action
  case result of
    Timeout -> do putStrLn (desc ++ " timed out.")
                  pure False
    Exception e -> do putStrLn (desc ++ " hit an exception: " ++ e)
                      pure False
    Success x -> k x

runTest :: TestCase -> IO Bool
runTest (TC { path = test_path
            , testResult = expected_result }) = do
  test_contents <- readFile (test_path <.> "tig")
  let result_file = test_path <.> "ll"
      exec_file   = test_path <.> "exe"   -- just for easy, e.g., deletion later

  tiger_o <- ensureStdLibAvailable

  reportUserCode "Lexing" (pure $ lexTiger test_contents) $ \ tokens -> do
    reportUserCode "Parsing" (pure $ parse tokens) $ \ expr -> do
      reportUserCode "Compiling Tiger to LLVM"
        (runCompileM (compileProgram expr)) $ \ actual_result ->
        case (actual_result, expected_result) of
          (Left error_msg, Nothing) -> do
            putStrLn $ "Compile error (as expected): " ++ error_msg
  
            pure True
          (Left error_msg, Just (output, retval)) -> do
            putStrLn $ "Unexpected compile error: " ++ error_msg
            putStrLn $ "Expected output:"
            putStr output
            putStrLn ""
            putStrLn $ "Expected return value: " ++ show retval
            pure False
          (Right llvm_prog, Nothing) -> do
            putStrLn $ "Expected a compile error, but compilation succeeded. Result in " ++ result_file
            writeFile result_file (render llvm_prog)
            pure False
          (Right llvm_prog, Just (expected_output, expected_retval)) -> do
            putStrLn $ "Compilation succeeded as expected."
            writeFile result_file (render llvm_prog)
            (clang_exit_code, clang_out, clang_err)
              <- readProcessWithExitCode "clang" [result_file, tiger_o, "-o", exec_file, "-Wno-override-module"] ""
            case clang_exit_code of
              ExitFailure _ -> do
                putStrLn "Compiling with clang failed."
                putStrLn clang_out
                putStrLn clang_err
                pure False
              ExitSuccess -> do
                putStrLn "Compiling with clang succeeded."
                reportUserCode "Running program"
                               (readProcessWithExitCode ("." </> exec_file) [] "") $
                  \ (actual_exit_code, actual_output, _) -> do
                  let actual_retval = case actual_exit_code of
                                        ExitSuccess   -> 0
                                        ExitFailure n -> n
                  retval_mismatch <- if actual_retval /= expected_retval
                                        then do putStrLn "Return value mismatch."
                                                putStrLn $ "Expected " ++ show expected_retval ++
                                                           " but got " ++ show actual_retval
                                                pure True
                                        else pure False
                  output_mismatch <- if actual_output /= expected_output
                                        then do putStrLn "Output mismatch."
                                                putStrLn $ "Expected \"" ++ expected_output ++ "\""
                                                putStrLn $ "But got  \"" ++ actual_output ++ "\""
                                                pure True
                                        else pure False

                  pure (not (retval_mismatch || output_mismatch))

