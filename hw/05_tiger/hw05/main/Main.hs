{- Author: Richard Eisenberg
   File: Main.hs

   Main program for HW03.
-}

module Main where

import CS350.CompileM     ( runCompileM )
import CS350.Args         ( ArgumentDescriptor, lookupArg, printArgDescriptor
                          , handleFilename, handleArgs, printUsage )

import Driver ( Settings(..), defaultSettings, processTiger )

import Data.List          ( intercalate )

import System.Environment ( getArgs, getProgName )
import System.Exit        ( exitFailure, exitSuccess )
import System.FilePath    ( isExtensionOf )

import Text.Printf        ( printf )


import System.FilePath
import CS350.Renderable
import FrontEnd
import Tiger.Lexer
import Tiger.Parser

--------------------------------------------------------------------
-- Command-line arguments

-- All arguments accepted by this program
allArguments :: [ArgumentDescriptor Settings]
allArguments = [ ( "--compile-llvm", "Run the student-written LLVM compiler."
                 , \s -> s { compileLLVM = True } )
               , ( "--no-compile-llvm", "Do not run the student-written LLVM compiler, but write '.ll' file. (default)"
                 , \s -> s { compileLLVM = False } )
               , ( "--clang", "Compile the resulting '.ll' file with clang. (default)"
                 , \s -> s { clang = True } )
               , ( "--no-clang", "Do not invoke clang to compile the '.ll' file."
                 , \s -> s { clang = False } )
               , ( "--run", "Run the clang-built executable."
                 , \s -> s { runClang = True } )
               , ( "--no-run", "Do not run the clang-built executable. (default)"
                 , \s -> s { runClang = False } )
               , ( "--print-hera", "Print the generated HERA code."
                 , \s -> s { printHERA = True } )
               , ( "--no-print-hera", "Do not print the generated HERA code. (default)"
                 , \s -> s { printHERA = False } )
               , ( "--write-hera", "Write a .hera file in the same place as .ll file. (default if compiling LLVM)"
                 , \s -> s { writeHERA = True } )
               , ( "--no-write-hera", "Do not write a .hera file."
                 , \s -> s { writeHERA = False } )
               , ( "--hera-c", "Compile the .hera file with HERA-C. Works only with --write-hera."
                 , \s -> s { heraC = True } )
               , ( "--no-hera-c", "Do not use HERA-C. (default)"
                 , \s -> s { heraC = False } )
               , ( "--assemble", "Use the student-written assembler."
                 , \s -> s { assemble = True } )
               , ( "--no-assemble", "Do not use the student-written assembler. (default)"
                 , \s -> s { assemble = False } )
               , ( "--simulate", "Use the student-written simulator. Works only with --assemble. (default if assembling)"
                 , \s -> s { simulate = True } )
               , ( "--no-simulate", "Do not simulate. With --assemble, prints the machine code."
                 , \s -> s { simulate = False } )
               ]

main :: IO ()
main = do
  (settings, rest_args)    <- handleArgs allArguments defaultSettings
  (tig_filepath, rest_args) <- handleFilename (printUsage allArguments) ".tig" rest_args
  case rest_args of
    [] -> pure ()
    _  -> do putStrLn $ "Unknown arguments: " ++ intercalate " " rest_args
             printUsage allArguments

  processTiger settings tig_filepath
