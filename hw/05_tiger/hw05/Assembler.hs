{- Author: <your names here>, with skeleton by Richard Eisenberg
   File: Assembler.hs

   Defines a HERA assembler.
-}

{-# OPTIONS_GHC -W -Wno-overflowed-literals -Wno-unused-imports #-}
  -- Disable warnings for unused imports as usual, but also for
  -- overflowed literals. This allows us to write, for example, 0xf8 as an Int8
  -- meaning (-8). (Normally, GHC would complain saying that 248 (the decimal
  -- value of 0xf8) is too big for an Int8.)

module Assembler where

import Data.Bits                ( shiftR, toIntegralSized )
  -- toIntegralSized is helpful when calculating whether you can make a relative jump
import Data.Char                ( ord )
import Data.Int                 ( Int8 )
import Data.List                ( genericLength )
import Data.Map                 ( Map )
import qualified Data.Map as M
import Data.Maybe               ( isJust )
import qualified Data.Vector as I
import Data.Word                ( Word16 )

import CS350.Panic       ( panic, unimplemented )
import CS350.Renderable  ( Renderable(..) )

import HERA.Assembly  ( AssemblyProgram, AInstruction(..), Label )
import HERA.Base      ( Register(..), Condition(..), dataBottom, rt, haltedPC )
import HERA.Machine   ( MachineProgram(..), MInstruction(..), DataSegment(..), Target(..) )

assemble :: AssemblyProgram -> MachineProgram
assemble insns = unimplemented "assemble"


