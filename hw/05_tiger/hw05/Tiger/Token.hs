{- Author: Richard Eisenberg
   File: Token.hs

   Defines lexical tokens for Tiger
-}

{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}

module Tiger.Token where

import GHC.Generics     ( Generic )
import Control.DeepSeq  ( NFData )

data Token
  = ARRAY
  | BREAK
  | DO
  | ELSE
  | END
  | FOR
  | FUNCTION
  | IF
  | IN
  | LET
  | NIL
  | OF
  | THEN
  | TO
  | TYPE
  | VAR
  | WHILE

  | LPAREN
  | RPAREN
  | LBRACKET
  | RBRACKET
  | LBRACE
  | RBRACE

  | COLON
  | COMMA
  | COLON_EQUALS
  | DOT
  | SEMI
  | MINUS
  | PLUS
  | TIMES
  | DIVIDES
  | EQUALS
  | NOT_EQUALS
  | LESS
  | GREATER
  | GREATER_EQUALS
  | LESS_EQUALS
  | AND
  | OR

  | INT_TYPE
  | STRING_TYPE

  | NUMBER Integer
  | STRING String
  | IDENTIFIER String

  deriving (Show, Eq, Generic, NFData)
