{- Author: Richard Eisenberg
   File: Internal.hs

   Internals to Unique implementation. Should be imported in strange
   situations only.
-}

module CS350.Unique.Internal (
    Unique, uniqueString, uniqueNumber, notUnique
  , mkUnique   --   <-- this is the function that is sketchy
  ) where

import CS350.Renderable        ( Renderable(..) )

-- under the hood, a Unique is just a positive integer
-- But we carry a String, too, for easy printing
data Unique = MkUnique String Integer

instance Eq Unique where
  -- only the numbers matter
  MkUnique _ n1 == MkUnique _ n2 = n1 == n2

instance Ord Unique where
  MkUnique _ n1 `compare` MkUnique _ n2 = n1 `compare` n2

instance Show Unique where
  show (MkUnique name n) = name ++ show n

instance Renderable Unique where
  render u = show u

-- get a unique string from a Unique
uniqueString :: Unique -> String
uniqueString = show

-- extract the number from a Unique
uniqueNumber :: Unique -> Integer
uniqueNumber (MkUnique _ n) = n

-- This value is identical to other instances of notUnique.
notUnique :: Unique
notUnique = MkUnique "" 0

-- gin up a fresh unique from its parts
mkUnique :: String -> Integer -> Unique
mkUnique = MkUnique
