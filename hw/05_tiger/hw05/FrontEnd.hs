{- Author: Richard Eisenberg, inspired by Steve Zdancewic
   File: FrontEnd.hs

   Compiles Tiger to LLVM.
-}

{-# LANGUAGE OverloadedLists #-}
{-# OPTIONS_GHC -W #-}

module FrontEnd where

import Tiger.Syntax as T
import LLVM.Lite    as L

import CS350.CompileM    ( CompileM, compileError )
import CS350.Panic
import CS350.Renderable  ( Renderable(..) )
import CS350.Unique      ( newUniqueString, Unique, newUnique )

import Control.Monad     ( when, zipWithM_, unless, replicateM )
import Control.Monad.Extra  ( mconcatMapM )
import Data.Bits         ( toIntegralSized )
import Data.Char         ( ord )
import Data.Foldable     ( toList )
import Data.Int          ( Int16 )
import Data.List         ( unzip4, zipWith4, genericLength )
import qualified Data.Map as M
import qualified Data.Set as S
import Data.Sequence     ( Seq(..), empty, (|>), (<|), (><), fromList )
import Data.Tuple.Extra  ( fst3 )
import Text.Printf       ( printf )


-- Overview -----------------------------------------------------------------

{- The job of the frontend is to translate Tiger's abstract syntax into
   the LLVM IR, implementing the source language semantics in terms of
   the target language constructs.

   Because the LLVM IR is typed, the frontend must also propagate
   enough type information so that we can generate appropriate type
   annotations at the LLVM level.
-}

-- Instruction Streams ------------------------------------------------------

{- The compiler emits code by generating a stream of instructions interleaved
   with declarations that should be hoisted to the global scope.

   The result of each translation function (typically) includes a stream.     -}
data Element
  = L Label                 -- Block labels
  | I Local Instruction     -- LL IR instruction
  | T Terminator           -- Block terminators
  | H Hoisted               -- A global declaration in LLVM
  deriving Show

data Hoisted
  = HGlobal (Named GlobalDecl)    -- Global declaration
  | HTy (Named L.Ty)              -- Type declaration
  | HFun (Named FunDecl)          -- Function declaration
  deriving Show

-- A Stream is a Sequence of Elements. We use Sequence instead of ordinary
-- lists because Sequences are more efficient for appending together.
-- You may happily pretend that a Seq is just a list -- only some function names
-- are different. Look online for Data.Sequence to see the relevant functions.
-- Note that the `mconcat` function is useful for concatenating Seqs.
type Stream = Seq Element

-- This is occasionally useful for debugging.
instance Renderable Element where
  render (L lbl)      = printf "L: %s" lbl
  render (I uid insn) = printf "I: %%%s = %s" uid (render insn)
  render (T t)        = printf "T: %s" (render t)
  render (H h)        = printf "H: %s" (render h)

instance Renderable Hoisted where
  render (HGlobal gbl)   = render gbl
  render (HTy ty)        = render ty
  render (HFun fun)      = render fun

-- Convert an instruction stream into a control flow graph and a list
-- of hoisted declarations.
buildCfg :: Stream -> (Cfg, [Hoisted])
buildCfg stream = ( Cfg { entry = entry_block
                        , blocks = labeled_blocks }
                  , entry_hoisted ++ more_hoisted )
  where
    (entry_block, entry_hoisted, lbl_stream) = get_entry_block stream
    (labeled_blocks, more_hoisted)           = get_labeled_blocks lbl_stream

    get_entry_block    = go_block empty []

    get_labeled_blocks (L lbl :<| es) = ((lbl, block) : other_blocks, globs ++ other_globs)
      where
        (block, globs, stream)      = go_block empty [] es
        (other_blocks, other_globs) = get_labeled_blocks stream

    get_labeled_blocks (H h :<| es)  = (blocks, h : other_hs)
      where
        (blocks, other_hs) = get_labeled_blocks es

    get_labeled_blocks (e :<| _)     = panic $ "Unexpected element: " ++ render e
    get_labeled_blocks Empty         = ([], [])

    go_block :: Seq (Local, L.Instruction)  -- accumulator for instructions
             -> [Hoisted]                   -- accumulator for hoisted definitions
                                            -- (order does not matter)
             -> Stream -> (Block, [Hoisted], Stream)
      -- returns the block, the hoisted globals, and the remainder of the
      -- stream
    go_block _     _     (L lbl :<| _)       = panic $ "Label in middle of block: " ++ lbl
    go_block insns globs (I uid insn :<| es) = go_block (insns |> (uid, insn)) globs es
    go_block insns globs (T term :<| es)     = ( Bl { instructions = toList insns, terminator = term }
                                               , globs
                                               , es )
    go_block insns globs (H h :<| es)        = go_block insns (h : globs) es
    go_block _     _     Empty               = panic "Unterminated block"

-- Helper functions -----------------------------------------------------

-- Make an appropriate L.GlobalInit for a given type.
mkGlobalInit :: Type -> GlobalInit 
mkGlobalInit (IBuiltIn TigerInt)    = IntGI 0
mkGlobalInit (IBuiltIn TigerString) = NullGI

-- generalization of zipWithM in Control.Monad
zipWith3M :: Monad m => (a -> b -> c -> m d) -> [a] -> [b] -> [c] -> m [d]
zipWith3M f (x : xs) (y : ys) (z : zs) = do
  result <- f x y z
  results <- zipWith3M f xs ys zs
  pure (result : results)
zipWith3M _ _ _ _ = pure []

-- Checking types -----------------------------------------------------------

{- Both Tiger and LLVM are *typed* languages. In the process of converting a
   Tiger program to LLVM, we must carefully track the types of definitions in
   Tiger so that we can produce well-typed LLVM. Along the way, we can also
   *type-check* the Tiger program to make sure that it is indeed well-typed.
   (After all, an ill-typed Tiger program can hardly be expected to produce
   a well-typed LLVM program.) The functions in this section do this type-
   checking.
   
-}

-- internal representation of types within the type-checker
-- distinct from Tiger types because users can redefine Tiger
-- type names. The "I" prefixes denote "Internal"
data Type
  = IBuiltIn BuiltInTy
  -- you will neeed to add more to compile types
  deriving Eq

instance Renderable Type where
  render (IBuiltIn b) = render b 

-- convenient abbreviations
tigerInt :: Type
tigerInt = IBuiltIn TigerInt

tigerString :: Type
tigerString = IBuiltIn TigerString

-- Ensure that the given type is equivalent to "int". If it isn't, issue
-- a compiler error.
checkIntType :: Type -> CompileM ()
checkIntType (IBuiltIn TigerInt) = pure ()
checkIntType ty = compileError $ "Expected an int, but got " ++ render ty

-- Ensure that the given result is really Nothing
checkVoidResult :: Maybe (Type, x) -> CompileM ()
checkVoidResult Nothing = pure ()
checkVoidResult (Just (ty, _))
  = compileError $ "Expected no value, but got something of type " ++ render ty

-- Ensure that the two given types are the same.
checkTypesEqual :: Type -> Type -> CompileM ()
checkTypesEqual ty1 ty2
  | ty1 == ty2   -- shortcut if they're identical already
  = pure ()

  | otherwise
  = compileError $ "Type error. Mismatch between " ++ render ty1 ++ " and " ++ render ty2

-- Contexts -----------------------------------------------------------------

{- We once again need to track a *compilation context*, which contains
   information needed in order to produce LLVM code from Tiger. This context
   gets modified when the Tiger code brings more definitions into scope.
-}

data Context = Ctx { ctx_vars :: M.Map T.VarId (Type, L.Operand)
                      -- map Tiger variable names to their types and LL operands
                   , ctx_types :: M.Map T.TypeId Type
                      -- map Tiger type names to types
                   , ctx_break :: Maybe L.Label
                      -- if we're in a loop, contains the label to jump to when
                      -- 'break'ing
                   , ctx_toplevel :: Bool
                      -- True if we're outside of every function; false if we're
                      -- inside a function
                   , ctx_funs :: M.Map T.FunId (L.Global, Maybe Type, [Type])
                      -- map Tiger function names to the LL name, the return
                      -- type (if there is one) and a list of argument types 
                   }

-- Get the type and operand associated with a Tiger variable.
-- Issues an error if the variable name is unknown.
lookupVar :: Context -> VarId -> CompileM (Type, L.Operand)
lookupVar (Ctx { ctx_vars = vars }) var
  | Just result <- M.lookup var vars = pure result
  | otherwise                        = compileError $ "Unbound variable " ++ var

-- Given a Type, gets the corresponding LLVM type.
lookupType :: Context -> Maybe Type -> CompileM L.Ty
lookupType _ Nothing = pure L.Void
lookupType _ (Just (IBuiltIn TigerInt)) = pure I16
lookupType _ (Just (IBuiltIn TigerString)) = pure pstring

-- Look up a TypeId to get a Type
lookupTypeId :: Context -> T.TypeId -> CompileM Type
lookupTypeId (Ctx { ctx_types = types }) typeid
  | Just ty <- M.lookup typeid types = pure ty
  | otherwise                        = compileError $ "Unknown type: " ++ typeid

-- Get the label that corresponds to breaking out of the innermost loop.
-- Fails if we are not in a loop.
lookupBreakLabel :: Context -> CompileM L.Label
lookupBreakLabel (Ctx { ctx_break = Just lbl }) = pure lbl
lookupBreakLabel (Ctx { ctx_break = Nothing  })
  = compileError "Use of 'break' outside of a loop"

-- Are we outside of any function?
isTopLevel :: Context -> Bool
isTopLevel = ctx_toplevel

-- Get the LLVM name of a function, along with the types the function
-- returns and expects.
lookupFunction :: Context -> T.FunId -> CompileM (L.Global, Maybe Type, [Type])
lookupFunction (Ctx { ctx_funs = funs }) fun
  | Just result <- M.lookup fun funs = pure result
  | otherwise                        = compileError $ "Unknown function: " ++ fun

-- Add a variable to the context, given the variable's Type, its Tiger name,
-- and an LLVM operand that denotes a pointer to the type. Note that the
-- LLVM type of the operand is a *pointer* to the type of the variable;
-- this supports mutable variables in Tiger.
bindVariable :: Context -> Type -> T.VarId -> L.Operand -> Context
bindVariable ctx@(Ctx { ctx_vars = vars }) ty var op
  = ctx { ctx_vars = M.insert var (ty, op) vars }

-- Add a set of functions to the context. These functions might be mutually
-- recursive; they should be declared in a contiguous block in Tiger.
-- The second argument is a list of function descriptors, including
-- the Tiger name of the function, the Tiger parameters to the function
-- (stored as a list of TyField, which is a name/type pair), and the return
-- type of the function (or Nothing if the function is really a procedure).
-- Next comes a list of LLVM global names, one per function.
bindFunctions :: Context -> [(T.FunId, [TyField], Maybe T.TypeId)] -> [L.Global]
              -> CompileM Context
bindFunctions ctx@(Ctx { ctx_funs = funs }) ((tiger_name, arg_fields, m_ret_id) : new_funs)
                                            (ll_name : ll_funs) = do
  arg_typs <- mapM tyfield_to_type arg_fields
  m_ret_ty <- case m_ret_id of
                Nothing -> pure Nothing
                Just id -> do ret_ty <- lookupTypeId ctx id
                              pure (Just ret_ty)
  let ctx1 = ctx { ctx_funs = M.insert tiger_name (ll_name, m_ret_ty, arg_typs) funs }
  bindFunctions ctx1 new_funs ll_funs

  where
    tyfield_to_type (TF _ id) = lookupTypeId ctx id

bindFunctions ctx _ _ = pure ctx  -- the lists are empty

-- Set the label to jump to when a Tiger program says "break"
bindBreakLabel :: Context -> L.Label -> Context
bindBreakLabel ctx lbl = ctx { ctx_break = Just lbl }

-- Update the context so that any break label is removed
unbindBreakLabel :: Context -> Context
unbindBreakLabel ctx = ctx { ctx_break = Nothing }

-- Update the context to know that we are no longer at the
-- top level (that is, the context is meant to be used *inside* a function)
setNotTopLevel :: Context -> Context
setNotTopLevel ctx = ctx { ctx_toplevel = False }

-- Prelude ------------------------------------------------------------------

{- Every compiler has some set of predefined constructs. This is sometimes called
   a "prelude". Our prelude items are defined in this section.
-}

stdlibFuns :: M.Map T.FunId (L.Global, Maybe Type, [Type])
stdlibFuns = M.fromList [ print, flush, getchar, ord, chr
                        , size, substring, concat, not, exit, print_int]
  where
    print     = ("print",     ("tiger_print",     Nothing,          [tigerString]))
    flush     = ("flush",     ("tiger_flush",     Nothing,          []))
    getchar   = ("getchar",   ("tiger_getchar",   Just tigerString, []))
    ord       = ("ord",       ("tiger_ord",       Just tigerInt,    [tigerString]))
    chr       = ("chr",       ("tiger_chr",       Just tigerString, [tigerInt]))
    size      = ("size",      ("tiger_size",      Just tigerInt,    [tigerString]))
    substring = ("substring", ("tiger_substring", Just tigerString, [tigerString, tigerInt, tigerInt]))
    concat    = ("concat",    ("tiger_concat",    Just tigerString, [tigerString, tigerString]))
    not       = ("not",       ("tiger_not",       Just tigerInt,    [tigerInt]))
    exit      = ("exit",      ("tiger_exit",      Nothing,          [tigerInt]))

    -- not in standard, but very useful
    print_int = ("print_int", ("tiger_print_int", Nothing,          [tigerInt]))

initialContext :: Context
initialContext = Ctx { ctx_vars = M.empty
                     , ctx_types = M.fromList [ ("int", tigerInt)
                                              , ("string", tigerString) ]
                     , ctx_break = Nothing
                     , ctx_toplevel = True
                     , ctx_funs = stdlibFuns 
                     }

-- These external functions should be declared for every LLVM program.
externals :: CompileM [L.Named L.FunTy]
externals = do
  ll_decls <- mapM to_ll_decl (M.elems stdlibFuns)
  pure $ [ N { name = "malloc", decl = FunTy (L.Ptr I16) [I16] }
         , N { name = "tstrcmp", decl = FunTy I16 [pstring, pstring] } ] ++
         ll_decls
  where
    to_ll_decl (ll_name, m_ret_type, arg_types) = do
      ll_ret_ty <- lookupType initialContext m_ret_type
      ll_arg_tys <- mapM (lookupType initialContext . Just) arg_types
      pure (N { name = ll_name
              , decl = FunTy ll_ret_ty ll_arg_tys })

-- Compiling Types ----------------------------------------------------------

{- Definitions around linking Tiger types to LLVM types. This section must
   expand considerably to support type definitions.
-}

int :: L.Ty
int = I16

string :: L.Ty
string = L.Struct [I16, L.Array 0 I16]

pstring :: L.Ty
pstring = Ptr string

-- LL IR helper functions ---------------------------------------------------

{- Generate a fresh identifier based on a given string.  Because Tiger
   identifiers cannot begin with _, the resulting string is guaranteed
   not to clash with another source language construct.
-}
newId :: String -> CompileM String
newId name = newUniqueString ('_' : name)

-- This convenience function produces an Element for an instruction, generating
-- a new LLVM local name along the way.
newInstruction :: L.Instruction -> CompileM (Element, L.Operand)
newInstruction insn = do
  local <- newId "local"
  pure (I local insn, LocalId local)

-- Allocate memory in the heap. The returned operand has the type given
-- (which must be a pointer type), and the size of the memory is indicated
-- in the given operand. (This is *not* need in HW05.)
malloc :: L.Ty -> L.Operand -> CompileM (Stream, L.Operand)
malloc ll_ty size_op = do
  (malloc_insn, malloc_op)
    <- newInstruction (L.Call (L.Ptr I16) (GlobalId "malloc")
                              [(I16, size_op)])
  (cast_insn, cast_op)
    <- newInstruction (Bitcast (L.Ptr I16) malloc_op ll_ty)

  pure ([malloc_insn, cast_insn], cast_op)

-- Compare two strings, pointers to which are stored in the operands given. (That
-- is, both operands should have type 'pstring'.)
-- The returned operand is an I16; it is a value
-- less than 0 iff the first string is less than the second (lexicographically),
-- equal to 0 iff the first string equals the second, and
-- greater than 0 iff the first string is greater than the second
tstrcmp :: L.Operand -> L.Operand -> CompileM (Stream, L.Operand)
tstrcmp lhs rhs = do
  (tstrcmp_insn, result)
    <- newInstruction (L.Call I16 (GlobalId "tstrcmp")
                              [(pstring, lhs), (pstring, rhs)])
  pure ([tstrcmp_insn], result)

-- Compile a declaration block ----------------------------------------------

{- According to the definition of Tiger, declarations come in three forms:
   variable declarations, type declarations, and function declarations.
   The latter two of these can be mutually recursive, meaning that two
   definitions might each refer to the other. To account for this, we view
   both type declarations and function declarations as groups; according
   to the spec, a mutually recursive group of type/function declarations
   must not have any intervening declarations of another sort. Accordingly,
   when we spot a type or function declaration, we must search forward for
   all such declarations, accumulate them into a list, and process them
   all at once.

   No such special treatmnt is necessary for variable declarations, which
   may not be recursive in Tiger.

   Because declarations bring new constructs into scope, the
   compileDeclarations function returns a new Context, extended to include
   the new constructs. It also returns a Stream that contains the type
   declarations, along with the variable declarations and function declarations.

   Special care must be taken around variable declarations. If these are
   at top-level, we want to make sure that the variables are accessible in
   all functions -- that is, they must be compiled into LLVM globals.
   If a variable declaration is not at top-level, it can just be alloca'd
   in the current function's stack, as we do not support nested function
   declarations.
-}

compileDeclarations :: Context -> [T.Dec] -> CompileM (Stream, Context)
compileDeclarations ctx (VarD vd : rest) = do
  (s1, lhs, rhs_ty, rhs_op) <- compileVarDec ctx vd
  ll_ty <- lookupType ctx (Just rhs_ty)
  (alloca, lhs_op) <- if isTopLevel ctx
    then do global_name <- newId lhs
            let global_init = mkGlobalInit rhs_ty
                glob = N { name = global_name
                         , decl = GD { globalType = ll_ty
                                     , globalInit = global_init }}
            pure (H (HGlobal glob), GlobalId global_name)

    else newInstruction (Alloca ll_ty)

  (store, _) <- newInstruction (Store ll_ty rhs_op lhs_op)
  let ctx1 = bindVariable ctx rhs_ty lhs lhs_op
  (s2, ctx2) <- compileDeclarations ctx1 rest -- use ctx1 because the new
                                              -- var is in scope in the rest

  pure (s1 >< alloca <| store <| s2, ctx2)

compileDeclarations ctx (TypeD td : rest) =
  compileTypeDeclarations ctx [td] rest

compileDeclarations ctx (FunD fd : rest) =
  compileFunDeclarations ctx [fd] rest

compileDeclarations ctx [] = pure (empty, ctx)

-- Compiling a VarDec produces:
--  1) a stream that computes the value assigned to the variable
--  2) the name of the Tiger variable
--  3) the type of the Tiger variable
--  4) the LLVM operand storing the initial value of the Tiger variable
compileVarDec :: Context -> VarDec -> CompileM (Stream, T.VarId, Type, L.Operand)
compileVarDec = unimplemented "compileVarDec"

-- Compiling a type declaration accumulates all type declarations
-- into one group and adds them to the context. Then, this context
-- is used to compile the remaining declarations.
compileTypeDeclarations :: Context -> [TyDec] -> [T.Dec] -> CompileM (Stream, Context)
compileTypeDeclarations ctx _ decls = compileDeclarations ctx decls   -- ignore types for now

-- Compiling a function declaration accumulates all function declarations
-- into one group and then compiles them. It then compiles the remaining
-- declarations in the list of declarations under the new context.
compileFunDeclarations :: Context -> [FunDec] -> [T.Dec] -> CompileM (Stream, Context)
compileFunDeclarations ctx fun_decs (FunD fd : decls)
  = compileFunDeclarations ctx (fd : fun_decs) decls
compileFunDeclarations ctx fun_decs decls = do
  unimplemented "compileFunDeclarations"
  {- In this clause of compileFunDeclarations, you have to do the hard work.
     Because functions can be mutually recursive, the first step is to arrange
     a call to bindFunctions; doing so will require extracting various information
     from the FunDecs and generating names for the LLVM functions (with newId).

     Then, you'll have to compile the function bodies in the extended context
     returned by bindFunctions. This, in turn, requires four steps, completed
     separately for each function (a helper function might be useful):

       1) Compile the argument list. Here, you have to come up with a name
          for the LLVM equivalent of the Tiger argument (with newId). More
          surprisingly, you will have to 'alloca' space for the argument.
          This is because arguments in Tiger are *editable*, as in this
          example:

            let function silly(x : int) : int = ( x := x + 1; x ) in silly(5) end

          Note that x is updated within the function silly. LLVM, of course,
          doesn't allow this directly. So, we treat x like a local variable,
          allocating space for its value with alloca, and then copying the
          passed-in value into that allocated space (with a 'store' instruction).

          The instructions to store the argument in the allocated space should
          end up at the beginning of the stream.

       2) Update the context to bind the parameter variables, remove the 'break'
          label, and set the context not to be at top-level. Use this updated
          context only when compiling the function at hand, not other functions:
          one function's parameters are definitely not in scope for other functions!

       3) Compile the expression in the function body, resulting in a stream.
          Don't forget to terminate the stream by returning the final value.

       4) Use buildCfg to convert the Stream into a Cfg, and use that to build
          an LLVM Named FunDecl. Put that Named FunDecl into the output stream
          as a "hoisted" element (along with all the hoisted elements returned
          from buildCfg).

     After compiling all the functions, don't forget to call compileDeclarations
     to nab the rest of the declarations in the enclosing 'let', with the context
     extended with the functions. (That is, this context should be the result
     of bindFunctions, *not* any of the contexts built while compiling the function
     bodies.) -}

-- Compile an Lvalue --------------------------------------------------------

{- Lvalues comprise a limited form of expression that denote a place that
   be assigned to. These include plain variables, array locations, and structure
   locations. Because we might want to update a value in an Lvalue, we compile
   an Lvalue into a *pointer* to the memory that might be updated. Thus, if
   our source Lvalue has type t, then compiling it yields an LLVM type
   (Ptr (lookupType t)).
-}

-- Returns a Stream, the type pointed to, and the operand where the resulting
-- pointer lives. Note that the type is the type pointed to, not the type
-- of the Lvalue itself.
compileLvalue :: Context -> T.Lvalue -> CompileM (Stream, Type, L.Operand)
compileLvalue = unimplemented "compileLvalue"
  -- For HW05, you need not handle the FieldLV or ArrayLV cases. Write these
  -- cases in (to avoid pattern-match incompleteness warnings), but just
  -- use 'unimplemented' to report an error.

-- Compile an expression ----------------------------------------------------

{- This is the main workhorse of the compiler. It returns a stream of instructions
   needed to execute the instruction.

   Invariant: The stream returned never ends with a terminator T. (If it did, we
              wouldn't be able to add more instructions.)

   If the Tiger expression results in no value, the second return value from
   this function is Nothing. Otherwise, it is the Type and Operand of the result.
-}

compileExp :: Context -> T.Exp -> CompileM (Stream, Maybe (Type, L.Operand))
compileExp = unimplemented "compileExp"
  {- For HW05 you need not handle Nil, NewRecord, or NewArray.
     As in compileLvalue, pattern-match on these, but simply call
     'unimplemented' to issue an error.

     A few tips:
      * Remember to type-check. Every part of an expression has a type.
        These types always have some restriction on them (except for
        expressions that are not the last expression in a sequence).
        Check to make sure the types are correct.

      * See the compileString function, below, for dealing with string
        literals.

      * Our version of Tiger does not support division. Issue an
        error if the user tries to divide.

      * Tiger integers are unbounded (stored as Haskell 'Integer'),
        but LLVM works only with I16. Do not worry about this:
        just use fromIntegral to convert.

      * Remember that & and | are short-circuting operators: they do
        not evaluate their right-hand operands if the final value is
        determined by the left-hand operand.

      * The natural way to implement comparisons will result in an
        LLVM I1. But Tiger will want an int, which is an LLVM I16.
        You will need to write some code that *widens* an I1 into an I16
        of the same value. This can be done with code that behaves
        like 'if b then 1 else 0'.

      * String comparisons require real work, as we need to compare
        individual letters in the strings. You can do this work via
        the function tstrcmp, which will be linked against your compiled
        LLVM code. The tstrcmp function takes in two strings and returns
        an int (I16). The return value is less than 0 iff the first string
        is less than the second, 0 iff the first string equals the second,
        and greater than 0 iff the first string is greater than the second.
        (This is just like C's strcmp function or Java's compareTo functions.)
        The call to tstrcmp can be arranged by calling the Haskell
        function tstrcmp defined above.

      * 'If' is a bit tricky. Use the example from the NB language in class
        for inspiration.

      * Remember to update the context with the 'break' label when compiling
        the loop constructs. -}

-- Compile an expression that is expected to result in a value. If the
-- expression does not result in a value, issue a type error to the user.
compileValueExp :: Context -> T.Exp -> CompileM (Stream, Type, L.Operand)
compileValueExp ctx exp = do
  (s, m_result) <- compileExp ctx exp
  case m_result of
    Just (ty, op) -> pure (s, ty, op)
    Nothing       -> compileError "Expected a value where none was found."

{- Getting string constants into LLVM is a bit tricky. The problem is that we want
   all strings to have the type { i16, [0 x i16] }* in LLVM, but we can't usefully define
   global constants of that type. Let

     %string = type { i16, [0 x i16] }

   That is, a string is its length, along with an array containing the actual data.
   Here might be an attempt to declare an actual string:

     @hello = global %string { i16 5, [5 x i16] [i16 104, i16 101, i16 108, i16 108, i16 111] }

   The problem is that this doesn't type-check, because a %string's second component
   should have 0 elements, not 5. So, we can't declare @hello this way.

   The solution to this is bitcast, which can change the type of a pointer.
   With this in mind, we declare @hello with its correct type, as follows:

     @hello = global { i16, [5 x i16] }
                     { i16 5, [5 x i16] [i16 104, i16 101, i16 108, i16 108, i16 111] }

   Now, when we want to access @hello, we bitcast it into the right type (with
   a supposedly 0-length array):

     %1 = bitcast { i16, [5 x i16] }* @hello to { i16, [0 x i16] }*

   At this point, %1 has the desired type. This is all done by compileString.
-}

-- Compile a string. The returned operand has type 'pstring' and points to
-- the string requested. The actual characters for the string are stored as
-- global data.
compileString :: String -> CompileM (Stream, Operand)
compileString string = do
  let len = length string
  string_name <- newId "string"
  let array_ty = L.Array len I16
      glob_ty  = L.Struct [I16, array_ty]
      string_glob = H $ HGlobal $
        N { name = string_name
          , decl = GD { globalType = glob_ty
                      , globalInit =
                        L.StructGI [ GD { globalType = I16
                                        , globalInit = IntGI (fromIntegral len) }
                                   , GD { globalType = array_ty
                                        , globalInit = ArrayGI (map (GD I16 .
                                                                     IntGI .
                                                                     fromIntegral .
                                                                     ord) string) }]}}

  (bitcast, result) <- newInstruction (Bitcast (L.Ptr glob_ty)
                                               (GlobalId string_name)
                                               pstring)

  pure ([string_glob, bitcast], result)

-- Compile a top-level program ----------------------------------------------

-- We will end up with a list of hoisted elements, but we need to repackage
-- these into an LLVM Program object.
partitionHoisteds :: [Hoisted] -> ([Named L.Ty], [Named GlobalDecl], [Named FunDecl])
partitionHoisteds = go [] [] []
  where
    go tys globs funs (HGlobal glob : hs) = go tys (glob:globs) funs hs
    go tys globs funs (HTy ty       : hs) = go (ty:tys) globs funs hs
    go tys globs funs (HFun fun     : hs) = go tys globs (fun:funs) hs
    go tys globs funs []                  = (tys, globs, funs)

compileProgram :: T.Exp -> CompileM L.Program
compileProgram = unimplemented "compileProgram"
  {- This one is not too bad: call compileValueExp on the given expression, check
     that its type is "int", and then build the resulting Program. Make sure to
     use 'externals' to set the 'externalDecls' field of a Program, so that the
     compiled LLVM code has access to the standard library. -}
