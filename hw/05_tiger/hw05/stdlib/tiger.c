/* Author: Richard Eisenberg
   File: tiger.c

   The Tiger standard library, in C. Link this against the files
   output from the Tiger compiler to execute.
*/

#include <stdio.h>
#include <stdlib.h>

// print a Tiger string to stdout
void tiger_print(short* s)
{
  for(int i = 0; i < *s; i++)
  {
    putchar(s[i+1]);
  }
}

// flush the stdout buffer
void tiger_flush()
{
  fflush(stdout);
}

// get a character from stdin as a one-character Tiger string
short* tiger_getchar()
{
  short* result = malloc(2);
  result[0] = 1;
  result[1] = getchar();
  return result;
}

// get the ascii value of the first character of a Tiger string
short tiger_ord(short* s)
{
  if(*s == 0)
  { 
    return -1;
  }

  return s[1];
}

// convert an ascii value into a one-character Tiger string
short* tiger_chr(short i)
{
  short* result = malloc(2);
  result[0] = 1;
  result[1] = i;
  return result;
}

// return the number of characters in a Tiger string
short tiger_size(short* s)
{
  return *s;
}

// get a substring of a Tiger string, starting at the
// 0-based index given of the length given
short* tiger_substring(short* s, short first, short n)
{
  if(first + n > *s)
  {
    fprintf(stderr, "Bad call to tiger_substring; string too short.\n");
    exit(1);
  }

  short* result = malloc(n);
  *result = n;
  for(int i = 0; i < n; i++)
  {
    result[1+i] = s[1+first+i];
  }

  return result;
}

// concatenate two Tiger strings
short* tiger_concat(short* s1, short* s2)
{
  short* result = malloc(*s1 + *s2 + 1);
  *result = *s1 + *s2;
  for(int i = 0; i < *s1; i++)
  {
    result[i+1] = s1[i+1];
  }
  for(int i = 0; i < *s2; i++)
  { 
    result[i+1+*s1] = s2[i+1];
  }
  return result;
}

// logical inverse
short tiger_not(short i)
{
  return i == 0;
}

// exit the program with the given value
void tiger_exit(short i)
{
  exit(i);
}

// not in standard, but very useful:
// print a Tiger int to the console
void tiger_print_int(short i)
{
  printf("%d", i);
}

// compare two strings
short tstrcmp(short* s1, short* s2)
{
  for(int i = 0; i < *s1 && i < *s2; i++)
  {
    if(s1[1+i] != s2[1+i])
    {
      return s1[1+i] - s2[1+i];
    }
  }

  return *s1 - *s2;
}
