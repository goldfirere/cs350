{- Author: Steve Zdancewic, translated to Haskell by Richard Eisenberg
   File: Driver.hs

   Main interface to functions in the HW03 codebase.
-}

{-# LANGUAGE ScopedTypeVariables, CPP #-}
{-# OPTIONS_GHC -W #-}

module Driver where

import CS350.CompileM   ( runCompileM )
import CS350.Renderable ( render )

import HERA.Assembly    ( AssemblyProgram )

import qualified Assembler
import qualified Simulator

import qualified BackEnd

import LLVM.Lite        ( Program )

import Tiger.Lexer      ( lexTiger )
import Tiger.Parser     ( parse )
import FrontEnd         ( compileProgram )

import Control.DeepSeq    ( force, rnf )
import Control.Exception  ( ErrorCall(..), evaluate, tryJust, IOException, catch
                          , displayException )
import Control.Monad      ( when, unless )
import System.Directory   ( doesFileExist, getModificationTime )
import System.Environment ( getEnv )
import System.Exit        ( exitFailure, ExitCode(..) )
import System.FilePath    ( dropExtension, replaceExtension, (</>) )
import System.IO.Error    ( isDoesNotExistError )
import System.Process     ( callProcess, readProcessWithExitCode )
import Text.Printf        ( printf )

-- configuration flags ------------------------------------------------------

data Settings = Set { compileLLVM  :: Bool  -- run the LLVM compiler?
                    , clang        :: Bool  -- run clang?
                    , runClang     :: Bool  -- run clang-built executable?
                    , printHERA    :: Bool  -- print the generated HERA code?
                    , writeHERA    :: Bool  -- write an .hera file with the compiled code?
                    , heraC        :: Bool  -- use the HERA-C backend?
                    , assemble     :: Bool  -- run the student-written assembler?
                    , simulate     :: Bool  -- run the student-written simulator?
                    }                       -- (If this last one is false but `assemble`
                                            -- is true, just print the assembled program.)

-- suitable defaults
defaultSettings :: Settings
defaultSettings = Set { compileLLVM  = False
                      , clang        = True
                      , runClang     = False
                      , printHERA    = False
                      , writeHERA    = True
                      , heraC        = False
                      , assemble     = False
                      , simulate     = True }

-- plumbing -----------------------------------------------------------------

failWith :: String -> IO a
failWith message = do
  putStrLn message
  exitFailure

exitCodeToNumber :: ExitCode -> Int
exitCodeToNumber ExitSuccess     = 0
exitCodeToNumber (ExitFailure n) = n

-- terminal output ----------------------------------------------------------

printBanner :: String -> IO ()
printBanner s =
  let dashes 0 = ""
      dashes n = "-" ++ (dashes (n-1)) in
  putStrLn (printf "%s %s\n" (dashes (79 - (length s))) s)

printLLProgram :: String -> Program -> IO ()
printLLProgram prog_name ll_ast = do
  printBanner (prog_name ++ ".ll")
  putStrLn (render ll_ast)

printHERAProgram :: String -> AssemblyProgram -> IO ()
printHERAProgram prog_name assembly = do
  printBanner (prog_name ++ ".hera")
  putStrLn (render assembly)

-- compiler pipeline --------------------------------------------------------

{- These functions implement the compiler pipeline for a single Tiger file:
     - parse the file
     - compile to a LLVM file
     - do further processing as requested
-}

processTiger :: Settings -> FilePath -> IO ()
processTiger settings filepath = do
  code_string <- readFile filepath
  evaluate (rnf code_string)
    `catch` \ (e :: IOException) -> failWith $ "Error reading file: " ++ displayException e

  printBanner "lexing"
  lexing_result <- evaluate (force (lexTiger code_string))
    `catch` \ (e :: ErrorCall) -> failWith $ "Error lexing: " ++ displayException e

  printBanner "parsing"
  parsing_result <- evaluate (force (parse lexing_result))
    `catch` \ (e :: ErrorCall) -> failWith $ "Error parsing: " ++ displayException e

  printBanner "compiling Tiger to LLVM"
  compile_result <- runCompileM (compileProgram parsing_result)

  case compile_result of
    Left error -> failWith error
    Right prog -> do

      evaluate (rnf prog)
        `catch` \ (e :: ErrorCall) -> failWith $ "Error compiling: " ++ displayException e

      let ll_path = replaceExtension filepath "ll"
      printBanner $ "writing LLVM file to " ++ ll_path
      writeFile ll_path (render prog)

      when (clang settings) $ do
#ifdef mingw32_HOST_OS
        failWith "Compiling via clang is not supported on Windows. Sorry."
#endif
        tiger_o <- ensureStdLibAvailable

        printBanner "running clang"
        let exe_path = dropExtension filepath
        callProcess "clang" [ll_path, tiger_o, "-o", exe_path, "-Wno-override-module"]

        when (runClang settings) $ do
          printBanner "executing compiled program"
          (exit_code, stdout, stderr) <- readProcessWithExitCode ("." </> exe_path) [] ""
          putStr stdout
          when (not (null stderr)) $ do
            putStrLn "stderr:"
            putStr stderr
          putStrLn ""
          putStrLn $ "Exit code: " ++ show (exitCodeToNumber exit_code)

      when (compileLLVM settings) $ do
        printBanner "LLVM compiler output"
        compile_llvm_result <- runCompileM (BackEnd.compileLLVM prog)

        assembly <- case compile_llvm_result of
                      Left err -> failWith $ "Error during LLVM compilation: " ++ err
                      Right a  -> pure a

        evaluate (rnf assembly)
          `catch` \ (e :: ErrorCall) -> failWith $ "Error during LLVM compilation: " ++ displayException e
        putStrLn ""

        let hera_string = render assembly
        when (printHERA settings) $ do
          printBanner "compiled HERA"
          putStrLn hera_string

        when (writeHERA settings) $ do
          let hera_file = replaceExtension filepath ".hera"
              hera_file_string = "#include <HERA.h>\n\nvoid HERA_main() {\n#include <Tiger-stdlib-stack-data.hera>\n" ++ hera_string ++ "\n#include <Tiger-stdlib-stack.hera>\n}\n"
          writeFile hera_file hera_file_string

          when (heraC settings) $ do
            hera_include_folder <- do
              result <- tryJust (\ ex -> if isDoesNotExistError ex then Just () else Nothing)
                                (getEnv "HERA_C_DIR")
              case result of
                Left _ -> failWith $ "Cannot use HERA-C because I don't know where to find HERA.h\n" ++
                                     "Perhaps add `export HERA_C_DIR=/path/to/HERA-C` to your\n" ++
                                     "~/.bash_profile file (but edit that directory to be correct)."
                Right dir -> pure dir
            printBanner "HERA compilation output"
            callProcess "g++" [ "-I" ++ hera_include_folder  -- find HERA-C folder
                              , "-w"                         -- suppress warnings
                              , "-x", "c++"                  -- compile like C++
                              , "-o", dropExtension filepath -- set output filename
                              , hera_file ]

              -- run executable
            printBanner "HERA execution output"
            callProcess ("." </> dropExtension filepath) []

        when (assemble settings) $ do
          machine_program <- do
            result <- tryJust (\ (ErrorCall msg) -> Just msg)
                              (evaluate $ force $ Assembler.assemble assembly)
                        -- the "evaluate" and "force" allow us to catch exceptions
                        -- thrown by the assembler. Otherwise, Haskell's laziness
                        -- might leave the exceptions unthrown.
            case result of
              Left err -> failWith $ "Assembler error: " ++ err
              Right mp -> pure mp

          if simulate settings
            then do printBanner "Running simulator"
                    machine <- Simulator.load machine_program
                    Simulator.run machine
                    Simulator.printMachine machine
            else do printBanner "Result of assembler"
                    putStrLn (render machine_program)

-- returns the location of tiger.o
ensureStdLibAvailable :: IO FilePath
ensureStdLibAvailable = do
  env_result <- tryJust (\ ex -> if isDoesNotExistError ex
                                 then Just ()
                                 else Nothing) $
                getEnv "TIGER_STDLIB"

  case env_result of
    Left () -> do
      file_exists <- doesFileExist "stdlib/tiger.o"

      do_build <- if file_exists
                    then do
                      o_file_time <- getModificationTime "stdlib/tiger.o"
                      c_file_time <- getModificationTime "stdlib/tiger.c"
                      pure (c_file_time >= o_file_time)
                    else pure True

      when do_build $ do
        printBanner "building standard library"
        callProcess "clang" ["-c", "stdlib/tiger.c", "-o", "stdlib/tiger.o"]

      pure "stdlib/tiger.o"

    Right path -> do
      file_exists <- doesFileExist path

      unless file_exists $ do
        putStrLn $ "TIGER_STDLIB defined to be " ++ path ++ " but that file does not exist"
        putStrLn "I don't know what to do now. Aborting..."
        exitFailure

      pure path
