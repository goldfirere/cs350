{- Author: Richard Eisenberg
   File: Unique.hs

   A monad that allows the generation of Uniques, values that
   are guaranteed to be unique.

   CS350 students are not expected to understand the details of the code
   here, but it's not that hard, if you want to learn about monads.
-}

{-# LANGUAGE GeneralizedNewtypeDeriving, TypeApplications, FlexibleInstances,
             MultiParamTypeClasses, UndecidableInstances #-}
{-# OPTIONS_GHC -W #-}

module CS350.Unique (
    Unique, uniqueString, uniqueNumber, notUnique
  , UniqueT, runUniqueT
  , UniqueM, runUniqueM
  , MonadUnique(newUnique), newUniqueString
  ) where

import CS350.Unique.Internal   ( Unique, mkUnique, uniqueString, notUnique, uniqueNumber )

import Control.Applicative     ( Alternative )
import Control.Monad           ( MonadPlus )
import Control.Monad.Trans     ( MonadTrans, lift )
import Control.Monad.State     ( StateT, MonadState(..), get, modify
                               , evalStateT )
import Control.Monad.Except    ( MonadError )
import Control.Monad.Reader    ( MonadReader )
import Control.Monad.Writer    ( MonadWriter )
import Control.Monad.Fix       ( MonadFix )
import Control.Monad.Fail      ( MonadFail )
import Control.Monad.IO.Class  ( MonadIO )
import Control.Monad.Cont      ( MonadCont )
import Data.Functor.Identity   ( Identity, runIdentity )

----------------------------------------------------
-- A monad capable of delivering Uniques

-- First, the general class of Unique-generating monads
class Monad m => MonadUnique m where
  newUnique :: String -> m Unique

-- This is a monad transformer that adds an ability to generate a Unique
-- value.
newtype UniqueT m a = UniqueT (StateT Integer m a)
  deriving ( Functor, Applicative, Monad, MonadTrans, MonadError e, MonadReader r
           , MonadWriter w, MonadFix, MonadFail, MonadIO, Alternative
           , MonadPlus, MonadCont )
  -- for completeness, this should also derive Contravariant, but some
  -- students in 2019 still had GHC 8.4, and it's just not worth fussing
  -- over.

instance MonadState s m => MonadState s (UniqueT m) where
  state f = lift $ state f

type UniqueM = UniqueT Identity

-- Generate a new unique value, different from all others
instance Monad m => MonadUnique (UniqueT m) where
  newUnique name = UniqueT $ do u <- get
                                modify (1+)
                                return (mkUnique name u)

-- run a computation in the UniqueT monad
runUniqueT :: Monad m => UniqueT m a -> m a
runUniqueT (UniqueT action) = evalStateT action 1

-- run a computation in the UniqueM monad
runUniqueM :: UniqueM a -> a
runUniqueM action = runIdentity (runUniqueT action)

-- produce a unique string
newUniqueString :: MonadUnique m => String -> m String
newUniqueString seed = uniqueString <$> newUnique seed
