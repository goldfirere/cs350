{- Author: Richard Eisenberg
   File: Renderable.hs

   Declares a class for types that have a user-readable representation.
   That is, these types can be rendered for the user.
-}

{-# LANGUAGE TypeSynonymInstances, FlexibleInstances #-}
  -- necessary to have an instance for String

module CS350.Renderable where

import Data.Int
import Data.Word

-- Members of this class have a friendly, user-understandable representation
class Renderable a where
  render :: a -> String

-- Append two strings, unless the first string is empty. If the first
-- string is empty, just return an empty string.
(++.) :: String -> String -> String
"" ++. _  = ""
s1 ++. s2 = s1 ++ s2
infixl 5 ++.   -- by using infixl, it guarantees that users will have to parenthesize

instance Renderable Word8 where
  render = show
instance Renderable Word16 where
  render = show

instance Renderable Int where
  render = show
instance Renderable Int8 where
  render = show
instance Renderable Int16 where
  render = show

instance Renderable String where
  render = show
