{- Author: Richard Eisenberg
   File: Main.hs

   Program to simulate a given HERA program, even if
   it has mutually recursive data dependencies (which HERA-C cannot
   deal with).
-}

{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -W #-}

module Main where

import CS350.Args   ( ArgumentDescriptor, handleArgs, handleFilename, printUsage )
import CS350.Renderable   ( render )

import HERA.Lexer    ( lexHera )
import HERA.Parser   ( parse )
import HERA.Assembly ( returnOffset )
import Assembler     ( assemble )
import Simulator     ( load, printMachine, step, memory )

import Control.DeepSeq    ( NFData, force )
import Control.Exception  ( IOException, ErrorCall, catch, displayException, evaluate )
import Control.Monad      ( when )
import Data.List          ( intercalate )

import qualified Data.Vector.Mutable as M

import System.Exit        ( exitFailure )

data Settings = Settings { showSteps :: Bool
                         , showMachineCode :: Bool
                         , showResult :: Bool }

defaultSettings :: Settings
defaultSettings = Settings { showSteps = False
                           , showMachineCode = False
                           , showResult = True }

allArguments :: [ArgumentDescriptor Settings]
allArguments = [ ( "--show-steps", "Prints the machine state after every step."
                 , \s -> s { showSteps = True } )
               , ( "--no-show-steps", "Do not print machine state after every step. (default)"
                 , \s -> s { showSteps = False } )
               , ( "--show-machine-code", "Prints the assembled MachineProgram before running."
                 , \s -> s { showMachineCode = True } )
               , ( "--no-show-machine-code", "Does not print the result of assembler. (default)"
                 , \s -> s { showMachineCode = False } )
               , ( "--show-result", "Prints the result of the program on last line of output. (default)"
                 , \s -> s { showResult = True } )
               , ( "--no-show-result", "Does not print the result of the program."
                 , \s -> s { showResult = False } ) ]

-- catch a call to `error`
catchError :: NFData a => a -> IO a
catchError thing = evaluate (force thing)
                   `catch` \ (e :: ErrorCall) -> do putStrLn (displayException e)
                                                    exitFailure

main :: IO ()
main = do
  (settings, rest_args)  <- handleArgs allArguments defaultSettings
  (hera_file, rest_args) <- handleFilename (printUsage allArguments) ".hera" rest_args
  case rest_args of
    [] -> pure ()
    _  -> do putStrLn $ "Unknown arguments: " ++ intercalate " " rest_args
             printUsage allArguments

  hera_string <- readFile hera_file
                 `catch` \ (e :: IOException) -> do putStrLn (displayException e)
                                                    printUsage allArguments
  hera_tokens   <- catchError (lexHera hera_string)
  hera_assembly <- catchError (parse hera_tokens)
  hera_mp       <- catchError (assemble hera_assembly)

  when (showMachineCode settings) $ do
    putStrLn "--------------- Assembled Machine Code ---------------------"
    putStrLn (render hera_mp)

  machine <- load hera_mp

  when (showSteps settings) $ do
    putStrLn "-------------- Machine Steps -------------------------------"
    putStrLn "### Initial state ###"
    printMachine machine

  let advance n = do done <- step machine
                     when (showSteps settings) $ do
                       putStrLn ""
                       putStrLn $ "### After step " ++ show n ++ " ###"
                       printMachine machine
                     if done
                       then pure n
                       else advance (n+1)

  num_steps <- advance 1

  putStrLn $ "Completed simulation in " ++ show num_steps ++ " steps."

  when (showResult settings) $ do
    putStrLn "--------------- Result -------------------------------------"
    result <- M.read (memory machine) (fromIntegral returnOffset)
    print result
