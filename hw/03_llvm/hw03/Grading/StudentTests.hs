{- Author: Richard Eisenberg
   File: StudentTests.hs

   Listing of test cases for student-visible tests.
-}

module Grading.StudentTests where

import Data.Word   ( Word16 )

type TestCase = (String, Word16)   -- (program name, expected result)

testCases :: [TestCase]
testCases = [ ("add", 14)
            , ("sub", 1)
            , ("mul", 45)
            , ("and", 0)
            , ("or", 1)
            , ("xor", 0)
            , ("shl", 168)
            , ("lshr", 10)
            , ("ashr", 5)
            , ("call", 42)
            , ("call1", 17)
            , ("call2", 19)
            , ("call3", 34)
            , ("call4", 34)
            , ("call5", 24)
            , ("call6", 26)
            , ("alloca1", 17)
            , ("alloca2", 17)
            , ("global1", 12)
            , ("return", 0)
            , ("return42", 42)
            , ("br1", 9)
            , ("br2", 17)
            , ("cbr1", 7)
            , ("cbr2", 9)
            , ("bitcast1", 3)
            , ("gep1", 6)
            , ("gep2", 4)
            , ("gep3", 1)
            , ("gep4", 2)
            , ("gep5", 4)
            , ("gep6", 7)
            , ("gep7", 7)
            , ("gep8", 2)
            , ("list1", 3)
            , ("cbr", 42)
            , ("factorial", 120)
            , ("factrect", 120)
            ]
