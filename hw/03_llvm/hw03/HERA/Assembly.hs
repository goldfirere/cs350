{- Author: Richard Eisenberg
   File: Assembly.hs

   Defines structures to describe HERA assembly code.
-}

{-# LANGUAGE TypeSynonymInstances, FlexibleInstances #-}
  -- These settings are required in order to allow the instance
  -- of Renderable for Program
{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}
   -- needed to derive NFData for AInstruction

{-# OPTIONS_GHC -W #-}

module HERA.Assembly where

import CS350.Renderable  ( Renderable(..) )
import CS350.WordN       ( Word4, Word5, Word6 )

import HERA.Base         ( Register, Condition )

import Data.Int    ( Int8 )
import Data.Word   ( Word8, Word16 )

import Control.DeepSeq   ( NFData )
import GHC.Generics      ( Generic )

-- Key definitions for our calling convention.
-- These are all offsets from the current FP.
pcRetOffset, fpAltOffset, param1Offset, returnOffset :: Word5
pcRetOffset = 0
fpAltOffset = 1
param1Offset = 3
returnOffset = 3

-- Labels for code blocks and global data
type Label = String

-- One assembly instruction
data AInstruction
  = ASetlo Register Int8
  | ASethi Register Word8
  | AAnd Register Register Register
  | AOr  Register Register Register
  | AXor Register Register Register
  | AAdd Register Register Register
  | ASub Register Register Register
  | AMul Register Register Register
  | AInc Register Word6         -- the value is δ, the actual amount to increment
  | ADec Register Word6         -- the value is δ, the actual amount to increment
  | ALsl Register Register
  | ALsr Register Register
  | ALsl8 Register Register
  | ALsr8 Register Register
  | AAsl Register Register
  | AAsr Register Register
  | AFon Word5
  | AFoff Word5
  | AFset5 Word5
  | AFset4 Word4
  | ASavef Register
  | ARstrf Register
  | ALoad Register Word5 Register
  | AStore Register Word5 Register
  | ABr Condition Label       -- NB: This is *different* than an MInstruction branch
  | ACall Register Label
  | AReturn Register Register
  | ASwi Word4
  | ARti

  -- Following are the pseudo-instructions
  | ASet Register Word16
  | ASetl Register Label    -- setting a data label
  | ASetrf Register Word16
  | AMove Register Register
  | ACmp Register Register
  | ANeg Register Register
  | ANot Register Register
  | ACon
  | ACoff
  | ACbon
  | ACcboff
  | AFlags Register
  | ALabel Label
  | ADlabel Label
  | AInteger Word16
  | AIntegerl Label         -- storing a labeled address in memory
  | ALpString String
  | ADskip Word16
  | AHalt
  | ANop
  deriving (Show, Eq, Generic, NFData)

-- A program is a sequence of instructions
type AssemblyProgram = [AInstruction]

----------------------------------------------------------
-- Pretty-printing

instance Renderable AInstruction where
  render (ASetlo d v)   = "SETLO(" ++ render d ++ "," ++ render v ++ ")"
  render (ASethi d v)   = "SETHI(" ++ render d ++ "," ++ render v ++ ")"
  render (AAnd d a b)   = "AND(" ++ render d ++ "," ++ render a ++ "," ++ render b ++ ")"
  render (AOr d a b)    = "OR(" ++ render d ++ "," ++ render a ++ "," ++ render b ++ ")"
  render (AXor d a b)   = "XOR(" ++ render d ++ "," ++ render a ++ "," ++ render b ++ ")"
  render (AAdd d a b)   = "ADD(" ++ render d ++ "," ++ render a ++ "," ++ render b ++ ")"
  render (ASub d a b)   = "SUB(" ++ render d ++ "," ++ render a ++ "," ++ render b ++ ")"
  render (AMul d a b)   = "MUL(" ++ render d ++ "," ++ render a ++ "," ++ render b ++ ")"
  render (AInc d del)   = "INC(" ++ render d ++ "," ++ render del ++ ")"
  render (ADec d del)   = "DEC(" ++ render d ++ "," ++ render del ++ ")"
  render (ALsl d b)     = "LSL(" ++ render d ++ "," ++ render b ++ ")"
  render (ALsr d b)     = "LSR(" ++ render d ++ "," ++ render b ++ ")"
  render (ALsl8 d b)    = "LSL8(" ++ render d ++ "," ++ render b ++ ")"
  render (ALsr8 d b)    = "LSR8(" ++ render d ++ "," ++ render b ++ ")"
  render (AAsl d b)     = "ASL(" ++ render d ++ "," ++ render b ++ ")"
  render (AAsr d b)     = "ASR(" ++ render d ++ "," ++ render b ++ ")"
  render (AFon v)       = "FON(" ++ render v ++ ")"
  render (AFoff v)      = "FOFF(" ++ render v ++ ")"
  render (AFset5 v)     = "FSET5(" ++ render v ++ ")"
  render (AFset4 v)     = "FSET4(" ++ render v ++ ")"
  render (ASavef d)     = "SAVEF(" ++ render d ++ ")"
  render (ARstrf d)     = "RSTRF(" ++ render d ++ ")"
  render (ALoad d o b)  = "LOAD(" ++ render d ++ "," ++ render o ++ "," ++ render b ++ ")"
  render (AStore d o b) = "STORE(" ++ render d ++ "," ++ render o ++ "," ++ render b ++ ")"
  render (ABr cnd trg)  = "B" ++ render cnd ++ "(" ++ trg ++ ")"
  render (ACall a b)    = "CALL(" ++ render a ++ "," ++ b ++ ")"
  render (AReturn a b)  = "RETURN(" ++ render a ++ "," ++ render b ++ ")"
  render (ASwi i)       = "SWI(" ++ render i ++ ")"
  render ARti           = "RTI()"

  render (ASet d v)     = "SET(" ++ render d ++ "," ++ render v ++ ")"
  render (ASetl d l)    = "SET(" ++ render d ++ "," ++ l ++ ")" -- NB: no "render l"
  render (ASetrf d v)   = "SETRF(" ++ render d ++ "," ++ render v ++ ")"
  render (AMove a b)    = "MOVE(" ++ render a ++ "," ++ render b ++ ")"
  render (ACmp a b)     = "CMP(" ++ render a ++ "," ++ render b ++ ")"
  render (ANeg d b)     = "NEG(" ++ render d ++ "," ++ render b ++ ")"
  render (ANot d b)     = "NOT(" ++ render d ++ "," ++ render b ++ ")"
  render ACon           = "CON()"
  render ACoff          = "COFF()"
  render ACbon          = "CBON()"
  render ACcboff        = "CCBOFF()"
  render (AFlags a)     = "FLAGS(" ++ render a ++ ")"
  render (ALabel l)     = "LABEL(" ++ l ++ ")"     -- NB: no "render l"; we don't want quotes
  render (ADlabel l)    = "DLABEL(" ++ l ++ ")"
  render (AInteger i)   = "INTEGER(" ++ render i ++ ")"
  render (AIntegerl l)  = "INTEGER(" ++ l ++ ")"
  render (ALpString s)  = "LP_STRING(" ++ render s ++ ")"
  render (ADskip n)     = "DSKIP(" ++ render n ++ ")"
  render AHalt          = "HALT()"
  render ANop           = "NOP()"

instance Renderable AssemblyProgram where
  render insns = unlines (map render insns)
