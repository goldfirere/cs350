-- -*- haskell -*-
-- The line above tells emacs to use Haskell-style syntax highlighting

{
{- Author: Steve Zdancewic, translated to Haskell (alex) by Richard Eisenberg
   File: Lexer.x

   An LLVMlite lexer.
-}

{-# OPTIONS_GHC -W -Wno-unused-imports -Wno-unused-top-binds -Wno-unused-matches #-}

module LLVM.Lexer ( lexLLVM ) where

import Prelude hiding ( EQ )

import Data.Int        ( Int16 )
import Data.List       ( stripPrefix )
import Data.List.Extra ( stripSuffix )

import Text.Read       ( readMaybe )

import LLVM.Token      ( Token(..) )

}

%wrapper "monad"   -- setting for alex

-- define abbreviations
@newline = [\r\n] | \r\n
$whitespace = [\t\ ]
$lowercase  = a-z
$uppercase  = A-Z
$character  = [$lowercase $uppercase]
$digit      = 0-9
@identifier = ($character | $digit | _) ($character | $digit | _ | \.)*
@string_lit = c \" [^ \"]* \"

-- define rules
:-

$whitespace+       ;
@newline+          ;
@string_lit        { withInput stringLiteral }
"*"                { tok STAR      }
","                { tok COMMA     }
":"                { tok COLON     }
"="                { tok EQUALS    }
"("                { tok LPAREN    }
")"                { tok RPAREN    }
"{"                { tok LBRACE    }
"}"                { tok RBRACE    }
"["                { tok LBRACKET  }
"]"                { tok RBRACKET  }
"i1"               { tok I1 }
"i8"               { tok I8 }
"i16"              { tok I16 }
"i32"              { tok I32 }
"i64"              { tok I64 }
"to"               { tok TO }
"br"               { tok BR }
"eq"               { tok EQ }
"ne"               { tok NE }
"or"               { tok OR }
"and"              { tok AND }
"add"              { tok ADD }
"sub"              { tok SUB }
"mul"              { tok MUL }
"xor"              { tok XOR }
"slt"              { tok SLT }
"sle"              { tok SLE }
"sgt"              { tok SGT }
"sge"              { tok SGE }
"shl"              { tok SHL }
"ret"              { tok RET }
"getelementptr"    { tok GEP }
"type"             { tok TYPE }
"null"             { tok NULL }
"lshr"             { tok LSHR }
"ashr"             { tok ASHR }
"call"             { tok CALL }
"icmp"             { tok ICMP }
"void"             { tok VOID }
"load"             { tok LOAD }
"entry"            { tok ENTRY }
"store"            { tok STORE }
"label"            { tok LABEL }
"global"           { tok GLOBAL }
"define"           { tok DEFINE }
"declare"          { tok DECLARE }
"external"         { tok EXTERNAL }
"alloca"           { tok ALLOCA }
"bitcast"          { tok BITCAST }

"%" "."? @identifier  { withInput (identifier UID) }
"@" "."? @identifier  { withInput (identifier GID) }

"x"                { tok CROSS } -- for Array types

"-"? $digit+       { withInput intLiteral }
@identifier        { withInput label }

";" [^ \n \r]* @newline            ;   -- comments
"declare" ([^ \n '\r'])* @newline  ;   -- declare acts as a comment for our IR

{

-- Handle constant tokens:
tok :: Token -> AlexInput -> Int -> Alex Token
tok t _ _ = pure t

-- useful combinator for token handlers that need to examine the matched string
withInput :: (String -> Alex Token) -> AlexInput -> Int -> Alex Token
withInput act (_, _, _, input) len = act (take len input)

-- Handle string literals:
stringLiteral :: String -> Alex Token
stringLiteral str
  | Just str_no_open_quote <- stripPrefix "c\"" str
  , Just str_no_ending     <- stripSuffix "\\00\"" str_no_open_quote
  = pure (STRING str_no_ending)

  | otherwise
  = alexError ("Invalid string literal: " ++ str)

-- local and global identifiers
identifier :: (String -> Token) -> String -> Alex Token
identifier ctor ('%' : '.' : rest) = pure $ ctor rest
identifier ctor ('%' : rest)       = pure $ ctor rest
identifier ctor ('@' : '.' : rest) = pure $ ctor rest
identifier ctor ('@' : rest)       = pure $ ctor rest
identifier _    str                = alexError ("Invalid identifier: " ++ str)

intLiteral :: String -> Alex Token
intLiteral str
  | Just num <- readMaybe str
  = pure (INT num)

  | otherwise
  = alexError ("Invalid numeric literal: " ++ str)

label :: String -> Alex Token
label = pure . LBL

-- This is called when the end of file is reached
alexEOF :: Alex Token
alexEOF = do
  (AlexPn _ line column, _, remaining_bytes, remaining_chars) <- alexGetInput
  if null remaining_bytes && null remaining_chars
    then pure EOF  -- success
    else alexError $ "Unexpected end of file at line " ++ show line ++ ", column " ++ show column

lexLLVM :: String -> Either String [Token]
lexLLVM input = runAlex input (go [])
  where
    go toks = do
      tok <- alexMonadScan
      case tok of
        EOF -> pure (reverse (EOF : toks))
        _   -> go (tok:toks)

}
