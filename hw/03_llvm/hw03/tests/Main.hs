{- Author: Richard Eisenberg
   File: Main.hs

   Runs the student-visible component of the testsuite.
   This will work only on powerpuff, as it uses resources available there.
-}

{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -W #-}

module Main where

import CS350.CompileM    ( runCompileM )
import CS350.Renderable  ( render )

import LLVM.Lexer        ( lexLLVM )
import LLVM.Parser       ( parse )

import BackEnd           ( compileLLVM )

import Grading.StudentTests  ( TestCase, testCases )

import Control.DeepSeq   ( force )
import Control.Exception ( Exception, IOException, ErrorCall
                         , try, catch, displayException, throwIO, evaluate )
import Control.Monad     ( when, unless )

import System.Directory  ( getPermissions, executable )
import System.Exit       ( exitFailure, exitSuccess )
import System.FilePath   ( (</>), (<.>) )
import System.Process    ( readProcess )

import Text.Read         ( readMaybe )

import System.IO.Temp    ( withSystemTempDirectory )

-- print an indented string, to better get your attention:
puts :: String -> IO ()
puts str = putStrLn $ "    " ++ str

printBanner :: IO ()
printBanner = puts "========================================================"

succeedWithBanner :: Int -> IO a
succeedWithBanner n = do printBanner
                         puts $ "Hooray! All " ++ show n ++ " tests pass."
                         printBanner
                         exitSuccess

failWithBanner :: IO a
failWithBanner = do printBanner
                    puts "Rats! Something went wrong with a test."
                    printBanner
                    exitFailure

------------------------------------------------------
-- Testing infrastructure

data TestFailure = TestFailure
  deriving Show
instance Exception TestFailure

-- Runs the given function over all the inputs (regardless of exceptions).
-- Returns the number of successful attempts.
mapAndRecoverM_ :: (a -> IO b) -> [a] -> IO Int
mapAndRecoverM_ _ [] = pure 0
mapAndRecoverM_ action (input:inputs) = do
  result <- try (action input)
  case result of
    Left (_ :: TestFailure) -> mapAndRecoverM_ action inputs
    Right _                 -> do n <- mapAndRecoverM_ action inputs
                                  pure (n+1)

------------------------------------------------------
-- Test runner

runTest :: FilePath -> TestCase -> IO ()
runTest dir (prog_name, expected_result) = do
  puts $ "Testing '" ++ prog_name ++ "':"
  program <- readFile ("/home/rae/pub/llprograms" </> prog_name <.> "ll")
             `catch` \ (e :: IOException) -> do
               puts $ "  Unable to read program '" ++ prog_name ++ "':"
               puts $ "  " ++ displayException e
               throwIO TestFailure
  lexed <- case lexLLVM program of
    Left err -> do puts $ "  Lexer error: " ++ err
                   throwIO TestFailure
    Right toks -> pure toks

  compile_result <- runCompileM $ do
    llvm <- parse lexed
    compileLLVM llvm

  assembly <- case compile_result of
    Left err -> do puts $ "  Compiler error: " ++ err
                   throwIO TestFailure
    Right assm -> evaluate (force assm)
                  `catch` \ (e :: ErrorCall) -> do
                    puts $ "  Call to 'error': " ++ displayException e
                    puts "  (You shouldn't call 'error'; use compileError instead.)"
                    throwIO TestFailure

  puts "  Compilation succeeded."

  let hera_file_name = dir </> prog_name <.> "hera"
      hera_file_cts  = "#include <HERA.h>\n\nvoid HERA_main() {\n" ++ render assembly ++ "\n}\n"
  writeFile hera_file_name hera_file_cts

  output <- readProcess "/home/rae/pub/herasim" [hera_file_name] ""

  last_line <- case lines output of
    [] -> do puts "  Empty output from herasim. Please post on Piazza."
             throwIO TestFailure
    ls -> pure (last ls)

  result <- case readMaybe last_line of
    Just r -> pure r
    _      -> do puts "  Result cannot be read as a number. Please post on Piazza."
                 throwIO TestFailure

  unless (result == expected_result) $ do
    puts $ "  Expected " ++ show expected_result ++ " but got " ++ show result
    throwIO TestFailure

  puts $ "  Correct execution result of " ++ show result ++ "."

runTests :: [TestCase] -> IO ()
runTests tests = do
  results <- withSystemTempDirectory "hw03-hera" $ \ dir ->
    mapAndRecoverM_ (runTest dir) tests

  if results == length tests
    then succeedWithBanner results
    else do printBanner
            puts $ "Passed " ++ show results ++ " out of " ++ show (length tests) ++ " tests."
            failWithBanner

----------------------------------------------
-- Main

main :: IO ()
main = do
    -- cabal spits out lots of stuff. This will get your attention.
  printBanner
  puts "CS350 TEST OUTPUT:"
  printBanner

  herasim_perms <- getPermissions "/home/rae/pub/herasim"
                   `catch` \ (e :: IOException) -> do
                     puts $ "Unable to find /home/rae/pub/herasim: " ++ displayException e
                     puts "Are you executing on a CS Linux machine (such as powerpuff)?"
                     puts "This testing module works only in the BMC CS Linux environment."
                     failWithBanner

  when (not (executable herasim_perms)) $ do
    puts "Unable to execute /home/rae/pub/herasim"
    puts "Are you on powerpuff? If so, post this bug on Piazza."
    failWithBanner

  runTests testCases
