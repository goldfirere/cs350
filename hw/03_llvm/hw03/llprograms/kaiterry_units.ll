define i64 @main(i64 %argc, i8** %argv) {
  %one = add i64 0, 1            ; 1
  %none = sub i64 0, %one        ; -1
  %five = add i64 0, 5           ; 5
  %nfive = sub i64 0, %five      ; -5
  %ten = add i64 0, 10           ; 10
  %nten = sub i64 0, %ten        ; -10
  %sixty = add i64 0, 60         ; 60
  %1 = add i64 %ten, %five       ; 15
  %2 = sub i64 %ten, %five       ; 5
  %3 = mul i64 %ten, %five       ; 50
  %4 = shl i64 %ten, %one        ; 20
  %5 = lshr i64 %nten, %sixty    ; 0 | 15
  %6 = ashr i64 %nten, %one      ; -5
  %7 = and i64 %five, %one       ; 1
  %8 = or i64 %ten, %five        ; 15
  %9 = xor i64 %ten, %five       ; 15
  %t1 = icmp eq i64 %1, 15       ; true
  %t2 = icmp eq i64 %2, 5        ; true
  %t3 = icmp eq i64 %3, 50       ; true
  %t4 = icmp eq i64 %4, 20       ; true
  %t5 = icmp eq i64 %5, 15       ; false | true
  %t6 = icmp eq i64 %6, %nfive   ; true
  %t7 = icmp eq i64 %7, 1        ; true
  %t8 = icmp eq i64 %8, 15       ; true
  %t9 = icmp eq i64 %9, 15       ; true
  %t9a = icmp ne i64 %9, 14      ; true
  %t9b = icmp slt i64 %9, 16     ; true
  %t9c = icmp sle i64 %9, 15     ; true
  %t9d = icmp sgt i64 %9, 14     ; true
  %t9e = icmp sgt i64 %9, %none  ; true
  %t9f = icmp sge i64 %9, 15     ; true
  %r1 = and i1 %t1, 1            ; true
  %r2 = and i1 %t2, %r1          ; true
  %r3 = and i1 %t3, %r2          ; true
  %r4 = and i1 %t4, %r3          ; true
  %r5 = and i1 %t5, %r4          ; false | true
  %r6 = and i1 %t6, %r5          ; false | true
  %r7 = and i1 %t7, %r6          ; false | true
  %r8 = and i1 %t8, %r7          ; false | true
  %r9 = and i1 %t9, %r8          ; false | true
  %r9a = and i1 %t9a, %r9        ; false | true
  %r9b = and i1 %t9b, %r9a       ; false | true
  %r9c = and i1 %t9c, %r9b       ; false | true
  %r9d = and i1 %t9d, %r9c       ; false | true
  %r9e = and i1 %t9e, %r9d       ; false | true
  %r9f = and i1 %t9f, %r9e       ; false | true
  br i1 %r9f, label %then1, label %else1
then1:
  ret i64 1
else1:
  ret i64 0
}

