%node = type {i64, %node*}

@head = global %node {i64 1, %node* @next1}
@next1 = global %node {i64 -2, %node* @next2}
@next2 = global %node {i64 10, %node* @next3}
@next3 = global %node {i64 6, %node* @next4}
@next4 = global %node {i64 -6, %node* @next5}
@next5 = global %node {i64 2, %node* @tail}
@tail = global %node {i64 4, %node* null}

define i64 @sumList() {
  %count = alloca i64                  ; int count
  store i64 0, i64* %count             ; count = 0
  %sum = alloca i64                    ; int sum
  store i64 0, i64* %sum               ; sum = 0
  %ptr = alloca i64*                   ; int* ptr
  %ptrN = getelementptr %node, %node* @head, i32 0, i32 0   ; &(@head->data)
  store i64* %ptrN, i64** %ptr         ; ptr = &(@head->data)
  br label %loop2
loop2:
  %currnode = load i64*, i64** %ptr    ; ptr
  %val = load i64, i64* %currnode      ; *ptr
  %currSum = load i64, i64* %sum       ; sum
  %newSum = add i64 %val, %currSum     ; sum + *ptr
  store i64 %newSum, i64* %sum         ; sum = sum + *ptr
  %tmp = load i64, i64* %count         ; count
  %tmp2 = add i64 %tmp, 1              ; count + 1
  store i64 %tmp2, i64* %count         ; count = count + 1
  %cmp = icmp eq i64 %tmp2, 7          ; count == 7
  br i1 %cmp, label %end2, label %update_pointer  ; if(count == 7)
update_pointer:
  %currptr = load i64*, i64** %ptr     ; ptr
  %tempptr = bitcast i64* %currptr to %node*   ; (node*)ptr
  %link = getelementptr %node, %node* %tempptr, i32 0, i32 1   ; &(((node*)ptr)->next)
  %newptr = load %node*, %node** %link                         ; ((node*)ptr)->next
  %castedptr = bitcast %node* %newptr to i64*                  ; (int*)(((node*)ptr)->next)
  store i64* %castedptr, i64** %ptr                            ; ptr = (int*)(((node*)ptr)->next)
  br label %loop2                                              ; continue
end2:
  %returnVal = load i64, i64* %sum
  ret i64 %returnVal
}


define i64 @main(i64 %argc, i8** %arcv) {
  %1 = call i64 @sumList()
  ret i64 %1
}
