{- Author: Steve Zdancewic, translated to Haskell by Richard Eisenberg
   File: Driver.hs

   Main interface to functions in the HW03 codebase.
-}

{-# OPTIONS_GHC -W #-}

module Driver where

import CS350.CompileM   ( CompileM, liftIO, compileError )
import CS350.Renderable ( render )

import HERA.Assembly    ( AssemblyProgram )

import qualified Assembler
import qualified Simulator

import qualified BackEnd

import LLVM.Lite        ( Program )
import LLVM.Lexer       ( lexLLVM )
import LLVM.Parser      ( parse )
import LLVM.Interpreter ( interp_prog )

import Control.DeepSeq    ( force )
import Control.Exception  ( ErrorCall(..), evaluate, tryJust )
import Control.Monad      ( when )
import System.Environment ( getEnv )
import System.FilePath    ( dropExtension, replaceExtension, takeBaseName )
import System.IO.Error    ( isDoesNotExistError )
import System.Process     ( callProcess )
import Text.Printf        ( printf )

-- configuration flags ------------------------------------------------------

data Settings = Set { interpretLL  :: Bool  -- run the ll interpreter?
                    , printLL      :: Bool  -- print the parsed ll code?
                    , compile      :: Bool  -- run the compiler?
                    , printHERA    :: Bool  -- print the generated HERA code?
                    , writeHERA    :: Bool  -- write an .hera file with the compiled code?
                    , heraC        :: Bool  -- use the HERA-C backend?
                    , assemble     :: Bool  -- run the student-written assembler?
                    , simulate     :: Bool  -- run the student-written simulator?
                    }                       -- (If this last one is false but `assemble`
                                            -- is true, just print the assembled program.)

-- suitable defaults
defaultSettings :: Settings
defaultSettings = Set { interpretLL  = False
                      , printLL      = False
                      , compile      = True
                      , printHERA    = False
                      , writeHERA    = True
                      , heraC        = False
                      , assemble     = False
                      , simulate     = False }

-- terminal output ----------------------------------------------------------

printBanner :: String -> IO ()
printBanner s =
  let dashes 0 = ""
      dashes n = "-" ++ (dashes (n-1)) in
  putStrLn (printf "%s %s\n" (dashes (79 - (length s))) s)

printLLProgram :: String -> Program -> IO ()
printLLProgram prog_name ll_ast = do
  printBanner (prog_name ++ ".ll")
  putStrLn (render ll_ast)

printHERAProgram :: String -> AssemblyProgram -> IO ()
printHERAProgram prog_name assembly = do
  printBanner (prog_name ++ ".hera")
  putStrLn (render assembly)


-- running the generated code -----------------------------------------------

interpret :: Program -> [String] -> CompileM ()
interpret program args = do
  liftIO $ printBanner "interpeter output"
  result <- interp_prog program args
  liftIO $ putStrLn (render result)


-- compiler pipeline --------------------------------------------------------

{- These functions implement the compiler pipeline for a single ll file:
     - parse the file
     - compile to a .hera file using backend.ml
     - do further processing as requested
-}
parseLL :: FilePath -> CompileM Program
parseLL filename = do
  program_string <- liftIO $ readFile filename
  tokens <- case lexLLVM program_string of
              Left lex_err -> compileError $ "Lexical error: " ++ lex_err
              Right tokens -> pure tokens
  parse tokens

processLL :: Settings -> FilePath -> Program -> CompileM ()
processLL settings filepath program = do
  let program_name = takeBaseName filepath

  when (printLL settings) $
    liftIO $ printLLProgram program_name program

  -- Optionally interpret it using the reference interperter.
  when (interpretLL settings) $
    interpret program []

  when (compile settings) $ do
    liftIO $ printBanner "compiler output"
    assembly <- BackEnd.compileLLVM program
    liftIO $ putStrLn ""

    let hera_string = render assembly
    when (printHERA settings) $ do
      liftIO $ printBanner "compiled HERA"
      liftIO $ putStrLn hera_string

    when (writeHERA settings) $ do
      let hera_file = replaceExtension filepath ".hera"
          hera_file_string = "#include <HERA.h>\n\nvoid HERA_main() {\n" ++ hera_string ++ "\n}\n"
      liftIO $ writeFile hera_file hera_file_string

      when (heraC settings) $ do
        hera_include_folder <- do
          result <- liftIO $ tryJust (\ ex -> if isDoesNotExistError ex then Just () else Nothing)
                                     (getEnv "HERA_C_DIR")
          case result of
            Left _ -> compileError $ "Cannot use HERA-C because I don't know where to find HERA.h\n" ++
                                     "Perhaps add `export HERA_C_DIR=/path/to/HERA-C` to your\n" ++
                                     "~/.bash_profile file (but edit that directory to be correct)."
            Right dir -> pure dir
        liftIO $ printBanner "HERA compilation output"
        liftIO $ callProcess "g++" [ "-I" ++ hera_include_folder  -- find HERA-C folder
                                   , "-w"                         -- suppress warnings
                                   , "-x", "c++"                  -- compile like C++
                                   , "-o", dropExtension filepath -- set output filename
                                   , hera_file ]

          -- run executable
        liftIO $ printBanner "HERA execution output"
        liftIO $ callProcess (dropExtension filepath) []

    when (assemble settings) $ do
      machine_program <- do
        result <- liftIO $ tryJust (\ (ErrorCall msg) -> Just msg)
                                   (evaluate $ force $ Assembler.assemble assembly)
                             -- the "evaluate" and "force" allow us to catch exceptions
                             -- thrown by the assembler. Otherwise, Haskell's laziness
                             -- might leave the exceptions unthrown.
        case result of
          Left err -> compileError $ "Assembler error: " ++ err
          Right mp -> pure mp

      if simulate settings
        then liftIO $ do
               printBanner "Running simulator"
               machine <- Simulator.load machine_program
               Simulator.run machine
               Simulator.printMachine machine
        else liftIO $ do
               printBanner "Result of assembler"
               putStrLn (render machine_program)
