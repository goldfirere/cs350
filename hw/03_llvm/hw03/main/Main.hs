{- Author: Richard Eisenberg
   File: Main.hs

   Main program for HW03.
-}

module Main where

import CS350.CompileM     ( runCompileM )
import CS350.Args         ( ArgumentDescriptor, lookupArg, printArgDescriptor
                          , handleFilename, handleArgs, printUsage )

import Driver ( Settings(..), defaultSettings, parseLL, processLL )

import Data.List          ( intercalate )

import System.Environment ( getArgs, getProgName )
import System.Exit        ( exitFailure, exitSuccess )
import System.FilePath    ( isExtensionOf )

import Text.Printf        ( printf )

--------------------------------------------------------------------
-- Command-line arguments

-- All arguments accepted by this program
allArguments :: [ArgumentDescriptor Settings]
allArguments = [ ( "--interpret", "Runs the reference interpreter and prints result."
                 , \s -> s { interpretLL = True } )
               , ( "--no-interpret", "Disables running the reference interpreter. (default)"
                 , \s -> s { interpretLL = False } )
               , ( "--print-ll", "Print the parsed LL code."
                 , \s -> s { printLL = True } )
               , ( "--no-print-ll", "Do not print the parsed LL code. (default)"
                 , \s -> s { printLL = False } )
               , ( "--compile", "Run the student-written compiler. (default)"
                 , \s -> s { compile = True } )
               , ( "--no-compile", "Do not run the student-written compiler."
                 , \s -> s { compile = False } )
               , ( "--print-hera", "Print the generated HERA code."
                 , \s -> s { printHERA = True } )
               , ( "--no-print-hera", "Do not print the generated HERA code. (default)"
                 , \s -> s { printHERA = False } )
               , ( "--write-hera", "Write a .hera file in the same place as .ll file. (default)"
                 , \s -> s { writeHERA = True } )
               , ( "--no-write-hera", "Do not write a .hera file."
                 , \s -> s { writeHERA = False } )
               , ( "--hera-c", "Compile the .hera file with HERA-C. Works only with --write-hera."
                 , \s -> s { heraC = True } )
               , ( "--no-hera-c", "Do not use HERA-C. (default)"
                 , \s -> s { heraC = False } )
               , ( "--assemble", "Use the student-written assembler."
                 , \s -> s { assemble = True } )
               , ( "--no-assemble", "Do not use the student-written assembler. (default)"
                 , \s -> s { assemble = False } )
               , ( "--simulate", "Use the student-written simulator. Works only with --assemble."
                 , \s -> s { simulate = True } )
               , ( "--no-simulate", "Do not simulate. With --assemble, prints the machine code. (default)"
                 , \s -> s { simulate = False } )
               ]

main :: IO ()
main = do
  (settings, rest_args)    <- handleArgs allArguments defaultSettings
  (ll_filepath, rest_args) <- handleFilename (printUsage allArguments) ".ll" rest_args
  case rest_args of
    [] -> pure ()
    _  -> do putStrLn $ "Unknown arguments: " ++ intercalate " " rest_args
             printUsage allArguments

  result <- runCompileM $ do
    program <- parseLL ll_filepath
    processLL settings ll_filepath program

  case result of
    Left err -> do putStrLn $ "Compile error: " ++ err
                   exitFailure
    Right _  -> exitSuccess
