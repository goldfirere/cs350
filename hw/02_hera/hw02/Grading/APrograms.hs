{- Author: Richard Eisenberg
   File: APrograms.hs

   Sample programs to test the assembler.
-}

{-# LANGUAGE OverloadedLists #-}
{-# OPTIONS_GHC -W #-}

module Grading.APrograms where

import Grading.Infra    ( TestAP(..) )
import HERA.Assembly    ( AssemblyProgram, AInstruction(..) )
import HERA.Machine     ( MachineProgram(..), DataSegment(..), Target(..), MInstruction(..) )
import HERA.Base        ( Register(..), Condition(..), fpAlt, pcRet, fp, sp )

easyTAPs :: [TestAP]
easyTAPs = [ TAP "dataSegs" dataSegsAP dataSegsMP dataSegsMP
           , TAP "nop" nop nopMP nopMP
           , TAP "sethi" sethi sethiMP sethiMP
           , TAP "set" set setMP setMP
           , TAP "cbon" cbon cbonMP cbonMP
           , TAP "neg" neg negMP negMP
           , TAP "somedata" somedata somedataMP somedataMP
           , TAP "string" string stringMP stringMP
           , TAP "loopy" loopy loopyMP loopyMPOpt
           , TAP "branches" branches branchesMP branchesMPOpt
           , TAP "doubleLabel" doubleLabel doubleLabelMP doubleLabelMPOpt
           ]

-- this is just because type-checking with OverloadedLists can be difficult
ap :: AssemblyProgram -> AssemblyProgram
ap = id

dataSegsAP = [ ADlabel "l1"
             , AInteger 1
             , AInteger 2
             , AInteger 3
             , ADlabel "l2"
             , ALpString "hello"
             , ADskip 5
             , ADlabel "l3"
             , AInteger 4
             , AInteger 5 ]

dataSegsMP = MP {text = [], dayta = [DS {location = 49152, contents = [1,2,3,5,104,101,108,108,111]},DS {location = 49166, contents = [4,5]}]}

nop = ap [ ANop ]
nopMP = MP {text = [MBr None (Relative 1)], dayta = [DS {location = 49152, contents = []}]}

sethi = ap [ ASet R1 0xabcd
         , ASet R2 5
         , AAdd R3 R1 R2 ]
sethiMP = MP {text = [MSetlo R1 (-51),MSethi R1 171,MSetlo R2 5,MSethi R2 0,MAdd R3 R1 R2], dayta = [DS {location = 49152, contents = []}]}

set = ap [ ASet R1 5 ]
setMP = MP {text = [MSetlo R1 5,MSethi R1 0], dayta = [DS {location = 49152, contents = []}]}

cbon = ap [ ACbon ]
cbonMP = MP {text = [MFon 16], dayta = [DS {location = 49152, contents = []}]}

neg = ap [ ASet R1 5
         , ANeg R1 R1 ]
negMP = MP {text = [MSetlo R1 5,MSethi R1 0,MFon 8,MSub R1 R0 R1], dayta = [DS {location = 49152, contents = []}]}

somedata = ap [ ADlabel "dats"
              , AInteger 3
              , AInteger 5
              , ASetl R1 "dats"
              , ALoad R2 0 R1
              , ALoad R3 1 R1 ]
somedataMP = MP {text = [MSetlo R1 0,MSethi R1 192,MLoad R2 0 R1,MLoad R3 1 R1], dayta = [DS {location = 49152, contents = [3,5]}]}

string = ap [ ADlabel "str"
            , ALpString "howdy"
            , ASetl R1 "str"
            , ALoad R2 0 R1 ]
stringMP = MP {text = [MSetlo R1 0,MSethi R1 192,MLoad R2 0 R1], dayta = [DS {location = 49152, contents = [5,104,111,119,100,121]}]}

loopy = ap [ ALabel "loop"
           , ABr None "loop" ]
loopyMP = MP {text = [MSetlo R11 0,MSethi R11 0,MBr None (Absolute R11)], dayta = [DS {location = 49152, contents = []}]}
loopyMPOpt = MP {text = [MBr None (Relative 0)], dayta = [DS {location = 49152, contents = []}]}

branches = ap [ ALabel "l1"
              , ABr Z "l3"
              , ALabel "l2"
              , ABr Nz "l1"
              , ABr G "l2"
              , ALabel "l3"
              , AAdd R1 R1 R1 ]
branchesMP = MP {text = [MSetlo R11 9,MSethi R11 0,MBr Z (Absolute R11),MSetlo R11 0,MSethi R11 0,MBr Nz (Absolute R11),MSetlo R11 3,MSethi R11 0,MBr G (Absolute R11),MAdd R1 R1 R1], dayta = [DS {location = 49152, contents = []}]}
branchesMPOpt = MP {text = [MBr Z (Relative 3),MBr Nz (Relative (-1)),MBr G (Relative (-1)),MAdd R1 R1 R1], dayta = [DS {location = 49152, contents = []}]}

doubleLabel = ap [ ALabel "l1"
                 , ALabel "l2"
                 , ABr None "l1"
                 , ABr None "l2"
                 , ABr None "l3"
                 , ABr None "l4"
                 , ALabel "l3"
                 , ALabel "l4"
                 , ANop ]
doubleLabelMP = MP {text = [MSetlo R11 0,MSethi R11 0,MBr None (Absolute R11),MSetlo R11 0,MSethi R11 0,MBr None (Absolute R11),MSetlo R11 12,MSethi R11 0,MBr None (Absolute R11),MSetlo R11 12,MSethi R11 0,MBr None (Absolute R11),MBr None (Relative 1)], dayta = [DS {location = 49152, contents = []}]}
doubleLabelMPOpt = MP {text = [MBr None (Relative 0),MBr None (Relative (-1)),MBr None (Relative 2),MBr None (Relative 1),MBr None (Relative 1)], dayta = [DS {location = 49152, contents = []}]}

-------------------------------------------------------------------

hardTAPs :: [TestAP]
hardTAPs = [ TAP "t1" t1 t1MP t1MPOpt
           , TAP "t2" t2 t2MP t2MPOpt
           , TAP "t3" t3 t3MP t3MPOpt
           , TAP "t4" t4 t4MP t4MPOpt
           , TAP "t9" t9 t9MP t9MPOpt
           , TAP "t10" t10 t10MP t10MPOpt
           , TAP "t11" t11 t11MP t11MPOpt
           , TAP "t18" t18 t18MP t18MPOpt
           , TAP "t22" t22 t22MP t22MPOpt
           , TAP "fac" facAP facMP facMPOpt
           , TAP "callAndReturn" callAndReturn callAndReturnMP callAndReturnMPOpt
           ]

t1 = ap [ASethi R1 128,ASethi R2 240,ASetlo R3 16,AAdd R4 R1 R2,ASub R5 R3 R1,ALsr R6 R2,ALsr8 R7 R4,AAsr R1 R1,AAsl R8 R2,AAsr R10 R6,ASetlo R9 8,ASetlo R9 (-56),AInc R8 9,ADec R1 14,AHalt]

t1MP = MP {text = [MSethi R1 128,MSethi R2 240,MSetlo R3 16,MAdd R4 R1 R2,MSub R5 R3 R1,MLsr R6 R2,MLsr8 R7 R4,MAsr R1 R1,MAsl R8 R2,MAsr R10 R6,MSetlo R9 8,MSetlo R9 (-56),MInc R8 9,MDec R1 14,MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11)], dayta = [DS {location = 49152, contents = []}]}
t1MPOpt = MP {text = [MSethi R1 128,MSethi R2 240,MSetlo R3 16,MAdd R4 R1 R2,MSub R5 R3 R1,MLsr R6 R2,MLsr8 R7 R4,MAsr R1 R1,MAsl R8 R2,MAsr R10 R6,MSetlo R9 8,MSetlo R9 (-56),MInc R8 9,MDec R1 14,MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11)], dayta = [DS {location = 49152, contents = []}]}

t2 = ap [ACbon,ASetlo R1 (-1),ASethi R1 255,ASetlo R2 2,AMul R1 R1 R2,AHalt]
t2MP = MP {text = [MFon 16,MSetlo R1 (-1),MSethi R1 255,MSetlo R2 2,MMul R1 R1 R2,MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11)], dayta = [DS {location = 49152, contents = []}]}
t2MPOpt = MP {text = [MFon 16,MSetlo R1 (-1),MSethi R1 255,MSetlo R2 2,MMul R1 R1 R2,MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11)], dayta = [DS {location = 49152, contents = []}]}

t3 = ap [ASet R1 61644,ASet R2 43746,AXor R1 R1 R2,AXor R2 R1 R2,AXor R1 R1 R2,AHalt]
t3MP = MP {text = [MSetlo R1 (-52),MSethi R1 240,MSetlo R2 (-30),MSethi R2 170,MXor R1 R1 R2,MXor R2 R1 R2,MXor R1 R1 R2,MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11)], dayta = [DS {location = 49152, contents = []}]}
t3MPOpt = MP {text = [MSetlo R1 (-52),MSethi R1 240,MSetlo R2 (-30),MSethi R2 170,MXor R1 R1 R2,MXor R2 R1 R2,MXor R1 R1 R2,MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11)], dayta = [DS {location = 49152, contents = []}]}

t4 = ap [ASet R4 16,ASet R5 65521,ALsl R4 R4,ALsr R3 R4,ALsl R5 R5,ALsl8 R7 R5,ALsr R5 R7,ASet R10 100,ALsl8 R10 R10,ALsr8 R9 R10,ASet R8 65436,AAsl R8 R8,AAsr R4 R4,AHalt]
t4MP = MP {text = [MSetlo R4 16,MSethi R4 0,MSetlo R5 (-15),MSethi R5 255,MLsl R4 R4,MLsr R3 R4,MLsl R5 R5,MLsl8 R7 R5,MLsr R5 R7,MSetlo R10 100,MSethi R10 0,MLsl8 R10 R10,MLsr8 R9 R10,MSetlo R8 (-100),MSethi R8 255,MAsl R8 R8,MAsr R4 R4,MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11)], dayta = [DS {location = 49152, contents = []}]}
t4MPOpt = MP {text = [MSetlo R4 16,MSethi R4 0,MSetlo R5 (-15),MSethi R5 255,MLsl R4 R4,MLsr R3 R4,MLsl R5 R5,MLsl8 R7 R5,MLsr R5 R7,MSetlo R10 100,MSethi R10 0,MLsl8 R10 R10,MLsr8 R9 R10,MSetlo R8 (-100),MSethi R8 255,MAsl R8 R8,MAsr R4 R4,MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11)], dayta = [DS {location = 49152, contents = []}]}

t9 = ap [AFset4 15,ABr Ge "Not_s_xor_v",ALabel "Jump_back_v",ABr L "s_xor_v",ALabel "Jump_back_sv",ABr Le "s_xor_v_or_z",ALabel "Jump_back_s_xor_v_or_z",ABr G "s_xor_v_or_z_not",ALabel "Jump_back_s_xor_v_or_z_not",ABr Ule "Notc_or_z",ALabel "Jump_back_notc_or_z",ABr Ug "Not_Notc_or_z",ALabel "Jump_back_Not_Notc_or_z",ABr None "End_of_test",ALabel "Not_s_xor_v",ASet R1 1,AFoff 4,ABr None "Jump_back_v",ALabel "s_xor_v",ASet R2 2,ABr None "Jump_back_sv",ALabel "s_xor_v_or_z",ASet R3 3,AFoff 3,ABr None "Jump_back_s_xor_v_or_z",ALabel "s_xor_v_or_z_not",ASet R4 4,AFon 2,ABr None "Jump_back_s_xor_v_or_z_not",ALabel "Notc_or_z",ASet R5 5,AFoff 2,ABr None "Jump_back_notc_or_z",ALabel "Not_Notc_or_z",ASet R6 6,ABr None "Jump_back_Not_Notc_or_z",ALabel "End_of_test",AHalt]
t9MP = MP {text = [MFset4 15,MSetlo R11 22,MSethi R11 0,MBr Ge (Absolute R11),MSetlo R11 28,MSethi R11 0,MBr L (Absolute R11),MSetlo R11 33,MSethi R11 0,MBr Le (Absolute R11),MSetlo R11 39,MSethi R11 0,MBr G (Absolute R11),MSetlo R11 45,MSethi R11 0,MBr Ule (Absolute R11),MSetlo R11 51,MSethi R11 0,MBr Ug (Absolute R11),MSetlo R11 56,MSethi R11 0,MBr None (Absolute R11),MSetlo R1 1,MSethi R1 0,MFoff 4,MSetlo R11 4,MSethi R11 0,MBr None (Absolute R11),MSetlo R2 2,MSethi R2 0,MSetlo R11 7,MSethi R11 0,MBr None (Absolute R11),MSetlo R3 3,MSethi R3 0,MFoff 3,MSetlo R11 10,MSethi R11 0,MBr None (Absolute R11),MSetlo R4 4,MSethi R4 0,MFon 2,MSetlo R11 13,MSethi R11 0,MBr None (Absolute R11),MSetlo R5 5,MSethi R5 0,MFoff 2,MSetlo R11 16,MSethi R11 0,MBr None (Absolute R11),MSetlo R6 6,MSethi R6 0,MSetlo R11 19,MSethi R11 0,MBr None (Absolute R11),MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11)], dayta = [DS {location = 49152, contents = []}]}
t9MPOpt = MP {text = [MFset4 15,MBr Ge (Relative 7),MBr L (Relative 10),MBr Le (Relative 12),MBr G (Relative 15),MBr Ule (Relative 18),MBr Ug (Relative 21),MBr None (Relative 23),MSetlo R1 1,MSethi R1 0,MFoff 4,MBr None (Relative (-9)),MSetlo R2 2,MSethi R2 0,MBr None (Relative (-11)),MSetlo R3 3,MSethi R3 0,MFoff 3,MBr None (Relative (-14)),MSetlo R4 4,MSethi R4 0,MFon 2,MBr None (Relative (-17)),MSetlo R5 5,MSethi R5 0,MFoff 2,MBr None (Relative (-20)),MSetlo R6 6,MSethi R6 0,MBr None (Relative (-22)),MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11)], dayta = [DS {location = 49152, contents = []}]}


t10 = ap [ABr Nz "If_z_is_not_on",ALabel "Jump_back_z",ABr Z "If_z_is_on",ALabel "Jump_back_nz",ABr Nv "If_v_is_not_on",ALabel "Jump_back_v",ABr V "If_v_is_on",ALabel "Jump_back_nv",ABr Nc "If_c_is_not_on",ALabel "Jump_back_c",ABr C "If_c_is_on",ALabel "Jump_back_nc",ABr Ns "If_s_is_not_on",ALabel "Jump_back_s",ABr S "If_s_is_on",ALabel "Jump_back_ns",ABr None "End_of_tests",ALabel "If_z_is_on",ASet R1 1,AFoff 2,ABr None "Jump_back_nz",ALabel "If_z_is_not_on",ASet R2 2,AFon 2,ABr None "Jump_back_z",ALabel "If_c_is_on",ASet R3 3,AFoff 8,ABr None "Jump_back_nc",ALabel "If_c_is_not_on",ASet R4 4,AFon 8,ABr None "Jump_back_c",ALabel "If_s_is_on",ASet R5 5,AFoff 1,ABr None "Jump_back_ns",ALabel "If_s_is_not_on",ASet R6 6,AFon 1,ABr None "Jump_back_s",ALabel "If_v_is_on",ASet R7 7,AFoff 4,ABr None "Jump_back_nv",ALabel "If_v_is_not_on",ASet R8 8,AFon 4,ABr None "Jump_back_v",ALabel "End_of_tests",AHalt]
t10MP = MP {text = [MSetlo R11 33,MSethi R11 0,MBr Nz (Absolute R11),MSetlo R11 27,MSethi R11 0,MBr Z (Absolute R11),MSetlo R11 69,MSethi R11 0,MBr Nv (Absolute R11),MSetlo R11 63,MSethi R11 0,MBr V (Absolute R11),MSetlo R11 45,MSethi R11 0,MBr Nc (Absolute R11),MSetlo R11 39,MSethi R11 0,MBr C (Absolute R11),MSetlo R11 57,MSethi R11 0,MBr Ns (Absolute R11),MSetlo R11 51,MSethi R11 0,MBr S (Absolute R11),MSetlo R11 75,MSethi R11 0,MBr None (Absolute R11),MSetlo R1 1,MSethi R1 0,MFoff 2,MSetlo R11 6,MSethi R11 0,MBr None (Absolute R11),MSetlo R2 2,MSethi R2 0,MFon 2,MSetlo R11 3,MSethi R11 0,MBr None (Absolute R11),MSetlo R3 3,MSethi R3 0,MFoff 8,MSetlo R11 18,MSethi R11 0,MBr None (Absolute R11),MSetlo R4 4,MSethi R4 0,MFon 8,MSetlo R11 15,MSethi R11 0,MBr None (Absolute R11),MSetlo R5 5,MSethi R5 0,MFoff 1,MSetlo R11 24,MSethi R11 0,MBr None (Absolute R11),MSetlo R6 6,MSethi R6 0,MFon 1,MSetlo R11 21,MSethi R11 0,MBr None (Absolute R11),MSetlo R7 7,MSethi R7 0,MFoff 4,MSetlo R11 12,MSethi R11 0,MBr None (Absolute R11),MSetlo R8 8,MSethi R8 0,MFon 4,MSetlo R11 9,MSethi R11 0,MBr None (Absolute R11),MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11)], dayta = [DS {location = 49152, contents = []}]}
t10MPOpt = MP {text = [MBr Nz (Relative 13),MBr Z (Relative 8),MBr Nv (Relative 35),MBr V (Relative 30),MBr Nc (Relative 17),MBr C (Relative 12),MBr Ns (Relative 23),MBr S (Relative 18),MBr None (Relative 33),MSetlo R1 1,MSethi R1 0,MFoff 2,MBr None (Relative (-10)),MSetlo R2 2,MSethi R2 0,MFon 2,MBr None (Relative (-15)),MSetlo R3 3,MSethi R3 0,MFoff 8,MBr None (Relative (-14)),MSetlo R4 4,MSethi R4 0,MFon 8,MBr None (Relative (-19)),MSetlo R5 5,MSethi R5 0,MFoff 1,MBr None (Relative (-20)),MSetlo R6 6,MSethi R6 0,MFon 1,MBr None (Relative (-25)),MSetlo R7 7,MSethi R7 0,MFoff 4,MBr None (Relative (-32)),MSetlo R8 8,MSethi R8 0,MFon 4,MBr None (Relative (-37)),MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11)], dayta = [DS {location = 49152, contents = []}]}

t11 = ap [ACbon,ASet R4 10,ASet R5 1,ASet R6 1,ASet R7 1,ASet R8 3,ALabel "Fibonacci_Computation_Start_while",ASub R0 R4 R8,ABr S "Fibonacci_Computation_Loop_Exit",AMove R7 R5,AAdd R5 R5 R6,AMove R6 R7,AInc R8 1,ABr None "Fibonacci_Computation_Start_while",ALabel "Fibonacci_Computation_Loop_Exit",AHalt]
t11MP = MP {text = [MFon 16,MSetlo R4 10,MSethi R4 0,MSetlo R5 1,MSethi R5 0,MSetlo R6 1,MSethi R6 0,MSetlo R7 1,MSethi R7 0,MSetlo R8 3,MSethi R8 0,MSub R0 R4 R8,MSetlo R11 22,MSethi R11 0,MBr S (Absolute R11),MOr R7 R5 R0,MAdd R5 R5 R6,MOr R6 R7 R0,MInc R8 1,MSetlo R11 11,MSethi R11 0,MBr None (Absolute R11),MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11)], dayta = [DS {location = 49152, contents = []}]}
t11MPOpt = MP {text = [MFon 16,MSetlo R4 10,MSethi R4 0,MSetlo R5 1,MSethi R5 0,MSetlo R6 1,MSethi R6 0,MSetlo R7 1,MSethi R7 0,MSetlo R8 3,MSethi R8 0,MSub R0 R4 R8,MBr S (Relative 6),MOr R7 R5 R0,MAdd R5 R5 R6,MOr R6 R7 R0,MInc R8 1,MBr None (Relative (-6)),MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11)], dayta = [DS {location = 49152, contents = []}]}

t18 = ap [ACbon,ASet R1 1,ASet R2 17,ASet R3 39,ALabel "FIRST_LOOP",ALsr R11 R2,ABr C "BREAK_OUT",ALsr R11 R3,ABr C "BREAK_OUT",ASet R11 2,AMul R1 R11 R1,ALsr R2 R2,ALsr R3 R3,ALabel "BREAK_OUT",AMove R4 R2,AMove R5 R3,ASet R6 1,ASet R7 0,ASet R8 0,ASet R9 1,ALabel "SECOND_LOOP",ALsr R11 R2,ABr C "BREAK_OUT_TWO",ALsr R2 R2,AAsr R11 R6,ABr C "ELSEFIRSTWHILE",AAsr R11 R7,ABr C "ELSEFIRSTWHILE",AAsr R6 R6,AAsr R7 R7,ABr None "ENDFIRSTWHILE",ALabel "ELSEFIRSTWHILE",AAdd R11 R6 R5,AAsr R6 R11,ASub R11 R7 R4,AAsr R7 R11,ALabel "ENDFIRSTWHILE",ALabel "BREAK_OUT_TWO",ALabel "THIRD_LOOP",ASub R11 R2 R3,ABr Z "BREAK_OUT_THREE",ALsr R11 R3,ABr C "ELIF",ALsr R3 R3,AAsr R11 R8,ABr C "NESTEDELSE",AAsr R11 R9,ABr C "NESTEDELSE",AAsr R8 R8,AAsr R9 R9,ABr None "ENDIFS",ALabel "NESTEDELSE",AAdd R11 R8 R5,AAsr R8 R11,ASub R11 R9 R4,AAsr R9 R11,ABr None "ENDIFS",ALabel "ELIF",ASub R11 R2 R3,ABr Nc "ELSE",AMove R11 R2,AMove R2 R3,AMove R3 R11,AMove R11 R6,AMove R6 R8,AMove R8 R11,AMove R11 R7,AMove R7 R9,AMove R9 R11,ABr None "ENDIFS",ALabel "ELSE",ASub R3 R3 R2,ASub R8 R8 R6,ASub R9 R9 R7,ALabel "ENDIFS",ALabel "BREAK_OUT_THREE",AMul R10 R1 R2,AMove R3 R10,AHalt]
t18MP = MP {text = [MFon 16,MSetlo R1 1,MSethi R1 0,MSetlo R2 17,MSethi R2 0,MSetlo R3 39,MSethi R3 0,MLsr R11 R2,MSetlo R11 20,MSethi R11 0,MBr C (Absolute R11),MLsr R11 R3,MSetlo R11 20,MSethi R11 0,MBr C (Absolute R11),MSetlo R11 2,MSethi R11 0,MMul R1 R11 R1,MLsr R2 R2,MLsr R3 R3,MOr R4 R2 R0,MOr R5 R3 R0,MSetlo R6 1,MSethi R6 0,MSetlo R7 0,MSethi R7 0,MSetlo R8 0,MSethi R8 0,MSetlo R9 1,MSethi R9 0,MLsr R11 R2,MSetlo R11 52,MSethi R11 0,MBr C (Absolute R11),MLsr R2 R2,MAsr R11 R6,MSetlo R11 48,MSethi R11 0,MBr C (Absolute R11),MAsr R11 R7,MSetlo R11 48,MSethi R11 0,MBr C (Absolute R11),MAsr R6 R6,MAsr R7 R7,MSetlo R11 52,MSethi R11 0,MBr None (Absolute R11),MAdd R11 R6 R5,MAsr R6 R11,MSub R11 R7 R4,MAsr R7 R11,MSub R11 R2 R3,MSetlo R11 100,MSethi R11 0,MBr Z (Absolute R11),MLsr R11 R3,MSetlo R11 81,MSethi R11 0,MBr C (Absolute R11),MLsr R3 R3,MAsr R11 R8,MSetlo R11 74,MSethi R11 0,MBr C (Absolute R11),MAsr R11 R9,MSetlo R11 74,MSethi R11 0,MBr C (Absolute R11),MAsr R8 R8,MAsr R9 R9,MSetlo R11 100,MSethi R11 0,MBr None (Absolute R11),MAdd R11 R8 R5,MAsr R8 R11,MSub R11 R9 R4,MAsr R9 R11,MSetlo R11 100,MSethi R11 0,MBr None (Absolute R11),MSub R11 R2 R3,MSetlo R11 97,MSethi R11 0,MBr Nc (Absolute R11),MOr R11 R2 R0,MOr R2 R3 R0,MOr R3 R11 R0,MOr R11 R6 R0,MOr R6 R8 R0,MOr R8 R11 R0,MOr R11 R7 R0,MOr R7 R9 R0,MOr R9 R11 R0,MSetlo R11 100,MSethi R11 0,MBr None (Absolute R11),MSub R3 R3 R2,MSub R8 R8 R6,MSub R9 R9 R7,MMul R10 R1 R2,MOr R3 R10 R0,MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11)], dayta = [DS {location = 49152, contents = []}]}
t18MPOpt = MP {text = [MFon 16,MSetlo R1 1,MSethi R1 0,MSetlo R2 17,MSethi R2 0,MSetlo R3 39,MSethi R3 0,MLsr R11 R2,MBr C (Relative 8),MLsr R11 R3,MBr C (Relative 6),MSetlo R11 2,MSethi R11 0,MMul R1 R11 R1,MLsr R2 R2,MLsr R3 R3,MOr R4 R2 R0,MOr R5 R3 R0,MSetlo R6 1,MSethi R6 0,MSetlo R7 0,MSethi R7 0,MSetlo R8 0,MSethi R8 0,MSetlo R9 1,MSethi R9 0,MLsr R11 R2,MBr C (Relative 13),MLsr R2 R2,MAsr R11 R6,MBr C (Relative 6),MAsr R11 R7,MBr C (Relative 4),MAsr R6 R6,MAsr R7 R7,MBr None (Relative 5),MAdd R11 R6 R5,MAsr R6 R11,MSub R11 R7 R4,MAsr R7 R11,MSub R11 R2 R3,MBr Z (Relative 31),MLsr R11 R3,MBr C (Relative 14),MLsr R3 R3,MAsr R11 R8,MBr C (Relative 6),MAsr R11 R9,MBr C (Relative 4),MAsr R8 R8,MAsr R9 R9,MBr None (Relative 21),MAdd R11 R8 R5,MAsr R8 R11,MSub R11 R9 R4,MAsr R9 R11,MBr None (Relative 16),MSub R11 R2 R3,MBr Nc (Relative 11),MOr R11 R2 R0,MOr R2 R3 R0,MOr R3 R11 R0,MOr R11 R6 R0,MOr R6 R8 R0,MOr R8 R11 R0,MOr R11 R7 R0,MOr R7 R9 R0,MOr R9 R11 R0,MBr None (Relative 4),MSub R3 R3 R2,MSub R8 R8 R6,MSub R9 R9 R7,MMul R10 R1 R2,MOr R3 R10 R0,MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11)], dayta = [DS {location = 49152, contents = []}]}


t22 = ap [ADlabel "X",AInteger 6,ADlabel "Y",AInteger 7,ADlabel "Z",AInteger 8,ADlabel "ArrayOfFourInts",ADskip 4,AInteger 1,AInteger 2,AInteger 3,AInteger 4,ADlabel "ArrayOfCubedInts",ADskip 4,ASetl R1 "X",ASetl R2 "Y",ASetl R3 "Z",ALoad R4 0 R1,ALoad R5 0 R2,ALoad R6 0 R3,AInc R4 5,AInc R5 5,AInc R6 5,AStore R4 2 R3,AStore R5 3 R3,AStore R6 4 R3,ACbon,ACall R12 "CubedInts",ASetl R14 "ArrayOfCubedInts",ALoad R7 0 R14,ALoad R8 1 R14,ALoad R9 2 R14,ALoad R10 3 R14,AHalt,ALabel "CubedInts",ASetl R1 "ArrayOfFourInts",AInc R1 4,ASetl R2 "ArrayOfCubedInts",ASet R3 4,ALabel "CubeNextInt",ADec R3 1,ABr L "Done",ALoad R7 0 R1,ALoad R8 0 R1,AMul R7 R7 R7,AMul R7 R7 R8,AStore R7 0 R2,AInc R1 1,AInc R2 1,ABr None "CubeNextInt",ALabel "Done",AReturn R12 R13]
t22MP = MP {text = [MSetlo R1 0,MSethi R1 192,MSetlo R2 1,MSethi R2 192,MSetlo R3 2,MSethi R3 192,MLoad R4 0 R1,MLoad R5 0 R2,MLoad R6 0 R3,MInc R4 5,MInc R5 5,MInc R6 5,MStore R4 2 R3,MStore R5 3 R3,MStore R6 4 R3,MFon 16,MSetlo R13 28,MSethi R13 0,MCall R12 R13,MSetlo R14 11,MSethi R14 192,MLoad R7 0 R14,MLoad R8 1 R14,MLoad R9 2 R14,MLoad R10 3 R14,MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11),MSetlo R1 3,MSethi R1 192,MInc R1 4,MSetlo R2 11,MSethi R2 192,MSetlo R3 4,MSethi R3 0,MDec R3 1,MSetlo R11 49,MSethi R11 0,MBr L (Absolute R11),MLoad R7 0 R1,MLoad R8 0 R1,MMul R7 R7 R7,MMul R7 R7 R8,MStore R7 0 R2,MInc R1 1,MInc R2 1,MSetlo R11 35,MSethi R11 0,MBr None (Absolute R11),MReturn R12 R13], dayta = [DS {location = 49152, contents = [6,7,8]},DS {location = 49159, contents = [1,2,3,4]},DS {location = 49167, contents = []}]}
t22MPOpt = MP {text = [MSetlo R1 0,MSethi R1 192,MSetlo R2 1,MSethi R2 192,MSetlo R3 2,MSethi R3 192,MLoad R4 0 R1,MLoad R5 0 R2,MLoad R6 0 R3,MInc R4 5,MInc R5 5,MInc R6 5,MStore R4 2 R3,MStore R5 3 R3,MStore R6 4 R3,MFon 16,MSetlo R13 28,MSethi R13 0,MCall R12 R13,MSetlo R14 11,MSethi R14 192,MLoad R7 0 R14,MLoad R8 1 R14,MLoad R9 2 R14,MLoad R10 3 R14,MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11),MSetlo R1 3,MSethi R1 192,MInc R1 4,MSetlo R2 11,MSethi R2 192,MSetlo R3 4,MSethi R3 0,MDec R3 1,MBr L (Relative 9),MLoad R7 0 R1,MLoad R8 0 R1,MMul R7 R7 R7,MMul R7 R7 R8,MStore R7 0 R2,MInc R1 1,MInc R2 1,MBr None (Relative (-9)),MReturn R12 R13], dayta = [DS {location = 49152, contents = [6,7,8]},DS {location = 49159, contents = [1,2,3,4]},DS {location = 49167, contents = []}]}


callAndReturn = ap [ACbon,ASet R1 171,ASet R2 205,ACall R12 "combineR3",AHalt,ALabel "combineR3",ALsl8 R1 R1,AAdd R3 R1 R2,AReturn R12 R13]
callAndReturnMP = MP {text = [MFon 16,MSetlo R1 (-85),MSethi R1 0,MSetlo R2 (-51),MSethi R2 0,MSetlo R13 11,MSethi R13 0,MCall R12 R13,MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11),MLsl8 R1 R1,MAdd R3 R1 R2,MReturn R12 R13], dayta = [DS {location = 49152, contents = []}]}
callAndReturnMPOpt = MP {text = [MFon 16,MSetlo R1 (-85),MSethi R1 0,MSetlo R2 (-51),MSethi R2 0,MSetlo R13 11,MSethi R13 0,MCall R12 R13,MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11),MLsl8 R1 R1,MAdd R3 R1 R2,MReturn R12 R13], dayta = [DS {location = 49152, contents = []}]}

facAP :: AssemblyProgram
facAP = [ ADlabel "arg_num"
        , AInteger 5

        , ALabel "main"

        , ASetl R1 "arg_num"   -- R1 points to data
        , ALoad R2 0 R1        -- we are computing fac(R2)

        , AMove fpAlt sp       -- Step 1 in "how to call"
        , AInc sp 4            -- Step 1 in "how to call"
        , AStore R2 3 fpAlt    -- Step 2 in "how to call"
        , ACall fpAlt "fac"    -- Step 4 in "how to call" (no static links here)
        , ALoad R3 3 fpAlt     -- Step 5 in "how to call"; R3 is result
        , ADec sp 4            -- Step 5 in "how to call"; restore sp

        , AHalt                -- end of "main"

        , ALabel "fac"
    -- R1 = parameter passed in; result of multiplication
    -- R2 = R1 - 1 (before rec. call); result of recursive call

        , AInc sp 2            -- Step 1 in "how to define"; 2 locals
        , AStore pcRet 0 fp    -- Step 2 in "how to define"; store return address
        , AStore fpAlt 1 fp    -- Step 2 in "how to define"; store old fp
        , AStore R1 4 fp       -- Step 2 in "how to define"; store old R1
        , AStore R2 5 fp       -- Step 2 in "how to define"; store old R2

        , ALoad R1 3 fp        -- Get parameter into R1
        , ASub R0 R1 R0        -- Compare R1 against 0
        , ABr Le "fac_base"    -- If R1 <= 0, branch to "fac_base"

        , AMove R2 R1          -- copy R1 to R2
        , ADec R2 1            -- decrement parameter for recursive call
        , AMove fpAlt sp       -- Step 1 in "how to call"
        , AInc sp 4            -- Step 1 in "how to call"
        , AStore R2 3 fpAlt    -- Step 2 in "how to call"
        , ACall fpAlt "fac"    -- Step 4 in "how to call"
        , ALoad R2 3 fpAlt     -- Step 5 in "how to call"; store result in R2
        , ADec sp 4            -- Step 5 in "how to call": restore sp
        , AMul R1 R1 R2        -- multiply recursive result by parameter
        , AStore R1 3 fp       -- Step 4 in "how to define"; return result

        , ALabel "fac_return"
        , ALoad R2 5 fp        -- Step 5 in "how to define"
        , ALoad R1 4 fp        -- Step 5 in "how to define"
        , ALoad fpAlt 1 fp     -- Step 5 in "how to define"
        , ALoad pcRet 0 fp     -- Step 5 in "how to define"
        , ADec sp 2            -- Step 5 in "how to define"
        , AReturn fpAlt pcRet

        , ALabel "fac_base"
        , ASet R1 1            -- result is 1
        , AStore R1 3 fp       -- store as return value
        , ABr None "fac_return" ]

facMP = MP {text = [MSetlo R1 0,MSethi R1 192,MLoad R2 0 R1,MOr R12 R15 R0,MInc R15 4,MStore R2 3 R12,MSetlo R13 14,MSethi R13 0,MCall R12 R13,MLoad R3 3 R12,MDec R15 4,MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11),MInc R15 2,MStore R13 0 R14,MStore R12 1 R14,MStore R1 4 R14,MStore R2 5 R14,MLoad R1 3 R14,MSub R0 R1 R0,MSetlo R11 42,MSethi R11 0,MBr Le (Absolute R11),MOr R2 R1 R0,MDec R2 1,MOr R12 R15 R0,MInc R15 4,MStore R2 3 R12,MSetlo R13 14,MSethi R13 0,MCall R12 R13,MLoad R2 3 R12,MDec R15 4,MMul R1 R1 R2,MStore R1 3 R14,MLoad R2 5 R14,MLoad R1 4 R14,MLoad R12 1 R14,MLoad R13 0 R14,MDec R15 2,MReturn R12 R13,MSetlo R1 1,MSethi R1 0,MStore R1 3 R14,MSetlo R11 36,MSethi R11 0,MBr None (Absolute R11)], dayta = [DS {location = 49152, contents = [5]}]}
facMPOpt = MP {text = [MSetlo R1 0,MSethi R1 192,MLoad R2 0 R1,MOr R12 R15 R0,MInc R15 4,MStore R2 3 R12,MSetlo R13 14,MSethi R13 0,MCall R12 R13,MLoad R3 3 R12,MDec R15 4,MSetlo R11 (-83),MSethi R11 222,MBr None (Absolute R11),MInc R15 2,MStore R13 0 R14,MStore R12 1 R14,MStore R1 4 R14,MStore R2 5 R14,MLoad R1 3 R14,MSub R0 R1 R0,MBr Le (Relative 19),MOr R2 R1 R0,MDec R2 1,MOr R12 R15 R0,MInc R15 4,MStore R2 3 R12,MSetlo R13 14,MSethi R13 0,MCall R12 R13,MLoad R2 3 R12,MDec R15 4,MMul R1 R1 R2,MStore R1 3 R14,MLoad R2 5 R14,MLoad R1 4 R14,MLoad R12 1 R14,MLoad R13 0 R14,MDec R15 2,MReturn R12 R13,MSetlo R1 1,MSethi R1 0,MStore R1 3 R14,MBr None (Relative (-9))], dayta = [DS {location = 49152, contents = [5]}]}
