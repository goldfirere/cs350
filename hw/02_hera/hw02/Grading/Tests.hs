{- Author: Richard Eisenberg
   File: Tests.hs

   Actual testing functions for HW02.
   See also Grading.MPrograms and Grading.APrograms
-}

{-# LANGUAGE OverloadedLists, DuplicateRecordFields, BangPatterns, OverloadedStrings,
             ScopedTypeVariables #-}

{-# OPTIONS_GHC -W -Wno-overflowed-literals #-}

module Grading.Tests where

import HERA.Machine ( MachineProgram(..), DataSegment(..) )
import HERA.Assembly ( AInstruction(..), AssemblyProgram )

import Data.List    ( genericLength )
import Control.Exception ( evaluate, SomeException, catch )
import System.Timeout ( timeout )

import Grading.Infra      ( Trace(..), snapshot, compareStates
                          , compareMachinePrograms, TestProgram(..), TestAP(..) )

import Simulator    ( step, load )
import Assembler    ( assemble )

average :: Fractional a => [a] -> a
average ns = sum ns / genericLength ns

-- run user code, catching exceptions and limiting to 30 seconds;
-- returns 0 if there is trouble
userCode :: IO Double -> IO Double
userCode action = do
  m_result <- timeout numMicroseconds $
              catch action (\ (_ :: SomeException) -> pure 0)
  pure $ case m_result of
    Just score -> score
    Nothing    -> 0
  where
    numMicroseconds = 30 * 10^6

testProgram :: TestProgram -> IO Double
testProgram (TP _ mp (MkTrace states)) = do
  machine <- load mp
  go [] machine states
  where
    go acc _ [] = evaluate (average acc)
    go acc machine (state : states) = do
      done <- step machine
      user_state <- snapshot machine

        -- the ! below forces evaluation of the right-hand side, meaning
        -- that we can garbage-collect the user_state as we proceed.
        -- Without this, I got heap overflows.
      let !score = compareStates user_state state

      if done
        then evaluate (average (score : replicate (length states) 0 ++ acc))
        else go (score : acc) machine states

-------------------------------------------------

-- in the IO monad for timeout and exception-handling
testAP :: TestAP -> IO Double
testAP (TAP _ ap mp1 mp2) = userCode $ do
  let user_mp = assemble ap
  evaluate $ max (compareMachinePrograms user_mp mp1)
                 (compareMachinePrograms user_mp mp2)

dataSegsAP :: AssemblyProgram
dataSegsAP = [ ADlabel "l1"
             , AInteger 1
             , AInteger 2
             , AInteger 3
             , ADlabel "l2"
             , ALpString "hello"
             , ADskip 5
             , ADlabel "l3"
             , AInteger 4
             , AInteger 5 ]

dataSegsMP :: MachineProgram
dataSegsMP = MP {text = [], dayta = [DS {location = 49152, contents = [1,2,3,5,104,101,108,108,111]},DS {location = 49166, contents = [4,5]}]}
