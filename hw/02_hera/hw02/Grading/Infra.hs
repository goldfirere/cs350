{- Author: Richard Eisenberg
   File: Infra.hs

   Infrastructure for testing HW02
-}

{-# LANGUAGE GeneralizedNewtypeDeriving, StandaloneDeriving #-}
{-# OPTIONS_GHC -W #-}

module Grading.Infra where

import HERA.Assembly  ( AssemblyProgram )
import HERA.Machine   ( MachineProgram(..), MInstruction(..), Target(..), DataSegment(..) )
import HERA.Base      ( Register(..), Condition(..), dataBottom )
import Simulator      ( Machine(..), load, step, defaultWord, sFlagIndex
                      , zFlagIndex )
import CS350.WordN    ( Word4 )

import Data.List      ( genericLength )
import Data.IORef     ( readIORef )
import Data.Vector    ( Vector )
import qualified Data.Vector as I
import Data.Word      ( Word16 )

import LED.EditDistance ( levenshteinDistance, defaultEditCosts )

import Control.DeepSeq ( force )

{---------------------------------------------
  I used this code to check my output against HERA-C's.
  If you use this, you will want to
    import System.Process
    import HERA.Lexer
    import HERA.Parser
    import System.Directory.Extra   ( from the `extra` package )
    import System.FilePath
    import Data.Foldable

-- returns paths to all the files in the tests subdirectory
findTestingFiles :: IO [FilePath]
findTestingFiles = filter (".hera" `isExtensionOf`) <$> listContents "./tests"

-- Not used during testing, per se, but you may enjoy it.
printResults :: FilePath -> IO ()
printResults input = do
  program <- readFile input
  let lexed     = lexHera program
      parsed    = parse lexed
      assembled = assemble parsed
  machine <- load assembled
  run machine

  putStrLn "\n\n\n\n\n\n"
  putStrLn "***************************"
  putStrLn input
  putStrLn "***************************"
  printMachine machine

invokeHera :: FilePath -> IO ()
invokeHera input = do
  let base = takeBaseName input
      target = "scratch" </> base
  callProcess "g++" ["-I/Users/rae/work/350/hera/HERA-C", "-w", "-x", "c++", input, "-o", target]

  putStrLn "\n\n\n\n\n\n\n"
  putStrLn "*************************"
  putStrLn input
  putStrLn "*************************"
  callProcess target []
----------------------------------------------}

------------------------------------------------
-- Testing the simulator via Traces

-- a MachineProgram along with its Trace
data TestProgram = TP String MachineProgram Trace

-- A (pure) state of a machine
data MachineState = MS { p_memory    :: ![(Word16, Word16)]   -- (address, value) pairs, in order
                       , p_registers :: ![(Word4, Word16)]    -- (reg#, value) pairs, in order
                       , p_sflag     :: Bool
                       , p_zflag     :: Bool
                       , p_pc        :: Word16 }
                    deriving (Show)

-- A trace holds a sequence of machine states
data Trace = MkTrace [MachineState]
  deriving (Show)

-- take a snapshot of a machine by storing its state
snapshot :: Machine -> IO MachineState
snapshot (M { memory    = mem
            , registers = regs
            , flags     = flgs
            , pcRef     = pc_ref }) = do
  pure_mem <- I.freeze mem
  pure_regs <- I.freeze regs
  pure_flgs <- I.freeze flgs
  pc_val <- readIORef pc_ref

  pure (MS { p_memory    = force $ foldMap (mk_pair defaultWord) (index pure_mem)
           , p_registers = force $ foldMap (mk_pair 0) (index pure_regs)
           , p_sflag     = pure_flgs I.! sFlagIndex
           , p_zflag     = pure_flgs I.! zFlagIndex
           , p_pc        = pc_val })

  where
    index = I.imap (,)

    mk_pair deflt (loc, value)
      | value == deflt = []
      | otherwise      = [(fromIntegral loc, value)]

-- get the trace of running a machine to completion
trace :: Machine -> IO Trace
trace machine = go []
  where
    go acc = do
      done <- step machine
      state <- snapshot machine

      pc_val <- readIORef (pcRef machine)
      let pc_oob = fromIntegral pc_val >= I.length (instructions machine)

      if done || pc_oob
        then pure (MkTrace (reverse (state : acc)))
        else go (state : acc)

-- get the trace of a MachineProgram
traceProgram :: MachineProgram -> IO Trace
traceProgram mp = do
  machine <- load mp
  trace machine

data Comparison = Comp { matches :: Integer   -- this many were equal
                       , checks  :: Integer   -- this many were checked
                       }

instance Semigroup Comparison where
  Comp { matches = m1, checks = c1 } <> Comp { matches = m2, checks = c2 }
    = Comp { matches = m1 + m2, checks = c1 + c2 }

instance Monoid Comparison where
  mempty = Comp { matches = 0, checks = 0 }

-- Equality scores range from 0 (not equal at all) to 1 (identical)
type EqualityScore = Double

-- a successful comparison
compSuccess :: Comparison
compSuccess = Comp { matches = 1, checks = 1 }

-- A failed comparison
compFailure :: Comparison
compFailure = Comp { matches = 0, checks = 1 }

-- compute an equality score relating two states.
-- Each non-default element (either non-0xbaba or non-0)
-- is compared and used to compute the equality score.
compareStates :: MachineState -> MachineState -> EqualityScore
compareStates (MS { p_memory    = mem1
                  , p_registers = regs1
                  , p_sflag     = sflag1
                  , p_zflag     = zflag1
                  , p_pc        = pc1 })
              (MS { p_memory    = mem2
                  , p_registers = regs2
                  , p_sflag     = sflag2
                  , p_zflag     = zflag2
                  , p_pc        = pc2 }) =
  (fromIntegral total_same) / (fromIntegral total_measured)
  where
    mem_comp = comparePairs defaultWord mem1 mem2
    reg_comp = comparePairs 0 regs1 regs2
      -- only look at relevant flags
    s_comp   = comp sflag1 sflag2
    z_comp   = comp zflag1 zflag2
    pc_comp  = comp pc1 pc2

    Comp { matches = total_same, checks = total_measured } = mconcat [ mem_comp
                                                                     , reg_comp
                                                                     , s_comp
                                                                     , z_comp
                                                                     , pc_comp ]

-- compare two elements
comp :: Eq a => a -> a -> Comparison
comp x y
  | x == y    = compSuccess
  | otherwise = compFailure

comparePairs :: (Ord index, Eq elt) => elt -> [(index, elt)] -> [(index, elt)] -> Comparison
comparePairs _     [] [] = mempty
comparePairs deflt (_ : rest1) [] = compFailure <> comparePairs deflt rest1 []
comparePairs deflt [] (_ : rest2) = compFailure <> comparePairs deflt [] rest2
comparePairs deflt all1@((index1, elt1) : rest1) all2@((index2, elt2) : rest2)
  | index1 < index2
  = compFailure <> comparePairs deflt rest1 all2
  | index1 > index2
  = compFailure <> comparePairs deflt all1 rest2
  -- indices are equal. compare elements
  | elt1 == deflt
  , elt2 == deflt
    -- these elements are boring
  = comparePairs deflt rest1 rest2
  | elt1 == elt2
  = compSuccess <> comparePairs deflt rest1 rest2
  | otherwise
  = compFailure <> comparePairs deflt rest1 rest2

-- compute an equality score for traces
compareTraces :: Trace -> Trace -> EqualityScore
compareTraces (MkTrace states1) (MkTrace states2) = go [] states1 states2
  where
    -- could almost use zipWith, but that aborts if one list runs out
    go :: [EqualityScore] -> [MachineState] -> [MachineState] -> EqualityScore
    go acc (s1:ss1) (s2:ss2) = go (compareStates s1 s2 : acc) ss1 ss2
    go acc []       (_ :ss2) = go (0 : acc)                []  ss2
    go acc (_ :ss1) []       = go (0 : acc)                ss1 []
    go acc []       []       = sum acc / genericLength acc

-------------------------------------------------------------------------
-- Testing the assembler via Levenshtein edit distance

-- two machine programs, because we test both optimizing assemblers and unoptimizing ones
data TestAP = TAP String AssemblyProgram MachineProgram MachineProgram

-- need these instances for edit distance.
deriving instance Ord Register
deriving instance Ord Condition
deriving instance Ord Target
deriving instance Ord MInstruction

-- returns a number between 0 and 1 indicating the similarity score
-- of the two machine programs. This is done by evaluating the
-- Levenshtein edit distance of the instructions, as well as the
-- memory layout. No attention is paid to the choice of layout of
-- data segments.
compareMachinePrograms :: MachineProgram -> MachineProgram -> Double
compareMachinePrograms (MP { text = text1, dayta = dayta1 })
                       (MP { text = text2, dayta = dayta2 })
  = (fromIntegral $ text_score + memory_score) / (fromIntegral $ text_max + memory_max)
  where
    text_distance = levenshteinDistance defaultEditCosts (I.toList text1) (I.toList text2)
    text_max      = max (I.length text1) (I.length text2)
    text_score    = max (text_max - text_distance) 0

    memory1 = layoutMemory dayta1
    memory2 = layoutMemory dayta2
    memory_distance = levenshteinDistance defaultEditCosts memory1 memory2
    memory_max      = max (length memory1) (length memory2)
    memory_score    = max (memory_max - memory_distance) 0

layoutMemory :: Vector DataSegment -> [Word16]
layoutMemory = go dataBottom [] . I.toList
  where
    go _memory_loc acc_words [] = reverse acc_words

    go memory_loc acc_words all_segs@(DS { location = next_loc, contents = words } : segs)
      | memory_loc < next_loc
      = go (memory_loc + 1) (0x0000 : acc_words) all_segs
      | memory_loc == next_loc
      = go (memory_loc + fromIntegral (I.length words)) (reverse (I.toList words) ++ acc_words) segs
      | otherwise  -- give up
      = reverse acc_words
