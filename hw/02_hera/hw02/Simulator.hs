{- Author: <your name here>, with skeleton by Richard Eisenberg
   File: Simulator.hs

   A simulator for the HERA-2.4 machine
-}

{-# LANGUAGE GADTs #-}
  -- We're not really using GADTs here, but sometimes type inference wants this
  -- to be enabled when writing functions over IOVectors.

{-# OPTIONS_GHC -W -Wno-unused-imports #-}
  -- enable standard warnings, but don't care about unused imports

module Simulator where

import Data.Bits             ( (.&.), (.|.), xor, shift, shiftL, shiftR, bit, testBit )
import Data.Char             ( ord )
import Data.Int              ( Int16, Int8 )
import qualified Data.Vector as I         -- "I" for immutable
import Data.Vector           ( Vector )   -- get Vector without qualification
import qualified Data.Vector.Mutable as M
import Data.Vector.Mutable   ( IOVector )
import Data.Word             ( Word16, Word8 )
import Data.IORef            ( IORef, newIORef, readIORef, writeIORef, modifyIORef )
                                -- an IORef is a mutable pointer
import Text.Printf           ( printf )

import Control.Monad         ( when, forM_ )

import HERA.Base             ( Register, Condition(..), fp, memorySize, numRegisters, haltedPC
                             , dataBottom )
import HERA.Machine          ( MInstruction(..), Target(..), MachineProgram(..)
                             , DataSegment(..) )
import CS350.WordN           ( Word5, bitList, listBits )
import CS350.Panic           ( panic, unimplemented )

-- Initialize with something easily recognizable:
defaultWord :: Word16
defaultWord = 0xbaba

-- We have 5 flags
numFlags :: Int
numFlags = 5

-- These are the flag indices:
sFlagIndex, zFlagIndex, vFlagIndex, cFlagIndex, cbFlagIndex :: Int
sFlagIndex = 0
zFlagIndex = 1
vFlagIndex = 2
cFlagIndex = 3
cbFlagIndex = 4

-- These flag-index/value pairs are pinned:
pinnedFlags :: [(Int, Bool)]
pinnedFlags = [(vFlagIndex, False), (cFlagIndex, False), (cbFlagIndex, True)]

------------------------------------------------------------
-- Machine definition

-- The state of our machine is laid out here:
data Machine = M { memory       :: IOVector Word16     -- the machine's memory
                 , registers    :: IOVector Word16     -- the register file
                 , flags        :: IOVector Bool       -- the flags
                 , pcRef        :: IORef Word16        -- the program counter
                 , instructions :: Vector MInstruction -- the program (immutable)
                 }

-- create a Machine to run a given assembled program
load :: MachineProgram -> IO Machine
load (MP { text = prog, dayta = dat }) = do
  mem  <- M.replicate memorySize defaultWord
  regs <- M.replicate numRegisters 0
  flgs <- M.replicate numFlags False
  pc   <- newIORef 0   -- PC starts at 0

  -- write the data to the data segment of memory
  let write_data base offset datum = M.write mem (base + offset) datum
      write_segment (DS { location = data_loc
                        , contents = bytes })
        = I.imapM_ (write_data (fromIntegral data_loc)) bytes
  I.mapM_ write_segment dat

  -- register 0 is always 0:
  M.write regs 0 0
  writePinnedFlags flgs

  -- construct the machine:
  pure (M { memory       = mem
          , registers    = regs
          , flags        = flgs
          , pcRef        = pc
          , instructions = prog })

-- print the machine state to the user
printMachine :: Machine -> IO ()
printMachine (M { memory    = mem
                , registers = regs
                , flags     = flgs
                , pcRef     = pc }) = do
  putStrLn "*** Non-0xbaba memory locations:"
  forM_ [0..memorySize-1] print_memory_loc

  putStrLn ""
  putStrLn "*** Registers:"
  forM_ [0..numRegisters-1] print_register

  putStrLn ""
  putStr "*** Flags: "
  s <- M.read flgs sFlagIndex
  z <- M.read flgs zFlagIndex
  printf "s = %s; z = %s\n" (show s) (show z)

  pc_val <- readIORef pc
  printf "*** Program counter: 0x%04x\n" pc_val

  where
    print_memory_loc addr = do
      val <- M.read mem addr
      when (val /= defaultWord) (printf "    0x%04x: 0x%04x\n" addr val)

    print_register reg = do
      val <- M.read regs reg
      printf "   r%2d = 0x%04x = %d\n" reg val val

-------------------------------------------------------------
-- Utility functions (feel free to add more)

-- Given a Register, get its index in the register vector
registerIndex :: Register -> Int
registerIndex = fromEnum  -- use the magic of the Enum class, to good effect

-- Convert a string into a length-prefixed sequence of words.
toLpString :: String -> [Word16]
toLpString str
  = fromIntegral (length str) : map (fromIntegral . ord) str

-- Given a reference to the flags register, set the pinned flags.
writePinnedFlags :: IOVector Bool -> IO ()
writePinnedFlags flgs = mapM_ write_flag pinnedFlags
  where
    write_flag (index, val) = M.write flgs index val

-------------------------------------------------------------
-- Main simulator

-- This steps the machine by one step forward.
--   1. Fetch the instruction from the location given by the PC
--   2. Process the instruction, updating registers and flags
--   3. Update the PC (unless the instruction is a branch, in which case
--      it's already been updated)
--
-- Returns whether or not the machine has halted (True means halted).
-- In our case, a halted machine has a PC at 0xdead.
step :: Machine -> IO Bool
step (M { memory       = mem
        , registers    = regs
        , flags        = flgs
        , pcRef        = pc
        , instructions = insns }) = do
  unimplemented "step"

-- Runs a machine until it has halted.
run :: Machine -> IO ()
run m = do
  done <- step m
  when (not done) $
    run m

-- Helpful for debugging: steps and prints the machine.
s :: Machine -> IO ()
s m = do
  step m
  printMachine m
