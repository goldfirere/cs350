{- Author: Richard Eisenberg
   File: Panic.hs

   Defines the `panic` command, useful when you've violated your
   own invariants.
-}

module CS350.Panic ( panic, unimplemented ) where

import Control.Exception ( Exception(..), throw )

-- This is the exception thrown when code calls `panic`.
newtype PanicException = Panic String
  deriving Show

instance Exception PanicException where
  displayException (Panic s) = "Compiler panic: " ++ s

-- Use this function when you need to handle a case that you can prove
-- is impossible. Do *not* use this for invalid input, as invalid input
-- is not provably impossible.
panic :: String -> a
panic msg = throw (Panic msg)

-- This function throws an error saying that a certain function
-- is unimplemented. Your submitted code should never do this, of course.
unimplemented :: String -> a
unimplemented function = panic ("Unimplemented function: " ++ function)
