{- Author: Richard Eisenberg
   File: Machine.hs

   Defines structures to represent a HERA machine.
-}

{-# OPTIONS_GHC -W #-}

module HERA.Machine where

import CS350.Renderable ( Renderable(..) )
import CS350.WordN      ( Word4, Word5, Word6 )

import HERA.Base        ( Register, Condition )

import Data.Int         ( Int8 )
import Data.List        ( intercalate )
import Data.Vector      ( Vector )
import qualified Data.Vector as I
import Data.Word        ( Word8, Word16 )

import Text.Printf      ( printf )

-- Target of a branch
data Target
  = Relative Int8
  | Absolute Register
  deriving (Show, Eq)

-- One machine instruction. Note that Int is signed,
-- while Word is unsigned.
data MInstruction
  = MSetlo Register Int8
  | MSethi Register Word8
  | MAnd Register Register Register
  | MOr Register Register Register
  | MXor Register Register Register
  | MAdd Register Register Register
  | MSub Register Register Register
  | MMul Register Register Register
  | MInc Register Word6         -- the value is δ, the actual amount to increment
  | MDec Register Word6         -- the value is δ, the actual amount to increment
  | MLsl Register Register
  | MLsr Register Register
  | MLsl8 Register Register
  | MLsr8 Register Register
  | MAsl Register Register
  | MAsr Register Register
  | MFon Word5
  | MFoff Word5
  | MFset5 Word5
  | MFset4 Word4
  | MSavef Register
  | MRstrf Register
  | MLoad Register Word5 Register
  | MStore Register Word5 Register
  | MBr Condition Target
  | MCall Register Register
  | MReturn Register Register
  | MSwi Word4
  | MRti
  deriving (Show, Eq)

-- One segment of data with a known location. Normally, the first is located
-- at `dataBottom`, but later segments may be placed anywhere thereafter; they
-- do not need to be contiguous
data DataSegment
  = DS { location :: Word16
       , contents :: Vector Word16
       }
  deriving (Show, Eq)

-- A program ready to be loaded on a HERA machine
data MachineProgram
  = MP { text  :: Vector MInstruction -- program text
       , dayta :: Vector DataSegment  -- pre-loaded data
       }
  deriving (Show, Eq)

-----------------------------------------------
-- Pretty-printing

instance Renderable Target where
  render (Relative b) = "R(" ++ render b ++ ")"
  render (Absolute o) = "(" ++ render o ++ ")"

instance Renderable MInstruction where
  render (MSetlo d v)   = "SETLO(" ++ render d ++ "," ++ render v ++ ")"
  render (MSethi d v)   = "SETHI(" ++ render d ++ "," ++ render v ++ ")"
  render (MAnd d a b)   = "AND(" ++ render d ++ "," ++ render a ++ "," ++ render b ++ ")"
  render (MOr d a b)    = "OR(" ++ render d ++ "," ++ render a ++ "," ++ render b ++ ")"
  render (MXor d a b)   = "XOR(" ++ render d ++ "," ++ render a ++ "," ++ render b ++ ")"
  render (MAdd d a b)   = "ADD(" ++ render d ++ "," ++ render a ++ "," ++ render b ++ ")"
  render (MSub d a b)   = "SUB(" ++ render d ++ "," ++ render a ++ "," ++ render b ++ ")"
  render (MMul d a b)   = "MUL(" ++ render d ++ "," ++ render a ++ "," ++ render b ++ ")"
  render (MInc d del)   = "INC(" ++ render d ++ "," ++ render del ++ ")"
  render (MDec d del)   = "DEC(" ++ render d ++ "," ++ render del ++ ")"
  render (MLsl d b)     = "LSL(" ++ render d ++ "," ++ render b ++ ")"
  render (MLsr d b)     = "LSR(" ++ render d ++ "," ++ render b ++ ")"
  render (MLsl8 d b)    = "LSL8(" ++ render d ++ "," ++ render b ++ ")"
  render (MLsr8 d b)    = "LSR8(" ++ render d ++ "," ++ render b ++ ")"
  render (MAsl d b)     = "ASL(" ++ render d ++ "," ++ render b ++ ")"
  render (MAsr d b)     = "ASR(" ++ render d ++ "," ++ render b ++ ")"
  render (MFon v)       = "FON(" ++ render v ++ ")"
  render (MFoff v)      = "FOFF(" ++ render v ++ ")"
  render (MFset5 v)     = "FSET5(" ++ render v ++ ")"
  render (MFset4 v)     = "FSET4(" ++ render v ++ ")"
  render (MSavef d)     = "SAVEF(" ++ render d ++ ")"
  render (MRstrf d)     = "RSTRF(" ++ render d ++ ")"
  render (MLoad d o b)  = "LOAD(" ++ render d ++ "," ++ render o ++ "," ++ render b ++ ")"
  render (MStore d o b) = "STORE(" ++ render d ++ "," ++ render o ++ "," ++ render b ++ ")"
  render (MBr cnd trg)  = "B" ++ render cnd ++ render trg
  render (MCall a b)    = "CALL(" ++ render a ++ "," ++ render b ++ ")"
  render (MReturn a b)  = "RETURN(" ++ render a ++ "," ++ render b ++ ")"
  render (MSwi i)       = "SWI(" ++ render i ++ ")"
  render MRti           = "RTI()"

instance Renderable DataSegment where
  render (DS { location = loc
             , contents = words })
    = printf "/* 0x%04x: */ DATA(" loc ++
      intercalate "," (map (printf "0x%04x") (I.toList words)) ++
      ")"

instance Renderable MachineProgram where
  render (MP { text = instructions
             , dayta = segments })
    = unlines (map render (I.toList segments)) ++
      unlines (map render (I.toList instructions))
