-- -*- haskell -*-
-- The line above tells emacs to use Haskell-style syntax highlighting

{
{- Author: Richard Eisenberg
   File: Lexer.x

   A lexer, written in Alex, for HERA files.
   This lexer does not provide error-handling functionality; it is really
   a somewhat flimsy attempt to take proper HERA programs into the
   Haskell AST (AInstruction) used.
-}

{-# OPTIONS_GHC -W -Wno-unused-imports -Wno-unused-top-binds #-}
   -- alex includes unnecessary imports and bindings

module HERA.Lexer ( Token(..), lexHera ) where

import CS350.Panic ( panic )

import Data.Char ( digitToInt )

}

%wrapper "basic"

-- By default, . excludes newlines.
$AnyChar = [.\n]

-- But \r is line a newline in comments
$NonNewline = . # [\r]

-- C++-style comments
@CommentElement    = ($AnyChar # \*) | \*+ ($AnyChar # \/)
@CommentTerminator = \*+ \/
@CommentTail       = @CommentElement* @CommentTerminator

@TraditionalComment = "/*" @CommentTail

@EndOfLineComment   = "//" $NonNewline*

@Comment        =  @TraditionalComment
                |  @EndOfLineComment

-- C preprocessor commands
@CPP = "#" $NonNewline*

-- identifiers
$Letter        = [A-Za-z_]
$LetterOrDigit = [0-9$Letter]
@Identifier    = $Letter $LetterOrDigit*

-- numbers
@Sign              = [\-\+]?

$Digit             = 0-9
$NonZeroDigit      = 1-9
@DecimalNumeral    = 0
                   | $NonZeroDigit
                   | $NonZeroDigit $Digit+
@SignedDecimal     = @Sign @DecimalNumeral

$HexDigit   = [0-9a-fA-F]
@HexDigits  = $HexDigit+
@HexNumeral = 0 [xX] @HexDigits
@SignedHex  = @Sign @HexNumeral

$OctalDigit   = [0-7]
@OctalNumeral = 0 $OctalDigit+
@SignedOctal  = @Sign @OctalNumeral

$BinaryDigit   = [01]
@BinaryDigits  = $BinaryDigit+
@BinaryNumeral = 0 [bB] @BinaryDigits
@SignedBinary  = @Sign @BinaryNumeral

-- strings
@EscapeSequence = \\ [btnfr\"\'\\]

@StringCharacter = $NonNewline # [\"\\]
                 | @EscapeSequence

@StringLiteral = \" @StringCharacter* \"


-- This next line says that definitions are over, and rules come next.
:-

^ @CPP                  ;    -- ignore preprocessor commands
$white                  ;
@Comment                ;
\;                      ;    -- ignore semicolons

"{"                     { const LBrace }
"}"                     { const RBrace }
"("                     { const LParen }
")"                     { const RParen }
","                     { const Comma }
@Identifier             { Identifier }
@SignedDecimal          { Number . read }
@SignedHex              { Number . read }
@SignedOctal            { Number . read }
@SignedBinary           { Number . readBin }
@StringLiteral          { StringLiteral . read }

{

data Token = LBrace
           | RBrace
           | LParen
           | RParen
           | Comma
           | Identifier String
           | Number Integer
           | StringLiteral String
  deriving (Show, Eq)

-- convert a binary literal into a number; there is nothing built-in that does this
readBin :: Integral a => String -> a
readBin ('-' : rest) = negate (readBin rest)
readBin ('+' : rest) = readBin rest
readBin (_zero : _b : digs) = go 0 (map digitToInt digs)
  where
    go :: Integral a => a -> [Int] -> a
    go acc []       = acc
    go acc (b : bs) = go (acc * 2 + fromIntegral b) bs
readBin other = panic $ "Zero- or one-character binary literal: " ++ other

lexHera :: String -> [Token]
lexHera = alexScanTokens   -- the name "alexScanTokens" is produced by the alex tool

}
