-- -*- haskell -*-
-- The line above tells emacs to use Haskell-style syntax highlighting

{
{- Author: Richard Eisenberg
   File: Parser.y

   This is a basic parser for HERA files. There is no error-handling here,
   just a bare-bones attempt to get a HERA source file into the Haskell-based
   AST.
-}

{-# OPTIONS_GHC -W -Wno-unused-matches -Wno-unused-top-binds -Wno-overlapping-patterns
                   -Wno-tabs -Wno-incomplete-patterns #-}
  -- happy produces lots of warnings. :(

module HERA.Parser ( parse ) where

import HERA.Lexer    ( Token(..) )
import HERA.Assembly ( AssemblyProgram, AInstruction(..) )
import HERA.Base     ( Register(..), rt, fpAlt, pcRet, fp, sp, Condition(..) )

}

%expect 0                  -- expect 0 conflicts in this grammar
%name parse                -- name the main parser "parse"
%tokentype { Token }       -- use the Token type exported by the lexer
%error { (error . show) }  -- just use the basic "error" function for errors

-- declare tokens
%token
  SETLO            { Identifier "SETLO" }
  SETHI            { Identifier "SETHI" }
  AND              { Identifier "AND"   }
  OR               { Identifier "OR"    }
  XOR              { Identifier "XOR"   }
  ADD              { Identifier "ADD"   }
  SUB              { Identifier "SUB"   }
  MUL              { Identifier "MUL"   }
  INC              { Identifier "INC"   }
  DEC              { Identifier "DEC"   }
  LSL              { Identifier "LSL"   }
  LSR              { Identifier "LSR"   }
  LSL8             { Identifier "LSL8"  }
  LSR8             { Identifier "LSR8"  }
  ASL              { Identifier "ASL"   }
  ASR              { Identifier "ASR"   }
  FON              { Identifier "FON"   }
  FOFF             { Identifier "FOFF"  }
  FSET5            { Identifier "FSET5" }
  FSET4            { Identifier "FSET4" }
  SAVEF            { Identifier "SAVEF" }
  RSTRF            { Identifier "RSTRF" }
  LOAD             { Identifier "LOAD"  }
  STORE            { Identifier "STORE" }
  BR               { Identifier "BR"    }
  BRR              { Identifier "BRR"   }
  BL               { Identifier "BL"    }
  BLR              { Identifier "BLR"   }
  BGE              { Identifier "BGE"   }
  BGER             { Identifier "BGER"  }
  BLE              { Identifier "BLE"   }
  BLER             { Identifier "BLER"  }
  BG               { Identifier "BG"    }
  BGR              { Identifier "BGR"   }
  BULE             { Identifier "BULE"  }
  BULER            { Identifier "BULER" }
  BUG              { Identifier "BUG"   }
  BUGR             { Identifier "BUGR"  }
  BZ               { Identifier "BZ"    }
  BZR              { Identifier "BZR"   }
  BNZ              { Identifier "BNZ"   }
  BNZR             { Identifier "BNZR"  }
  BC               { Identifier "BC"    }
  BCR              { Identifier "BCR"   }
  BNC              { Identifier "BNC"   }
  BNCR             { Identifier "BNCR"  }
  BS               { Identifier "BS"    }
  BSR              { Identifier "BSR"   }
  BNS              { Identifier "BNS"   }
  BNSR             { Identifier "BNSR"  }
  BV               { Identifier "BV"    }
  BVR              { Identifier "BVR"   }
  BNV              { Identifier "BNV"   }
  BNVR             { Identifier "BNVR"  }
  CALL             { Identifier "CALL"  }
  RETURN           { Identifier "RETURN" }
  SWI              { Identifier "SWI"    }
  RTI              { Identifier "RTI"    }
  SET              { Identifier "SET"    }
  SETRF            { Identifier "SETRF"  }
  MOVE             { Identifier "MOVE"   }
  CMP              { Identifier "CMP"    }
  NEG              { Identifier "NEG"    }
  NOT              { Identifier "NOT"    }
  CON              { Identifier "CON"    }
  COFF             { Identifier "COFF"   }
  CBON             { Identifier "CBON"   }
  CCBOFF           { Identifier "CCBOFF" }
  FLAGS            { Identifier "FLAGS"  }
  LABEL            { Identifier "LABEL"  }
  DLABEL           { Identifier "DLABEL" }
  INTEGER          { Identifier "INTEGER" }
  LP_STRING        { Identifier "LP_STRING" }
  DSKIP            { Identifier "DSKIP" }
  HALT             { Identifier "HALT" }
  NOP              { Identifier "NOP" }

  R0               { Identifier "R0" }
  R1               { Identifier "R1" }
  R2               { Identifier "R2" }
  R3               { Identifier "R3" }
  R4               { Identifier "R4" }
  R5               { Identifier "R5" }
  R6               { Identifier "R6" }
  R7               { Identifier "R7" }
  R8               { Identifier "R8" }
  R9               { Identifier "R9" }
  R10              { Identifier "R10" }
  R11              { Identifier "R11" }
  R12              { Identifier "R12" }
  R13              { Identifier "R13" }
  R14              { Identifier "R14" }
  R15              { Identifier "R15" }
  Rt               { Identifier "Rt" }
  FP_alt           { Identifier "FP_alt" }
  PC_ret           { Identifier "PC_ret" }
  FP               { Identifier "FP" }
  SP               { Identifier "SP" }

  number           { Number $$ }
  string           { StringLiteral $$ }
  label            { Identifier $$ }

  '{'              { LBrace }
  '}'              { RBrace }
  '('              { LParen }
  ')'              { RParen }
  ','              { Comma }

-- this ends the definition of tokens, and starts the parsing rules
%%

program :: { AssemblyProgram }
program : preamble instructions postamble { $2 }

preamble :: { a }
preamble : {- empty -}             { error "inspected preamble" }
         | labels '(' ')' '{'      { error "inspected preamble" }

postamble :: { a }
postamble : {- empty -}    { error "inspected postamble" }
          | '}'            { error "inspected postamble" }

labels :: { [String] }
labels : {- empty -}       { [] }
       | label labels      { $1 : $2 }

instructions :: { AssemblyProgram }
instructions : {- empty -}                { [] }
             | instruction instructions   { $1 : $2 }

instruction :: { AInstruction }
instruction : SETLO '(' reg ',' number ')'            { ASetlo $3 (fromIntegral $5) }
            | SETHI '(' reg ',' number ')'            { ASethi $3 (fromIntegral $5) }
            | AND '(' reg ',' reg ',' reg ')'         { AAnd $3 $5 $7 }
            | OR  '(' reg ',' reg ',' reg ')'         { AOr  $3 $5 $7 }
            | XOR '(' reg ',' reg ',' reg ')'         { AXor $3 $5 $7 }
            | ADD '(' reg ',' reg ',' reg ')'         { AAdd $3 $5 $7 }
            | SUB '(' reg ',' reg ',' reg ')'         { ASub $3 $5 $7 }
            | MUL '(' reg ',' reg ',' reg ')'         { AMul $3 $5 $7 }
            | INC '(' reg ',' number ')'              { AInc $3 (fromIntegral $5) }
            | DEC '(' reg ',' number ')'              { ADec $3 (fromIntegral $5) }
            | LSL '(' reg ',' reg ')'                 { ALsl $3 $5 }
            | LSR '(' reg ',' reg ')'                 { ALsr $3 $5 }
            | LSL8 '(' reg ',' reg ')'                { ALsl8 $3 $5 }
            | LSR8 '(' reg ',' reg ')'                { ALsr8 $3 $5 }
            | ASL '(' reg ',' reg ')'                 { AAsl $3 $5 }
            | ASR '(' reg ',' reg ')'                 { AAsr $3 $5 }
            | FON '(' number ')'                      { AFon (fromIntegral $3) }
            | FOFF '(' number ')'                     { AFoff (fromIntegral $3) }
            | FSET5 '(' number ')'                    { AFset5 (fromIntegral $3) }
            | FSET4 '(' number ')'                    { AFset4 (fromIntegral $3) }
            | SAVEF '(' reg ')'                       { ASavef $3 }
            | RSTRF '(' reg ')'                       { ARstrf $3 }
            | LOAD '(' reg ',' number ',' reg ')'     { ALoad $3 (fromIntegral $5) $7 }
            | STORE '(' reg ',' number ',' reg ')'    { AStore $3 (fromIntegral $5) $7 }
            | BR    '(' label ')'                     { ABr None $3 }
            | BRR   '(' label ')'                     { ABr None $3 }
            | BL    '(' label ')'                     { ABr L $3 }
            | BLR   '(' label ')'                     { ABr L $3 }
            | BGE   '(' label ')'                     { ABr Ge $3 }
            | BGER  '(' label ')'                     { ABr Ge $3 }
            | BLE   '(' label ')'                     { ABr Le $3 }
            | BLER  '(' label ')'                     { ABr Le $3 }
            | BG    '(' label ')'                     { ABr G $3 }
            | BGR   '(' label ')'                     { ABr G $3 }
            | BULE  '(' label ')'                     { ABr Ule $3 }
            | BULER '(' label ')'                     { ABr Ule $3 }
            | BUG   '(' label ')'                     { ABr Ug $3 }
            | BUGR  '(' label ')'                     { ABr Ug $3 }
            | BZ    '(' label ')'                     { ABr Z $3 }
            | BZR   '(' label ')'                     { ABr Z $3 }
            | BNZ   '(' label ')'                     { ABr Nz $3 }
            | BNZR  '(' label ')'                     { ABr Nz $3 }
            | BC    '(' label ')'                     { ABr C $3 }
            | BCR   '(' label ')'                     { ABr C $3 }
            | BNC   '(' label ')'                     { ABr Nc $3 }
            | BNCR  '(' label ')'                     { ABr Nc $3 }
            | BS    '(' label ')'                     { ABr S $3 }
            | BSR   '(' label ')'                     { ABr S $3 }
            | BNS   '(' label ')'                     { ABr Ns $3 }
            | BNSR  '(' label ')'                     { ABr Ns $3 }
            | BV    '(' label ')'                     { ABr V $3 }
            | BVR   '(' label ')'                     { ABr V $3 }
            | BNV   '(' label ')'                     { ABr Nv $3 }
            | BNVR  '(' label ')'                     { ABr Nv $3 }
            | CALL '(' reg ',' label ')'              { ACall $3 $5 }
            | RETURN '(' reg ',' reg ')'              { AReturn $3 $5 }
            | SWI '(' number ')'                      { ASwi (fromIntegral $3) }
            | RTI '(' ')'                             { ARti }
            | SET '(' reg ',' number ')'              { ASet $3 (fromIntegral $5) }
            | SET '(' reg ',' label ')'               { ASetl $3 $5 }
            | SETRF '(' reg ',' number ')'            { ASetrf $3 (fromIntegral $5) }
            | MOVE '(' reg ',' reg ')'                { AMove $3 $5 }
            | CMP '(' reg ',' reg ')'                 { ACmp $3 $5 }
            | NEG '(' reg ',' reg ')'                 { ANeg $3 $5 }
            | NOT '(' reg ',' reg ')'                 { ANot $3 $5 }
            | CON '(' ')'                             { ACon }
            | COFF '(' ')'                            { ACoff }
            | CBON '(' ')'                            { ACbon }
            | CCBOFF '(' ')'                          { ACcboff }
            | FLAGS '(' reg ')'                       { AFlags $3 }
            | LABEL '(' label ')'                     { ALabel $3 }
            | DLABEL '(' label ')'                    { ADlabel $3 }
            | INTEGER '(' number ')'                  { AInteger (fromIntegral $3) }
            | LP_STRING '(' string ')'                { ALpString $3 }
            | DSKIP '(' number ')'                    { ADskip (fromIntegral $3) }
            | HALT '(' ')'                            { AHalt }
            | NOP '(' ')'                             { ANop }

reg :: { Register }
reg : R0     { R0 }
    | R1     { R1 }
    | R2     { R2 }
    | R3     { R3 }
    | R4     { R4 }
    | R5     { R5 }
    | R6     { R6 }
    | R7     { R7 }
    | R8     { R8 }
    | R9     { R9 }
    | R10    { R10 }
    | R11    { R11 }
    | R12    { R12 }
    | R13    { R13 }
    | R14    { R14 }
    | R15    { R15 }
    | Rt     { rt }
    | FP_alt { fpAlt }
    | PC_ret { pcRet }
    | FP     { fp }
    | SP     { sp }
