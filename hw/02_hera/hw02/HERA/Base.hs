{- Author: Richard Eisenberg, based on work by Steve Zdancewic
   File: HERA.hs

   Representation of HERA machine code and assembly.
-}

{-# OPTIONS_GHC -W #-}

module HERA.Base where

import CS350.Renderable ( Renderable(..) )

import Data.Word ( Word16 )

-- The addresses below are cribbed from HERA_data.h.

-- Addresses in the HERA machine are 16 bits.
type Address = Word16

-- Where the stack starts.
stackBottom :: Address
stackBottom = 0x0000

-- Where the stack ends.
stackTop :: Address
stackTop = 0x3fff

-- Where the heap begins.
heapBottom :: Address
heapBottom = 0x4000

-- Where the heap ends.
heapTop :: Address
heapTop = 0xbfff

-- Where the global data store begins.
dataBottom :: Address
dataBottom = 0xc000

-- Where the global data store ends.
dataTop :: Address
dataTop = 0xffdf

-- How big is the memory?
memorySize :: Int
memorySize = 2^16

-- We have 16 registers
numRegisters :: Int
numRegisters = 16

-- This special PC value means the program has halted. If a program
-- is too long (57,005 instructions, to be exact), the PC might reach
-- this value spuriously. We ignore this possibility.
haltedPC :: Address
haltedPC = 0xdead

-- Registers
data Register
  = R0
  | R1
  | R2
  | R3
  | R4
  | R5
  | R6
  | R7
  | R8
  | R9
  | R10
  | R11
  | R12
  | R13
  | R14
  | R15
  deriving (Show, Eq, Enum, Bounded)

-- Common abbreviations
rt, fpAlt, pcRet, fp, sp :: Register
rt = R11
fpAlt = R12
pcRet = R13
fp = R14
sp = R15

-- Types of conditions
data Condition
  = None
  | L    -- < 0
  | Ge   -- >= 0
  | Le   -- <= 0
  | G    -- > 0
  | Ule  -- unsigned result <= 0
  | Ug   -- unsigned result > 0
  | Z    -- == 0
  | Nz   -- /= 0
  | C    -- if carry, or unsigned result >= 0
  | Nc   -- if not carray / unsigned result < 0
  | S    -- if "sign" (negative)
  | Ns   -- if not "sign"
  | V    -- overflow
  | Nv   -- no overflow
  deriving (Show, Eq, Enum, Bounded)

-------------------------------------------------------------------------
-- Pretty-printing via Renderable

instance Renderable Register where
  render = show

instance Renderable Condition where
  render None = "R"
  render L    = "L"
  render Ge   = "GE"
  render Le   = "LE"
  render G    = "G"
  render Ule  = "ULE"
  render Ug   = "UG"
  render Z    = "Z"
  render Nz   = "NZ"
  render C    = "C"
  render Nc   = "NC"
  render S    = "S"
  render Ns   = "NS"
  render V    = "V"
  render Nv   = "NV"
