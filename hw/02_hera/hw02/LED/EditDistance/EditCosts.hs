{-# OPTIONS_GHC -funbox-strict-fields #-}

module LED.EditDistance.EditCosts (
    Costs(..),
    EditCosts(..), deletionCost, insertionCost, substitutionCost, transpositionCost,
    defaultEditCosts, isDefaultEditCosts
  ) where

data Costs a = ConstantCost !Int
             | VariableCost (a -> Int)

{-# INLINE cost #-}
cost :: Costs a -> a -> Int
cost (ConstantCost i) _ = i
cost (VariableCost f) x = f x

data EditCosts a = EditCosts {
    deletionCosts :: Costs a,             -- ^ Cost of deleting the specified character from the left string
    insertionCosts :: Costs a,            -- ^ Cost of inserting the specified characters into the right string
    substitutionCosts :: Costs (a, a),    -- ^ Cost of substituting a character from the left string with one from the right string -- with arguments in that order.
    transpositionCosts :: Costs (a, a)    -- ^ Cost of moving one character backwards and the other forwards -- with arguments in that order.
  }

{-# INLINE deletionCost #-}
deletionCost :: EditCosts a -> a -> Int
deletionCost ec deleted = cost (deletionCosts ec) deleted

{-# INLINE insertionCost #-}
insertionCost :: EditCosts a -> a -> Int
insertionCost ec inserted = cost (insertionCosts ec) inserted

{-# INLINE substitutionCost #-}
substitutionCost :: EditCosts a -> a -> a -> Int
substitutionCost ec old new = cost (substitutionCosts ec) (old, new)

{-# INLINE transpositionCost #-}
transpositionCost :: EditCosts a -> a -> a -> Int
transpositionCost ec backwards forwards = cost (transpositionCosts ec) (backwards, forwards)

defaultEditCosts :: EditCosts a
defaultEditCosts = EditCosts {
    deletionCosts = ConstantCost 1,
    insertionCosts = ConstantCost 1,
    substitutionCosts = ConstantCost 1,
    transpositionCosts = ConstantCost 1
}

isDefaultEditCosts :: EditCosts a -> Bool
isDefaultEditCosts (EditCosts { deletionCosts = ConstantCost 1, insertionCosts = ConstantCost 1, substitutionCosts = ConstantCost 1, transpositionCosts = ConstantCost 1 }) = True
isDefaultEditCosts _ = False
