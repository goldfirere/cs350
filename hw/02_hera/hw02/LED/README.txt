The files in this directory and subdirectories are lightly
adapted from the edit-distance package.

That package works only with strings, and I (Richard Eisenberg)
simply generalized it to work with arbitrary ordered things.
It would also be easy to generalize further to Eq-things, but
that would be less efficient.

See also the LICENSE file, which covers this subdirectory.
