{- Author: Richard Eisenberg
   File: StudentTest.hs

   Runs the student-visible tests. This file is *not* run by the autograder.
-}

{-# OPTIONS_GHC -W #-}

-- it's an executable, so the module name must be `main`
module Main where

import Grading.Infra ( TestProgram(..), TestAP(..) )
import Grading.Tests   ( testProgram, testAP )
import Grading.MPrograms  ( easyTestPrograms, hardTestPrograms )
import Grading.APrograms  ( easyTAPs, hardTAPs )

main :: IO ()
main = do
  putStrLn "Testing simulator on easy programs:"
  mapM_ runTest easyTestPrograms

  putStrLn "\nTesting assembler on easy programs:"
  mapM_ runTestAP easyTAPs

  putStrLn "\nTesting simulator on hard programs:"
  mapM_ runTest hardTestPrograms

  putStrLn "\nTesting assembler on hard programs:"
  mapM_ runTestAP hardTAPs

runTest :: TestProgram -> IO ()
runTest tp@(TP name _ _) = do
  result <- testProgram tp
  printResult result name

runTestAP :: TestAP -> IO ()
runTestAP tap@(TAP name _ _ _) = do
  result <- testAP tap
  printResult result name

printResult :: Double -> String -> IO ()
printResult score name =
  putStrLn $ "Score on test " ++ name ++ ": " ++ show score
