{- Author: Richard Eisenberg
   File: Fac.hs

   An encoding of the fac.hera HERA program, all assembled and loaded.
-}

{-# OPTIONS_GHC -W -Wno-overflowed-literals #-}

module Fac where

import HERA.Assembly  ( AssemblyProgram, AInstruction(..) )
import HERA.Machine ( MachineProgram(..), MInstruction(..), Target(..)
                    , DataSegment(..) )
import HERA.Base    ( Register(..), Condition(..), rt, pcRet, fpAlt, fp, sp )

import qualified Data.Vector as I

facAP :: AssemblyProgram
facAP = [ ADlabel "arg_num"
        , AInteger 5

        , ALabel "main"

        , ASetl R1 "arg_num"   -- R1 points to data
        , ALoad R2 0 R1        -- we are computing fac(R2)

        , AMove fpAlt sp       -- Step 1 in "how to call"
        , AInc sp 4            -- Step 1 in "how to call"
        , AStore R2 3 fpAlt    -- Step 2 in "how to call"
        , ACall fpAlt "fac"    -- Step 4 in "how to call" (no static links here)
        , ALoad R3 3 fpAlt     -- Step 5 in "how to call"; R3 is result
        , ADec sp 4            -- Step 5 in "how to call"; restore sp

        , AHalt                -- end of "main"

        , ALabel "fac"
    -- R1 = parameter passed in; result of multiplication
    -- R2 = R1 - 1 (before rec. call); result of recursive call

        , AInc sp 2            -- Step 1 in "how to define"; 2 locals
        , AStore pcRet 0 fp    -- Step 2 in "how to define"; store return address
        , AStore fpAlt 1 fp    -- Step 2 in "how to define"; store old fp
        , AStore R1 4 fp       -- Step 2 in "how to define"; store old R1
        , AStore R2 5 fp       -- Step 2 in "how to define"; store old R2

        , ALoad R1 3 fp        -- Get parameter into R1
        , ASub R0 R1 R0        -- Compare R1 against 0
        , ABr Le "fac_base"    -- If R1 <= 0, branch to "fac_base"

        , AMove R2 R1          -- copy R1 to R2
        , ADec R2 1            -- decrement parameter for recursive call
        , AMove fpAlt sp       -- Step 1 in "how to call"
        , AInc sp 4            -- Step 1 in "how to call"
        , AStore R2 3 fpAlt    -- Step 2 in "how to call"
        , ACall fpAlt "fac"    -- Step 4 in "how to call"
        , ALoad R2 3 fpAlt     -- Step 5 in "how to call"; store result in R2
        , ADec sp 4            -- Step 5 in "how to call": restore sp
        , AMul R1 R1 R2        -- multiply recursive result by parameter
        , AStore R1 3 fp       -- Step 4 in "how to define"; return result

        , ALabel "fac_return"
        , ALoad R2 5 fp        -- Step 5 in "how to define"
        , ALoad R1 4 fp        -- Step 5 in "how to define"
        , ALoad fpAlt 1 fp     -- Step 5 in "how to define"
        , ALoad pcRet 0 fp     -- Step 5 in "how to define"
        , ADec sp 2            -- Step 5 in "how to define"
        , AReturn fpAlt pcRet

        , ALabel "fac_base"
        , ASet R1 1            -- result is 1
        , AStore R1 3 fp       -- store as return value
        , ABr None "fac_return" ]

facMP :: MachineProgram
facMP = MP { text = I.fromList
             [ MSetlo R1 0x00              -- 0x00
             , MSethi R1 0xc0
             , MLoad R2 0 R1
             , MOr fpAlt sp R0
             , MInc sp 4
             , MStore R2 3 fpAlt
             , MSetlo R13 0x0e
             , MSethi R13 0x00
             , MCall fpAlt R13             -- 0x08
             , MLoad R3 3 fpAlt
             , MDec sp 4
             , MSetlo rt 0xad
             , MSethi rt 0xde
             , MBr None (Absolute rt)
             -- fac: (0x000e)
             , MInc sp 2
             , MStore pcRet 0 fp
             , MStore fpAlt 1 fp           -- 0x10
             , MStore R1 4 fp
             , MStore R2 5 fp
             , MLoad R1 3 fp
             , MSub R0 R1 R0
             , MBr Le (Relative 0x13)      -- 0x15
             , MOr R2 R1 R0
             , MDec R2 1
             , MOr fpAlt sp R0             -- 0x18
             , MInc sp 4
             , MStore R2 3 fpAlt
             , MSetlo R13 0x0e
             , MSethi R13 0x00
             , MCall fpAlt R13
             , MLoad R2 3 fpAlt
             , MDec sp 4
             , MMul R1 R1 R2               -- 0x20
             , MStore R1 3 fp
             -- fac_return: (0x0022)
             , MLoad R2 5 fp
             , MLoad R1 4 fp
             , MLoad fpAlt 1 fp
             , MLoad pcRet 0 fp
             , MDec sp 2
             , MReturn fpAlt pcRet
             -- fac_base: (0x0028)
             , MSetlo R1 0x01              -- 0x28
             , MSethi R1 0x00
             , MStore R1 3 fp
             , MBr None (Relative 0xf7) ]  -- 0x2b; 0xf7 is (-9)
           , dayta = I.fromList [ DS { location = 0xc000, contents = I.fromList [5] } ] }

facMPLong :: MachineProgram
facMPLong = MP { text = I.fromList
                 [ MSetlo R1 0x00              -- 0x00
                 , MSethi R1 0xc0
                 , MLoad R2 0 R1
                 , MOr fpAlt sp R0
                 , MInc sp 4
                 , MStore R2 3 fpAlt
                 , MSetlo R13 0x0e
                 , MSethi R13 0x00
                 , MCall fpAlt R13             -- 0x08
                 , MLoad R3 3 fpAlt
                 , MDec sp 4
                 , MSetlo rt 0xad
                 , MSethi rt 0xde
                 , MBr None (Absolute rt)
                 -- fac: (0x000e)
                 , MInc sp 2
                 , MStore pcRet 0 fp
                 , MStore fpAlt 1 fp           -- 0x10
                 , MStore R1 4 fp
                 , MStore R2 5 fp
                 , MLoad R1 3 fp
                 , MSub R0 R1 R0
                 , MSetlo rt 0x2a
                 , MSethi rt 0x00
                 , MBr Le (Absolute rt)
                 , MOr R2 R1 R0                -- 0x18
                 , MDec R2 1
                 , MOr fpAlt sp R0
                 , MInc sp 4
                 , MStore R2 3 fpAlt
                 , MSetlo R13 0x0e
                 , MSethi R13 0x00
                 , MCall fpAlt R13
                 , MLoad R2 3 fpAlt            -- 0x20
                 , MDec sp 4
                 , MMul R1 R1 R2
                 , MStore R1 3 fp
                 -- fac_return: (0x0024)
                 , MLoad R2 5 fp
                 , MLoad R1 4 fp
                 , MLoad fpAlt 1 fp
                 , MLoad pcRet 0 fp
                 , MDec sp 2                   -- 0x28
                 , MReturn fpAlt pcRet
                 -- fac_base: (0x002a)
                 , MSetlo R1 0x01
                 , MSethi R1 0x00
                 , MStore R1 3 fp
                 , MSetlo rt 0x24
                 , MSethi rt 0x00
                 , MBr None (Absolute rt) ]
               , dayta = I.fromList [ DS { location = 0xc000, contents = I.fromList [5] } ] }
