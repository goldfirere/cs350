{- Author: Richard Eisenberg
   File: Main.hs

   This program runs the testing harness for students. You do not need
   to edit or submit this file; a different Main.hs is used during grading, regardless.
-}

module Main where

import GradedTests
import ProvidedTests

import Test.HUnit ( runTestTT, Counts(..) )

main = do
  gradedCounts <- gradedTests

  putStrLn "Student-provided tests:"
  providedCounts <- runTestTT providedTests

  reportCounts (mconcat (providedCounts : gradedCounts))

reportCounts :: Counts -> IO ()
reportCounts (Counts { cases = cs, tried = ts, errors = es, failures = fs }) = do
  putStrLn "Summary:"
  putStrLn $ " Total tests specified: " ++ show cs
  putStrLn $ " Total tests attempted: " ++ show ts
  putStrLn $ " Total tests succeeded: " ++ show (ts - es - fs)
  putStrLn $ " Total tests failed:    " ++ show (es + fs)
  

