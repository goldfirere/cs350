{- HW01
   This is an _individual_ project -- please complete all of the work yourself!
-}

{- A Haskell file requires certain declarations up at the top. These will be
   explained later.
-}

{-# OPTIONS_GHC -Wincomplete-patterns #-}

module HelloHaskell where

import Prelude hiding ( map, reverse, lookup )

{-
  GOAL: get up to speed with Haskell; write a simple arithmetic interpreter

  This tutorial-style project refers to parts of the book "Real World Haskell",
  by Bryan O'Sullivan, Don Stewart, and John Goerzen, linked from the course
  web page. It provides a very good reference for learning Haskell.
  In the problems below when you see a note like "See RWH 5.2"
  please refer to that section of the book.

  Please also take some time to skim the other available resources on the course
  homepage -- there are links to the standard libraries, and on-line tutorials.
  It is not possible to cover all of Haskell's features in just a couple days,
  so we expect you to be able to learn most of what you need on your own.

  Also, please feel to post on Piazza for help if you get stuck.
  Learning Haskell is essential for your successful completion of later projects,
  but it is not supposed to be too onerous in itself!
-}

{-
  Files in this project:

  HelloHaskell.hs  -- (this file) the main project file you will need to modify

  Autograde.hs     -- a unit test framework used throughout the course
  GradedTests.hs   -- tests that we will run to assess the correctness
                      of your homework
  ProvidedTests.hs -- tests that you write to help with debugging and for
                      a grade (as requested below)
  Main.hs          -- the main executable for this project
-}


--------------------------------------------------------------------------------
--
-- PART 1: Haskell Basics
--
--------------------------------------------------------------------------------

-- Haskell one-line comments are written starting with '--'
{- Multiline comments are written with '{-' and '-}' delimiters.  They nest. -}

{-
  Haskell is strongly typed and provides a standard suite of base types:
    Int     - 64 bit integers 0, 1, (-1), etc.
    Integer - size-unbounded integers 0, 1, (-1), etc.
    Bool    - 'True' and 'False'
    ()      -  a 'trivial' type that has only one member, namely ();
               both type and term are called "unit"
    String  - "a string" or "another"
    Float, Char, etc. will also crop up occasionally
  For more details, see RWH Chapter 2; Section "Some common basic types"
-}

{-
  Haskell is an expression-oriented, functional language.  This means you can't
  do imperative updates to variables as you do in languages like C, Java, etc.

  Instead, you define variables like this:
-}

anInt :: Int
anInt = 3

anotherInt :: Int
anotherInt = 3 * 14

{-
  The '... :: Int' lines above are type signatures -- these
  are optional in Haskell programs (type inference will figure them out), but it
  is good style to use type signatures on the top-level definitions you make
  because it will improve the error messages that the Haskell typechecker
  generates when you make a mistake.

  For example, it is an error to add a float and an int:
-}
{- Uncomment to get a type error:

anError :: Int
anError = 3 + (1.0 :: Float)
-}

{-
  Haskell is also a *layout-sensitive* language. This means that the column
  where each line starts is relevant in understanding that line of code. (Spaces
  within a line do not matter.) Top-level definitions must start at the
  beginning of a line. Other constructs that we'll see later also set a new
  indentation level, which we'll review as we see them.

  Haskell is also case-sensitive. The identifier camelcase is different from
  the identifier camelCase. Haskell also distinguishes between identifiers
  that start with a capital letter from those that do not. We have already
  seen that variables start with a lower-case letter; this is not just
  a convention, but is a rule in the language: we cannot declare variables
  that begin with an upper-case letter.
-}

{-
  'let' expressions can be nested.  The scope of a let-bound variable is
  delimited by the 'in' keyword.
-}

-- bind z to the value 39
z :: Int
z = let x = 3         -- 'let' starts an indentation level: x and y are aligned
        y = x + x in  -- 'let' is recursive; both x and y are in scope here...
    y * y + x         -- ... and here. A 'let' block ends with 'in'

-- bind z2 to the value 39
z2 :: Int
z2 = y * y + x  -- x and y are in scope here
  where  -- 'where' is an alternative to 'let'. It can be written at the
         -- end of a definition. It, too, starts an indentation level.
    x = 3
    y = x + x

-- Once bound, a variable (like 'z' above) never changes.

{-
  The most important type of values in Haskell is the function type.
  Function types are written like 'Int -> Int', which is the type of a function
  that takes an int argument and produces an int result.
  See RWH Chapter 2, section "Function types and purity"

  Function values are introduced using a '\' and the '->' syntax:
   \ x -> x + x    -- a function that takes an int and doubles it

  Functions are first class -- they can passed around just like integers or
  other primitive data.
-}

-- bind the variable 'double' of type 'int -> int' to a function:
double :: Int -> Int
double = \x -> x + x

{-
  Functions are called or 'applied' by juxtaposition -- the space ' '
  between a function name and its arguments is the function application site.
  Unlike Java or C, no parentheses are needed, exept for grouping and
  precedence:
-}
doubledZ :: Int
doubledZ = double z                  -- call double on z

quadrupledZ :: Int
quadrupledZ = double (double z)      -- parens needed for grouping

sextupledZ :: Int
sextupledZ = quadrupledZ + (double z)

{-
  Functions with more than one argument have types like:
  'Int -> Int -> Int', which is the type of a function that takes an Int
  and returns a function that itself takes an Int and returns an Int.
  i.e. 'Int -> Int -> Int' is just 'Int -> (Int -> Int)'
-}

mult :: Int -> Int -> Int
mult = \ x -> \ y -> x * y

squaredZ :: Int
squaredZ = mult z z -- multiply z times z

{-
  Because functions like 'mult' above return functions, they can be
  partially applied:
-}

multBy3 :: Int -> Int
multBy3 = mult 3   -- partially apply mult to 3

multBy4 :: Int -> Int
multBy4 = mult 4   -- partially apply mult to 4

meaningOfLife :: Int
meaningOfLife = multBy3 14  -- call the partially applied function

excellentScore :: Int
excellentScore = multBy4 25 -- compute 100

{-
  The '\' ... '->' syntax above is a bit heavy, so Haskell provides syntactic sugar
  for abbreviating function definitions, avoiding the need for '\'.

  For example, we can write double like this:
-}
double' :: Int -> Int
double' x = x + x   -- Note: you can put ' in variable names

-- and mult like this:
mult' :: Int -> Int -> Int
mult' x y = x * y

-- We still call them in the same way as before:
quadrupledZ' :: Int
quadrupledZ' = double' (double' z)  -- parens needed for grouping

multBy3' :: Int -> Int
multBy3' = mult' 3  -- partially apply mult' to 3

-- Functions are first-class values. They can be passed to other functions:
twice :: (Int -> Int) -> Int -> Int
twice f x =
  -- f is a function from Int to Int
  f (f x)

quadrupledZAgain :: Int
quadrupledZAgain = twice double z  -- pass double to twice


-- Haskell's interactive interpreter ---------------------------------------
{-

  You can play around with Haskell programs in GHCi.  This is an
  interactive Haskell session that accepts Haskell expressions on the command line,
  compiles them, runs the resulting bytecode, and then prints out the result.
  GHCi is a great way to learn the Haskell basics, and
  for trying out one-file programs like this one.

  To start GHCi, just run the 'ghci' command from the command line.
  You should see:
  Whether your run it from the command line or Eclipse, you should see:

    GHCi, version 8.6.1: http://www.haskell.org/ghc/  :? for help
    Prelude>

  "Prelude>" is the prompt

  You can type Haskell expressions at the prompt like this:

  Prelude> 3 + 4
  7

  To quit, use the ':quit' directive:

  Prelude> :quit

  You can load .hs files into GHCi via the ':load' directive:

  Prelude> :load HelloHaskell.hs
  ...


  In this case, GHCi behaves as though you entered in all of the contents of
  the .hs file at the command prompt.

  Once you ':load' a file, you can then interact with the functions and other
  values it defines at the top-level.  This can be very useful when playing
  around with your programs, debugging, or testing functions.  Note that you
  have to  either start GHCi from the directory containing the file you want to
  load, or use the ':cd dirname' directive to change directories to the right
  location.

  Try it now -- load this "HelloHaskell.hs" file into GHCi.
  If you succeed, you should be able to print out the values defined in this
  file.

  GHCi also allows you to view the type of a declaration, like this:

  Prelude> :type twice
  twice :: (Int -> Int) -> Int -> Int

  All commands in GHCi can be abbreviated. So, we often use ':t' instead
  of ':type' and ':l' instead of ':load'.
-}

-- Compiling ----------------------------------------------------------------
{-
  You can also run your project using the test harness and the 'Main.hs' file.
  Just run 'ghc Main.hs' at the command line; GHC will figure out all the files
  it needs to compile.

  (If your system complains that it doesn't know about Test.HUnit, try running
  `cabal install HUnit`. That should get you going again.)

  To run, just execute './Main', which will execute the test harness.

  The test harness is a useful way to determine how much of the assignment you
  have completed.  We will be using (a variant of) this test harness to
  partially automate grading of your project code.  We will also do some
  manual grading of your code, and we may withhold some of the tests cases
  we run -- you should definitely test your projects thoroughly on your own.
  Later in the course we will encourage you (and may even require you) to
  submit test cases to Piazza so that you can help each
  other test your implementations.

  Hint: examining the test cases in GradedTests.hs can help you figure out
  the specifications of the code you are supposed to implement.
-}

-- PART 1 Problems ----------------------------------------------------------
{-
  Complete the following definitions as directed -- you can tell when you get
  them right because the unit tests specified in part1Tests of the
  GradedTests.hs file will succeed when you run ./Main.

  Note that (error "unimplemented") is a function that takes any
  argument and raises an exception -- you will have to replace these
  definitions with other function definitions to pass the tests.
-}

-- Problem 1-1
{-
  The 'pieces' variable below is bound to the wrong value.  Bind it to one that
  makes the first case of part1Tests "Problem 1" succeed. See the
  GradedTests.hs file.
-}
pieces :: Int
pieces = -1

-- Implement a function cube that takes an int value and produces its cube.
cube :: Int -> Int
cube = error "cube unimplemented"

-- Problem 1-2
{-
  Write a function "centsOf" that takes
   q - a number of quarters
   d - a number of dimes
   n - a number of nickels
   p - a number of pennies
  (all numbers non-negative)
  and computes the total value of the coins in cents:
-}
centsOf :: Int -> Int -> Int -> Int -> Int
centsOf = error "centsOf unimplemented"

-- Problem 1-3
{-
  Edit the function argument of the "Student-Provided Problem 3" test in
  ProvidedTests.hs so that "case1" passes, given the definition below.  You
  will need to remove the function body starting with error and replace it
  with something else.
-}
prob3Ans :: Int
prob3Ans = 42

{-
  Edit the function argument of the "Student-Provided Problem 3" test in
  ProvidedTests.hs so that "case2" passes, given the definition below:
-}
prob3Case2 :: Int -> Int
prob3Case2 x = prob3Ans - x

{-
  Replace 0 with a literal integer argument in the "Student-Provided Problem 3"
  test in ProvidedTests.hs so that "case3" passes, given the definition below:
-}
prob3Case3 :: Int
prob3Case3 =
  let aux = prob3Case2 10 in
  double aux

{-
  In this and later projects, you can add your own test cases to the
  ProvidedTests.hs file.  They are automatically run by the test harness.
-}

--------------------------------------------------------------------------------
--
-- PART 2: Tuples, Polymorphism, Pattern Matching
--
--------------------------------------------------------------------------------

{-
  NOTE: See RWH Chapter 2, section "Useful composite data types"
  Tuples are a built-in aggregate datatype that generalizes
  pairs, triples, etc.   Tuples have types like:
      (Int, Bool, String)
  At the value level, tuples are written using the same syntax
  as the types, so that (5, True, "hi") would have type
  (Int, Bool, String):
-}

triple :: (Int, Bool, String)
triple = (3, True, "some string")

-- Tuples can nest
pairOfTriples :: ((Int, Bool, String), (Int, Bool, String))
pairOfTriples = (triple, triple)

{-
  IMPORTANT!!  Be sure to learn this!

  You can destruct tuples and most other kinds of data by "pattern-matching".

  Pattern matching is a fundamental concept in Haskell: most
  non-trivial Haskell types are usually "destructed" or "taken apart" by
  pattern matching.  See RWH Chapter 3, section "Pattern matching"
  A "pattern" looks like a value, except that it can have 'holes'
  marked by _ that indicate irrelevant parts of the pattern, and
  binders, indicated by variables.

  Consider:

  f pat1 = case1
  f pat2 = case2

  This evaluates the argument to f until it reaches a value.
  Then, that value is 'matched' against the patterns pat1, pat2, etc.
  until a match is found.  When the first match is found, the
  variables appearing in the pattern are bound to the corresponding parts
  of the value and the case associated with the pattern is executed.

  If no match is found, GHC will raise an exception.
  If your patterns are not exhaustive -- i.e. they do not cover
  all of the possible cases, the compiler will issue a (usually very
  helpful) warning with -Wincomplete-patterns enabled.
  The {-# OPTIONS_GHC ... #-} at the top of this file enables
  -Wincomplete-patterns while processing this file. It is in your
  best interest to include this pragma in every Haskell file
  you write.
-}

{-
  Tuples are polymorphic -- you can create tuples of any
  datatypes.  See RWH Chapter 3, section "Parameterised types"

  The polymorphic parts of types in Haskell are written
  with variables starting with a lower-case letter.
  What you might write in Java as
   List<A>  you would write in Haskell as 'List a'
  Similarly, Map<A,B> would be written as 'Map a b'
-}

-- Example pattern matching against tuples:
firstOfThree :: (a, b, c) -> a
firstOfThree (x, _, _) = x

t1 :: Int
t1 = firstOfThree triple    -- binds t1 to 3

secondOfThree :: (a, b, c) -> b
secondOfThree (_, x, _) = x

t2 :: Bool
t2 = secondOfThree triple   -- binds t2 to True

{-
  This generic function takes an arbitrary input, x, and
  returns a pair both of whose components are the given input:
-}
pairUp :: a -> (a, a)
pairUp x = (x, x)



-- Part 2 Problems ----------------------------------------------------------

{-
  Problem 2-1

  Complete the definition of thirdOfThree; be sure to give it
  the correct type signature:
-}
thirdOfThree = error "thirdOfThree unimplemented"

{-
  Problem 2-2

  Implement a function compose_pair of the given type that takes
  a pair of functions and composes them in sequence.  Note that
  you must return a function.  See the test cases in GradedTests.hs
  for examples of its use.
-}

composePair :: (b -> c, a -> b) -> a -> c
composePair = error "composePair unimplemented"


--------------------------------------------------------------------------------
--
-- PART 3: Lists and Recursion
--
--------------------------------------------------------------------------------

{-
  Haskell has a build-in datatype of polymorphic lists.

  [] is the nil list
  if
    h   is a head-value of type t
    tl  is a list of elements of type t
  then
   h:tl  is a list with h as the head and tl as the tail

  `:`  is pronounced "cons", as it constructs a list.
-}

list1 :: [Int]
list1 = 3:2:1:[]

-- Lists can also be written using the [v1,v2,v3] notation:

list1' :: [Int]
list1' = [3,2,1]     -- this is equivalent to list1

-- Lists are homogeneous -- they hold values of only one type:
{- Uncomment to get a type error; recomment to compile:
badList = [1,"hello",true]
-}


{-
  As usual in Haskell, we use pattern matching to destruct lists.
  For example, to determine whether a list has length 0 we need to
  do a case-analysis (via pattern matching) to see whether it is nil
  or is non-empty.  The following function takes a list l and
  determines whether l is empty:
-}
isEmpty :: [a] -> Bool
isEmpty []    = True  -- nil case
isEmpty (_:_) = False -- non-nil case

ans1 :: Bool
ans1 = isEmpty []  -- evaluates to True

ans2 :: Bool
ans2 = isEmpty list1  -- evaluates to False

{-
  Lists are an example of a "disjoint union" data type -- they are either
  empty [] or have some head value consed on to a tail h:tl.
  Haskell provides programmers with mechanisms to define their own polymorphic
  disjoint union and potentially recursive types using the 'data' keyword.
  See RWH Chapter 3.
-}

-- A user-defined polymorphic type (List a) can be defined within Haskell by:

data MyList a
  = Nil                         -- my version of []
  | Cons a (MyList a)           -- Cons h tl is my version of h:tl
  deriving ( Eq   -- This means we can compare MyLists for equality
           , Show -- This means we can print MyLists for debugging
           )

{-
  We build a MyList by using its 'constructors' (specified in the branches)
  For example, compare myList1 below to list1 defined by built-in lists
  above:
-}
myList1 :: MyList Int
myList1 = Cons 3 (Cons 2 (Cons 1 Nil))

{-
  Pattern matching against a user-defined datatype works the same as for
  a built-in type.  The cases we need to consider in a pattern are given by
  the cases of the type definition.  For example, to write the isEmpty
  function for MyList we do the following.  Compare it with isEmpty:
-}
isMyListEmpty :: MyList a -> Bool
isMyListEmpty Nil        = True
isMyListEmpty (Cons _ _) = False

{-
  IMPORTANT!! Be sure to learn this!

  Recursion:  The built in list type and the MyList type we defined above
  are recursive -- they are defined in terms of themselves.  To implement
  useful functions over such datatypes, we often need to use recursion.

  In a function definition, you can call the function being defined.  As usual
  you must be careful not to introduce infinite loops.

  Recursion plus pattern matching is a very powerful combination.  Here is
  a recursive function that sums the elements of an integer list:
-}

sumList :: [Int] -> Int
sumList []     = 0
sumList (x:xs) = x + sum xs  -- note the recursive call to sum

sumAns1 :: Int
sumAns1 = sumList [1,2,3]    -- evaluates to 6

{-
  Here is a function that takes a list and determines whether it is
  sorted and contains no duplicates according to the built-in
  polymorphic inequality test <

  Note that it uses nested pattern matching to name the
  first two elements of the list in the third case of the match:
-}
isSorted :: Ord a => [a] -> Bool
isSorted [] = True        -- zero elements: always sorted
isSorted (_:[]) = True    -- one element: always sorted
isSorted (h1 : h2 : tl)   -- more than one element: compare and recur
  = h1 < h2 && isSorted (h2:tl)

{-
  The isSorted function uses *constrained polymorphism*. The 'Ord a =>'
  piece is a *constraint*; it says that the isSorted function works for
  any type, as long as that type is ordered. Try removing the 'Ord a =>'
  constraint, and you will get an error on the use of <, which is allowed
  only over ordered types.

  RWH Chapter 6 covers this in detail.
-}

isSortedAns1 :: Bool
isSortedAns1 = isSorted [1,2,3]    -- True

isSortedAns2 :: Bool
isSortedAns2 = isSorted [1,3,2]    -- false

{-
  The standard library Data.List implements many useful functions for
  list manipulation. Because these are so useful, in fact, many operations
  have been generalized to work over a variety of structures; this makes
  their types hard to read. The best place to get easy-to-read documentation
  on these functions is at
  http://hackage.haskell.org/package/base-4.12.0.0/docs/GHC-OldList.html
  The functions listed there are really imported from Data.List, even though
  that file describes GHC.OldList.

  You will recreate some of these functions below. Here is map, one of the
  most useful:
-}
map :: (a -> b) -> [a] -> [b]
map _ []       = []
map f (x : xs) = f x : map f xs

{-
  The map function is standard in Haskell and is automatically available
  in all Haskell programs. To prevent GHC from getting confused between
  the definition of map here and the standard one, the top of this file
  says that we wish to *hide* map when importing the Prelude. The Prelude
  is automatically imported in every Haskell file and contains many useful
  definitions. It is documented at

  http://hackage.haskell.org/package/base-4.12.0.0/docs/Prelude.html
-}

mapAns1 :: [Int]
mapAns1 = map double [1,2,3]   -- evaluates to [2,4,6]

mapAns2 :: [(Int, Int)]
mapAns2 = map pairUp [1,2,3]  -- evaluates to [(1,1),(2,2),(3,3)]

{-
  The MyList type is isomorphic to the built-in lists.
  The recursive function below converts a MyList to a built-in list.
-}

myListToList :: MyList a -> [a]
myListToList Nil         = []
myListToList (Cons x xs) = x : myListToList xs

{-
  Haskell defines its String type to be just a list of Characters. So,
  functions that work on lists will work on Strings, too.
-}

stringAsListEx1 :: Bool
stringAsListEx1 = isSorted "ghosty"   -- this is True

stringAsListEx2 :: Bool
stringAsListEx2 = isSorted "ghouly"   -- this is False

-- Part 3 Problems ----------------------------------------------------------

{-
  Problem 3-1

  Implement listToMyList with the type signature below; this is
  the inverse of the myListToList function given above.
-}

listToMyList :: [a] -> MyList a
listToMyList = error "listToMyList unimplemented"

{-
  Problem 3-2

  Implement the function append, which takes two lists (of the same
  types) and concatenates them.  Do not use the library function.

  (append [1,2,3] [4,5]) should evaluate to [1,2,3,4,5]
  (append [] []) should evaluate to []

  The append function is normally written with the infix operator ++ in
  Haskell.
-}

append :: [a] -> [a] -> [a]
append = error "append unimplemented"

{-
  Problem 3-3

  Implement the library function reverse, which reverses a list. In this solution,
  you might want to call append.  Do not use the library function.
-}

reverse :: [a] -> [a]
reverse = error "reverse unimplemented"

{-
  Problem 3-4

  Read part of RWH Chapter 4 about "tail recursion" and implement a tail
  recursive version of reverse. Note that you will need a helper function that
  takes an extra parameter -- it should be defined using a local let or where
  definition. The reverse_t function itself should not be recursive. Tail
  recursion is important to efficiency -- GHC will compile a tail recursive
  function to a simple loop.
-}

reverse_t :: [a] -> [a]
reverse_t = error "reverse_t unimplemented"

{-
  Problem 3-5

  Implement insert, a function that, given an element x and a sorted list l
  (i.e. one for which isSorted returns True) returns the list obtained by
  inserting x into the list l at the proper location.  Note that if x is
  already in the list then insert should just return the original list.

  You will need to use a guard (see RWH Chapter 3, "Conditional evaluation with guards").
  Note also that the "Ord a =>" constraint below means that we know that
  the type 'a' has an ordering; this is what allows us to compare two elements
  of type 'a'.
-}

insert :: Ord a => a -> [a] -> [a]
insert = error "insert unimplemented"

{-
  Problem 3-6

  Implement union, a function that takes two sorted lists and returns the
  sorted list containing all of the elements from both of the two input lists.
  Hint: you might want to use the insert function that you just defined.
-}

union :: Ord a => [a] -> [a] -> [a]
union = error "union unimplemented"


--------------------------------------------------------------------------------
--
-- PART 4: Expression Trees and Interpreters
--
--------------------------------------------------------------------------------

{- TERMINOLOGY: "Object" level vs. "Meta" level

  When we implement a compiler, we use code in one programming language to
  implement the features of another language.  The language we are implementing
  is called the "object language" -- it is the "object of study".  In contrast,
  the language we use to implement the object language in is called the
  "meta language" -- it is used to "talk about" the object language.  In this
  course, Haskell will usually be the "meta language".

  We will implement several different object languages in this course.  Within
  the metalanguage, we use ordinary datatypes: lists, tuples, trees, unions,etc.
  to represent the features of the object language.  A compiler is just a
  function that translates one representation of an object language into
  another (usually while preserving some notion of a program's 'behavior').

  The representation of a language is often best done using an "abstract syntax
  tree" -- a representation that hides concrete details about parsing,
  infix syntax, syntactic sugar, etc.
-}

{-
  In this course we will be working with many kinds of abstract syntax trees.
  Such trees are a convenient way of representing the syntax of a programming
  language.  In Haskell, we build such datatypes using the technology we have
  already seen above.

  Here is a simple datatype of arithemetic expressions.
-}

-- An object language: a simple datatype of 64-bit integer expressions
data Exp
  = Var String       -- string representing an object-language variable
  | Const Int        -- a constant Int value
  | Add Exp Exp      -- sum of two expressions
  | Mult Exp Exp     -- product of two expressions
  | Neg Exp          -- negation of an expression
  deriving (Eq, Show)

{-
  An object-language arithmetic expression whose concrete (ASCII) syntax is
  "2 * 3" could be represented like this:
-}
e1 :: Exp
e1 = Mult (Const 2) (Const 3)   -- "2 * 3"

{-
  If the object-level expression contains variables, we represent them as
  strings, like this:
-}
e2 :: Exp
e2 =  Add (Var "x") (Const 1)    -- "x + 1"

-- Here is a more complex expression that involves multiple variables:
e3 :: Exp
e3 = Mult (Var "y") (Mult e2 (Neg e2))     -- "y * ((x+1) * -(x+1))"

{-
   Problem 4-1

  Implement varsOf -- a function that, given an expression e returns
  a list containing exactly the strings representing variables that appear
  in the expression.  The result should be set-like -- that is, it should
  contain no duplicates and be sorted (according to isSorted).

  For example:
    varsOf e1 should produce []
    varsOf e2 should produce ["x"]
    varsOf e3 should produce ["x","y"]

  Hint: you need to pattern match on the Exp.
  Hint: you probably want to use the 'union' function you wrote for Problem 3-5.
-}

varsOf :: Exp -> [String]
varsOf = error "varsOf unimplemented"

{-
  How should we _interpret_ (i.e. give meaning to) an expression?

   Some examples:
     Add (Const 3) (Const 5)                     denotes 8
     Mult (Const 2) (Add (Const 3) (Const 5L))   denotes 16

  What about Add (Var "x") (Const 1)?
  What should we do with (Var "x")?

  Each expression denotes an Int value, but since it can contain variables,
  we need an "evaluation context" that maps variable names to Int values.

  NOTE: an _evaluation context_ is just a finite map from variables to values.

  One simple (but not particularly efficient) way to represent a context is as
  an "association list" that is just a list of (String, Int) pairs:
-}

type Ctxt = [(String, Int)]

{-
  This definition is a *type synonym*, making Ctxt mean the exact same thing
  as [(String, Int)]. This is different from *data* declarations, which
  intoroduce new user-defined data types.
-}

-- Here are some example evalution contexts:
ctxt1 :: Ctxt
ctxt1 = [("x", 3)]            -- maps "x" to 3

ctxt2 :: Ctxt
ctxt2 = [("x", 2), ("y", 7)]  -- maps "x" to 2, "y" to 7

{-
  When interpreting an expression, we need to look up the value
  associated with a variable in an evaluation context.
   For example:
      lookup "x" ctxt1    should yield 3
      lookup "x" ctxt2    should yield 2
      lookup "y" cxtx2    should yield 7

  What if we lookup a variable that doesn't appear in the context?  In that
  case we raise an exception.  The simplest way to do this in Haskell is
  to call 'error', which has type String -> a. That is, 'error' takes a String
  argument and returns a value of any type, whatsoever. We will see more nuanced
  ways of throwing exceptions later in the course.

  There is one other case to consider: if the context has two bindings for a
  variable, the one closer to the head of the list should take precedence.
   For example:
   lookup "x" [("x", 1),("x", 2)]    should yield 1 (not 2)
-}

{-
  Problem 4-2

  Implement the lookup function with the signature given below. It should find
  the Int value associated with a given string in the ctxt c.  If there is no
  such value, it should call 'error'.
-}

lookup :: String -> Ctxt -> Int
lookup = error "lookup unimplemented"

{-
  Problem 4-3

  At last, we can write an interpreter for Exp.  This is just a function
  'interpret' that, given an evaluation context c and an expression e, computes
  an Int value corresponding to the expression.  If the expression mentions a
  variable that does not appear in the context, interpret should call error.

  For example:
     interpret ctxt1 e1    should yield 6
     interpret ctxt1 e2    should yield 4
     interpret ctxt1 e3    should call error

  Note that the interpreter should recursively call itself to obtain the Int
  values of subexpressions.

  You should test your interpeter on more examples than just those provided in
  GradedTests.hs.
-}

interpret :: Ctxt -> Exp -> Int
interpret = error "interpret unimplemented"

{-
  Problem 4-4

  Now, write an _optimizer_ for expressions.  An optimizer is just a function
  that tries to reduce the number of operations by doing some work "at compile
  time" rather than at "run time".

  For example:
     optimize (Add (Const 3) (Const 4))     might yield (Const 7)
     optimize (Mult (Const 0) (Var "x"))    might yield (Const 0L)

  No matter what optimizations your function performs, it should not change the
  value computed by the expression (if there is one).  That is, for every
  context c and exp e, if c has bindings for all the variables in e, it should
  be the case that:

      (interpret c e)  =  (interpret c (optimize e))

  Note that it is not always possible to reduce the size of an expression:
  (Var "x")  is already "optimal". You will also have to optimize sub-
  expressions before applying further optimizations -- this means that you will
  need to use nested match expressions.

  Here is a (slightly) more complex example:
   optimize (Add (Const 3) (Mult (Const 0) (Var "x")))    yields (Const 3)

  Note that your optimizer won't be complete (unless you work *very* hard)
  for example,
    Add (Var "x") (Neg (Add (Var "x") (Neg (Const 1))))      "x - (x - 1)"
  is equivalent to (Const 1), but we do not expect your optimizer to
  discover that.  Indeed, there are many algebraic laws that would be hard
  to fully exploit.

  You can get full credit for this problem if your optimizer:
    (1) doesn't change the meaning of an expression in any context
        that provides bindings for all of the expression's variables
    (2) recursively reduces the size of the expression in "obvious" cases
          -- adding 0, multiplying by 0 or 1, constants, etc.

  Hint: what simple optimizations can you do with Neg?
-}

optimize :: Exp -> Exp
optimize = error "optimize unimplemented"

--------------------------------------------------------------------------------
--
-- PART 5: Compiling the Expression Language to a Stack-Based Language
--
--------------------------------------------------------------------------------

{-
  The interpreter for the expression language above is simple, but the language
  itself is fairly high-level in the sense that we are using many meta-language
  (Haskell) features to define the behavior of the object language.  For example,
  the natural way of defining the 'interpret' function as a recursive Haskell
  function means that we rely on Haskell's function calls and its order of
  evaluation.

  Let's look at a closely-related but "lower-level" language for defining
  arithmetic expressions.

  Unlike the language Exp defined above, which has nested subexpressions, a
  program in the new language will simply be a list of instructions that specify
  a sequence of actions to carry out. Also unlike the Exp language, which uses
  Haskell's recursive functions and hence a (meta-level) call stack to keep track
  of the order in which subexpressions are processed, this new language will
  explicitly manipulate its own stack of Int values.

  (ASIDE: historically, several companies actually built calculators that used
   this "reverse polish notation" programming language to express their
   computations.  Search online for it to find out more...)
-}

{-
  The instructions of the new language are defined as follows. Note that the
  instructions IMul, IAdd, and INeg pop their input values from the stack and
  push their results onto the stack.
-}

data Insn
  = IPushC Int        -- push an int64 constant onto the stack
  | IPushV String     -- push (lookup string ctxt) onto the stack
  | IMul              -- multiply the top two values on the stack
  | IAdd              -- add the top two values on the stack
  | INeg              -- negate the top value on the stack
  deriving (Eq, Show)

-- A stack program is just a list of instructions.
type Program = [Insn]

{-
  The stack itself is represented as a list of Int values, where the head of
  the list is the top of the stack.
-}
type Stack = [Int]

{-
  The operational semantics (i.e. behavior) of this new language can easily be
  specified by saying how one instruction step is performed.  Each step takes a
  stack and returns the new stack resulting from carrying out the computation.

  Note that this function is not recursive, and also that it might raise an
  exception if the stack doesn't have enough entries.  As with the Exp
  language interpreter, we use a context to lookup the value of variables.
-}

step :: Ctxt -> Stack -> Insn -> Stack
step _ s             (IPushC n) = n : s
step c s             (IPushV x) = lookup x c : s
step _ (v1 : v2 : s) IMul       = v1 * v2 : s
step _ (v1 : v2 : s) IAdd       = v1 + v2 : s
step _ (v : s)       INeg       = (-v) : s
step _ _ _ = error "stack had too few values"

{-
  To define how a program executes, we simply iterate over the
  instructions, threading the stack through.
-}

execute :: Ctxt -> Stack -> Program -> Stack
execute _ s []         = s  -- no more instructions to execute
execute c s (i : cont) = execute c (step c s i) cont

{-
  If you want to be slick, you can write the above equivalently using
  foldl (which is tail recursive and hence iterative):
-}
execute' :: Ctxt -> Stack -> Program -> Stack
execute' c = foldl (step c)

{-
  Let us define 'answer' to mean the sole value of a stack containing only one
  element.  This makes sense since, if a program left the stack empty, there
  wouldn't be any Int value computed and if there is more than one value on
  the stack, that means that the program 'stopped' too early.
-}
answer :: Stack -> Int
answer [n] = n
answer _   = error "no answer"

{-
  Finally, we can 'run' a program in a given context by executing it starting
  from the empty stack and returning the answer that the program computes.
-}
run :: Ctxt -> Program -> Int
run c p = answer (execute c [] p)

{-
  As an example program in this stack language, consider the following program
  that runs to produce the answer 6.

  Compare this program to the 'exp' program called e1 above. They both compute
  the value 2 * 3.
-}
p1      = [IPushC 2, IPushC 3, IMul]
answer1 = run [] p1

{-
  Problem 5

  Implement a function 'compile' that takes a program in the Exp language and
  translates it to an equivalent program in the Stack language.

  For example, (compile e1) should yield the program p1 given above.

  Correctness means that:
  For all expressions e, contexts c (defining the variables in e), and Int
  values v:

     (interpret c e) = v       if and only if
     (run c (compile e)) = v

  Hints:
   - Think about how to define your compiler compositionally so that you build
     up the sequence of instructions for a compound expression like Add e1 e2
     from the programs that compute e1 and e2.

   - You may want to use the append function to glue together two programs.

   - You should test the correctness of your compiler on several examples.
-}

compile :: Exp -> Program
compile = error "compile unimplemented"


--------------
-- Epilogue --
--------------

{-
  Whew!  That was a whirl-wind tour of Haskell.  There are still quite a few
  features we haven't seen -- we'll mention them in the next project and later
  as needed.  However, even with this little bit of Haskell and a few basic ideas
  you've probably seen in other languages, you can already go quite far.

  Take a quick read through Test/Assert.hs -- this is a small libary for writing
  unit tests that we'll use for grading throughout this course. It makes
  extensive use of unions, lists, strings, ints, and a few List and Printf
  library functions, but otherwise it should be fairly readable to you already...
-}
