{- Author: Richard Eisenberg
   File: Autograde.hs

   This is a much reduced version of the Autograde module used internally
   to drive the autograder facility on Gradescope. You are not expected
   to be able to understand this code.
-}

{-# LANGUAGE OverloadedStrings #-}

module Autograde where

import Data.Text ( Text )
import qualified Data.Text.IO as T
import Test.HUnit ( Test(..), Counts(..), runTestTT, Assertion, assertFailure )
import Data.Monoid ( (<>) )
import Control.Monad ( when )
import Control.DeepSeq
import Control.Exception

-- These two declarations allow us to combine Counts easily
instance Semigroup Counts where
  c1 <> c2 = Counts { cases = cases c1 + cases c2
                    , tried = tried c1 + tried c2
                    , errors = errors c1 + errors c2
                    , failures = failures c1 + failures c2 }

instance Monoid Counts where
  mempty = Counts 0 0 0 0

runTest :: Test -> IO Counts
runTest tst = do
  counts <- runTestTT tst
  validate counts
  return counts

mkTest :: Text  -- name
       -> Int -- max score
       -> Test  -- student's tests
       -> Test  -- my secret tests
       -> IO Counts
mkTest name _max stud_tests _my_tests = do
  T.putStrLn $ "Running tests for " <> name
  runTest stud_tests

mkVisTest :: Text
          -> Int
          -> Test
          -> IO Counts
mkVisTest name _max stud_tests = do
  T.putStrLn $ "Running tests for " <> name
  runTest stud_tests

mkSecretTest :: Text
             -> Int   -- max score
             -> Test
             -> IO Counts
mkSecretTest _name _max _my_test = return mempty

validate :: Counts -> IO ()
validate counts
  = when (tried counts /= cases counts) $
    putStrLn $ "Not all cases tried: " ++ show counts

-- useful utility function

-- Fully evaluates the argument and returns Right it. If 'error' is
-- called during evaluation, returns Left the error string.
tryEval :: NFData a => a -> IO (Either String a)
tryEval x = do
  result <- try (evaluate (force x))
  return $ case result of
    Left (ErrorCall err_string) -> Left err_string
    Right success               -> Right success

assertException :: NFData a => a -> Assertion
assertException x = do
  result <- tryEval x
  case result of
    Left _   -> return ()
    Right _  -> assertFailure "Exception expected"

testException :: NFData a => a -> Test
testException = TestCase . assertException
