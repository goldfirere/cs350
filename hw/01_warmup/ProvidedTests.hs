
module ProvidedTests where

import Test.HUnit
import HelloHaskell

-- These tests are provided by you; they will be graded manually

-- You should add additional test cases here to help you debug your program.
-- These use the testing library HUnit. Learn more at https://github.com/hspec/HUnit

providedTests :: Test
providedTests = TestList [
  "Student-Provided Tests For Problem 1-3" ~: [
    ("case1" ~: error "Problem 3 case1 test unimplemented" ~?= prob3Ans),
    ("case2" ~: error "Problem 3 case2 test unimplemented" ~?= (prob3Case2 17)),
    ("case3" ~: prob3Case3 ~?= 0)
  ]
 ]
