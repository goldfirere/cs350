{-# LANGUAGE OverloadedStrings, ExtendedDefaultRules #-}


module GradedTests where

import Test.HUnit
import HelloHaskell  ( pieces, cube, centsOf, thirdOfThree, composePair, triple
                     , pairUp, double, reverse, listToMyList, myListToList, reverse_t
                     , append, union, insert, MyList(..), Exp(..), lookup
                     , interpret, optimize, run, compile, Ctxt, Insn(..)
                     , varsOf, ctxt1, ctxt2, e1, e2, e3
                     )
import Autograde
import Prelude hiding ( reverse, lookup )



-- Test suite for HelloHaskell.hs

{- Do NOT modify this file -- we will overwrite it with our
   own version when we test your project.
-}

-- These tests will be used to grade your assignment

hiddenTests :: Test
hiddenTests = TestList []  -- this is replaced when grading for real

-- *** Part 1 Tests ***
part1Tests = sequence [
  mkVisTest "Problem1-1" 3
    -- ~?= checks whether the two values are equal
    (TestList [ "pieces" ~: pieces ~?= 8
              , "cube0"  ~: cube 0 ~?= 0
              , "cube1"  ~: cube 1 ~?= 1
              , "cube2"  ~: cube 2 ~?= 8
              , "cube3"  ~: cube (-1) ~?= (-1)])
  ,

  mkVisTest "Problem1-2" 3
    (TestList [ "centsOf1" ~: centsOf 0 0 0 0 ~?= 0
              , "centsOf2" ~: centsOf 1 1 1 1 ~?= 41
              , "centsOf3" ~: centsOf 1 2 3 4 ~?= 64
              , "centsOf4" ~: centsOf 1 0 0 0 ~?= 25
              , "centsOf5" ~: centsOf 0 1 0 0 ~?= 10
              , "centsOf6" ~: centsOf 0 0 1 0 ~?= 5
              , "centsOf7" ~: centsOf 0 0 0 1 ~?= 1
              ])
  ]

--- *** Part 2 Tests ***
part2Tests = sequence [
  mkVisTest "Problem2-1" 3
    (TestList ["thirdOfThree1" ~: thirdOfThree triple ~?= "some string"
              ,"thirdOfThree2" ~: thirdOfThree (1,2,3) ~?= 3
              ,"thirdOfThree3" ~: thirdOfThree ((),"a",False) ~?= False
              ])
  ,
  mkVisTest "Problem2-2" 5
    (let id :: Int -> Int
         id x = x

         const3 :: String -> Int
         const3 _ = 3
      in
      TestList ["composePair1" ~: composePair (id, const3) "a" ~?= 3
               ,"composePair2" ~: composePair (fst, pairUp) "a" ~?= "a"
               ,"composePair3" ~: composePair (double, fst) (pairUp 5) ~?= 10
               ])
  ]

-- *** Part 3 Tests ***
part3Tests = sequence [
  mkVisTest "Problem3-1" 5
    (TestList [
    ("listToMyList1" ~: listToMyList [] ~?= Nil),
    ("listToMyList2" ~: listToMyList [1] ~?= (Cons 1 Nil)),
    ("listToMyList3" ~: listToMyList ["a","b"] ~?= (Cons "a" (Cons "b" Nil))),
    ("listToMyList4" ~: myListToList (listToMyList [1,2,3,4,5]) ~?= [1,2,3,4,5])
    ])
  ,

  mkVisTest "Problem3-2" 5 (TestList [
    ("append1" ~: append [] [] ~?= []),
    ("append2" ~: append [] [1] ~?= [1]),
    ("append3" ~: append [1] [] ~?= [1]),
    ("append4" ~: append [1] [1] ~?= [1,1]),
    ("append5" ~: append [1,2] [3] ~?= [1,2,3]),
    ("append6" ~: append [1] [2,3] ~?= [1,2,3]),
    ("append7" ~: append [True] [False] ~?= [True,False])
  ])
  ,

  mkVisTest "Problem3-3" 5 (TestList [
    ("reverse1" ~: reverse [] ~?= []),
    ("reverse2" ~: reverse [1] ~?= [1]),
    ("reverse3" ~: reverse [1,2] ~?= [2,1]),
    ("reverse4" ~: reverse ["a","b"] ~?= ["b","a"])
  ])
  ,

  mkVisTest "Problem3-4" 5 (TestList [
    ("reverse_t1" ~: reverse_t [] ~?= []),
    ("reverse_t2" ~: reverse_t [1] ~?= [1]),
    ("reverse_t3" ~: reverse_t [1,2] ~?= [2,1]),
    ("reverse_t4" ~: reverse_t ["a","b"] ~?= ["b","a"])
  ])
  ,

  mkVisTest "Problem3-5" 5 (TestList [
    ("insert1" ~: insert 1 [] ~?= [1]),
    ("insert2" ~: insert 1 [1] ~?= [1]),
    ("insert3" ~: insert 1 [2] ~?= [1,2]),
    ("insert4" ~: insert 1 [0] ~?= [0,1]),
    ("insert5" ~: insert 1 [0,2] ~?= [0,1,2]),
    ("insert6" ~: insert "b" ["a","c"] ~?= ["a","b","c"])
  ]),

  mkVisTest "Problem3-6" 5 (TestList [
    ("union1" ~: union [] [] ~?= []),
    ("union2" ~: union [1] [] ~?= [1]),
    ("union3" ~: union [] [1] ~?= [1]),
    ("union4" ~: union [1] [1] ~?= [1]),
    ("union5" ~: union [1] [2] ~?= [1,2]),
    ("union6" ~: union [2] [1] ~?= [1,2]),
    ("union7" ~: union [1,3] [0,2] ~?= [0,1,2,3]),
    ("union8" ~: union [0,2] [1,3] ~?= [0,1,2,3])
  ])
  ]


-- *** Part 4 Tests ***



part4Tests = sequence [
  mkTest "Problem4-1" 5 (TestList [
    ("varsOf1" ~: varsOf e1 ~?= []),
    ("varsOf2" ~: varsOf e2 ~?= ["x"]),
    ("varsOf3" ~: varsOf e3 ~?= ["x", "y"])])
    hiddenTests
  ,

  mkTest "Problem4-2" 5 (TestList [
    ("lookup1" ~: lookup "x" ctxt1 ~?= 3),
    ("lookup2" ~: lookup "x" ctxt2 ~?= 2),
    ("lookup3" ~: lookup "y" ctxt2 ~?= 7),
    ("lookup4" ~: testException (lookup "y" ctxt1)),
    ("lookup5" ~: lookup "x" [("x", 1),("x", 2)] ~?= 1)])
    hiddenTests
  ,

  mkVisTest "Problem4-3" 5 (TestList [
    ("interpret1" ~: interpret ctxt1 e1 ~?= 6),
    ("interpret2" ~: interpret ctxt1 e2 ~?= 4),
    ("interpret3" ~: testException (interpret ctxt1 e3))
  ]),

  mkSecretTest "Problem4-3harder" 5 (TestList [
  
  ]),

  mkVisTest "Problem4-4" 5 (TestList [
    ("optimize1" ~: optimize (Add (Const 3) (Const 4)) ~?= (Const 7)),
    ("optimize2" ~: optimize (Mult (Const 0) (Var "x")) ~?= (Const 0)),
    ("optimize3" ~: optimize (Add (Const 3) (Mult (Const 0) (Var "x"))) ~?= (Const 3))
  ]),

  mkSecretTest "Problem4-4harder" 5 (TestList [
  
  ]),

  mkSecretTest "Problem4-4hardest" 5 (TestList [
  
  ]),

  mkSecretTest "Problem5" 5 (TestList [
  
  ])
  ]

gradedTests = concat <$> sequence [part1Tests
                                  ,part2Tests
                                  ,part3Tests
                                  ,part4Tests
                                  ]
