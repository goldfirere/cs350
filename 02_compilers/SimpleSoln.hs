{- Author: Steve Zdancewic
   Translator: Richard Eisenberg
   File: Simple.hs

   An implementation of the *simple* language.
-}

module Simple where

-- grammar -----------------------------------------------------------------
{-
 BNF grammar for this simple language:
  <exp> ::=
         |  <X>
         |  <exp> + <exp>
         |  <exp> * <exp>
         |  <exp> < <exp>
         |  <integer contant>
         |  (<exp>)

  <cmd> ::=
         |  skip
         |  <X> = <exp>
         |  ifNZ <exp> { <cmd> } else { <cmd> }
         |  whileNZ <exp> { <cmd> }
         |  <cmd>; <cmd>
-}

-- Abstract Syntax (AST) ----------------------------------------------------

type Var = String

data Exp
  = Var Var
  | Add Exp Exp
  | Mul Exp Exp
  | Lt  Exp Exp
  | Lit Int

data Cmd
  = Skip
  | Assn Var Exp
  | IfNZ Exp Cmd Cmd
  | WhileNZ Exp Cmd
  | Seq Cmd Cmd



-- AST for Factorial Example ------------------------------------------------
{-
        X = 6;
	ANS = 1;
	whileNZ (x) {
  		ANS = ANS * X;
  		X = X + -1;
	}
-}

factorial :: Cmd
factorial =
  let x   = "X"
      ans = "ANS" in
  Seq (Assn x (Lit 6))
      (Seq (Assn ans (Lit 1))
           (WhileNZ (Var x)
                    (Seq (Assn ans (Mul (Var ans) (Var x)))
                         (Assn x (Add (Var x) (Lit (-1)))))))

-- Interpreter --------------------------------------------------------------

type State = Var -> Int

interpretExp :: State -> Exp -> Int
interpretExp s (Var x)     = s x
interpretExp s (Add e1 e2) = interpretExp s e1 + interpretExp s e2
interpretExp s (Mul e1 e2) = interpretExp s e1 * interpretExp s e2
interpretExp s (Lt  e1 e2) = if interpretExp s e1 < interpretExp s e2 then 1 else 0
interpretExp _ (Lit i)     = i

update :: State -> Var -> Int -> State
update s x v = \ y -> if x == y then v else s y

interpretCmd :: State -> Cmd -> State
interpretCmd s Skip = s
interpretCmd s (Assn x e) = update s x (interpretExp s e)
interpretCmd s (IfNZ e c1 c2) = if interpretExp s e /= 0
                                then interpretCmd s c1
                                else interpretCmd s c2
interpretCmd s (WhileNZ e c) = interpretCmd s (IfNZ e (Seq c (WhileNZ e c)) Skip)
interpretCmd s (Seq c1 c2) = let s1 = interpretCmd s c1 in
                             interpretCmd s1 c2

initState :: State
initState = \ _ -> 0
