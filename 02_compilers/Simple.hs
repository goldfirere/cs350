{- Author: Steve Zdancewic
   Translator: Richard Eisenberg
   File: Simple.hs

   An implementation of the *simple* language.
-}

module Simple where

-- grammar -----------------------------------------------------------------
{-
 BNF grammar for this simple language:
  <exp> ::=
         |  <X>
         |  <exp> + <exp>
         |  <exp> * <exp>
         |  <exp> < <exp>
         |  <integer contant>
         |  (<exp>)

  <cmd> ::=
         |  skip
         |  <X> = <exp>
         |  ifNZ <exp> { <cmd> } else { <cmd> }
         |  whileNZ <exp> { <cmd> }
         |  <cmd>; <cmd>
-}

-- Abstract Syntax (AST) ----------------------------------------------------

type Var = String

data Exp
  = Var Var
  | Add Exp Exp
  | Mul Exp Exp
  | Lt  Exp Exp
  | Lit Int

data Cmd
  = Skip
  | Assn Var Exp
  | IfNZ Exp Cmd Cmd
  | WhileNZ Exp Cmd
  | Seq Cmd Cmd



-- AST for Factorial Example ------------------------------------------------
{-
        X = 6;
	ANS = 1;
	whileNZ (x) {
  		ANS = ANS * X;
  		X = X + -1;
	}
-}

factorial :: Cmd
factorial =
  let x   = "X"
      ans = "ANS" in
  Seq (Assn x (Lit 6))
      (Seq (Assn ans (Lit 1))
           (WhileNZ (Var x)
                    (Seq (Assn ans (Mul (Var ans) (Var x)))
                         (Assn x (Add (Var x) (Lit (-1)))))))

-- Interpreter --------------------------------------------------------------

type State = Var -> Int

interpretExp :: State -> Exp -> Int
interpretExp = undefined

update :: State -> Var -> Int -> State
update s x v = \ y -> if x == y then v else s y

interpretCmd :: State -> Cmd -> State
interpretCmd = undefined

initState :: State
initState = \ _ -> 0
