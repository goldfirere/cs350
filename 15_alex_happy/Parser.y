-- -*- mode: haskell -*-
-- The above line sets the highlighting mode in Emacs

{
{- Author: Richard Eisenberg
   File: Parser.y

   Parses the BL boolean logic language.
-}

module Parser where

import Lexer
import BL

}

%name parse
%tokentype { Token }
%error     { (error . show) }

%token ID        { ID $$   }
       TRUE      { TRUE    }
       FALSE     { FALSE   }
       '|'       { OR      }
       '&'       { AND     }
       '->'      { IMPLIES }
       '~'       { NOT     }
       '('       { LPAREN  }
       ')'       { RPAREN  }

%right '->'
%left '|'
%left '&'
%nonassoc '~'

%%

bexp : TRUE            { Tru }
     | FALSE           { Fals }
     | ID              { Var $1 }
     | bexp '|' bexp   { Or $1 $3 }
     | bexp '&' bexp   { And $1 $3 }
     | bexp '->' bexp  { Implies $1 $3 }
     | '~' bexp        { Not $2 }
     | '(' bexp ')'    { $2 }

{

-- Convenient helper function
lexParse :: String -> BExp
lexParse = parse . lexBL

}
