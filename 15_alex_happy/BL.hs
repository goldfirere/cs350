{- Author: Richard Eisenberg
   File: BL.hs

   Defines the BL boolean logic language.
-}

module BL where

data BExp
  = Tru
  | Fals
  | Var String
  | Or BExp BExp
  | And BExp BExp
  | Implies BExp BExp
  | Not BExp
  deriving Show

render :: BExp -> String
render Tru = "true"
render Fals = "false"
render (Var s) = s
render (Or e1 e2) = "(" ++ render e1 ++ " | " ++ render e2 ++ ")"
render (And e1 e2) = "(" ++ render e1 ++ " & " ++ render e2 ++ ")"
render (Implies e1 e2) = "(" ++ render e1 ++ " -> " ++ render e2 ++ ")"
render (Not e) = "~" ++ render e

type Ctxt = [(String, Bool)]

eval :: Ctxt -> BExp -> Bool
eval _ Tru     = True
eval _ Fals    = False
eval c (Var s)
  | Just b <- lookup s c = b
  | otherwise            = error $ "Unknown variable: " ++ s
eval c (Or e1 e2) = eval c e1 || eval c e2
eval c (And e1 e2) = eval c e1 && eval c e2
eval c (Implies e1 e2) = not (eval c e1) || eval c e2
eval c (Not e) = not (eval c e)
