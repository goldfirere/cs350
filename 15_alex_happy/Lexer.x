-- -*- mode: haskell -*-
-- The line above sets the mode in Emacs

{
{- Author: Richard Eisenberg
   File: Lexer.x

   Lexer for the BL boolean-logic language.
-}

module Lexer where

}

%wrapper "basic"

@identifier = [A-Za-z]+

BL :-

-- this ignores whitespace; the $white macro is predefined
$white+             ;

true                { const TRUE }
false               { const FALSE }
@identifier         { ID }
"|"                 { const OR }
"&"                 { const AND }
"->"                { const IMPLIES }
"~"                 { const NOT }
"("                 { const LPAREN }
")"                 { const RPAREN }

{

data Token
  = ID String
  | TRUE
  | FALSE
  | OR
  | AND
  | IMPLIES
  | NOT
  | LPAREN
  | RPAREN
  deriving Show

lexBL :: String -> [Token]
lexBL = alexScanTokens

}
