{- Author: Richard Eisenbeg
   File: BL.hs

   Abstract syntax and evaluator for the Boolean logic language.
-}

module BL where

data Exp
  = Tru
  | Fals
  | Id String
  | Or Exp Exp
  | And Exp Exp
  | Implies Exp Exp
  | Not Exp
  deriving Show

render :: Exp -> String
render Tru             = "true"
render Fals            = "false"
render (Id s)          = s
render (Or e1 e2)      = "(" ++ render e1 ++ " | " ++ render e2 ++ ")"
render (And e1 e2)     = "(" ++ render e1 ++ " & " ++ render e2 ++ ")"
render (Implies e1 e2) = "(" ++ render e1 ++ " -> " ++ render e2 ++ ")"
render (Not e)         = "~" ++ render e

type Ctxt = [(String, Bool)]

eval :: Ctxt -> Exp -> Bool
eval _ Tru     = True
eval _ Fals    = False
eval c (Id s)
  | Just b <- lookup s c = b
  | otherwise            = error $ "Unknown variable: " ++ s
eval c (Or e1 e2) = eval c e1 || eval c e2
eval c (And e1 e2) = eval c e1 && eval c e2
eval c (Implies e1 e2) = not (eval c e1) || eval c e2
eval c (Not e) = not (eval c e)
