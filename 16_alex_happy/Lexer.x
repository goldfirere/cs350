-- -*- mode: haskell -*-

{
{- Author: Richard Eisenberg
   File: Lexer.x

   Lexer for the Boolean logic language.
-}

module Lexer where
}

%wrapper "basic"

$upper = A-Z
$lower = a-z
@identifier = [$upper$lower]+

BL :-

$white        ;
"true"        { \ _ -> TRUE    }
"false"       { \ _ -> FALSE   }
@identifier   { \ s -> VAR s   }
"&"           { \ _ -> AND     }
"|"           { \ _ -> OR      }
"~"           { \ _ -> NOT     }
"->"          { \ _ -> IMPLIES }
"("           { \ _ -> LPAREN  }
")"           { \ _ -> RPAREN  }

{

data Token
  = TRUE
  | FALSE
  | AND
  | OR
  | NOT
  | LPAREN
  | RPAREN
  | IMPLIES
  | VAR String
  deriving Show

-- lexBL is a friendlier name than alexScanTokens
lexBL :: String -> [Token]
lexBL = alexScanTokens

}
