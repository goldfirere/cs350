-- -*- mode: haskell -*-

{
{- Author: Richard Eisenberg
   File: Parser.y

   Parser for the Boolean logic language.
-}

module Parser where

import Lexer
import BL
}

%name parse                    -- `parse` is the name of the parser function
%tokentype { Token }           -- `Token` is the element in our input list
%error     { (error . show) }  -- (error . show) is called when trouble strikes

-- Declare terminals
%token
  TRUE    { TRUE    }
  FALSE   { FALSE   }
  '&'     { AND     }
  '|'     { OR      }
  '~'     { NOT     }
  '('     { LPAREN  }
  ')'     { RPAREN  }
  '->'    { IMPLIES }
  VAR     { VAR $$  }

-- Declare precendences
%right '->'
%left '|'
%left '&'
%nonassoc '~'

%%

BExp : TRUE            { Tru   }
     | FALSE           { Fals  }
     | VAR             { Id $1 }
     | BExp '|' BExp   { Or $1 $3 }
     | BExp '&' BExp   { And $1 $3 }
     | BExp '->' BExp  { Implies $1 $3 }
     | '~' BExp        { Not $2 }
     | '(' BExp ')'    { $2 }

-- These rules are currently ignored, but they would work
-- as an alternative to using precedence directives.
B1 : B2 '->' B1        { Implies $1 $3 }
   | B2                { $1 }

B2 : B2 '|' B3         { Or $1 $3 }
   | B3                { $1 }

B3 : B3 '&' B4         { And $1 $3 }
   | B4                { $1 }

B4 : '~' B4            { Not $2 }
   | '(' B1 ')'        { $2 }
   | TRUE              { Tru }
   | FALSE             { Fals }
   | VAR               { Id $1 }
