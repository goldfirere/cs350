-- -*- mode: haskell -*-

{
{- Author: Richard Eisenberg
   File:   Exp.y

   A simple LR(1) grammar for expressions.
-}

module Exp where
}

%expect    0
%name      parse
%tokentype { Token }
%error     { (error . show) }

%token
  NUMBER    { NUMBER $$ }
  '+'       { PLUS }
  '*'       { TIMES }
  '('       { LPAREN }
  ')'       { RPAREN }

%%

-- all productions just produce (), as we don't care about parsing
-- output for this example

sum : sum '+' prod       { () }
    | prod               { () }

prod : prod '*' factor   { () }
     | factor            { () }

factor : '(' sum ')'     { () }
       | NUMBER          { () }

{

data Token
  = NUMBER Integer
  | PLUS
  | TIMES
  | LPAREN
  | RPAREN
  deriving Show

}
