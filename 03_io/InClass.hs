{- Author: Richard Eisenberg, inspired by Steve Zdancewic
   File: Stack.hs

   Demonstrates ways of working with a stack.
-}

{-# OPTIONS_GHC -W -Wno-unused-imports #-}

module Stack where

import Control.Monad ( forM_, when )

import Data.Vector       ( Vector )
import qualified Data.Vector as I   -- I for immutable
import Data.Vector.Mutable  ( IOVector )
import qualified Data.Vector.Mutable as M
import Data.IORef

-- This is based on the last part of Assignment 1.

-- In this version, we'll use a *functional* context. That is, a context
-- will be a function mapping strings to ints.
type Ctxt = String -> Int

-- An empty context maps all strings to error
emptyCtxt :: Ctxt
emptyCtxt v = error (v ++ " not found")

-- Extend a context with a new binding
extendCtxt :: Ctxt -> String -> Int -> Ctxt
extendCtxt ctxt new_var new_val
  = \query -> if query == new_var then new_val else ctxt query
  -- This creates a new function that checks if the query matches the new
  -- binding. If so, return the new value. Otherwise, look it up in the
  -- original context.

-- Build a context from a list of (String, Int) pairs.
buildContext :: [(String, Int)] -> Ctxt
buildContext []                  = emptyCtxt
buildContext ((var, val) : rest) = extendCtxt (buildContext rest) var val

-- One instruction in our stack machine
data Insn
  = IPushC Int        -- push an int64 constant onto the stack
  | IPushV String     -- push (lookup string ctxt) onto the stack
  | IMul              -- multiply the top two values on the stack
  | IAdd              -- add the top two values on the stack
  | INeg              -- negate the top value on the stack
  deriving (Eq, Show)

-- A stack program is just a list of instructions.
type Program = [Insn]

data Machine = M { stack :: IOVector Int -- in C: int[]
                 , spRef :: IORef Int    -- in C: int*
                            -- "stack pointer reference"
                            -- one past the last item on stack
                 , instructions :: Vector Insn
                                         -- in C: const Insn[]
                 , pcRef :: IORef Int    -- in C: int*
                            -- "program counter reference"
                 }
newMachine :: Int -> Program -> IO Machine
newMachine size prog = do
  st <- M.new size
  sp <- newIORef 0   -- like malloc in C or "new" in Java
  let insns = I.fromList prog
  pc <- newIORef 0   -- programs start at the beginning

  pure (M { stack = st, spRef = sp
          , instructions = insns, pcRef = pc })

-- Print to stdout the current machine state (without the context)
printMachine :: Machine -> IO ()
printMachine (M { stack        = st
                , spRef        = sp
                , instructions = insns
                , pcRef        = pc }) = do
  
  putStrLn "Stack:"

  sp_val <- readIORef sp    -- in C: sp_val = *sp;
                            -- in Java: sp_val = sp.value;
  putStrLn "  SP --> "

   -- Java's for(stack_loc : reverse ....) { ... }
  forM_ (reverse [0..sp_val-1]) $ \ stack_loc -> do
    stack_val <- M.read st stack_loc  -- stack_val = st[stack_loc]
    putStrLn ("         " ++ show stack_val)

  putStrLn ""
  putStrLn "Instructions:"
  pc_val <- readIORef pc
  forM_ [0 .. I.length insns - 1] $ \ insn_loc -> do
    if (insn_loc == pc_val)
      then putStr "  PC --> "
      else putStr "         "
    let insn = insns I.! insn_loc
    putStrLn (show insn)


step :: Ctxt -> Machine -> IO ()
step ctxt (M { stack = st
             , spRef = sp
             , instructions = insns
             , pcRef = pc }) = do
  -- access instruction where pc is pointing
  pc_val <- readIORef pc
  let insn = insns I.! pc_val

  -- perform instruction
  case insn of
    IPushC n -> push n
    IPushV v -> push (ctxt v)
    IMul     -> do n1 <- pop
                   n2 <- pop
                   push (n1 * n2)
    IAdd     -> do n1 <- pop
                   n2 <- pop
                   push (n1 + n2)
    INeg     -> do n <- pop
                   push (-n)

  writeIORef pc (pc_val + 1)   -- *pc = pc_val + 1

  where
    push :: Int -> IO ()
    push n = do
      sp_val <- readIORef sp
      when (sp_val == M.length st)
        (error "stack not big enough")
      M.write st sp_val n
      writeIORef sp (sp_val + 1)

    pop :: IO Int
    pop = do
      sp_val <- readIORef sp
      when (sp_val == 0)
        (error "stack underflow")  -- like Java's throw 
      let new_sp_val = sp_val - 1
      writeIORef sp new_sp_val
      M.read st new_sp_val

{-
-- Carry out one instruction, in a given context, producing an output stack.
step :: Ctxt -> Stack -> Insn -> Stack
step _ s             (IPushC n) = n : s
step c s             (IPushV x) = c x : s
step _ (v1 : v2 : s) IMul       = v1 * v2 : s
step _ (v1 : v2 : s) IAdd       = v1 + v2 : s
step _ (v : s)       INeg       = (-v) : s
step _ _ _ = error "stack had too few values"

-- Executing a program means repeatedly processing instructions
execute :: Ctxt -> Stack -> Program -> Stack
execute _ s []         = s  -- no more instructions to execute
execute c s (i : cont) = execute c (step c s i) cont

-- Extract the final, sole value from the stack. It must have 1 element.
answer :: Stack -> Int
answer [n] = n
answer _   = error "no answer"

-- Run a program in a given context for its variables
run :: Ctxt -> Program -> Int
run c p = answer (execute c [] p)

-- Example:
p1      = [IPushC 2, IPushC 3, IMul]
answer1 = run emptyCtxt p1
-}
