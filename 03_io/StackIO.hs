{- Author: Richard Eisenberg, inspired by Steve Zdancewic
   File: Stack.hs

   Demonstrates ways of working with a stack, using mutable operations.
-}

{-# OPTIONS_GHC -W -Wno-unused-imports #-}

module Stack where

import Control.Monad ( forM_, when )

import Data.Vector                        ( Vector )
import qualified Data.Vector as I   -- I for immutable
import Data.Vector.Mutable                ( IOVector )
import qualified Data.Vector.Mutable as M
import Data.IORef

-- This is based on the last part of Assignment 1.

-- In this version, we'll use a *functional* context. That is, a context
-- will be a function mapping strings to ints.
type Ctxt = String -> Int

-- An empty context maps all strings to error
emptyCtxt :: Ctxt
emptyCtxt v = error (v ++ " not found")

-- Extend a context with a new binding
extendCtxt :: Ctxt -> String -> Int -> Ctxt
extendCtxt ctxt new_var new_val
  = \query -> if query == new_var then new_val else ctxt query
  -- This creates a new function that checks if the query matches the new
  -- binding. If so, return the new value. Otherwise, look it up in the
  -- original context.

-- Build a context from a list of (String, Int) pairs.
buildContext :: [(String, Int)] -> Ctxt
buildContext []                  = emptyCtxt
buildContext ((var, val) : rest) = extendCtxt (buildContext rest) var val

-- One instruction in our stack machine
data Insn
  = IPushC Int        -- push an int64 constant onto the stack
  | IPushV String     -- push (lookup string ctxt) onto the stack
  | IMul              -- multiply the top two values on the stack
  | IAdd              -- add the top two values on the stack
  | INeg              -- negate the top value on the stack
  deriving (Eq, Show)

-- A stack program is just a list of instructions.
type Program = [Insn]

-- A stack machine has a data space for the stack, as well as the index of one
-- past the top of the stack (the bottom is always 0), the instructions,
-- and the program counter (PC), which tells us which instruction to run
-- next.
data Machine = M { stack        :: IOVector Int
                 , spRef        :: IORef Int
                 , instructions :: Vector Insn
                 , pcRef        :: IORef Int }

-- Create a new machine of the given size, ready to execute a given program
-- in the given context.
newMachine :: Int -> Program -> IO Machine
newMachine size prog = do
  st <- M.new size
  sp <- newIORef 0   -- SP starts at 0
  let insns = I.fromList prog
  pc <- newIORef 0   -- PC starts at 0

  pure (M { stack        = st
          , spRef        = sp
          , instructions = insns
          , pcRef        = pc })

-- Print to stdout the current machine state (without the context)
printMachine :: Machine -> IO ()
printMachine (M { stack        = st
                , spRef        = sp
                , instructions = insns
                , pcRef        = pc }) = do
  
  putStrLn "Stack:"

  sp_val <- readIORef sp
  putStrLn "  SP --> "
  forM_ (reverse [0..sp_val-1]) $ \ stack_loc -> do
    stack_val <- M.read st stack_loc
    putStrLn ("         " ++ show stack_val)

  putStrLn ""
  putStrLn "Instructions:"
  pc_val <- readIORef pc
  forM_ [0 .. I.length insns - 1] $ \ insn_loc -> do
    if (insn_loc == pc_val)
      then putStr "  PC --> "
      else putStr "         "
    let insn = insns I.! insn_loc
    putStrLn (show insn)

-- Run the machine by one step in the given context.
-- Returns whether the machine is done running (True means "done")
step :: Ctxt -> Machine -> IO Bool
step ctxt (M { stack        = st
             , spRef        = sp
             , instructions = insns
             , pcRef        = pc }) = do
  -- Fetch the instruction
  pc_val <- readIORef pc
  let insn = insns I.! pc_val

  -- Perform the instruction
  case insn of
    IPushC n -> push n
    IPushV x -> push (ctxt x)
    IMul     -> do n1 <- pop
                   n2 <- pop
                   push (n1 * n2)
    IAdd     -> do n1 <- pop
                   n2 <- pop
                   push (n1 + n2)
    INeg     -> do n <- pop
                   push (-n)

  -- Increment the PC
  let new_pc_val = pc_val + 1
  writeIORef pc new_pc_val

  pure (new_pc_val == I.length insns)

  where
    push n = do
      sp_val <- readIORef sp
      when (sp_val == M.length st) $
        error "Out of stack space"

      M.write st sp_val n
      modifyIORef sp (+1)

    pop = do
      sp_val <- readIORef sp
      when (sp_val == 0) $
        error "stack underflow"

      let new_sp_val = sp_val - 1
      writeIORef sp new_sp_val
      M.read st new_sp_val

-- Executing a machine means repeatedly processing instructions
execute :: Ctxt -> Machine -> IO ()
execute ctxt m = do
  done <- step ctxt m
  when (not done) $
    execute ctxt m

-- Extract the final, sole value from the machine. The stack must have 1 element.
answer :: Machine -> IO Int
answer (M { stack = st
          , spRef = sp }) = do
  sp_val <- readIORef sp
  when (sp_val /= 1) $
    error ("Stack has " ++ show sp_val ++ " values at end of run.")

  M.read st 0

-- Run a program in a given context for its variables, with a given stack size
run :: Int -> Ctxt -> Program -> IO Int
run size ctxt prog = do
  m <- newMachine size prog
  execute ctxt m
  answer m

-- Example:
p1       = [IPushC 2, IPushC 3, IMul]
answer1  = run 10 emptyCtxt p1
overflow = run 1 emptyCtxt p1

