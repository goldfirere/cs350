{- Author: Richard Eisenberg
   File: Args.hs

   Utilities for dealing with command-line arguments.
   "Real" programs should use something more sophisticated, like optparse-applicative
-}

{-# LANGUAGE ScopedTypeVariables #-}
  -- this allows the handle function's type to refer to the settings type variable
{-# LANGUAGE RankNTypes #-}
  -- this allows the (forall a. IO a) parameter to handleFilename

module CS350.Args where

import System.Environment   ( getArgs, getProgName )
import System.Exit          ( exitFailure )
import System.FilePath      ( isExtensionOf )
import Text.Printf          ( printf )

-- A description of a command-line argument
type ArgumentDescriptor settings
  = ( String    -- the user-written spelling of the argument
    , String    -- helpful description of the argument
    , settings -> settings   -- how the argument affects the settings
    )

-- Print out an argument description
printArgDescriptor :: ArgumentDescriptor settings -> IO ()
printArgDescriptor (arg, help_text, _) =
  putStrLn (printf "  %-18s  %s" arg help_text)

-- Find an argument handler by name
lookupArg :: [ArgumentDescriptor settings]
          -> String
          -> Maybe (settings -> settings)
lookupArg [] _ = Nothing  -- failed search
lookupArg ((arg, _, settings_fun) : rest) needle
  | arg == needle = Just settings_fun
  | otherwise     = lookupArg rest needle

handleArgs :: forall settings
           .  [ArgumentDescriptor settings]
           -> settings   -- default
           -> IO (settings, [String])  -- returns the settings,
                                       -- and any unconsumed args in original order
handleArgs arg_descs deflt = do
  args <- getArgs
  handle deflt [] args
  where
    handle :: settings -> [String]        -- accumulators for results
           -> [String]                    -- arguments
           -> IO (settings, [String])
    handle settings acc []         = pure (settings, reverse acc)
    handle settings acc (arg:args)
      | Just settings_fun <- lookupArg arg_descs arg
      = handle (settings_fun settings) acc args
      | otherwise
      = handle settings (arg : acc) args

-- look for precisely one filename ending with the given extension
handleFilename :: (forall a. IO a)     -- print usage and fail
               -> String -> [String] -> IO (FilePath, [String])
handleFilename print_usage ext = go Nothing []
  where
    go m_path acc [] = case m_path of
      Just path -> pure (path, reverse acc)
      Nothing   -> do putStrLn $ "No input file (with extension " ++ ext ++ ") found."
                      print_usage
    go m_path acc (arg:args)
      | ext `isExtensionOf` arg
      = case m_path of
          Just _  -> do putStrLn "Cannot handle more than one input file at a time."
                        print_usage
          Nothing -> go (Just arg) acc args
      | otherwise
      = go m_path (arg:acc) args

-- Prints help text and fails; assumes an input file
printUsage :: [ArgumentDescriptor settings] -> IO a
printUsage arg_descs = do
  prog_name <- getProgName
  putStrLn $ "Usage: " ++ prog_name ++ " [options] <input file>"
  putStrLn "Available options:"
  mapM_ printArgDescriptor arg_descs
  exitFailure
