{- Author: Richard Eisenberg
   File: NB.hs

   An implementation of the NB language of numeric and boolean expressions.
-}

{-# LANGUAGE OverloadedLists #-}
  -- This allows use to use [...] notation for Sequences

{-# OPTIONS_GHC -W #-}

module NB where

import CS350.Panic       ( panic )
import CS350.Renderable  ( Renderable, render)
import CS350.CompileM    ( CompileM, compileError, runCompileM )
import CS350.Unique      ( newUniqueString )

import LLVM.Lite as L

import Control.Monad ( when )
import Data.Foldable ( toList )
import Data.Sequence ( Seq(..), (><), (|>), empty )
import Text.Printf   ( printf )

--------------------------
-- Abstract syntax

data Exp
  = Literal Lit
  | Binary Exp Op Exp
  | If Exp Exp Exp
  deriving Show

data Lit
  = NumLit Integer
  | BoolLit Bool
  deriving (Show, Eq)

data Op
  = Plus
  | Minus
  | Times
  | Less
  | Greater
  | Equals
  | And
  | Or
  deriving Show

-------------------------
-- Pretty-printing

instance Renderable Exp where
  render (Literal lit)     = render lit
  render (Binary e1 op e2) = printf "(%s %s %s)" (render e1) (render op) (render e2)
  render (If e1 e2 e3)     = printf "(if %s then %s else %s)" (render e1) (render e2) (render e3)

instance Renderable Lit where
  render (NumLit num)    = show num
  render (BoolLit True)  = "true"
  render (BoolLit False) = "false"

instance Renderable Op where
  render Plus    = "+"
  render Minus   = "-"
  render Times   = "*"
  render Less    = "<"
  render Greater = ">"
  render Equals  = "="
  render NB.And  = "&"
  render NB.Or   = "|"

-------------------------
-- Evaluation

eval :: Exp -> Lit
eval (Literal lit)          = lit
eval (Binary e1 Plus e2)    = evalArith (+) e1 e2
eval (Binary e1 Minus e2)   = evalArith (-) e1 e2
eval (Binary e1 Times e2)   = evalArith (*) e1 e2
eval (Binary e1 Less e2)    = evalCompare (<) e1 e2
eval (Binary e1 Greater e2) = evalCompare (>) e1 e2
eval (Binary e1 Equals e2)  = BoolLit (eval e1 == eval e2)
eval (Binary e1 NB.And e2)  = evalLogic (&&) e1 e2
eval (Binary e1 NB.Or e2)   = evalLogic (||) e1 e2
eval (If e1 e2 e3)
  | BoolLit b1 <- eval e1
  = if b1 then eval e2 else eval e3
  | otherwise
  = error "Invalid operation"

evalArith :: (Integer -> Integer -> Integer) -> Exp -> Exp -> Lit
evalArith fun e1 e2
  | NumLit n1 <- eval e1
  , NumLit n2 <- eval e2
  = NumLit (fun n1 n2)

  | otherwise
  = error "Invalid operation"

evalCompare :: (Integer -> Integer -> Bool) -> Exp -> Exp -> Lit
evalCompare fun e1 e2
  | NumLit n1 <- eval e1
  , NumLit n2 <- eval e2
  = BoolLit (fun n1 n2)

  | otherwise
  = error "Invalid operation"

evalLogic :: (Bool -> Bool -> Bool) -> Exp -> Exp -> Lit
evalLogic fun e1 e2
  | BoolLit b1 <- eval e1
  , BoolLit b2 <- eval e2
  = BoolLit (fun b1 b2)

  | otherwise
  = error "Invalid operation"

---------------------------
-- Type checking

data Type
  = Number
  | Boolean
  deriving (Show, Eq)

check :: Exp -> Maybe Type
check (Literal lit) = Just (checkLit lit)
check (Binary e1 op e2) = case op of
  Plus    -> checkArith e1 e2
  Minus   -> checkArith e1 e2
  Times   -> checkArith e1 e2
  Less    -> checkCompare e1 e2
  Greater -> checkCompare e1 e2
  Equals  -> checkEquals e1 e2
  NB.And  -> checkLogic e1 e2
  NB.Or   -> checkLogic e1 e2
check (If e1 e2 e3)
  | Just Boolean <- check e1
  , Just t2 <- check e2
  , Just t3 <- check e3
  , t2 == t3
  = Just t2
check _ = Nothing

checkLit :: Lit -> Type
checkLit (NumLit _)  = Number
checkLit (BoolLit _) = Boolean

checkArith :: Exp -> Exp -> Maybe Type
checkArith e1 e2
  | Just Number <- check e1
  , Just Number <- check e2
  = Just Number

  | otherwise
  = Nothing

checkCompare :: Exp -> Exp -> Maybe Type
checkCompare e1 e2
  | Just Number <- check e1
  , Just Number <- check e2
  = Just Boolean

  | otherwise
  = Nothing

checkEquals :: Exp -> Exp -> Maybe Type
checkEquals e1 e2
  | Just t1 <- check e1
  , Just t2 <- check e2
  , t1 == t2
  = Just Boolean

  | otherwise
  = Nothing

checkLogic :: Exp -> Exp -> Maybe Type
checkLogic e1 e2
  | Just Boolean <- check e1
  , Just Boolean <- check e2
  = Just Boolean

  | otherwise
  = Nothing

---------------------------
-- Type-directed compilation

data Element
  = L Label
  | I Local Instruction
  | T Terminator
  deriving Show

type Stream = Seq Element
  -- A "Seq" is just like a list, but it's efficient adding to either end of it
  -- (unlike a list, which only likes additions at its beginning)

-- This convenience function produces an Element for an instruction, generating
-- a new LLVM local name along the way.
newInstruction :: Instruction -> CompileM (Element, Operand)
newInstruction insn = do
  local <- newUniqueString "local"
  pure (I local insn, LocalId local)

compile :: Exp -> CompileM (Stream, Type, Operand)
compile (Literal lit) = pure (empty, ty, op)
  where (ty, op) = compileLit lit
compile (Binary e1 op e2) = do
  (s1, ty1, operand1) <- compile e1
  (s2, ty2, operand2) <- compile e2

  (insn, ty, operand) <- case op of
    Plus    -> compileArith Add ty1 ty2 operand1 operand2
    Minus   -> compileArith Sub ty1 ty2 operand1 operand2
    Times   -> compileArith Mul ty1 ty2 operand1 operand2
    Less    -> compileCompare Slt ty1 ty2 operand1 operand2
    Greater -> compileCompare Sgt ty1 ty2 operand1 operand2
    Equals  -> compileEquals ty1 ty2 operand1 operand2
    NB.And  -> compileLogic L.And ty1 ty2 operand1 operand2
    NB.Or   -> compileLogic L.Or ty1 ty2 operand1 operand2

  pure (s1 >< (s2 |> insn), ty, operand)
compile (If e1 e2 e3) = do
  (s1, ty1, operand1) <- compile e1
  (s2, ty2, operand2) <- compile e2
  (s3, ty3, operand3) <- compile e3

  when (ty1 /= Boolean) $
    compileError "Condition is not a boolean."

  when (ty2 /= ty3) $
    compileError "Branches of 'if' have different types."

  let ll_ty = compileType ty2
  (alloca, ptr_result) <- newInstruction (Alloca ll_ty)
  (store_then, _)      <- newInstruction (Store ll_ty operand2 ptr_result)
  (store_else, _)      <- newInstruction (Store ll_ty operand3 ptr_result)
  (load, result)       <- newInstruction (Load (Ptr ll_ty) ptr_result)

  then_lbl <- newUniqueString "then"
  else_lbl <- newUniqueString "else"
  merge_lbl <- newUniqueString "merge"

  pure ( s1 ><
         [ alloca
         , T (Cbr operand1 then_lbl else_lbl)
         , L then_lbl ] ><
         s2 ><
         [ store_then
         , T (Br merge_lbl)
         , L else_lbl ] ><
         s3 ><
         [ store_else
         , T (Br merge_lbl)
         , L merge_lbl
         , load ]
       , ty2, result )

compileLit :: Lit -> (Type, Operand)
compileLit (NumLit n)  = (Number, Const (fromIntegral n))
compileLit (BoolLit b) = (Boolean, Const (if b then 1 else 0))

compileArith :: L.BinaryOp -> Type -> Type -> Operand -> Operand
             -> CompileM (Element, Type, Operand)
compileArith op ty1 ty2 operand1 operand2 = do
  when (ty1 /= Number || ty2 /= Number) $
    compileError "Arithmetic can be done only on numbers."

  (insn, result) <- newInstruction (Binop op I16 operand1 operand2)
  pure (insn, Number, result)

compileCompare :: L.Condition -> Type -> Type -> Operand -> Operand
               -> CompileM (Element, Type, Operand)
compileCompare cond ty1 ty2 operand1 operand2 = do
  when (ty1 /= Number || ty2 /= Number) $
    compileError "Comparison can be done only on numbers."

  (insn, result) <- newInstruction (Icmp cond I16 operand1 operand2)
  pure (insn, Boolean, result)

compileEquals :: Type -> Type -> Operand -> Operand
              -> CompileM (Element, Type, Operand)
compileEquals ty1 ty2 operand1 operand2 = do
  when (ty1 /= ty2) $
    compileError "Cannot compare different types for equality."

  (insn, result) <- newInstruction (Icmp Eq (compileType ty1) operand1 operand2)
  pure (insn, Boolean, result)

compileLogic :: L.BinaryOp -> Type -> Type -> Operand -> Operand
             -> CompileM (Element, Type, Operand)
compileLogic op ty1 ty2 operand1 operand2 = do
  when (ty1 /= Boolean || ty2 /= Boolean) $
    compileError "Logical operations can be done only on booleans."

  (insn, result) <- newInstruction (Binop op I1 operand1 operand2)
  pure (insn, Boolean, result)

compileType :: NB.Type -> L.Ty
compileType Number  = I16
compileType Boolean = I1

---------------------------
-- Converting a Stream to a Program

buildCfg :: Stream -> Cfg
buildCfg stream = Cfg { entry = entry_block
                      , blocks = labeled_blocks }
  where
    (entry_block, lbl_stream) = get_entry_block stream
    labeled_blocks            = get_labeled_blocks lbl_stream

    get_entry_block = go_block empty

    get_labeled_blocks (L lbl :<| es) = (lbl, block) : other_blocks
      where
        (block, stream) = go_block empty es
        other_blocks    = get_labeled_blocks stream

    get_labeled_blocks (e :<| _)     = panic $ "Unexpected element: " ++ show e
    get_labeled_blocks Empty         = []

    go_block :: Seq (Local, L.Instruction)  -- accumulator for instructions
             -> Stream -> (Block, Stream)
      -- returns the block and the remainder of the stream
    go_block _     (L lbl :<| _)       = panic $ "Label in middle of block: " ++ lbl
    go_block insns (I uid insn :<| es) = go_block (insns |> (uid, insn)) es
    go_block insns (T term :<| es)     = ( Bl { instructions = toList insns, terminator = term }
                                         , es )
    go_block _     Empty               = panic "Unterminated block"

compileProgram :: Exp -> CompileM Program
compileProgram e = do
  (s, ty, op) <- compile e

  let cfg = buildCfg (s |> T (Ret (compileType ty) (Just op)))
      main = N { name = "main"
               , decl = FD { functionType   = FunTy I16 [I16, Ptr (Ptr I8)]
                           , functionParams = ["argc", "argv"]
                           , functionCfg    = cfg }}
      program = Prog { typeDecls = []
                     , globalDecls = []
                     , functionDecls = [main] }

  pure program

writeProgram :: FilePath -> Exp -> IO ()
writeProgram path e = do
  result <- runCompileM (compileProgram e)
  case result of
    Left err   -> putStrLn err
    Right prog -> do writeFile path (render prog)
                     putStrLn $ "Successfully wrote program to " ++ path ++ "."
