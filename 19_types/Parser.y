-- -*- mode: haskell -*-

{
{- Author: Richard Eisenberg
   File: Parser.y

   A parser for the ℕ𝔹 language.
-}

module Parser where

import CS350.Renderable ( render )

import Lexer ( Token(..), lexNB )
import NB    ( Exp(..), Lit(..), Op(..) )

}

%expect    0
%name      parse
%tokentype { Token }
%error     { (error . show) }

%token
  'true'    { TRUE      }
  'false'   { FALSE     }
  'if'      { IF        }
  'then'    { THEN      }
  'else'    { ELSE      }
  '('       { LPAREN    }
  ')'       { RPAREN    }
  '+'       { PLUS      }
  '-'       { MINUS     }
  '*'       { TIMES     }
  '<'       { LESS      }
  '>'       { GREATER   }
  '='       { EQUALS    }
  '&'       { AND       }
  '|'       { OR        }
   NUMBER   { NUMBER $$ }

%left 'else'
%left '|'
%left '&'
%nonassoc '<' '>' '='
%left '+' '-'
%left '*'

%%

exp : lit                               { Literal $1           }
    | exp '+' exp                       { Binary $1 Plus $3    }
    | exp '-' exp                       { Binary $1 Minus $3   }
    | exp '*' exp                       { Binary $1 Times $3   }
    | exp '<' exp                       { Binary $1 Less $3    }
    | exp '>' exp                       { Binary $1 Greater $3 }
    | exp '=' exp                       { Binary $1 Equals $3  }
    | exp '&' exp                       { Binary $1 And $3     }
    | exp '|' exp                       { Binary $1 Or $3      }
    | 'if' exp 'then' exp 'else' exp    { If $2 $4 $6          }
    | '(' exp ')'                       { $2                   }

lit : NUMBER       { NumLit $1     }
    | 'true'       { BoolLit True  }
    | 'false'      { BoolLit False }

{
-- convenient for testing: parses and then prints out an expression
roundTrip :: String -> String
roundTrip = render . parse . lexNB
}
