-- -*- mode: haskell -*-

{
{- Author: Richard Eisenberg
   File: Lexer.x

   A lexer for the ℕ𝔹 language.
-}

module Lexer where

}

%wrapper "basic"

$digit = 0-9
@number = $digit+

NB :-

$white        ;

true          { const TRUE    }
false         { const FALSE   }
if            { const IF      }
then          { const THEN    }
else          { const ELSE    }

"("           { const LPAREN  }
")"           { const RPAREN  }
"+"           { const PLUS    }
"-"           { const MINUS   }
"*"           { const TIMES   }
"<"           { const LESS    }
">"           { const GREATER }
"="           { const EQUALS  }
"&"           { const AND     }
"|"           { const OR      }

@number       { NUMBER . read }

{

data Token
  = TRUE
  | FALSE
  | IF
  | THEN
  | ELSE
  | LPAREN
  | RPAREN
  | PLUS
  | MINUS
  | TIMES
  | LESS
  | GREATER
  | EQUALS
  | AND
  | OR
  | NUMBER Integer
  deriving Show

lexNB :: String -> [Token]
lexNB = alexScanTokens

}
