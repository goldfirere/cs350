{- Author: Richard Eisenberg
   File: Safety.hs

   Uses advanced Haskell magic to check type safety of ℕ𝔹.
-}

{-# LANGUAGE TypeApplications, ScopedTypeVariables, FlexibleContexts,
             UndecidableInstances, MonoLocalBinds, StandaloneDeriving,
             DerivingVia, DeriveGeneric, DeriveAnyClass #-}

module Safety where

import NB

import CS350.Renderable  ( render )
import Data.Maybe        ( isJust )

import Test.QuickCheck   ( Arbitrary(..), Property, (==>), stdArgs, maxSuccess
                         , quickCheckWith )
import Generic.Random    ( GArbitrary, SizedOptsDef, BaseCase, GUniformWeight
                         , genericArbitraryU' )
import GHC.Generics      ( Generic )
import Control.DeepSeq   ( NFData, rnf )
import Control.Exception ( evaluate )

newtype GenArbitrary a = GenArbitrary a

instance (GArbitrary SizedOptsDef a, BaseCase a, GUniformWeight a)
           => Arbitrary (GenArbitrary a) where
  arbitrary = GenArbitrary <$> genericArbitraryU' @a

deriving instance Generic Exp
deriving instance Generic Lit
deriving instance Generic Op

deriving instance NFData Exp
deriving instance NFData Lit
deriving instance NFData Op

deriving via GenArbitrary Exp instance Arbitrary Exp
deriving via GenArbitrary Lit instance Arbitrary Lit
deriving via GenArbitrary Op  instance Arbitrary Op

typeSafety :: Exp -> Property
typeSafety e =
  isJust (check e) ==> rnf (eval e)

checkSafety :: IO ()
checkSafety = quickCheckWith stdArgs { maxSuccess = 10000 } typeSafety
