% -*- mode: LaTeX -*-

\documentclass[12pt,pdflatex]{article}

%\usepackage{times}
%\usepackage{hw}
%\usepackage{code}
%\usepackage{macros}
\usepackage[dvipsnames]{xcolor}
%\usepackage{fullpage}
%\usepackage{epsfig}
\usepackage[all]{xy}
\usepackage{relsize}
\usepackage{listings}
\usepackage{changepage}
\usepackage{inconsolata}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage[T1]{fontenc}  % this gets underscores right!

\lstset{
  basicstyle=\smaller\upshape\ttfamily,
  keywordstyle=\bfseries,
  framexleftmargin=1ex,
  framexrightmargin=1ex,
  showstringspaces=false,
  commentstyle=\itshape\rmfamily,
  columns=fixed,
  stringstyle=\ttfamily
}


\addtolength{\oddsidemargin}{-1in}
\addtolength{\evensidemargin}{-1in}
\addtolength{\textwidth}{2in}
\addtolength{\topmargin}{-1in}
\addtolength{\textheight}{2in}

%include polycode.fmt
%include rae.fmt

\newcommand{\keyword}[1]{\textcolor{BlueViolet}{\textbf{#1}}}
\newcommand{\id}[1]{\textsf{\textsl{#1}}}
\newcommand{\varid}[1]{\textcolor{Sepia}{\id{#1}}}
\newcommand{\conid}[1]{\textcolor{OliveGreen}{\id{#1}}}
\newcommand{\tick}{\text{\textquoteright}}
\newcommand{\package}[1]{\textsf{#1}}

%if style==newcode
\begin{code}
module Practice where

import CS350.Unique
import Data.Int ( Int16 )
\end{code}
%endif


\begin{document}

\begin{center}
\bf
CMSC 350: Compiler Design \\
Practice Midterm Questions
\end{center}

\begin{enumerate}

\item \textbf{True or False}

Mark each statement as either true or false.
\newcommand{\tfitem}{\item \quad T \quad F \quad}

\begin{enumerate}[itemsep=2ex]
\tfitem  The typical compiler consists of several phases,
including: lexing, parsing, transformation to an IR, optimization, and
finally, code generation (in that order).

\tfitem The LLVM is a ``Static Single Assignment'' IR, which means
that all local identifiers (e.g. @%1@, @%x@, etc.)
are allocated on the stack.

\tfitem Other than the entry block, the order in which basic blocks
appear within an LLVM function declaration does not affect the meaning
of the program.

\tfitem An assembler determines the memory address of every piece of data in
the source assembly language program.

\tfitem The HERA memory address space contains $2^{16}$ bytes.

\end{enumerate}


\pagebreak
\item \textbf{Compilation and Intermediate Representations}

In this problem, we consider different ways to compile boolean
expressions to a very simplified LLVM IR target.  A Haskell
representation of the LLVM subset we use is given in Appendix~B.

The appendix also gives a datatype of boolean-valued expressions
(repeated below), which will constitute the source language
for the purposes of this problem:

\begin{code}
data BoolExp
    =  VarE String
    |  TrueE
    |  FalseE
    |  NotE  BoolExp
    |  AndE  BoolExp  BoolExp
    |  OrE   BoolExp  BoolExp
\end{code}

The usual way to compile such a boolean expression to an LLVM-like IR
is to generate code that computes an answer, yielding the result in an
operand.  The function |compileBoolExpBlock|, given in the
appendix, does exactly that; it relies on the helper
|compileBoolExp| to do most of the hard work of ``flattening''
nested expressions into LL code.  (Note that a source variable
|VarE x| is compiled to a LL uid |Id x|, which
would be pretty-printed as @%x@.)

\paragraph{(a)} (Multiple Choice) Which of the following is the (pretty-printed) LL code that is
  obtained by compiling the following |example| via |compileBoolExpBlock|?
\begin{code}
example :: BoolExp
example = NotE (OrE (VarE "a") (AndE (VarE "b") TrueE))
\end{code}

\begin{lstlisting}[language=llvm]
(i)                                     (ii)
%tmp2 = or  i1 %a, %tmp1                %tmp0 = xor i1 false, %a
%tmp1 = and i1 %b, %tmp2                %tmp1 = or  i1 %b, %tmp0
%tmp0 = xor i1 false, %tmp1             %tmp2 = and i1 %tmp1, true
  ret i1 %tmp0                            ret i1 %tmp2


(iii)                                   (iv)
%tmp2 = and i1 %b, true                 %tmp0 = and i1 %a, true
%tmp1 = or  i1 %a, %tmp2                %tmp1 = or  i1 %b, %tmp0
%tmp0 = xor i1 false, %tmp1             %tmp2 = xor i1 false, %tmp1
  ret i1 %tmp0                            ret i1 %tmp2
\end{lstlisting}

\pagebreak

\paragraph{Conditional Statements} Suppose that we want to use |compileBoolExp|
as a subroutine for compiling conditional statements:

@if (b) { stmt1 } else { stmt2 }@ \quad represented
via the abstract syntax: \qquad |If b stmt1 stmt2|.

Suppose |b| is |AndE (VarE "x") huge|, where |huge| is a
long, complicated boolean expression.
A compiler that uses the |compileBoolExp| from the appendix
would generate code for the @if@ statement that looks like:

\begin{lstlisting}[language=llvm]
compileStmt (If b stmt1 stmt2) =

  [...huge_code... %tmp0 = ...]          ; could be very long      (!)
  %tmp1 = and i1 %x, %tmp0               ; materialize b into tmp1 (!)
  br i1 %tmp1, label %then, label %else  ; branch on b's value     (!)

%then:
  [... stmt1 ...]   ; code for stmt1

%else:
  [... stmt2 ...]   ; code for stmt2
\end{lstlisting}

Note that LL code above ``materializes'' the value of |b| into the
operand @%tmp1@ and then does a conditional branch to either
the block labeled @%then@ or the block labeled
@%else@, as appropriate.

\paragraph{Short Circuiting} We will now explore an alternate compilation strategy for boolean
expressions that allows for short-circuit evaluation and is
tailored for when the |BoolExp| is the guard of an
@if@ statement.  In the example above, it is a waste of
time to compute the whole boolean answer if @x@ happens to be
false---in that case, we know just by looking at @x@ that we should
jump immediately to the @%else@ branch; there's no need to
compute |huge|.


This observation suggests that we can profit by creating a variant of
|compileBoolExp| called |compileBoolExpBr|  that, rather than computing a boolean value
into an operand, instead takes in two labels (one for the ``true''
branch and one for the ``false'' branch) and emits code that jumps
directly to the correct one, short circuiting the computation as soon
as it's clear which branch to take.

For example, consider the result of compiling the |huge| example
above, where we supply the @then@ and @else@ labels:
%if style == newcode
\begin{code}
huge :: BoolExp
huge = undefined
\end{code}
%endif

\begin{code}
s :: Stream 
s = runUniqueM (compileBoolExpBr (AndE (VarE "x") huge) "then" "else")
\end{code}

When we pretty-print |s| as a sequence of LL blocks, we get the
following code, which can replace the lines marked \textit{(!)} in the
code at the top of this page.

\begin{lstlisting}[language=llvm]
  br i1 %x, label %lbl1, label %else    ; if %x is false, jump to %else
%lbl1:
  [...stream for huge...]
\end{lstlisting}
Note that the compiler generated the intermediate label
@%lbl1@, which marks the start of the |huge| code block.
The stream for |huge| will contain terminators that branch to
@%then@ or @%else@ as appropriate.
Remarkably, this compilation strategy will never need to emit
instructions!  A boolean expression can be completely compiled into a
series of labels and terminators (branches or conditional branches).

\pagebreak
(Multiple Choice)  It is not too hard to work out what |compileBoolExpBr| should
do by translating some small examples by hand.
%
Mark the code that is a correct (pretty-printed)
output for each call to |compileBoolExpBr|.
There is only one correct answer for each case.

\paragraph{(b)}
|compileBoolExpBr (NotE (VarE "a")) "then" "else" = |?

\bigskip
\begin{lstlisting}[language=llvm]
(i)                                         (ii)
  br i1 %a, label %then, label %else          br i1 %a, label %else, label %then


(iii)                                       (iv)
  br i1 %a, label %else, label %lbl0          br i1 %a, label %then, label %lbl0
lbl0:                                       lbl0:
  br i1 %b, label %else                       br i1 %b, label %then
\end{lstlisting}

\vspace{0.5in}

\paragraph{(c)}
|compileBoolExpBr (OrE (VarE "a") (VarE "b")) "then" "else" = |?

\bigskip
\begin{lstlisting}[language=llvm]
(i)                                         (ii)
  br i1 %a, label %lbl0, label %else          br i1 %a, label %then, label %lbl0
lbl0:                                       lbl0:
  br i1 %b, label %then, label %else          br i1 %b, label %then, label %else


(iii)                                       (iv)
  br i1 %a, label %else, label %lbl0          br i1 %a, label %else, label %lbl0
lbl0:                                       lbl0:
  br i1 %b, label %then, label %else          br i1 %b, label %else, label %then
\end{lstlisting}

\vspace{0.5in}

\paragraph{(d)}
|compileBoolExpBr (AndE (NotE (VarE "a")) (VarE "b")) "then" "else" = |?

\bigskip
\begin{lstlisting}[language=llvm]
(i)                                         (ii)
  br i1 %a, label %lbl0, label %else          br i1 %a, label %then, label %lbl0
lbl0:                                       lbl0:
  br i1 %b, label %then, label %else          br i1 %b, label %then, label %else


(iii)                                       (iv)
  br i1 %a, label %else, label %lbl0          br i1 %a, label %then, label %lbl0
lbl0:                                       lbl0:
  br i1 %b, label %then, label %else          br i1 %b, label %then, label %else
\end{lstlisting}

\pagebreak

\paragraph{(e)} Complete the implementation of
|compileBoolExpBr| below by filling in each blank with
the correct |Terminator| (for the first two blanks) or the
correct |Label| (for the remaining blanks) to implement the
short-circuiting code generator.  Note that the |AndE| and |OrE|
cases introduce a new |lmid| label, which labels the start of the
second subexpression.  We have done the |TrueE| case for you.

\newcommand{\answerline}{\text{\rule[-6pt]{3in}{.5pt}\phantom{\rule{.1pt}{3ex}}}}
%if style==newcode
%format ___ = undefined
%else
%format ___ = "\answerline"
%endif

\begin{code}
compileBoolExpBr :: BoolExp -> Label -> Label -> UniqueM Stream
compileBoolExpBr b ltrue lfalse = case b of
    TrueE  -> pure [T (Br ltrue)]

    FalseE -> pure [T (___)]

    VarE s -> pure [T (___)]

    NotE c -> compileBoolExpBr c  (___)
                                  (___)

    AndE b1 b2 -> do
      lmid <- newLabel "mid"
      p1 <- compileBoolExpBr b1  (___)
                                 (___)
      p2 <- compileBoolExpBr b2  (___)
                                 (___)
      pure (concat [p1, [L lmid], p2])

    OrE b1 b2 -> do
      lmid <- newLabel "mid"
      p1 <- compileBoolExpBr b1  (___) 
                                 (___)
      p2 <- compileBoolExpBr b2  (___)
                                 (___)
      pure (concat [p1, [L lmid], p2])
\end{code}

%if style==newcode
\begin{code}
compileBoolExpBrC :: BoolExp -> Label -> Label -> UniqueM Stream
compileBoolExpBrC b ltrue lfalse = case b of
    TrueE  -> pure [T (Br ltrue)]

    FalseE -> pure [T (Br lfalse)]

    VarE s -> pure [T (Cbr (Id s) ltrue lfalse)]

    NotE c -> compileBoolExpBrC c lfalse ltrue

    AndE b1 b2 -> do
      lmid <- newLabel "mid"
      p1 <- compileBoolExpBrC b1 lmid lfalse
      p2 <- compileBoolExpBrC b2 ltrue lfalse
      pure (concat [p1, [L lmid], p2])

    OrE b1 b2 -> do
      lmid <- newLabel "mid"
      p1 <- compileBoolExpBrC b1 ltrue lmid
      p2 <- compileBoolExpBrC b2 ltrue lfalse
      pure (concat [p1, [L lmid], p2])
\end{code}
%endif

\pagebreak
\item \textbf{LLVM IR: Structured Data and @getelementptr@}

  Consider the following C structured datatypes and global variables
  @array@ and @node@. @RGB@ is a record with three fields and @Node@ is a
  linked-list node that contains an inlined RGB value.

\begin{lstlisting}[language=c]
struct RGB {int r; int g; int b;};
struct Node {struct RGB rgb; struct Node* next;};

struct RGB array[] = {{255,0,0}, {0,255,0}};
struct Node* node;
\end{lstlisting}

This program can be represented in LLVM as the following definitions:

\begin{lstlisting}[language=llvm]
%RGB = type { i16, i16, i16 }
%Node = type { %RGB, %Node* }

@@array = global [2 x %RGB] [%RGB {i16 255, i16 0, i16 0}, %RGB {i16 0, i16 255, i16 0}]
@@node = global %Node ...
\end{lstlisting}

(Multiple Choice) For each LLVM instruction below, mark the
corresponding C expression (i.e. such that a subsequent load from
@%ptr@ would get the value of the expression).

\newcommand{\nbox}{\ensuremath{\Box}}
\newcommand{\ybox}{\ensuremath{\Box}}

\begin{enumerate}
\item  @%ptr = getelementptr %Node, %Node* @@node, i32 0, i32 1@

\medskip

\nbox{} @node@
\qquad
\nbox{} @&node->rgb@
\qquad
\ybox{} @&node->next@
\qquad
\nbox{} @&node->next->rgb@

\bigskip

\item @%ptr = getelementptr  [2 x %RGB], [2 x %RGB]* @@array, i64 1, i64 0, i32 1@

\nbox{} @&array[1][0][1]@
\qquad
\nbox{} @array[1].r@
\qquad
\ybox{} @array[1].g@
\qquad
\nbox{} @array[1].b@

\bigskip

\item @%ptr = getelementptr %Node, %Node* @@node, i32 0, i32 0, i32 2@

\nbox{} @node.rgb[2]@
\qquad
\ybox{} @node.rgb.b@
\qquad
\nbox{} @node[2].rgb@
\qquad
\nbox{} @node.next.rgb@
\end{enumerate}


\pagebreak

Suppose that you wanted to compile an Haskell-like
language into the LLVM IR.  One issue is how to represent Haskell-style
``algebraic datatypes''.  Complete the following questions which
prompt you to think about a strategy for representing the following
Haskell type at the LLVM IR-level.  (For the purposes of this question,
ignore the fact that Haskell supports parameterized datatypes.)

\begin{code}
data Dat
  = A
  | B Int16
  | C Dat Dat
\end{code}

\begin{enumerate}
\setcounter{enumii}{3}
\item A Haskell value of type |Dat| consists of a ``tag'' (e.g. |A|,
  |B|, or |C|) and some associated data values (either an
  |Int16| or a pair of references to |Dat| values).
  Write down a (small) number of LLVM datatypes (including at least
  one for |Dat|), that can together be used to represent values of
  type |Dat|.  Briefly justify your representation strategy.

\bigskip
@%Dat = type@ \answerline\answerline

\end{enumerate}

\pagebreak

\item \textbf{HERA calling conventions}

Suppose we had a method of linking a HERA program with a C program.
The linkage would depend on having a unified calling convention
between the C code and the HERA code.

As some of you have noted, HERA lacks a modulus operator. So, we
might write this operation in C, as follows:

\begin{lstlisting}[language=c]
int modulus(int a, int b) {
  if(a < b)
    return a;
  else
    return modulus(a - b, b);
}
\end{lstlisting}

We assume that C @int@s are compiled into HERA words, that the @modulus@
function lives at a label named @modulus@, and obeys the HERA calling
conventions we have discussed.

Write a HERA function @divisibleBy3@ that returns whether or not
its one argument is divible by 3. That is, the function returns 1
if the argument is divisible by 3 and returns 0 if it is not.

The first and last lines of the function are provided for you.

\begin{lstlisting}
LABEL(divisibleBy3)































RETURN(fpAlt, pcRet)
\end{lstlisting}

\end{enumerate}
\pagebreak

\begin{center}
\huge{Appendix}
\end{center}

This Haskell module implements an IR that is a drastic simplification of
the LLVM subset that you have been working with. 
 In particular, all identifiers in this fragment name @i1@
(i.e. boolean) values.  It does not support function calls, mutable
state (via alloca or pointers), datastructures, etc.  However, it does
support control flow blocks conditional branches, direct branches, and
the @ret@ instruction.

\begin{code}
type Local = String
type Label = String

-- Binary operations on boolean values
data BinaryOp = And | Or | Xor

-- Syntactic Values
data Operand
  = Const  Bool     -- only boolean constants!
  | Id     Local    -- a local identifier

-- Instructions
data Instruction
  = Binop BinaryOp Operand Operand

-- Block terminators
data Terminator
  = Ret Operand               -- @ret i64 %s@                     
  | Br  Label                 -- @br label %lbl@                  
  | Cbr Operand Label Label   -- @br i1 %s, label %l1, label %l2@

-- Basic blocks
data Block = Bl { instructions :: [(Local, Instruction)], terminator :: Terminator }

-- Control Flow Graph: a pair of an entry block and a set labeled blocks
data Cfg = Cfg  { entry   :: Block
                , blocks  :: [(Label, Block)]
                }
\end{code}

\subsection*{An abstract datatype of boolean-valued expressions:}

\begin{spec}
data BoolExp
    =  VarE String
    |  TrueE
    |  FalseE
    |  NotE  BoolExp
    |  AndE  BoolExp  BoolExp
    |  OrE   BoolExp  BoolExp
\end{spec}

\pagebreak

\subsection*{Compile boolean expressions into @LL@ code:}

The function |compileBoolExp| compiles a boolean expression into a
sequence of @LL@ instructions (and the locals they define) and
returns an operand that holds the result.   Note that for any boolean
value $b$, the result of $\mathtt{false} \; \mathtt{XOR}\; b$ is $\neg b$ (logical negation).
Function |compileBoolExpBlock| packages the results of
compiling the expression as a basic block.

\begin{code}
-- Elements of an instruction stream
data Element
  = T Terminator
  | L Label

-- Streams:
-- A stream is just a sequence of elements
type Stream = [Element]

-- Create a fresh label:
newLabel :: String -> UniqueM String
newLabel = newUniqueString

-- Compile a binary operation:
compileBinaryOp :: BinaryOp -> BoolExp -> BoolExp -> UniqueM ([(Local, Instruction)], Operand)
compileBinaryOp bop c1 c2 = do
    local <- newUniqueString "tmp"
    (c1_code, c1_opnd)  <- compileBoolExp c1
    (c2_code, c2_opnd)  <- compileBoolExp c2
    pure (c1_code ++ c2_code ++ [(local, Binop bop c1_opnd c2_opnd)], Id local)

-- Compile a BoolExp
compileBoolExp :: BoolExp -> UniqueM ([(Local, Instruction)], Operand)
compileBoolExp TrueE     = pure ([], Const True)
compileBoolExp FalseE    = pure ([], Const False)
compileBoolExp (VarE s)  = pure ([], Id s)
compileBoolExp (NotE c)  = do
  local <- newUniqueString "tmp"
  (c_code, c_opnd) <- compileBoolExp c
  pure (c_code ++ [(local, Binop Xor (Const False) c_opnd)], Id local)
compileBoolExp (AndE  c1 c2) = compileBinaryOp And  c1 c2
compileBoolExp (OrE   c1 c2) = compileBinaryOp Or   c1 c2

-- Compile a BoolExp into a basic block
compileBoolExpBlock :: BoolExp -> UniqueM Block
compileBoolExpBlock b = do
  (insns, opnd) <- compileBoolExp b
  pure (Bl { instructions = insns, terminator = Ret opnd })
\end{code}

\end{document}

