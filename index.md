---
title: "CS 350: Compiler Design"
---

<div id="header">

| **CS 350: Compiler Design**
| Prof. Richard Eisenberg
| Spring 2019
| Bryn Mawr College

</div>

\$navbar\$

General information
===================

<div id="info_table">

----------------------         -----------------------------------------------------------------------------------------------------------------------------------------
Instructor:                    [Richard Eisenberg](http://cs.brynmawr.edu/~rae)
Email:                         `rae@cs.brynmawr.edu`
Office Phone:                  610-526-5061
Home Phone (emergencies only): 484-344-5924
Cell Phone (emergencies only): 201-575-6474 (no texts, please)
Office:                        Park 204
Office Hours:                  Wednesdays 2:30-4:00pm; Fridays 1:00-2:00pm
                               If these don't work, do not fret. Email instead.
<span class="strut" />
Lecture:                       MW 1:10-2:30pm in Park 336
Lecture Recordings:            at Tegrity: access via [Moodle](https://moodle.brynmawr.edu/course/view.php?id=2540); look for link on right side of screen.
Lab:                           M 2:40-4:00pm in Park 231
Website:                       <http://cs.brynmawr.edu/cs350>
GitLab Repo:                   <https://gitlab.com/goldfirere/cs350>
Piazza Q&A Forum:              <https://piazza.com/brynmawr/spring2019/cs350/home>
----------------------         -----------------------------------------------------------------------------------------------------------------------------------------

</div>

Goals of course
---------------

<div id="goals">

By the end of this course, you will be able to...

* write substantial programs in Haskell
* reflect on coding design choices
* describe how compilers work
* relate topics from theoretical computer science (e.g., finite automata, graph coloring algorithms) to practical concerns

During the course, you will...

* design code
* write code
* debug code
* learn to work intensively with a partner

</div>

This course is an introduction to the art of designing a compiler. A compiler is
a computer program that translates computer programs from one language into computer
programs in another language. The language you use to implement your compiler is called
the *meta language*. The language your compiler takes as input is called the
*object language* or *source language*. The language your compiler uses as its output
is called the *target language*. Source languages tend to be higher level (that is,
closer to the way humans think) than target languages. Target languages tend to
be lower level (that is, closer to the way machines operate) than source languages.
In this way, a compiler translates a program produced by humans to a program
executable by a computer.

Why study compilers?

 * Compilers are fundamentally necessary constructions in computer science. Without
   them, the field would have never gotten off the ground.
 * A compiler is a *large* program but with a simple mechanic: in the end, all a compiler
   does is read an input file (written in the source language) and write an output file
   (written in the target language). Studying a system with complex internals but
   small surface area leads to interesting computer science.
 * All computer science students are already familiar with the basic function of
   a compiler, lowering the barrier to learning more.
 * The steps taken by a compiler are directly informed by advances in theoretical
   computer science. Compiler-writing requires both a strong ability to program
   and also an appreciation of computer science theory.

Although most students will not go on to write compilers again in the future, the
fundamental patterns learned while writing a compiler will surely come up in a career
in computer science.

In this course, our meta language will be [Haskell](https://www.haskell.org/), our
source language will be Tiger (as described in our textbook), and our target language
will be HERA (as developed chiefly by Dave Wonnacott at Haverford).

Course Philosophy
-----------------

This is a *coding intensive* course. You should plan for at least 10 hours
of work outside of class per week. One of the goals of this course is to have
students work on a large programming project; this can be done only with the
requisite time demands.

Lecture will comprise a good deal of background material, helpful in coding
up your assignments. Unlike other classes I have taught, most lectures will
not have in-class-exercise time.

<img class="textbook" src="images/dragon.jpg" alt="Aho et al. text cover"/>
<img class="textbook" src="images/appel.jpg" alt="Appel text cover"/>

Materials
=========

<div id="materials">

Textbooks
---------

The required textbook for this course is:

* **Modern Compiler Implementation in ML**, by Andrew W. Appel. Cambridge University
Press, 1998. Available at the campus bookstore and in electronic format through
the library. (Can a text describing a computer science topic still be "modern" even
if it is 20 years old? Yes. Compiler design went through a transformation in the 1980s
and 1990s but has not changed much since then. Work in this area since the 1990s is
in *optimization* and related techniques, which will not be studied in depth in this
course.)

Students wishing to get a more thorough background in compilers may wish to read:

* **Compilers: Principles, Techniques, & Tools**, 2nd Edition,
by Alfred V. Aho, Monica S. Lam, Ravi Sethi,
and Jeffrey D. Ullman. Pearson Education, 2007. Available at Haverford's White Science
Library.

HERA
----

HERA is the Haverford Educational RISC Architecture, developed chiefly by David G. Wonnacott
of Haverford's CS department.

* [HERA booklet](resources/HERA2_4_0.pdf)
* [HERA quick reference](resources/HERA2_4_0-quick.pdf)
* [Errata](https://docs.google.com/document/d/1fhgaAMjyWz0MAoe1fssMR97LQgQUDCQFsnaQhA5r_Io/edit#heading=h.y77f1zkgxne7)
* [HERA-C tarball](resources/hera-c.tar.gz)
* [Script](resources/hera) to help you compile files for use with HERA-C. Read instructions inside.

Haskell resources
-----------------

* [The Glasgow Haskell Compiler User's Guide](https://downloads.haskell.org/~ghc/8.6.3/docs/html/users_guide/), for GHC 8.6.3. The Glasgow Haskell Compiler (GHC) is the major compiler for Haskell. It is installed on powerpuff.

* Haskell's [base](http://hackage.haskell.org/package/base) package, which contains
  most of the definitions that ship with GHC.

* [Stackage](https://www.stackage.org/) contains a powerful type-based search facility
(called [Hoogle](https://github.com/ndmitchell/hoogle)) that searches many libraries.

* The [Haskell Platform](https://www.haskell.org/platform/) is my recommended
  way of installing Haskell on your system. Others recommend using
  [Stack](https://docs.haskellstack.org/en/stable/README/).

* [Real World Haskell](http://book.realworldhaskell.org/) is a useful resource
  in learning Haskell.

</div>

