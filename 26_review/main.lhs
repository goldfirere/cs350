% -*- mode: LaTeX -*-

\documentclass[12pt,pdflatex]{article}

\usepackage{times}
\usepackage[dvipsnames]{xcolor}
%\usepackage{fullpage}
\usepackage{epsfig}
\usepackage[all]{xy}
\usepackage{relsize}
\usepackage{listings}
\usepackage{changepage}
\usepackage{inconsolata}

%include polycode.fmt
%include rae.fmt

\addtolength{\oddsidemargin}{-1in}
\addtolength{\evensidemargin}{-1in}
\addtolength{\textwidth}{2in}
\addtolength{\topmargin}{-1in}
\addtolength{\textheight}{2in}

\usepackage{listings}
\lstset{
  language=[Objective]Caml,
  basicstyle=\smaller\upshape\ttfamily,
  keywordstyle=\bfseries,
  framexleftmargin=1ex,
  framexrightmargin=1ex,
  showstringspaces=false,
  commentstyle=\itshape\rmfamily,
  columns=stringstyle
}

\newcommand{\bnfor}{\mid}

\newcommand{\keyword}[1]{\textcolor{BlueViolet}{\textbf{#1}}}
\newcommand{\id}[1]{\textsf{\textsl{#1}}}
\newcommand{\varid}[1]{\textcolor{Sepia}{\id{#1}}}
\newcommand{\conid}[1]{\textcolor{OliveGreen}{\id{#1}}}
\newcommand{\tick}{\text{\textquoteright}}
\newcommand{\package}[1]{\textsf{#1}}

\newcommand{\ruleN}[3]{\ensuremath{\displaystyle\frac{#2}{#3}\textsc{#1}}}



\begin{document}

\begin{center}\bf
CMSC 350: Compiler Design\\
Second Exam Practice Questions
\end{center}

\begin{enumerate}

\item
  Consider the following unambiguous context-free grammar over the symbols @a@
  and @b@ that accepts \textit{palindromes}---strings that read the
  same forward and backward.  For example, @a@, @bab@, and
  @aabaa@ are all accepted by this grammar, whereas @ab@,
  @aab@, and @baaaba@ are not.

\[
  \begin{array}{rcl}
    P & ::= & \\
       & \bnfor & \epsilon  \\
       & \bnfor & @a@ \\
       & \bnfor & @b@ \\
      & \bnfor & @a@ P @a@ \\
      & \bnfor & @b@ P @b@ \\
  \end{array}
\]

\begin{enumerate}
\item (Yes or No) Is this grammar deterministically parsable by an LL(1) parser? 
Briefly explain.

\vspace{2in}

\item (Yes or No) Is this grammar deterministically parsable by an LR(1) parser? 
 Briefly explain.

\vspace{2in}

\item (Yes or No) Is it possible to write a regular expression that matches the
same strings accepted by this grammar? Briefly explain.

\end{enumerate}

\pagebreak
\item
  Consider the following unambiguous context-free grammar over the symbols @a@
  and @b@ that accepts \textit{balanced} strings in which every
  @a@ is followed by a matching @b@.  For example, @ab@,
  @aabb@, and @aababb@ are all accepted, but @ba@, @a@, and
  @aab@ are not.

\[
  \begin{array}{rcl}
    Q & ::= & \\
       & \bnfor & \epsilon  \\
      & \bnfor & Q @a@ Q @b@ \\
  \end{array}
\]

\begin{enumerate}

\item (Yes or No) Is this grammar deterministically parsable by an LL(1) parser?
  Briefly explain.

\vspace{2in}

\item (Yes or No) Is this grammar deterministically parsable by an LR(1) parser?
  Briefly explain.

\vspace{2in}

\item (Yes or No) Is it possible to write a regular expression that matches the
same strings accepted by this grammar? Briefly explain.


\end{enumerate}

\pagebreak
\item The following ambiguous grammar of mathematical expressions includes
  infix binary multiplication @*@ and exponentiation operators
  \texttt{\^{}} as well as tokens representing variables (ranged over
  by $x$).

\[
  \begin{array}{rcl}
    E & ::= & \\
       & \bnfor & x \\
      & \bnfor & E \; @*@ \; E \\
      & \bnfor & E \; @^@ \; E \\
  \end{array}
\]
Write down a disambiguated grammar that accepts the same set of
strings, but makes multiplication left associative, exponentiation
right associative, and gives exponentiation higher precedence than
multiplication.  For example,  the token sequence
\verb|x * y ^ z ^ w * v * u| would be parsed as though it had
parentheses inserted like:
\verb|(x * (y ^ (z ^ w)) * v) * u| .

\pagebreak
\item \textbf{Typechecking}

  In this problem we will consider the typechecking rules for a simple
  Haskell-like programming language that has built-in support for
  \textit{sum types}.  To aid your intuition, we could implement sum
  types in Haskell using the following type declaration:

\begin{code}
data Either a b 
  =  Left a 
  |  Right b
\end{code}

  Our new language will use the notation $\tau_1 + \tau_2$ as syntax
  for a built-in type that acts like $@Either@\,\tau_1\,\tau_2$ does
  in Haskell.  We create values of type $\tau_1+\tau_2$ using the
  @Left@ and @Right@ constructors, and we inspect such values using
  @case@, whose semantics is the same as in Haskell.

  The grammar and (most of the) typechecking rules for this
  language is given in Appendix A.

  \begin{enumerate}
  \item Which of the following terms are well-typed according to the
    rules shown in the Appendix?   (Circle \textit{all} correct answers.)

    \begin{enumerate}
    \item @let x = 3 in (let x = 4 in x)@
    \item @let x = (let x = 4 in x) in x@
    \item @let x = (let y = 4 in x) in y@
    \item @let x = 3 in Left x@
    \end{enumerate}

  \item Recall that in our course compiler, we implemented the
    typechecker by viewing the judgment $\Gamma \vdash e : \tau$ as
    a Haskell function that takes in (Haskell representations of)
    $\Gamma$ and $e$ as inputs and produces (the Haskell representation
    of) $\tau$ as the output.  That same strategy can no longer be
    used for this toy language, even with just the rules shown in the
    appendix.  Briefly explain why not.

    \vspace{2in}

  \item Write the correct inference rule for the @case@ expression.  Assume that
    the operational behavior of @case@ is the same as in Haskell.
    It is OK if
    variables bound in the @case@ branches shadow occurrences from an outer scope.


  \end{enumerate}



\pagebreak
\item \textbf{Optimization}

Consider the following function written in Java.

\begin{lstlisting}[language=java]
int foo(int n, int[] a, int[] b) {
  int sum = 0;
  for(int i = 0; i < n; i = i + 1) {
    a[i] = a[i] + b[2];
    sum = sum + a[i];
  }    
  return sum;
}
\end{lstlisting}
Now consider this \textit{incorrectly} ``optimized'' version, which
tries to hoist the expression @b[2]@ outside of the loop:
\begin{lstlisting}[language=java]
int foo_opt(int n, int[] a, int[] b) {
  int sum = 0;
  int tmp = b[2];
  for(int i = 0; i < n; i = i + 1) {
    a[i] = a[i] + tmp;
    sum = sum + a[i];
  }    
  return sum;
}
\end{lstlisting}
There are at least \textit{two} different reasons why the proposed
``optimization'' is incorrect, one having to do with aliasing, and one
that does not involve aliasing.

\begin{enumerate}

\item First complete the code below to give a set of inputs that
  \textit{use} aliasing to demonstrate that the calls @foo(n,a,b)@
  and @foo_opt(n,a,b)@ have different behaviors:

\begin{lstlisting}[language=java]
int[] a = new int[] {0, 1, 2, 3};   

int n = 

int[] b = 
\end{lstlisting}


\item Now complete the code snippet to give a set of inputs that
  does not use aliasing, yet still demonstrates that the calls @foo(n,a,b)@
  and @foo_opt(n,a,b)@ have different behaviors:

\begin{lstlisting}[language=java]
int[] a = new int[] {0, 1, 2, 3};  

int n = 

int[] b = 
\end{lstlisting}

\end{enumerate}


\pagebreak
\item We have seen that a source C variable declaration like
  the one shown on the left below will typically first be translated
  to the LLVM code shown on the right, which uses @alloca@:

\begin{lstlisting}
int x = 0;                 %_var_x = alloca i64
                           store i64 0, i64*  %_var_x
                           /* here */
\end{lstlisting}

Which of the following LLVM instructions, if it appeared at the point
marked @/* here */@ in the LLVM code, would \textit{prevent} @alloca@-promotion
of @%_var_x@ (i.e. replacing its uses with uids instead of pointer lookups)? 
There may be zero or more than one answer.

\begin{enumerate}
\item @%y = load i64* %_var_x@
\item @store i64* %_var_x, i64** %_var_y@
\item @%y = call i64* foo(i64* %_var_x)@
\item @store i64 %_var_y, i64* %_var_x@
\end{enumerate}


\pagebreak
\item \textbf{Register Allocation}



\begin{enumerate}
\item Consider the following straight-line LLVM code.

\begin{lstlisting}[language=llvm]
%c = call i64 init()
%a = add i64 %c, %c
%b = add i64 %c, %c
%e = add i64 %a, %b
%d = add i64 %c, %e
ret i64 %d
\end{lstlisting}

Label the vertices of the following \textit{interference graph} (where
the solid lines denote interference edges) so that it corresponds to
the code above.  Each label @%a@--@%e@ should be used once.
(There may be more than one correct solution.)

\begin{center}
\begin{minipage}{2in}
\xymatrix 
{
            & *+++[Fo]{\quad} \ar@@{-}[dr]  \ar@@{-}[dl] & \\
*+++[Fo]{\quad}  \ar@@{-}[rr]    &   & *+++[Fo]{\quad} \ar@@{-}[dd] \\ \\
*+++[Fo]{\quad}         &   & *+++[Fo]{\quad} \\
}
\end{minipage}
\end{center}

\item What is the minimal number of colors needed to color the graph?

\vspace{1in}

\item Briefly explain the purpose of \textit{coalescing} nodes of the
  interference graph.

\end{enumerate}

\end{enumerate}


\pagebreak
\appendix

\section*{APPENDIX A: Typechecking Sum Types}

Grammar for a simple expression-based language with sum types:

\newcommand{\bnfeq}{\mathrel{{:}{:}{=}}}

\newcommand{\tint}{@int@}
\newcommand{\expr}{e}
\newcommand{\integer}{n}

\[
\begin{array}{rcll}
\tau & \bnfeq &   & \mbox{Types} \\
       & \bnfor & \tint \\
       & \bnfor & \tau_1 + \tau_2 \\
\\
\expr & \bnfeq &          & \mbox{Expressions} \\
      & \bnfor & x          & \quad \mbox{variables} \\
      & \bnfor & \integer & \quad \mbox{integer constants} \\
      & \bnfor & @let@\,x = \expr_1\,@in@\, \expr_2 & \quad \mbox{local lets} \\
      & \bnfor & @Left@\,\expr & \quad \mbox{sum constructors} \\
      & \bnfor & @Right@\,\expr \\
      & \bnfor & @case@\,\expr\,@of Left@\,x_1 \to \expr_1; @Right@\,x_2 \to \expr_2 & \quad
                                                           \mbox{case analysis}\\
      & \bnfor & (\expr) & \\
\\
\Gamma & \bnfeq &    & \mbox{Typechecking Contexts} \\
             & \bnfor & \cdot \\
             & \bnfor & x {:} \tau, \Gamma \\
\end{array}
\]



Typechecking expressions is defined, in part, by the following inference
rules.  Recall that the notation $x{:}\tau \in  \Gamma$ means that $x$ occurs in the context
$\Gamma$ at type $\tau$ and $x \notin \Gamma$ means that $x$ is not
bound in $\Gamma$.

\[
\begin{array}{c}
\fbox{$\Gamma \vdash \expr : \tau$}
\\ \\
\ruleN{var}{x {:} \tau \in \Gamma}{\Gamma \vdash x : \tau}
\qquad \qquad
\ruleN{int}{}{\Gamma \vdash \integer : \tint}
\\ \\
\ruleN{let}{x \not \in \Gamma \quad \Gamma \vdash e_1 : \tau_1 \quad x{:} \tau_1,\Gamma \vdash e_2 : \tau_2}
               {\Gamma \vdash @let@\,x = e_1\,@in@\,e_2 : \tau_2}
\\ \\
\ruleN{left}{\Gamma \vdash e : \tau_1}{\Gamma \vdash @Left@\,e : \tau_1 + \tau_2}
\qquad \qquad 
\ruleN{right}{\Gamma \vdash e : \tau_2}{\Gamma \vdash @Right@\,e : \tau_1 + \tau_2}
\end{array}
\]


\end{document}
