{- Author: Steve Zdancewic, translated by Richard Eisenberg
   File: IR1.hs

   Intermediate representation for an expression language.
-}

{-# OPTIONS_GHC -W #-}

module IR1 where

import Data.Int            ( Int16 )
import CS350.Unique
import CS350.Renderable    ( Renderable(..) )
import Text.Printf         ( printf )
import Data.List           ( intercalate )

-- source language ----------------------------------------------------------

type VarName = String

-- An object language: a simple datatype of 16-bit integer expressions
data Exp
  = VarE VarName         -- string representing an object-language variable
  | ConstE Int16         -- a constant Int16 value
  | AddE Exp Exp         -- sum of two expressions
  | MulE Exp Exp         -- product of two expressions
  | NegE Exp             -- negation of an expression
  deriving (Show, Eq)

-- (1 + X4) + (3 + (X1 * 5)

example :: Exp
example = AddE (AddE (ConstE 1) (VarE "X4"))
               (AddE (ConstE 3) (MulE (VarE "X1") (ConstE 5)))


-- simple let language intermediate representation --------------------------

-- syntactic values
data Operand
  = Id Unique
  | Const Int16
  | Var VarName
  deriving (Show, Eq)

-- binary operations
data BinaryOp
  = Add
  | Mul
  deriving (Show, Eq)

-- instructions
-- note that there is no nesting of operations!
data Instruction
  = Let Unique BinaryOp Operand Operand
  deriving (Show, Eq)

data Program = P { instructions :: [Instruction]
                 , result       :: Operand
                 }
  deriving (Show, Eq)

-- Pretty printing
instance Renderable Operand where
  render (Id u)    = render u
  render (Const c) = render c
  render (Var x)   = "var" ++ x

instance Renderable BinaryOp where
  render Add = "add"
  render Mul = "mul"

instance Renderable Instruction where
  render (Let u bop op1 op2) =
    printf "let %s = %s %s %s"
      (render u) (render bop) (render op1) (render op2)

instance Renderable Program where
  render (P { instructions = insns, result = ret }) =
    intercalate " in\n" (map render insns) ++
    printf " in\n  return %s" (render ret)


-------------------------------------------------------
-- Compilation

{- Expressions produce answers, so the result of compiling an expression
   is a list of instructions and an operand that will contain the final
   result of comping the expression.

   we can share the code common to binary operations.
-}

compileBop :: BinaryOp -> Exp -> Exp -> UniqueM Program
compileBop bop e1 e2 = do
  P { instructions = ins1, result = ret1 } <- compileExp e1
  P { instructions = ins2, result = ret2 } <- compileExp e2
  ret <- newUnique
  pure (P { instructions = ins1 ++ ins2 ++ [Let ret bop ret1 ret2], result = Id ret })

compileExp :: Exp -> UniqueM Program
compileExp (VarE x)
  = pure (P { instructions = [], result = Var x })
compileExp (ConstE c)
  = pure (P { instructions = [], result = Const c })
compileExp (AddE e1 e2)   = compileBop Add e1 e2
compileExp (MulE e1 e2)   = compileBop Mul e1 e2
compileExp (NegE e1)      = compileBop Mul e1 (ConstE (-1))
