{- Author: Steve Zdancewic, translated by Richard Eisenberg
   File: IR2.hs

   Intermediate representation for an expression-and-command language
-}

{-# LANGUAGE FlexibleInstances #-}  -- necessary for Renderable Program instance
{-# OPTIONS_GHC -W #-}

module IR2 where

import Data.Int            ( Int16 )
import CS350.Unique
import CS350.Renderable    ( Renderable(..) )
import Text.Printf         ( printf )
import Data.List           ( intercalate )

-- source language ----------------------------------------------------------

-- This variant of the language treats variables as mutable.

type VarName = String

-- An object language: a simple datatype of 16-bit integer expressions
data Exp
  = VarE VarName         -- string representing an object-language variable
  | ConstE Int16         -- a constant Int16 value
  | AddE Exp Exp         -- sum of two expressions
  | MulE Exp Exp         -- product of two expressions
  | NegE Exp             -- negation of an expression
  deriving (Show, Eq)

-- Abstract syntax of commands
data Command
  = Skip                       -- skip
  | Assign VarName Exp         -- X := e
  | Seq Command Command        -- c1 ; c2
  deriving (Show, Eq)

-- (1 + X4) + (3 + (X1 * 5))

example :: Exp
example = AddE (AddE (ConstE 1) (VarE "X4"))
               (AddE (ConstE 3) (MulE (VarE "X1") (ConstE 5)))
{-
   X1 := (1 + X4) + (3 + (X1 * 5)) ;
   Skip ;
   X2 := X1 * X1 ;
-}
exampleCmd :: Command
exampleCmd =
  Seq (Assign "X1" example)
      (Seq Skip
           (Assign "X2" (MulE (VarE "X1") (VarE "X1"))))

-------------------------------------
-- Intermediate representation

-- operands
data Operand
  = Id Unique
  | Const Int16
  deriving (Show, Eq)

-- binary operations
data BinaryOp
  = Add
  | Mul
  deriving (Show, Eq)

-- instructions
-- note that there is no nesting of operations!
data Instruction
  = Let Unique BinaryOp Operand Operand
  | Load Unique VarName
  | Store VarName Operand
  deriving (Show, Eq)

type Program = [Instruction]

-------------------------------
-- Pretty-printing

instance Renderable Operand where
  render (Id u)    = render u
  render (Const c) = render c

instance Renderable BinaryOp where
  render Add = "add"
  render Mul = "mul"

instance Renderable Instruction where
  render (Let u bop op1 op2) =
    printf "let %s = %s %s %s"
      (render u) (render bop) (render op1) (render op2)
  render (Load u x) =
    printf "let %s = load %s"
      (render u) ("var" ++ x)
  render (Store x op) =
    printf "let _ = store %s %s"
      (render op) ("var" ++ x)

instance Renderable Program where
  render insns =
    intercalate " in\n" (map render insns) ++
    printf " in\n  ()"

-------------------------------------------------------
-- Compilation

compileBop :: BinaryOp -> Exp -> Exp -> UniqueM (Program, Operand)
compileBop bop e1 e2 = do
  (ins1, ret1) <- compileExp e1
  (ins2, ret2) <- compileExp e2
  ret <- newUnique
  pure (ins1 ++ ins2 ++ [Let ret bop ret1 ret2], Id ret)

compileExp :: Exp -> UniqueM (Program, Operand)
compileExp (VarE x) = do ret <- newUnique
                         pure ([Load ret x], Id ret)
compileExp (ConstE c)   = pure ([], Const c)
compileExp (AddE e1 e2) = compileBop Add e1 e2
compileExp (MulE e1 e2) = compileBop Mul e1 e2
compileExp (NegE e1)    = compileBop Mul e1 (ConstE (-1))

compileCmd :: Command -> UniqueM Program
compileCmd Skip = pure []
compileCmd (Assign x e) = do (ins1, ret1) <- compileExp e
                             pure (ins1 ++ [Store x ret1])
compileCmd (Seq c1 c2) = do ins1 <- compileCmd c1
                            ins2 <- compileCmd c2
                            pure (ins1 ++ ins2)
