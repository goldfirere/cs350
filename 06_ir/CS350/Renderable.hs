{- Author: Richard Eisenberg
   File: Renderable.hs

   Declares a class for types that have a user-readable representation.
   That is, these types can be rendered for the user.
-}

{-# LANGUAGE TypeSynonymInstances, FlexibleInstances #-}
  -- necessary to have an instance for String

module CS350.Renderable where

import Data.Int
import Data.Word
import CS350.WordN

class Renderable a where
  render :: a -> String

instance Renderable Word4 where
  render = show
instance Renderable Word5 where
  render = show
instance Renderable Word6 where
  render = show
instance Renderable Word8 where
  render = show
instance Renderable Word16 where
  render = show

instance Renderable Int8 where
  render = show
instance Renderable Int16 where
  render = show

instance Renderable String where
  render = show
