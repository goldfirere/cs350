{- Author: Richard Eisenberg
   File: Word6.hs

   Defines a 6-bit unsigned word. This file uses Haskell
   features CS350 students are not expected to know about.
-}

{-# LANGUAGE TypeApplications, DerivingStrategies, GeneralizedNewtypeDeriving,
             ScopedTypeVariables, KindSignatures, DataKinds, AllowAmbiguousTypes #-}

module CS350.WordN ( Word4, Word5, Word6, bitList, listBits ) where

import Control.Arrow ( (***), first )

import Data.Word
import Data.Bits
import Data.Ix
import Data.Coerce
import Data.Proxy

import GHC.TypeLits

newtype WordN (n :: Nat) = WordN Word

newtype Word6 = Word6 (WordN 6)
  deriving newtype ( Bounded, Enum, Eq, Integral, Num, Ord, Read, Real, Show, Ix
                   , FiniteBits, Bits )

newtype Word5 = Word5 (WordN 5)
  deriving newtype ( Bounded, Enum, Eq, Integral, Num, Ord, Read, Real, Show, Ix
                   , FiniteBits, Bits )

newtype Word4 = Word4 (WordN 4)
  deriving newtype ( Bounded, Enum, Eq, Integral, Num, Ord, Read, Real, Show, Ix
                   , FiniteBits, Bits )

nat :: forall n a. (KnownNat n, Num a) => a
nat = fromInteger $ natVal @n Proxy

instance KnownNat n => Bounded (WordN n) where
  minBound = 0
  maxBound = WordN (bit (nat @n) - 1)

instance KnownNat n => Enum (WordN n) where
  succ     = (+ 1)
  pred     = subtract 1
  toEnum   = fromIntegral
  fromEnum = fromIntegral

instance Eq (WordN n) where
  WordN a == WordN b = a == b
  WordN a /= WordN b = a /= b

instance KnownNat n => Integral (WordN n) where
  WordN a `quotRem` WordN b = (fromIntegral *** fromIntegral) (a `quotRem` b)
  toInteger (WordN a) = toInteger a

instance KnownNat n => Num (WordN n) where
  WordN a + WordN b = fromIntegral (a + b)
  WordN a - WordN b = fromIntegral (a - b)
  WordN a * WordN b = fromIntegral (a * b)
  negate a          = complement a + 1
  abs               = id
  signum (WordN a)  = WordN (signum a)
  fromInteger a     = WordN (fromInteger a .&. fromIntegral (maxBound @(WordN n)))

instance Ord (WordN n) where
  compare = coerce (compare @Word)

instance KnownNat n => Read (WordN n) where
  readsPrec prec str = map (first fromIntegral) (readsPrec prec str)

instance KnownNat n => Real (WordN n) where
  toRational (WordN a) = toRational a

instance Show (WordN n) where
  show (WordN a) = show a

instance Ix (WordN n) where
  range = coerce (range @Word)
  index = coerce (index @Word)
  inRange = coerce (inRange @Word)

instance KnownNat n => FiniteBits (WordN n) where
  finiteBitSize _ = nat @n

instance KnownNat n => Bits (WordN n) where
  (.&.) = coerce ((.&.) @Word)
  (.|.) = coerce ((.|.) @Word)
  xor   = coerce (xor @Word)

  complement (WordN a) = fromIntegral (complement a)
  shift (WordN a) n = fromIntegral (shift a n)

  rotate (WordN a) n   -- thanks to https://gitlab.haskell.org/ghc/ghc/blob/master/libraries/base/GHC/Word.hs
    | n' == 0   = WordN a
    | otherwise = fromIntegral (shift a n' .|. shiftR a (nat @n - n'))
    where
      n' = n `mod` nat @n

  bitSize _      = nat @n
  bitSizeMaybe _ = Just (nat @n)
  isSigned _     = False
  testBit        = coerce (testBit @Word)
  bit            = fromIntegral . bit @Word
  popCount       = coerce (popCount @Word)

---------------------------------------------------
-- Utility functions

-- Converts a word into a list of Bools denoting the values
-- of the bits. The index into the list is the same as the
-- bit number. This means that the 4-bit word 1100 gets
-- converted into [False, False, True, True]. Note that this
-- is in *reverse* order, but it's probably the order that
-- you want.
bitList :: FiniteBits n => n -> [Bool]
bitList w = go (finiteBitSize w) w
  where
    go 0 _ = []
    go n w = testBit w 0 : go (n-1) (w `shiftR` 1)

-- Converts a list of Bools into a Word; inverse of bitList
listBits :: Bits n => [Bool] -> n
listBits [] = zeroBits
listBits (b : bs) = (listBits bs `shiftL` 1) .|. (if b then bit 0 else zeroBits)
