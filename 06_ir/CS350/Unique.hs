{- Author: Richard Eisenberg
   File: Unique.hs

   A monad that allows the generation of Uniques, values that
   are guaranteed to be unique.

   CS350 students are not expected to understand the details of the code
   here, but it's not that hard, if you want to learn about monads.
-}

{-# LANGUAGE GeneralizedNewtypeDeriving, TypeApplications #-}

module CS350.Unique ( UniqueM, Unique, runUniqueM, newUnique ) where

import CS350.Renderable

import Control.Monad.State

import Data.Coerce

newtype UniqueM a = UniqueM (State Integer a)
  deriving (Functor, Applicative, Monad, MonadState Integer)

-- under the hood, a Unique is just a positive integer
newtype Unique = MkUnique Integer
  deriving Eq

instance Show Unique where
  show (MkUnique n) = "u" ++ show n

instance Renderable Unique where
  render u = show u

-- Generate a new unique value, different from all others
newUnique :: UniqueM Unique
newUnique = UniqueM $ do u <- get
                         modify (1+)
                         return (coerce u)

runUniqueM :: UniqueM a -> a
runUniqueM action = evalState @Integer (coerce action) 0
