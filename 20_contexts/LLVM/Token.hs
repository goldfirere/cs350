{- Author: Steve Zdancewic, translated to Haskell by Richard Eisenberg
   File: Token.hs

   The Token type, used to communicate between the lexer and the parser.
-}

module LLVM.Token where

import Prelude hiding ( EQ )

import CS350.Renderable  ( Renderable(..) )

import Data.Int  ( Int16 )

-- This Token type carries info from the lexer to the parser.
data Token
  = EOF
  | STAR             -- the * symbol
  | COMMA            -- ,
  | COLON            -- :
  | EQUALS           -- =
  | LPAREN           -- (
  | RPAREN           -- )
  | LBRACE           -- {
  | RBRACE           -- }
  | LBRACKET         -- [
  | RBRACKET         -- ]

-- Reserved Words
  | CROSS            -- x
  | I1               -- i1
  | I8               -- i8
  | I16              -- i16
  | I32              -- i32
  | I64              -- i64
  | TO               -- to
  | BR               -- br
  | EQ               -- eq
  | NE               -- ne
  | OR               -- or
  | AND              -- and
  | ADD              -- add
  | SUB              -- sub
  | MUL              -- mul
  | XOR              -- xor
  | SLT              -- slt
  | SLE              -- sle
  | SGT              -- sgt
  | SGE              -- sge
  | SHL              -- shl
  | RET              -- ret
  | TYPE             -- type
  | NULL             -- null
  | LSHR             -- lshr
  | ASHR             -- ashr
  | CALL             -- call
  | ICMP             -- icmp
  | VOID             -- void
  | LOAD             -- load
  | STORE            -- store
  | LABEL            -- label
  | ENTRY            -- entry
  | GLOBAL           -- global
  | DEFINE           -- define
  | ALLOCA           -- alloca
  | BITCAST          -- bitcast
  | GEP              -- getelementptr

  | INT Int16        -- Int16 values
  | LBL String       -- Labels
  | GID String       -- Globals
  | UID String       -- Lobal
  | STRING String    -- string literal
  deriving (Show, Eq)

instance Renderable Token where
  render EOF             = "<eof>"
  render STAR            = "*"
  render COMMA           = ","
  render COLON           = ":"
  render EQUALS          = "="
  render LPAREN          = "("
  render RPAREN          = ")"
  render LBRACE          = "{"
  render RBRACE          = "}"
  render LBRACKET        = "["
  render RBRACKET        = "]"
  render CROSS           = "x"
  render I1              = "i1"
  render I8              = "i8"
  render I16             = "i16"
  render I32             = "i32"
  render I64             = "i64"
  render TO              = "to"
  render BR              = "br"
  render EQ              = "eq"
  render NE              = "ne"
  render OR              = "or"
  render AND             = "and"
  render ADD             = "add"
  render SUB             = "sub"
  render MUL             = "mul"
  render XOR             = "xor"
  render SLT             = "slt"
  render SLE             = "sle"
  render SGT             = "sgt"
  render SGE             = "sge"
  render SHL             = "shl"
  render RET             = "ret"
  render TYPE            = "type"
  render NULL            = "null"
  render LSHR            = "lshr"
  render ASHR            = "ashr"
  render CALL            = "call"
  render ICMP            = "icmp"
  render VOID            = "void"
  render LOAD            = "load"
  render STORE           = "store"
  render LABEL           = "label"
  render ENTRY           = "entry"
  render GLOBAL          = "global"
  render DEFINE          = "define"
  render ALLOCA          = "alloca"
  render BITCAST         = "bitcast"
  render GEP             = "getelementptr"
  render (INT i)         = render i
  render (LBL l)         = "%" ++ l
  render (GID g)         = "@" ++ g
  render (UID l)         = "%" ++ l
  render (STRING string) = "c\"" ++ string ++ "\\00\""
