{- Author: Richard Eisenberg
   File: CompileM.hs

   A monad suitable for the backbone of a compiler.
   It provides error-reporting facilities and the ability to generate
   fresh uniques. It also allows arbitrary I/O through the liftIO
   function.

   This file uses advanced Haskell techniques.
-}

{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# OPTIONS_GHC -W #-}

module CS350.CompileM (
    CompileT, CompileM, runCompileT, runCompileM
  , compileError, liftIO )
  where

import CS350.Unique ( UniqueT, MonadUnique(..), runUniqueT )

import Control.Monad.Except ( ExceptT, MonadError, throwError, runExceptT )
import Control.Monad.Trans  ( MonadTrans(..) )
import Control.Monad.RWS    ( MonadRWS )
import Control.Monad.Reader ( MonadReader )
import Control.Monad.Writer ( MonadWriter )
import Control.Monad.State  ( MonadState )
import Control.Monad.Fix    ( MonadFix )
import Control.Monad.Fail   ( MonadFail )
import Control.Monad.IO.Class ( MonadIO, liftIO )
import Control.Applicative  ( Alternative )
import Control.Monad        ( MonadPlus )
import Control.Monad.Cont   ( MonadCont )

{-
-- imports for the IO implementation of CompileM
import CS350.Unique.Internal ( mkUnique )

import Data.IORef            ( IORef, newIORef, readIORef, writeIORef )
import Control.Exception     ( Exception(..), throwIO, try )
import System.IO.Unsafe      ( unsafePerformIO )
-}

------------------------------------------------------------------
-- CompileT

class MonadUnique m => MonadCompile m where
  compileError :: String -> m a

newtype CompileT m a = CompileT (ExceptT String (UniqueT m) a)
  deriving ( Functor, Applicative, Monad, MonadRWS r w s, MonadReader r
           , MonadWriter w, MonadState s, MonadError String, MonadFix
           , MonadFail, MonadIO, Alternative, MonadPlus, MonadCont )
  -- for completeness, this should also derive Contravariant, but some
  -- students in 2019 still had GHC 8.4, and it's just not worth fussing
  -- over.

instance MonadTrans CompileT where
  lift action = CompileT $ lift (lift action)

instance Monad m => MonadUnique (CompileT m) where
  newUnique str = CompileT $ lift (newUnique str)

instance Monad m => MonadCompile (CompileT m) where
  compileError = throwError

-- Run a CompileT computation, returning an Either
runCompileT :: Monad m => CompileT m a -> m (Either String a)
runCompileT (CompileT action) = runUniqueT (runExceptT action)

------------------------------------------------------------------
-- CompileM

-- standard use of CompileT:
newtype CompileM a = CompileM (CompileT IO a)
  deriving ( Functor, Applicative, Monad, MonadError String
           , MonadFix, MonadFail, MonadIO, Alternative, MonadPlus
           , MonadCompile, MonadUnique )

-- Run a CompileM computation
runCompileM :: CompileM a -> IO (Either String a)
runCompileM (CompileM action) = runCompileT action

{-
While CompileM is a cleaner design than using IO directly, it can be easier
to debug programs in the IO monad, because you can run actions right in GHCi.
So, you may choose to comment out the above definitions about CompileM and
use these instead. You will also need to change the imports at the top of the
file.
-}
{-
type CompileM = IO

{-# NOINLINE globallyUniqueSeed #-}
globallyUniqueSeed :: IORef Integer
globallyUniqueSeed = unsafePerformIO $ newIORef 1

instance MonadUnique IO where
  newUnique str = do
    seed <- readIORef globallyUniqueSeed
    writeIORef globallyUniqueSeed (seed+1)
    pure (mkUnique str seed)

data CompileError = CompileError String
  deriving Show

instance Exception CompileError where
  displayException (CompileError s) = s

instance MonadCompile IO where
  compileError str = throwIO (CompileError str)

runCompileM :: CompileM a -> IO (Either String a)
runCompileM action = do
  result <- try action
  pure $ case result of
    Left (CompileError s) -> Left s
    Right success         -> Right success
-}
