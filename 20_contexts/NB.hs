{- Author: Richard Eisenberg
   File: NB.hs

   An implementation of the NB language of numeric and boolean expressions.
-}

{-# LANGUAGE OverloadedLists #-}
  -- This allows use to use [...] notation for Sequences

{-# OPTIONS_GHC -W #-}

module NB where

import CS350.Panic       ( panic )
import CS350.Renderable  ( Renderable, render)
import CS350.CompileM    ( CompileM, compileError, runCompileM )
import CS350.Unique      ( newUniqueString )

import LLVM.Lite as L

import Control.Monad ( when )
import Data.Foldable ( toList )
import Data.Sequence ( Seq(..), (><), (|>), empty )
import Text.Printf   ( printf )

import qualified Data.Map as M

--------------------------
-- Abstract syntax

type Var = String

data Type
  = Number
  | Boolean
  deriving (Show, Eq)

data Exp
  = Variable Var
  | Literal Lit
  | Binary Exp Op Exp
  | If Exp Exp Exp
  | Let Var (Maybe Type) Exp Exp
  deriving Show

data Lit
  = NumLit Integer
  | BoolLit Bool
  deriving (Show, Eq)

data Op
  = Plus
  | Minus
  | Times
  | Less
  | Greater
  | Equals
  | And
  | Or
  deriving Show

-------------------------
-- Pretty-printing

instance Renderable Type where
  render Number  = "N"
  render Boolean = "B"

instance Renderable Exp where
  render (Variable x)      = x
  render (Literal lit)     = render lit
  render (Binary e1 op e2) = printf "(%s %s %s)" (render e1) (render op) (render e2)
  render (If e1 e2 e3)     = printf "(if %s then %s else %s)" (render e1) (render e2) (render e3)
  render (Let x m_t e1 e2) = printf "(let %s %s= %s in %s)"
                                    x (case m_t of Nothing -> ""
                                                   Just ty -> printf ": %s " (render ty))
                                    (render e1) (render e2)

instance Renderable Lit where
  render (NumLit num)    = show num
  render (BoolLit True)  = "true"
  render (BoolLit False) = "false"

instance Renderable Op where
  render Plus    = "+"
  render Minus   = "-"
  render Times   = "*"
  render Less    = "<"
  render Greater = ">"
  render Equals  = "="
  render NB.And  = "&"
  render NB.Or   = "|"

-------------------------
-- Evaluation

type EvalContext = M.Map Var Lit

eval :: EvalContext -> Exp -> Lit
eval ctx (Variable x)
  | Just lit <- M.lookup x ctx  = lit
  | otherwise                   = error $ "unbound variable: " ++ x
eval _   (Literal lit)          = lit
eval ctx (Binary e1 Plus e2)    = evalArith ctx (+) e1 e2
eval ctx (Binary e1 Minus e2)   = evalArith ctx (-) e1 e2
eval ctx (Binary e1 Times e2)   = evalArith ctx (*) e1 e2
eval ctx (Binary e1 Less e2)    = evalCompare ctx (<) e1 e2
eval ctx (Binary e1 Greater e2) = evalCompare ctx (>) e1 e2
eval ctx (Binary e1 Equals e2)  = BoolLit (eval ctx e1 == eval ctx e2)
eval ctx (Binary e1 NB.And e2)  = evalLogic ctx (&&) e1 e2
eval ctx (Binary e1 NB.Or e2)   = evalLogic ctx (||) e1 e2
eval ctx (If e1 e2 e3)
  | BoolLit b1 <- eval ctx e1
  = if b1 then eval ctx e2 else eval ctx e3
  | otherwise
  = error "Invalid operation"
eval ctx (Let x _ e1 e2)        = eval ctx' e2
  where ctx' = M.insert x (eval ctx e1) ctx

evalArith :: EvalContext -> (Integer -> Integer -> Integer) -> Exp -> Exp -> Lit
evalArith ctx fun e1 e2
  | NumLit n1 <- eval ctx e1
  , NumLit n2 <- eval ctx e2
  = NumLit (fun n1 n2)

  | otherwise
  = error "Invalid operation"

evalCompare :: EvalContext -> (Integer -> Integer -> Bool) -> Exp -> Exp -> Lit
evalCompare ctx fun e1 e2
  | NumLit n1 <- eval ctx e1
  , NumLit n2 <- eval ctx e2
  = BoolLit (fun n1 n2)

  | otherwise
  = error "Invalid operation"

evalLogic :: EvalContext -> (Bool -> Bool -> Bool) -> Exp -> Exp -> Lit
evalLogic ctx fun e1 e2
  | BoolLit b1 <- eval ctx e1
  , BoolLit b2 <- eval ctx e2
  = BoolLit (fun b1 b2)

  | otherwise
  = error "Invalid operation"

---------------------------
-- Type checking

type TypeContext = M.Map Var Type

check :: TypeContext -> Exp -> Maybe Type
check ctx (Variable x)  = M.lookup x ctx
check _   (Literal lit) = Just (checkLit lit)
check ctx (Binary e1 op e2) = case op of
  Plus    -> checkArith ctx e1 e2
  Minus   -> checkArith ctx e1 e2
  Times   -> checkArith ctx e1 e2
  Less    -> checkCompare ctx e1 e2
  Greater -> checkCompare ctx e1 e2
  Equals  -> checkEquals ctx e1 e2
  NB.And  -> checkLogic ctx e1 e2
  NB.Or   -> checkLogic ctx e1 e2
check ctx (If e1 e2 e3)
  | Just Boolean <- check ctx e1
  , Just t2 <- check ctx e2
  , Just t3 <- check ctx e3
  , t2 == t3
  = Just t2
check ctx (Let x m_ty e1 e2)
  | Just ty1 <- check ctx e1
  , case m_ty of
      Nothing -> True   -- nothing to check here
      Just ty -> ty == ty1
  = check (M.insert x ty1 ctx) e2
check _ _ = Nothing

checkLit :: Lit -> Type
checkLit (NumLit _)  = Number
checkLit (BoolLit _) = Boolean

checkArith :: TypeContext -> Exp -> Exp -> Maybe Type
checkArith ctx e1 e2
  | Just Number <- check ctx e1
  , Just Number <- check ctx e2
  = Just Number

  | otherwise
  = Nothing

checkCompare :: TypeContext -> Exp -> Exp -> Maybe Type
checkCompare ctx e1 e2
  | Just Number <- check ctx e1
  , Just Number <- check ctx e2
  = Just Boolean

  | otherwise
  = Nothing

checkEquals :: TypeContext -> Exp -> Exp -> Maybe Type
checkEquals ctx e1 e2
  | Just t1 <- check ctx e1
  , Just t2 <- check ctx e2
  , t1 == t2
  = Just Boolean

  | otherwise
  = Nothing

checkLogic :: TypeContext -> Exp -> Exp -> Maybe Type
checkLogic ctx e1 e2
  | Just Boolean <- check ctx e1
  , Just Boolean <- check ctx e2
  = Just Boolean

  | otherwise
  = Nothing

---------------------------
-- Type-directed compilation

type CompContext = M.Map Var (Operand, Type)

data Element
  = L Label
  | I Local Instruction
  | T Terminator
  deriving Show

type Stream = Seq Element
  -- A "Seq" is just like a list, but it's efficient adding to either end of it
  -- (unlike a list, which only likes additions at its beginning)

-- This convenience function produces an Element for an instruction, generating
-- a new LLVM local name along the way.
newInstruction :: Instruction -> CompileM (Element, Operand)
newInstruction insn = do
  local <- newUniqueString "local"
  pure (I local insn, LocalId local)

compile :: CompContext -> Exp -> CompileM (Stream, Type, Operand)
compile ctx (Variable x)
  | Just (op, ty) <- M.lookup x ctx
  = pure (empty, ty, op)
  | otherwise
  = compileError $ "Unbound variable: " ++ x
compile _   (Literal lit) = pure (empty, ty, op)
  where (ty, op) = compileLit lit
compile ctx (Binary e1 op e2) = do
  (s1, ty1, operand1) <- compile ctx e1
  (s2, ty2, operand2) <- compile ctx e2

  (insn, ty, operand) <- case op of
    Plus    -> compileArith Add ty1 ty2 operand1 operand2
    Minus   -> compileArith Sub ty1 ty2 operand1 operand2
    Times   -> compileArith Mul ty1 ty2 operand1 operand2
    Less    -> compileCompare Slt ty1 ty2 operand1 operand2
    Greater -> compileCompare Sgt ty1 ty2 operand1 operand2
    Equals  -> compileEquals ty1 ty2 operand1 operand2
    NB.And  -> compileLogic L.And ty1 ty2 operand1 operand2
    NB.Or   -> compileLogic L.Or ty1 ty2 operand1 operand2

  pure (s1 >< (s2 |> insn), ty, operand)
compile ctx (If e1 e2 e3) = do
  (s1, ty1, operand1) <- compile ctx e1
  (s2, ty2, operand2) <- compile ctx e2
  (s3, ty3, operand3) <- compile ctx e3

  when (ty1 /= Boolean) $
    compileError "Condition is not a boolean."

  when (ty2 /= ty3) $
    compileError "Branches of 'if' have different types."

  let ll_ty = compileType ty2
  (alloca, ptr_result) <- newInstruction (Alloca ll_ty)
  (store_then, _)      <- newInstruction (Store ll_ty operand2 ptr_result)
  (store_else, _)      <- newInstruction (Store ll_ty operand3 ptr_result)
  (load, result)       <- newInstruction (Load (Ptr ll_ty) ptr_result)

  then_lbl <- newUniqueString "then"
  else_lbl <- newUniqueString "else"
  merge_lbl <- newUniqueString "merge"

  pure ( s1 ><
         [ alloca
         , T (Cbr operand1 then_lbl else_lbl)
         , L then_lbl ] ><
         s2 ><
         [ store_then
         , T (Br merge_lbl)
         , L else_lbl ] ><
         s3 ><
         [ store_else
         , T (Br merge_lbl)
         , L merge_lbl
         , load ]
       , ty2, result )
compile ctx (Let x m_ty e1 e2) = do
  (s1, ty1, operand1) <- compile ctx e1

  case m_ty of
    Nothing -> pure ()
    Just ty -> when (ty1 /= ty) $
                 compileError "Type mismatch in 'let'."

  let ctx' = M.insert x (operand1, ty1) ctx
  (s2, ty2, operand2) <- compile ctx' e2

  pure (s1 >< s2, ty2, operand2)

compileLit :: Lit -> (Type, Operand)
compileLit (NumLit n)  = (Number, Const (fromIntegral n))
compileLit (BoolLit b) = (Boolean, Const (if b then 1 else 0))

compileArith :: L.BinaryOp -> Type -> Type -> Operand -> Operand
             -> CompileM (Element, Type, Operand)
compileArith op ty1 ty2 operand1 operand2 = do
  when (ty1 /= Number || ty2 /= Number) $
    compileError "Arithmetic can be done only on numbers."

  (insn, result) <- newInstruction (Binop op I16 operand1 operand2)
  pure (insn, Number, result)

compileCompare :: L.Condition -> Type -> Type -> Operand -> Operand
               -> CompileM (Element, Type, Operand)
compileCompare cond ty1 ty2 operand1 operand2 = do
  when (ty1 /= Number || ty2 /= Number) $
    compileError "Comparison can be done only on numbers."

  (insn, result) <- newInstruction (Icmp cond I16 operand1 operand2)
  pure (insn, Boolean, result)

compileEquals :: Type -> Type -> Operand -> Operand
              -> CompileM (Element, Type, Operand)
compileEquals ty1 ty2 operand1 operand2 = do
  when (ty1 /= ty2) $
    compileError "Cannot compare different types for equality."

  (insn, result) <- newInstruction (Icmp Eq (compileType ty1) operand1 operand2)
  pure (insn, Boolean, result)

compileLogic :: L.BinaryOp -> Type -> Type -> Operand -> Operand
             -> CompileM (Element, Type, Operand)
compileLogic op ty1 ty2 operand1 operand2 = do
  when (ty1 /= Boolean || ty2 /= Boolean) $
    compileError "Logical operations can be done only on booleans."

  (insn, result) <- newInstruction (Binop op I1 operand1 operand2)
  pure (insn, Boolean, result)

compileType :: NB.Type -> L.Ty
compileType Number  = I16
compileType Boolean = I1

---------------------------
-- Converting a Stream to a Program

buildCfg :: Stream -> Cfg
buildCfg stream = Cfg { entry = entry_block
                      , blocks = labeled_blocks }
  where
    (entry_block, lbl_stream) = get_entry_block stream
    labeled_blocks            = get_labeled_blocks lbl_stream

    get_entry_block = go_block empty

    get_labeled_blocks (L lbl :<| es) = (lbl, block) : other_blocks
      where
        (block, stream) = go_block empty es
        other_blocks    = get_labeled_blocks stream

    get_labeled_blocks (e :<| _)     = panic $ "Unexpected element: " ++ show e
    get_labeled_blocks Empty         = []

    go_block :: Seq (Local, L.Instruction)  -- accumulator for instructions
             -> Stream -> (Block, Stream)
      -- returns the block and the remainder of the stream
    go_block _     (L lbl :<| _)       = panic $ "Label in middle of block: " ++ lbl
    go_block insns (I uid insn :<| es) = go_block (insns |> (uid, insn)) es
    go_block insns (T term :<| es)     = ( Bl { instructions = toList insns, terminator = term }
                                         , es )
    go_block _     Empty               = panic "Unterminated block"

compileProgram :: Exp -> CompileM Program
compileProgram e = do
  (s, ty, op) <- compile M.empty e

  let cfg = buildCfg (s |> T (Ret (compileType ty) (Just op)))
      main = N { name = "main"
               , decl = FD { functionType   = FunTy I16 [I16, Ptr (Ptr I8)]
                           , functionParams = ["argc", "argv"]
                           , functionCfg    = cfg }}
      program = Prog { typeDecls = []
                     , globalDecls = []
                     , functionDecls = [main] }

  pure program

writeProgram :: FilePath -> Exp -> IO ()
writeProgram path e = do
  result <- runCompileM (compileProgram e)
  case result of
    Left err   -> putStrLn err
    Right prog -> do writeFile path (render prog)
                     putStrLn $ "Successfully wrote program to " ++ path ++ "."
