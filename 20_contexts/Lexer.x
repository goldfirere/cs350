-- -*- mode: haskell -*-

{
{- Author: Richard Eisenberg
   File: Lexer.x

   A lexer for the ℕ𝔹 language.
-}

module Lexer where

}

%wrapper "basic"

$digit = 0-9
@number = $digit+

@identifier = [a-z]+

NB :-

$white        ;

true          { const TRUE    }
false         { const FALSE   }
if            { const IF      }
then          { const THEN    }
else          { const ELSE    }
let           { const LET     }
in            { const IN      }
N             { const N       }
B             { const B       }
@identifier   { IDENTIFIER    }

"("           { const LPAREN  }
")"           { const RPAREN  }
"+"           { const PLUS    }
"-"           { const MINUS   }
"*"           { const TIMES   }
"<"           { const LESS    }
">"           { const GREATER }
"="           { const EQUALS  }
"&"           { const AND     }
"|"           { const OR      }
":"           { const COLON   }

@number       { NUMBER . read }

{

data Token
  = TRUE
  | FALSE
  | IF
  | THEN
  | ELSE
  | LET
  | IN
  | N
  | B
  | LPAREN
  | RPAREN
  | PLUS
  | MINUS
  | TIMES
  | LESS
  | GREATER
  | EQUALS
  | AND
  | OR
  | COLON
  | NUMBER Integer
  | IDENTIFIER String
  deriving Show

lexNB :: String -> [Token]
lexNB = alexScanTokens

}
